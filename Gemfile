source 'https://rubygems.org'

ruby '2.3.1'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '5.0.0'
#use pg
gem 'pg'
# Use SCSS for stylesheets
gem 'sass-rails'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails'
# See https://github.com/rails/execjs#readme for more supported runtimes
gem 'therubyracer', platforms: :ruby
gem 'sprockets-rails'
gem 'aasm', '4.11.0'

# Use jquery as the JavaScript library
gem 'jquery-rails'
# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
gem 'turbolinks'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder'
# bundle exec rake doc:rails generates the API under doc/api.
gem 'sdoc', '~> 0.4.0', group: :doc

gem 'browserify-rails'

gem 'validates_timeliness', '~> 4.0'

gem 'carmen-rails', '~> 1.0.0'
gem 'momentjs-rails', '2.11.1'
gem 'alertifyjs-rails'

source 'https://rails-assets.org' do
  gem 'rails-assets-datetimepicker'
end

####### custom gems #########

# users
gem 'devise', '4.2.0'

gem 'simple_form', '3.2.1'

# UI

gem 'semantic-ui-sass', '2.1.6.0'

gem 'jquery-ui-rails', '5.0.5'

gem 'awesome_nested_set'

gem 'nested_form'

# Browser dependent formatting

gem 'browser', '~> 1.0'

# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use Unicorn as the app server
# gem 'unicorn'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

gem 'remotipart', github: 'mshibuya/remotipart'
gem 'rails_admin', '>= 1.0.0.rc'
gem 'cancancan', '~> 1.10'
gem 'enumerize'
gem 'paperclip', :git=> 'https://github.com/thoughtbot/paperclip', :ref => '523bd46c768226893f23889079a7aa9c73b57d68'
gem 'aws-sdk', '< 2.0'
gem 'gmaps-autocomplete-rails'

gem 'figaro'
gem 'httparty'
gem 'groupdate', '3.0.0'
gem 'builder', '3.2.2'
gem 'signet'
gem 'clockwork'
gem 'redis', '~> 3.3'
gem 'resque'
gem 'resque-web', require: 'resque_web'

gem 'sentry-raven', '~> 0.15.6'

gem 'prawn', '~> 2.1'
gem 'prawn-table', '~> 0.1.0'

gem 'wicked_pdf'
gem 'wkhtmltopdf-binary'

gem 'simpleconfig'
gem 'textacular'

group :test do
  gem 'clockwork-test'
  gem 'rails-controller-testing'
end

group :development, :test, :staging do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug'
  gem 'rspec-rails', '3.5.1'
  gem 'faker'
  gem 'factory_girl_rails', '4.7.0', require: false
end

group :development do
  gem 'timecop'
  # Access an IRB console on exception pages or by using <%= console %> in views
  gem 'letter_opener'
  gem 'web-console', '3.3.1'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
end

group :development, :test do
  gem 'pry-rails'
end

group :test, :staging do
  gem 'cucumber-rails', '1.4.4', :require => false
  gem 'database_cleaner'
  gem 'selenium-webdriver', '2.48.1'
  gem 'simplecov', :require => false
  gem "shoulda-matchers", ">= 3.0.0", require: false
end

group :production do
  gem 'rails_12factor'
  gem 'wkhtmltopdf-heroku'
end

gem 'doorkeeper'
gem 'rack-cors', :require => 'rack/cors'

gem 'will_paginate'
