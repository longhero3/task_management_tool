class CreateTaskSummaries < ActiveRecord::Migration[5.0]
  def change
    create_table :task_summaries do |t|
      t.references :task, foreign_key: true
      t.decimal :task_completed
      t.decimal :budget_spent_ratio
      t.decimal :task_completed_over_budget_consumed

      t.timestamps
    end
  end
end
