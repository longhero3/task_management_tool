class RemovePayRateFromUser < ActiveRecord::Migration
  def change
    remove_column :users, :pay_rate
    remove_column :users, :extra_pay_rate
  end
end
