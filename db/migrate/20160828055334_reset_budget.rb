class ResetBudget < ActiveRecord::Migration[5.0]
  def up

    tasks = Task.select{|task| task.target_start.nil? || task.target_finish.nil? || task.target_start >= task.target_finish || task.budgeted_cost < task.budget_taken.ceil || task.budgeted_cost < task.actual_cost.ceil}
    tasks.each do |task|
      if task.target_start.nil?
        # task.target_start = Time.now - 1.day
        task.update_column('target_start', Time.now - 1.day)
      end

      if task.target_finish.nil?
        # task.target_finish = Time.now + 1.day
        task.update_column('target_finish', Time.now + 1.day)
      end

      if task.target_start >= task.target_finish
        # task.target_finish = task.target_start + 1.day
        task.update_column('target_finish', task.target_start + 1.day)

      end
      if task.budgeted_cost < task.budget_taken.ceil || task.budgeted_cost < task.actual_cost.ceil
        task.update_column('budgeted_cost', [task.actual_cost.ceil, task.budget_taken.ceil].max)
      end
      tasks.append(task.parent) if task.parent.present?
    end
  end
end
