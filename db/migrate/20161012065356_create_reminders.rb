class CreateReminders < ActiveRecord::Migration[5.0]
  def change
    create_table :reminders do |t|
      t.references :remindable, polymorphic: true
      t.string     :type
      t.string     :attribute_name
      t.decimal    :threshold_value, precision: 11, scale: 2
      t.integer    :threshold_comparator
      t.integer    :due_remind_days
      t.timestamps
    end
  end
end
