class AddPayrollCalendarIdToXeroTimesheets < ActiveRecord::Migration
  def change
    add_column :xero_timesheets, :pay_roll_calendar_id, :string
  end
end
