class CreateCostTypes < ActiveRecord::Migration
  def change
    create_table :cost_types do |t|
      t.string :cost_type

      t.timestamps null: false
    end
  end
end
