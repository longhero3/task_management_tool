class CreateTimesheetSnapshots < ActiveRecord::Migration
  def change
    create_table :timesheet_snapshots do |t|
      t.string :content

      t.timestamps null: false
    end
  end
end
