class AddPayRateToPayItemCategories < ActiveRecord::Migration[5.0]
  def change
    add_column :pay_item_categories, :pay_rate, :decimal
  end
end
