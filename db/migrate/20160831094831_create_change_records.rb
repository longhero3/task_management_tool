class CreateChangeRecords < ActiveRecord::Migration[5.0]
  def change
    create_table :change_records do |t|
      t.belongs_to :remindable, :polymorphic => true, null: false
      t.string :name, null: false
      t.string :old_value, null: false
      t.string :new_value, null: false
      t.timestamps
    end
  end
end
