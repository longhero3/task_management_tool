class ImportInitialHighSchoolData < ActiveRecord::Migration
  def change
    I18n.t(:high_schools).values.each { |name| HighSchool.create(name: name)};
  end
end
