class ImportDefaultJobTitles < ActiveRecord::Migration
  def change
    I18n.t(:job_titles).values.each { |name| JobTitle.create(name: name)};
  end
end
