class CreateSprints < ActiveRecord::Migration[5.0]
  def change
    create_table :sprints do |t|
      t.datetime :target_start, null: false
      t.datetime :target_end, null: false
      t.decimal  :actual_cost, precision: 11, scale: 2
      t.timestamps
    end
  end
end
