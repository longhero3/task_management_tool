class AddWagesRefToUser < ActiveRecord::Migration
  def change
    add_reference :users, :wage, index: true, foreign_key: true
  end
end
