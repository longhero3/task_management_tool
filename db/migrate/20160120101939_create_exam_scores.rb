class CreateExamScores < ActiveRecord::Migration
  def change
    create_table :exam_scores do |t|
      t.decimal :score
      t.references :exam_field, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
