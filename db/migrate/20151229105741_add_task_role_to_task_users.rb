class AddTaskRoleToTaskUsers < ActiveRecord::Migration
  def change
    add_column :task_users, :task_role, :string, default: 'member'
  end
end
