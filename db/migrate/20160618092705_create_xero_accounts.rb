class CreateXeroAccounts < ActiveRecord::Migration
  def change
    create_table :xero_accounts do |t|
      t.string :employee_id
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
