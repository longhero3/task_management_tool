class AddAvatarColumnsToProfileImages < ActiveRecord::Migration
  def up
    add_attachment :profile_images, :avatar
  end

  def down
    remove_attachment :profile_images, :avatar
  end
end
