class AddPayRateToWorkPeriods < ActiveRecord::Migration
  def change
    add_column :work_periods, :pay_rate, :decimal
  end
end
