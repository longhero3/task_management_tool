class AddStatusToXeroTimesheets < ActiveRecord::Migration
  def change
    add_column :xero_timesheets, :status, :string, default: 'DRAFT'
  end
end
