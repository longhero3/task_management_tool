class FixPayUpdate < ActiveRecord::Migration
  def change
    PayItem.all.each do |pay_item|
      category = PayItemCategory.find_by_name(pay_item.name)
      pay_item.update_columns(earnings_rate_id: category.try(:earnings_rate_id))
    end
  end
end
