class AddPayFieldsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :pay_rate, :decimal
    add_column :users, :extra_pay_rate, :decimal
  end
end
