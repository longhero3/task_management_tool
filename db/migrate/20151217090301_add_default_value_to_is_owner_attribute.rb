class AddDefaultValueToIsOwnerAttribute < ActiveRecord::Migration
  def change
    change_column :task_users, :isOwner, :boolean, :default => false
  end
end
