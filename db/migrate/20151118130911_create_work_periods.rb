class CreateWorkPeriods < ActiveRecord::Migration
  def change
    create_table :work_periods do |t|
      t.decimal :amount_hours, :precision => 4, :scale =>2
      t.datetime :date_worked
      t.datetime :start_time
      t.datetime :end_time
      t.timestamps null: false
    end
  end
end
