class CreateXeroTimesheets < ActiveRecord::Migration
  def change
    create_table :xero_timesheets do |t|
      t.string :timesheet_id
      t.references :user, index: true, foreign_key: true
      t.references :xero_account, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
