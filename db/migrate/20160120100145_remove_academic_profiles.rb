class RemoveAcademicProfiles < ActiveRecord::Migration
  def change
    drop_table :academic_profiles
  end
end
