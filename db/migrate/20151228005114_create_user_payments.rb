class CreateUserPayments < ActiveRecord::Migration
  def change
    create_table :user_payments do |t|
      t.references :user, index: true, foreign_key: true
      t.decimal :pay_rate
      t.decimal :extra_pay_rate
      t.timestamps null: false
    end
  end
end
