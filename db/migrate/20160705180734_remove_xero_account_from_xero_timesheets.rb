class RemoveXeroAccountFromXeroTimesheets < ActiveRecord::Migration
  def change
    remove_column :xero_timesheets, :xero_account_id
  end
end
