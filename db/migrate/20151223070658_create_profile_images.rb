class CreateProfileImages < ActiveRecord::Migration
  def change
    create_table :profile_images do |t|

      t.timestamps null: false
    end
  end
end
