class CreatePayItems < ActiveRecord::Migration
  def change
    create_table :pay_items do |t|
      t.references :task, index: true, foreign_key: true
      t.string :earnings_rate_id

      t.timestamps null: false
    end
  end
end
