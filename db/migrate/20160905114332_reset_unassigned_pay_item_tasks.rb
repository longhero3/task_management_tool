class ResetUnassignedPayItemTasks < ActiveRecord::Migration[5.0]
  def change
    pay_items = PayItem.where(earnings_rate_id: '')
    unassigned_category = PayItemCategory.find_by_name('Unassigned Item')
    if unassigned_category
      pay_items.update_all(name: unassigned_category.name, earnings_rate_id: unassigned_category.earnings_rate_id)
    end
  end
end
