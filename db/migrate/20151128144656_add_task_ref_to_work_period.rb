class AddTaskRefToWorkPeriod < ActiveRecord::Migration
  def change
    add_reference :work_periods, :task, index: true, foreign_key: true
  end
end
