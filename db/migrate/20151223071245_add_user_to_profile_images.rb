class AddUserToProfileImages < ActiveRecord::Migration
  def change
    add_reference :profile_images, :user, index: true, foreign_key: true
  end
end
