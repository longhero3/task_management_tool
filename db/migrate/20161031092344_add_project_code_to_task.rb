class AddProjectCodeToTask < ActiveRecord::Migration[5.0]
  def change
    add_column :tasks, :project_code, :string
  end
end
