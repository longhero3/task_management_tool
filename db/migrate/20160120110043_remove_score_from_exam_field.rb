class RemoveScoreFromExamField < ActiveRecord::Migration
  def change
    remove_column :exam_fields, :score
  end
end
