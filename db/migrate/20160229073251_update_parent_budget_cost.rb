class UpdateParentBudgetCost < ActiveRecord::Migration
  def change
    Task.all.to_a.each do |task|
      task.update_columns(budgeted_cost: task.children.sum(:budgeted_cost)) if task.children.count > 0
    end
  end
end
