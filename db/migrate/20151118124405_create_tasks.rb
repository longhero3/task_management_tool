class CreateTasks < ActiveRecord::Migration
  def change
    create_table :tasks do |t|
      t.string :title
      t.text :description
      t.datetime :target_start
      t.datetime :target_finish
      t.datetime :actual_start
      t.datetime :actual_finish
      t.decimal :budgeted_cost, :precision => 11, :scale =>2
      t.decimal :actual_cost, :precision => 11, :scale =>2
      t.integer :priority

      t.integer :parent_id, :null => true, :index => true
      t.integer :lft, :null => false, :index => true
      t.integer :rgt, :null => false, :index => true

      # optional fields
      t.integer :depth, :null => false, :default => 0
      t.integer :children_count, :null => false, :default => 0

      t.timestamps null: false
    end
  end
end

