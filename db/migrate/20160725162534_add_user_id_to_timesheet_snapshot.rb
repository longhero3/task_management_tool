class AddUserIdToTimesheetSnapshot < ActiveRecord::Migration
  def change
    add_reference :timesheet_snapshots, :user, index: true, foreign_key: true
  end
end
