class SetDefaultPayRateToAllWorkPeriods < ActiveRecord::Migration[5.0]
  def change
    WorkPeriod.all.each do |wp|
      pay_rate = wp.try(:task).try(:pay_rate) || 0
      wp.update(pay_rate: pay_rate)
    end
    WorkPeriod.where(pay_rate: nil).update_all(pay_rate: 1)
  end
end
