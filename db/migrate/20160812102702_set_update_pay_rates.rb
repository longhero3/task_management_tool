class SetUpdatePayRates < ActiveRecord::Migration
  def up
    Task.where(pay_rate: 0.0).each do |task|
      task.update_attributes(pay_rate: 1.0)
    end
    change_column :tasks, :pay_rate, :decimal, :default => 1.0
  end

  def down
    change_column :tasks, :pay_rate, :decimal, :default => 0.0
  end
end
