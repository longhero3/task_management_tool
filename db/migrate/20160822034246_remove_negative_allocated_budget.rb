class RemoveNegativeAllocatedBudget < ActiveRecord::Migration
  def up
    Task.where("pay_rate <= 0").each do |task|
      task.update_attribute('pay_rate', '1.0')
    end

    tasks = Task.select{|task| task.budgeted_cost < task.children.created.sum(:budgeted_cost)}
    tasks.each do |task|
      task.update_attributes(budgeted_cost: task.children.created.sum(:budgeted_cost)) if task.budgeted_cost < task.children.created.sum(:budgeted_cost)
      tasks.append(task.parent) if !task.root?
    end


  end
end
