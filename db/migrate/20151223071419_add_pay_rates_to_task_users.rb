class AddPayRatesToTaskUsers < ActiveRecord::Migration
  def change
    add_column :task_users, :pay_rate, :decimal
    add_column :task_users, :extra_pay_rate, :decimal
  end
end
