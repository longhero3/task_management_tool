class GenerateDataForTaskSummary < ActiveRecord::Migration[5.0]
  def change
    Task.all.each do |task|
      task.create_task_summary(task_completed: task.task_completed, budget_spent_ratio: task.budget_spent_ratio,
                               task_completed_over_budget_consumed: task.task_completed_over_budget_consumed)
    end
  end
end
