class AddTotalProgressToTasks < ActiveRecord::Migration
  def change
    add_column :tasks, :total_progress, :float, default: 0
  end
end
