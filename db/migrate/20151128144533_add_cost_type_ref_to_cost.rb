class AddCostTypeRefToCost < ActiveRecord::Migration
  def change
    add_reference :costs, :cost_type, index: true, foreign_key: true
  end
end
