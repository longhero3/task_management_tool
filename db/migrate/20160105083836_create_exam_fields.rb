class CreateExamFields < ActiveRecord::Migration
  def change
    create_table :exam_fields do |t|
      t.string :name
      t.integer :score
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
