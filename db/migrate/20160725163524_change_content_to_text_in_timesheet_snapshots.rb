class ChangeContentToTextInTimesheetSnapshots < ActiveRecord::Migration
  def change
    change_column :timesheet_snapshots, :content, :text
  end
end
