class AddDescriptionToWorkPeriod < ActiveRecord::Migration
  def change
    add_column :work_periods, :description, :text
  end
end
