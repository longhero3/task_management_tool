class CreatePayItemCategories < ActiveRecord::Migration
  def change
    create_table :pay_item_categories do |t|
      t.string :earnings_rate_id
      t.string :name

      t.timestamps null: false
    end
  end
end
