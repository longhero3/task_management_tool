class AddIsLeafToTasks < ActiveRecord::Migration
  def change
    add_column :tasks, :is_leaf, :boolean, default: true

    Task.all.each do |task|
      task.update_column(:is_leaf, task.leaf?)
    end
  end
end
