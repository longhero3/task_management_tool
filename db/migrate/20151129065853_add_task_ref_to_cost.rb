class AddTaskRefToCost < ActiveRecord::Migration
  def change
    add_reference :costs, :task, index: true, foreign_key: true
  end
end
