class AddProgressToWorkPeriod < ActiveRecord::Migration
  def change
    add_column :work_periods, :progress, :float
  end
end
