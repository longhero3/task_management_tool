class CreateSubjectFields < ActiveRecord::Migration
  def change
    create_table :subject_fields do |t|
      t.string :name
      t.boolean :assessed_for_proficiency
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
