class AddFlagToTaskUser < ActiveRecord::Migration
  def change
    add_column :task_users, :isOwner, :boolean
  end
end
