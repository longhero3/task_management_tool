class ChangeBudgetedCostDefaultTo1 < ActiveRecord::Migration
  def change
    change_column :tasks, :budgeted_cost, :decimal, default: 1
    nil_tasks = Task.where(budgeted_cost: 0)
    nil_tasks.update_all(budgeted_cost: 1)
  end
end
