class AddColumnsToTaskUser < ActiveRecord::Migration
  def change
    add_column :task_users, :user_id, :integer
    add_column :task_users, :task_id, :integer
  end
end
