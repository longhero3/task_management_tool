class AddColourTcToTasks < ActiveRecord::Migration
  def change
    add_column :tasks, :colour_tcte, :boolean, :null => false, :default => true
  end
end
