class CreateWages < ActiveRecord::Migration
  def change
    create_table :wages do |t|
      t.boolean :is_salary
      t.decimal :rate, :precision =>10, :scale => 2
      t.text :description

      t.timestamps null: false
    end
  end
end
