class AddEarningsTypeToPayItemCategories < ActiveRecord::Migration
  def change
    add_column :pay_item_categories, :earnings_type, :string
  end
end
