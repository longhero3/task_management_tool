class ChangeDefaultValueToBudgetedCost < ActiveRecord::Migration[5.0]
  def up
	change_column :tasks, :budgeted_cost, :decimal, default: 0.0
  end

  def down
	change_column :tasks, :budgeted_cost, :decimal, default: 1.0
  end
end
