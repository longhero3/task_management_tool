class AddUserRefToWorkPeriod < ActiveRecord::Migration
  def change
    add_reference :work_periods, :user, index: true, foreign_key: true
  end
end
