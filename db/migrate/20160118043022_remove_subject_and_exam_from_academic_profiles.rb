class RemoveSubjectAndExamFromAcademicProfiles < ActiveRecord::Migration
  def change
    remove_column :academic_profiles, :primary_subject
    remove_column :academic_profiles, :exam
  end
end
