class AddMoreFieldsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :grad_job_title, :string
    add_column :users, :second_email, :string
    add_column :users, :contact_number, :string
  end
end
