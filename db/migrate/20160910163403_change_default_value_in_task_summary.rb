class ChangeDefaultValueInTaskSummary < ActiveRecord::Migration[5.0]
  def change
    change_column :task_summaries, :task_completed, :decimal, :default => 0.0
    change_column :task_summaries, :budget_spent_ratio, :decimal, :default => 0.0
    change_column :task_summaries, :task_completed_over_budget_consumed, :decimal, :default => 0.0
  end
end
