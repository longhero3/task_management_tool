class CreatePayrollCalendars < ActiveRecord::Migration
  def change
    create_table :payroll_calendars do |t|
      t.string :payroll_calendar_id
      t.string :name
      t.string :calendar_type
      t.datetime :start_date
      t.datetime :payment_date

      t.timestamps null: false
    end
  end
end
