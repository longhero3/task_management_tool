class RenamePayrollCalendarIdFromXeroTimesheet < ActiveRecord::Migration
  def change
    rename_column :xero_timesheets, :pay_roll_calendar_id, :payroll_calendar_id
  end
end
