class AddDefaultValueToBudgetedCost < ActiveRecord::Migration
  def change
    change_column :tasks, :budgeted_cost, :decimal, default: 0
    nil_tasks = Task.where(budgeted_cost: nil)
    nil_tasks.update_all(budgeted_cost: 0)
  end
end
