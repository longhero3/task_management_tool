class AddMaxDepthToTasks < ActiveRecord::Migration
  def change
    add_column :tasks, :max_depth, :integer, default: 0
  end
end
