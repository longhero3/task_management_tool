class AddColumnWeeklyReportsStartInTasks < ActiveRecord::Migration[5.0]
  def change
    add_column :tasks, :weekly_reports_start, :integer, default: 1, null: false
  end
end
