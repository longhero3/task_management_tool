class AddPayRateToTasks < ActiveRecord::Migration
  def change
    add_column :tasks, :pay_rate, :decimal, default: 0
  end
end
