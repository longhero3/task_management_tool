class CreateAcademicProfiles < ActiveRecord::Migration
  def change
    create_table :academic_profiles do |t|
      t.integer :year
      t.string :residential_address
      t.string :state
      t.string :primary_subject
      t.string :exam
      t.references :user, index: true, foreign_key: true
      t.string :high_school

      t.timestamps null: false
    end
  end
end
