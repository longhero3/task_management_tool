class SetDefaultPayItem < ActiveRecord::Migration
  def change
    change_column :pay_item_categories, :earnings_rate_id, :string, :default => 'unknown'
    pay_item_category = PayItemCategory.find_or_create_by(name: 'Unassigned Item')
    Task.pay_item_unassigned.all.each do |task|
      PayItem.create(task: task, earnings_rate_id: pay_item_category.earnings_rate_id, name: pay_item_category.name)
    end
  end
end
