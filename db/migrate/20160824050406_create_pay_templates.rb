class CreatePayTemplates < ActiveRecord::Migration
  def change
    create_table :pay_templates do |t|
      t.references :xero_account, index: true, foreign_key: true
      t.string :earnings_rate_id
      t.decimal :pay_rate

      t.timestamps null: false
    end
  end
end
