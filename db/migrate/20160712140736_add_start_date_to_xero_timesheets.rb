class AddStartDateToXeroTimesheets < ActiveRecord::Migration
  def change
    add_column :xero_timesheets, :start_date, :datetime
  end
end
