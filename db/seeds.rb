# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
max = User.create(first_name: 'Max', last_name: 'test', email: 'max.test@example.com', password: 'password', date_of_birth: '01/01/1994', role: 'super_admin', grad_job_title: 'Mr', second_email:'max.test1@example.com', contact_number: '12345678')
mik = User.create(first_name: 'Mik', last_name: 'test', email: 'mik.test@example.com', password: 'password', date_of_birth: '01/01/1994', role: 'super_admin', grad_job_title: 'Mr', second_email:'max.test2@example.com', contact_number: '12345678')
dan = User.create(first_name: 'Dan', last_name: 'test', email: 'dan.test@example.com', password: 'password', date_of_birth: '01/01/1994', role: 'admin', grad_job_title: 'Mr', second_email:'max.test3@example.com', contact_number: '12345678')
long = User.create(first_name: 'Long', last_name: 'test', email: 'long.test@example.com', password: 'password', date_of_birth: '01/01/1994', role: 'staff', grad_job_title: 'Mr', second_email:'max.test4@example.com', contact_number: '12345678')
aaron =User.create(first_name: 'Aaron', last_name: 'test', email: 'aaron.test@example.com', password: 'password', date_of_birth: '01/01/1994', role: 'manager', grad_job_title: 'Mr', second_email:'max.test5@example.com', contact_number: '12345678')
mike =User.create(first_name: 'Michael', last_name: 'Qiu', email: 'michael.qiu@gradready.com.au', password: 'password', date_of_birth: '01/01/1994', role: 'super_admin', grad_job_title: 'Mr', second_email:'max.test6@example.com', contact_number: '12345678')

parent_task = Task.create(title: "Parent Task 1", description: "123", target_start: 3.days.ago,
            target_finish: 1.day.from_now, actual_start: 3.days.ago, actual_finish: 1.day.from_now,
            budgeted_cost: 12, actual_cost: 24, priority: 1)
parent_task2 = Task.create(title: "Parent Task 2", description: "123", target_start: 3.days.ago,
                          target_finish: 1.day.from_now, actual_start: 3.days.ago, actual_finish: 1.day.from_now,
                          budgeted_cost: 12, actual_cost: 24, priority: 1)

children_task1 = parent_task.children.create(title: "Children Task 1", description: "123", target_start: 3.days.ago,
                                            target_finish: 1.day.from_now, actual_start: 3.days.ago, actual_finish: 1.day.from_now,
                                            budgeted_cost: 12, actual_cost: 24, priority: 1)

children_task11 = children_task1.children.create(title: "Children Task 1 1", description: "123", target_start: 3.days.ago,
                                                 target_finish: 1.day.from_now, actual_start: 3.days.ago, actual_finish: 1.day.from_now,
                                                 budgeted_cost: 12, actual_cost: 24, priority: 1)

children_task2 = parent_task.children.create(title: "Children Task 2", description: "123", target_start: 3.days.ago,
                                            target_finish:  1.day.from_now, actual_start: 3.days.ago, actual_finish: 1.day.from_now,
                                            budgeted_cost: 12, actual_cost: 24, priority: 1)

parent_task.task_users.create(user: max, isOwner: true, pay_rate: nil, extra_pay_rate: nil, task_role: "task_owner")
parent_task.task_users.create(user: mik, isOwner: false, pay_rate: nil, extra_pay_rate: nil, task_role: "member")
parent_task.task_users.create(user: long, isOwner: false, pay_rate: nil, extra_pay_rate: nil, task_role: "member")
parent_task.task_users.create(user: dan, isOwner: false, pay_rate: nil, extra_pay_rate: nil, task_role: "member")

parent_task2.task_users.create(user: max, isOwner: true, pay_rate: nil, extra_pay_rate: nil, task_role: "task_owner")
parent_task2.task_users.create(user: long, isOwner: false, pay_rate: nil, extra_pay_rate: nil, task_role: "member")

children_task1.task_users.create(user: max, isOwner: true, pay_rate: nil, extra_pay_rate: nil, task_role: "task_owner")
children_task1.task_users.create(user: mik, isOwner: false, pay_rate: nil, extra_pay_rate: nil, task_role: "member")
children_task1.task_users.create(user: long, isOwner: false, pay_rate: nil, extra_pay_rate: nil, task_role: "member")
children_task1.task_users.create(user: dan, isOwner: false, pay_rate: nil, extra_pay_rate: nil, task_role: "member")

children_task2.task_users.create(user: max, isOwner: true, pay_rate: nil, extra_pay_rate: nil, task_role: "task_owner")
children_task2.task_users.create(user: mik, isOwner: false, pay_rate: nil, extra_pay_rate: nil, task_role: "member")
children_task2.task_users.create(user: long, isOwner: false, pay_rate: nil, extra_pay_rate: nil, task_role: "member")
children_task2.task_users.create(user: dan, isOwner: false, pay_rate: nil, extra_pay_rate: nil, task_role: "member")

children_task11.task_users.create(user: max, isOwner: true, pay_rate: nil, extra_pay_rate: nil, task_role: "task_owner")
children_task11.task_users.create(user: mik, isOwner: false, pay_rate: nil, extra_pay_rate: nil, task_role: "member")
children_task11.task_users.create(user: long, isOwner: false, pay_rate: nil, extra_pay_rate: nil, task_role: "member")
children_task11.task_users.create(user: dan, isOwner: false, pay_rate: nil, extra_pay_rate: nil, task_role: "member")

