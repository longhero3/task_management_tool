# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170410104713) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "pg_trgm"

  create_table "addresses", force: :cascade do |t|
    t.string   "street"
    t.string   "suburb"
    t.string   "state"
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_addresses_on_user_id", using: :btree
  end

  create_table "change_records", force: :cascade do |t|
    t.string   "remindable_type", null: false
    t.integer  "remindable_id",   null: false
    t.string   "name",            null: false
    t.string   "old_value",       null: false
    t.string   "new_value",       null: false
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.index ["remindable_type", "remindable_id"], name: "index_change_records_on_remindable_type_and_remindable_id", using: :btree
  end

  create_table "comments", force: :cascade do |t|
    t.text     "comment"
    t.integer  "user_id"
    t.integer  "task_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["task_id"], name: "index_comments_on_task_id", using: :btree
    t.index ["user_id"], name: "index_comments_on_user_id", using: :btree
  end

  create_table "cost_types", force: :cascade do |t|
    t.string   "cost_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "costs", force: :cascade do |t|
    t.decimal  "amount",       precision: 8, scale: 2
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.integer  "user_id"
    t.integer  "cost_type_id"
    t.integer  "task_id"
    t.index ["cost_type_id"], name: "index_costs_on_cost_type_id", using: :btree
    t.index ["task_id"], name: "index_costs_on_task_id", using: :btree
    t.index ["user_id"], name: "index_costs_on_user_id", using: :btree
  end

  create_table "employees", force: :cascade do |t|
    t.string   "employee_id"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "email"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "exam_fields", force: :cascade do |t|
    t.string   "name"
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_exam_fields_on_user_id", using: :btree
  end

  create_table "exam_scores", force: :cascade do |t|
    t.decimal  "score"
    t.integer  "exam_field_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.index ["exam_field_id"], name: "index_exam_scores_on_exam_field_id", using: :btree
  end

  create_table "high_schools", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "job_titles", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "oauth_access_grants", force: :cascade do |t|
    t.integer  "resource_owner_id", null: false
    t.integer  "application_id",    null: false
    t.string   "token",             null: false
    t.integer  "expires_in",        null: false
    t.text     "redirect_uri",      null: false
    t.datetime "created_at",        null: false
    t.datetime "revoked_at"
    t.string   "scopes"
    t.index ["token"], name: "index_oauth_access_grants_on_token", unique: true, using: :btree
  end

  create_table "oauth_access_tokens", force: :cascade do |t|
    t.integer  "resource_owner_id"
    t.integer  "application_id"
    t.string   "token",                               null: false
    t.string   "refresh_token"
    t.integer  "expires_in"
    t.datetime "revoked_at"
    t.datetime "created_at",                          null: false
    t.string   "scopes"
    t.string   "previous_refresh_token", default: "", null: false
    t.index ["refresh_token"], name: "index_oauth_access_tokens_on_refresh_token", unique: true, using: :btree
    t.index ["resource_owner_id"], name: "index_oauth_access_tokens_on_resource_owner_id", using: :btree
    t.index ["token"], name: "index_oauth_access_tokens_on_token", unique: true, using: :btree
  end

  create_table "oauth_applications", force: :cascade do |t|
    t.string   "name",                      null: false
    t.string   "uid",                       null: false
    t.string   "secret",                    null: false
    t.text     "redirect_uri",              null: false
    t.string   "scopes",       default: "", null: false
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.index ["uid"], name: "index_oauth_applications_on_uid", unique: true, using: :btree
  end

  create_table "pay_item_categories", force: :cascade do |t|
    t.string   "earnings_rate_id", default: "unknown"
    t.string   "name"
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.string   "earnings_type"
    t.decimal  "pay_rate"
  end

  create_table "pay_items", force: :cascade do |t|
    t.integer  "task_id"
    t.string   "earnings_rate_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.string   "name"
    t.index ["task_id"], name: "index_pay_items_on_task_id", using: :btree
  end

  create_table "pay_templates", force: :cascade do |t|
    t.integer  "xero_account_id"
    t.string   "earnings_rate_id"
    t.decimal  "pay_rate"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.index ["xero_account_id"], name: "index_pay_templates_on_xero_account_id", using: :btree
  end

  create_table "payroll_calendars", force: :cascade do |t|
    t.string   "payroll_calendar_id"
    t.string   "name"
    t.string   "calendar_type"
    t.datetime "start_date"
    t.datetime "payment_date"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
  end

  create_table "profile_images", force: :cascade do |t|
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.integer  "user_id"
    t.index ["user_id"], name: "index_profile_images_on_user_id", using: :btree
  end

  create_table "reminder_users", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "reminder_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["reminder_id"], name: "index_reminder_users_on_reminder_id", using: :btree
    t.index ["user_id"], name: "index_reminder_users_on_user_id", using: :btree
  end

  create_table "reminders", force: :cascade do |t|
    t.string   "remindable_type"
    t.integer  "remindable_id"
    t.string   "type"
    t.string   "attribute_name"
    t.decimal  "threshold_value",      precision: 11, scale: 2
    t.integer  "threshold_comparator"
    t.integer  "due_remind_days"
    t.datetime "created_at",                                    null: false
    t.datetime "updated_at",                                    null: false
    t.index ["remindable_type", "remindable_id"], name: "index_reminders_on_remindable_type_and_remindable_id", using: :btree
  end

  create_table "sprints", force: :cascade do |t|
    t.datetime "target_start",                          null: false
    t.datetime "target_end",                            null: false
    t.decimal  "actual_cost",  precision: 11, scale: 2
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
  end

  create_table "subject_fields", force: :cascade do |t|
    t.string   "name"
    t.boolean  "assessed_for_proficiency"
    t.integer  "user_id"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.index ["user_id"], name: "index_subject_fields_on_user_id", using: :btree
  end

  create_table "task_summaries", force: :cascade do |t|
    t.integer  "task_id"
    t.decimal  "task_completed",                      default: "0.0"
    t.decimal  "budget_spent_ratio",                  default: "0.0"
    t.decimal  "task_completed_over_budget_consumed", default: "0.0"
    t.datetime "created_at",                                          null: false
    t.datetime "updated_at",                                          null: false
    t.index ["task_id"], name: "index_task_summaries_on_task_id", using: :btree
  end

  create_table "task_users", force: :cascade do |t|
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
    t.integer  "user_id"
    t.integer  "task_id"
    t.boolean  "isOwner",        default: false
    t.decimal  "pay_rate"
    t.decimal  "extra_pay_rate"
    t.string   "task_role",      default: "member"
  end

  create_table "tasks", force: :cascade do |t|
    t.string   "title"
    t.text     "description"
    t.datetime "target_start"
    t.datetime "target_finish"
    t.datetime "actual_start"
    t.datetime "actual_finish"
    t.decimal  "budgeted_cost",                                 default: "0.0"
    t.decimal  "actual_cost",          precision: 11, scale: 2
    t.integer  "priority"
    t.integer  "parent_id"
    t.integer  "lft",                                                               null: false
    t.integer  "rgt",                                                               null: false
    t.integer  "depth",                                         default: 0,         null: false
    t.integer  "children_count",                                default: 0,         null: false
    t.datetime "created_at",                                                        null: false
    t.datetime "updated_at",                                                        null: false
    t.decimal  "pay_rate",                                      default: "1.0"
    t.integer  "max_depth",                                     default: 0
    t.string   "status",                                        default: "created"
    t.boolean  "is_leaf",                                       default: true
    t.float    "total_progress",                                default: 0.0
    t.boolean  "colour_tcte",                                   default: true,      null: false
    t.integer  "sprint_id"
    t.integer  "weekly_reports_start",                          default: 1,         null: false
    t.string   "project_code"
    t.index ["lft"], name: "index_tasks_on_lft", using: :btree
    t.index ["parent_id"], name: "index_tasks_on_parent_id", using: :btree
    t.index ["rgt"], name: "index_tasks_on_rgt", using: :btree
    t.index ["sprint_id"], name: "index_tasks_on_sprint_id", using: :btree
  end

  create_table "timesheet_snapshots", force: :cascade do |t|
    t.text     "content"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "user_id"
    t.index ["user_id"], name: "index_timesheet_snapshots_on_user_id", using: :btree
  end

  create_table "user_payments", force: :cascade do |t|
    t.integer  "user_id"
    t.decimal  "pay_rate"
    t.decimal  "extra_pay_rate"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.index ["user_id"], name: "index_user_payments_on_user_id", using: :btree
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "",      null: false
    t.string   "encrypted_password",     default: "",      null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,       null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "first_name"
    t.string   "last_name"
    t.date     "date_of_birth"
    t.string   "role",                   default: "staff", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "wage_id"
    t.string   "grad_job_title"
    t.string   "second_email"
    t.string   "contact_number"
    t.integer  "grad_year"
    t.string   "high_school"
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
    t.index ["wage_id"], name: "index_users_on_wage_id", using: :btree
  end

  create_table "wages", force: :cascade do |t|
    t.boolean  "is_salary"
    t.decimal  "rate",        precision: 10, scale: 2
    t.text     "description"
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
  end

  create_table "work_periods", force: :cascade do |t|
    t.decimal  "amount_hours", precision: 4, scale: 2
    t.datetime "date_worked"
    t.datetime "start_time"
    t.datetime "end_time"
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.integer  "user_id"
    t.integer  "task_id"
    t.text     "description"
    t.float    "progress"
    t.decimal  "pay_rate"
    t.index ["task_id"], name: "index_work_periods_on_task_id", using: :btree
    t.index ["user_id"], name: "index_work_periods_on_user_id", using: :btree
  end

  create_table "xero_accounts", force: :cascade do |t|
    t.string   "employee_id"
    t.integer  "user_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["user_id"], name: "index_xero_accounts_on_user_id", using: :btree
  end

  create_table "xero_timesheets", force: :cascade do |t|
    t.string   "timesheet_id"
    t.integer  "user_id"
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
    t.string   "payroll_calendar_id"
    t.datetime "start_date"
    t.string   "status",              default: "DRAFT"
    t.index ["user_id"], name: "index_xero_timesheets_on_user_id", using: :btree
  end

  add_foreign_key "addresses", "users"
  add_foreign_key "comments", "tasks"
  add_foreign_key "comments", "users"
  add_foreign_key "costs", "cost_types"
  add_foreign_key "costs", "tasks"
  add_foreign_key "costs", "users"
  add_foreign_key "exam_fields", "users"
  add_foreign_key "exam_scores", "exam_fields"
  add_foreign_key "oauth_access_grants", "oauth_applications", column: "application_id"
  add_foreign_key "oauth_access_tokens", "oauth_applications", column: "application_id"
  add_foreign_key "pay_items", "tasks"
  add_foreign_key "pay_templates", "xero_accounts"
  add_foreign_key "profile_images", "users"
  add_foreign_key "reminder_users", "reminders"
  add_foreign_key "reminder_users", "users"
  add_foreign_key "subject_fields", "users"
  add_foreign_key "task_summaries", "tasks"
  add_foreign_key "tasks", "sprints"
  add_foreign_key "timesheet_snapshots", "users"
  add_foreign_key "user_payments", "users"
  add_foreign_key "users", "wages"
  add_foreign_key "work_periods", "tasks"
  add_foreign_key "work_periods", "users"
  add_foreign_key "xero_accounts", "users"
  add_foreign_key "xero_timesheets", "users"
end
