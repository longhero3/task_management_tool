var React = require('react');
var ReactDOM = require('react-dom');

export class AdminWorkPeriodIndex extends React.Component{
  constructor(props){
    super(props);
    this.state = {
      workPeriods: [],
      currentUser: null
    }
  }

  getChildContext(){
    return ({
      getWorkPeriods: function(){
        this.handleFilter({start_date: '', end_date: '', user_id: ''});
      }.bind(this),
      currentUser: this.state.currentUser
    })
  }

  handleFilter(data){
    $.ajax({
      url: '/work_periods/filter_by.json',
      dataType: 'json',
      data: {
        work_period: data
      },
      success: function(data) {
        $('#work_periods_view_wrapper .dimmer').removeClass('active');
        this.state.currentUser = data.current_user;
        this.setState({workPeriods: data.work_periods});
      }.bind(this),
      error: function(xhr, status, err) {
      }.bind(this)
    });
  }

  componentWillMount(){
    this.handleFilter({start_date: '', end_date: '', user_id: ''});
  }

  render(){
    return(
      <div id='work_periods_view_wrapper' className='ui segment'>
        <AdminWorkPeriodFilter onFilterChanged={this.handleFilter.bind(this)}/>
        <AdminWorkPeriods workPeriods={this.state.workPeriods} ref='work_periods'/>
        <div className="ui active inverted dimmer">
          <div className="ui text loader">Loading Hours Logged ...</div>
        </div>
      </div>
      );
  }
}
AdminWorkPeriodIndex.childContextTypes = {
  getWorkPeriods: React.PropTypes.func,
  currentUser: React.PropTypes.object
}

var mountElement = document.getElementById('admin_work_periods_container');
if (mountElement!= null){
  ReactDOM.render(<AdminWorkPeriodIndex/>, mountElement);
}
