var React = require('react');

export class AdminWorkPeriods extends React.Component{
  constructor(props){
    super(props);
    this.state = {
      workPeriods: props.workPeriods
    }
  }

  componentWillReceiveProps(newProps){
    this.state = newProps;
    this.forceUpdate();
  }

  deleteWorkPeriod(workPeriodID, arrayIndex){
    var result = confirm('Do you want to delete this work period? The user who logged these hours will be notified.');
    if(result == true){
      $.ajax({
        url: 'work_periods/'+ workPeriodID + '.json',
        method: 'DELETE',
        type: 'json',
        data: {
          id: workPeriodID
        },
        success: function(){
          this.state.workPeriods.splice(arrayIndex, 1);
          this.forceUpdate();
        }.bind(this)
      });
    }
  }

  handleUpdate(data){
    $.ajax({
      url: 'work_periods/'+ data.id + '.json',
      method: 'PUT',
      type: 'json',
      data: data,
      success: function(){
      }.bind(this)
    });
  }

  render(){
    return (
      <table className='ui celled table'>
        <thead>
          <tr>
            <th>Task</th>
            <th>Date</th>
            <th>Hour Logged</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {this.state.workPeriods.map(function(workPeriod, index){
            return (
              <AdminWorkPeriodRow workPeriod={workPeriod} key={'work_period_' + workPeriod.id}
                onWorkPeriodDelete={this.deleteWorkPeriod.bind(this, workPeriod.id, index)}
                onWorkPeriodUpdate={this.handleUpdate.bind(this)}
              />
            )}, this)}
        </tbody>
      </table>
    );
  }
}