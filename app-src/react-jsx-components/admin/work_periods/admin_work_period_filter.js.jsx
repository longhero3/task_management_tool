var React = require('react')

export class AdminWorkPeriodFilter extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      work_period: {
        start_date: '',
        end_date: '',
        user_id: ''
      }
    }
  }

  componentDidMount(){
    $('.start-date-filter').on('change', function(e){
      this.handleChange('start_date', $(e.target).val());
    }.bind(this));

    $('.end-date-filter').on('change', function(e){
      this.handleChange('end_date', $(e.target).val());
    }.bind(this));

    $('.user-chooser').search({
      apiSettings: {
        url: '/users/search_users?term={query}'
      },
      fields: {
        results: 'users',
        title: 'full_name',
        id: 'id'
      },
      errors: {
        noResults: ''
      },
      onSelect: function(result, response){
        this.handleChange('user_id', result.id);
      }.bind(this)
    });
  }

  handleChange(attr, value){
    this.state.work_period[attr] = value;
    this.props.onFilterChanged(this.state.work_period);
  }

  resetWorkPeriods(){
    $('.start-date-filter').val('');
    $('.end-date-filter').val('');
    $('.user-chooser .prompt').val('');

    this.props.onFilterChanged({
      start_date: null,
      end_date: null,
      user_id: null
    });
  }

  render(){
    return (
      <div className='ui grid'>
        <div className='six wide column'>
          <div className='ui search user-chooser'>
            <div className='ui fluid icon input'>
              <input className='prompt'
              ref='user_id'
              placeholder='User ...'
              />
              <i className="user icon"></i>
            </div>
          </div>
        </div>
        <div className='four wide column'>
          <div className='ui fluid input'>
            <input className='datepicker start-date-filter'
            ref='start_date'
            placeholder='From ...'
            />
          </div>
        </div>

        <div className='four wide column'>
          <div className='ui fluid input'>
            <input className='datepicker end-date-filter'
            ref='end_date'
            placeholder='To ...'
            />
          </div>
        </div>

        <div className='two wide column'>
          <button onClick={this.resetWorkPeriods.bind(this)} className='ui fluid button green'>Reset</button>
        </div>
      </div>
      );
  }
}