var React = require('react');
var ReactDOM = require('react-dom');
var ReactPaginate = require('react-paginate');

export class WorkPeriodIndex extends React.Component{
  constructor(props){
    super(props);
    this.state = {
      workPeriods: [],
      currentUser: null,
      offset: 0,
      total_pages_count: 0
    }
  }

  getChildContext(){
    return ({
      getWorkPeriods: function(){
        this.handleFilter({start_date: '', end_date: ''});
      }.bind(this),
      currentUser: this.state.currentUser
    })
  }

  handleFilter(data){
    $.ajax({
      url: '/work_periods/filter_by.json',
      dataType: 'json',
      data: {
        work_period: data,
        page: this.state.offset + 1,
        per_page: 10
      },
      success: function(data) {
        $('#work_periods_view_wrapper .dimmer').removeClass('active');
        this.state.currentUser = data.current_user;
        this.setState({
          workPeriods: data.work_periods,
          total_pages_count: Math.ceil(data.total_work_periods_count / 10)
        });
      }.bind(this),
      error: function(xhr, status, err) {
      }.bind(this)
    });
  }

  handlePageClick(data){
    let selected = data.selected;
    let offset = Math.ceil(selected);

    this.setState({offset: offset}, function(){

      this.handleFilter({start_date: '', end_date: '', user_id: ''});
    });
  }

  componentWillMount(){
    this.handleFilter({start_date: '', end_date: '', user_id: ''});
  }

  render(){
    return(
      <div id='work_periods_view_wrapper' className='ui segment'>
        <WorkPeriodFilter onFilterChanged={this.handleFilter.bind(this)}/>
        <div className="paginate_center">
          <ReactPaginate previousLabel={"previous"}
            nextLabel={"next"} breakLabel={<a href="">...</a>}
            breakClassName={"break-me"}
            pageCount={this.state.total_pages_count} marginPagesDisplayed={3}
            pageRangeDisplayed={2} onPageChange={this.handlePageClick.bind(this)}
            containerClassName={"pagination"} subContainerClassName={"pages pagination"}
            activeClassName={"active"}
          />
        </div>
        <WorkPeriods workPeriods={this.state.workPeriods} ref='work_periods'/>
        <div className="ui active inverted dimmer">
          <div className="ui text loader">Loading Hours Logged ...</div>
        </div>
      </div>
      );
  }
}
WorkPeriodIndex.childContextTypes = {
  getWorkPeriods: React.PropTypes.func,
  currentUser: React.PropTypes.object
}

var mountElement = document.getElementById('work_periods_container');
if (mountElement!= null){
  ReactDOM.render(<WorkPeriodIndex/>, mountElement);
}
