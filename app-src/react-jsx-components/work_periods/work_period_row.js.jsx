var React = require('react')
var moment = require('moment')

export class WorkPeriodRow extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      workPeriod: props.workPeriod
    };
  }

  componentDidMount(){
    $('.popup-control').popup();
  }

  onDeleteBtnClicked(e){
    e.preventDefault();
    if(this.state.workPeriod.edit_hour_enabled) {
      this.props.onWorkPeriodDelete();
    } else {
      alert('You cannot delete these hours because they are in the locked previous pay cycle.')
    }
  }

  onUpdate(key){
    if(key == 'amount_hours'){
      var preBudget = (parseFloat(this.refs[key].state.text)
        - parseFloat(this.state.workPeriod.amount_hours)) *
        parseFloat(this.state.workPeriod.pay_rate) + parseFloat(this.state.workPeriod.actual_cost);
      if(preBudget > parseFloat(this.state.workPeriod.budgeted_cost)) {
        var result = confirm('The edited hours logged will exceed the task budget. Continue?');
        if(result == true) {
          this.updateWorkPeriod(key);
        }
        else {
          this.refs[key].state.text = this.state.workPeriod.amount_hours;
          this.forceUpdate();
        }
      } else {
        this.updateWorkPeriod(key)
      }
    } else {
      this.updateWorkPeriod(key)
    }
  }

  updateWorkPeriod(key) {
    var hash = {
      id: this.state.workPeriod.id,
      work_period: {
      }
    }
    hash.work_period[key] = this.refs[key].state.text;
    this.setState({work_period: {"#{key}": parseInt(this.refs[key].state.text)}});
    this.props.onWorkPeriodUpdate(hash);
  }

  hoursValidator(text){
    return isNaN(text) == false && parseInt('0' + text, 10) > 0;
  }

  render(){
    return(
      <tr className='popup-control' data-content={this.state.workPeriod.description}>
        <td>{this.state.workPeriod.task_title}</td>
        <td>{moment(this.state.workPeriod.date_worked).format('DD/MM/YYYY')}</td>
        <td>
          <EditableField
          onSave={this.onUpdate.bind(this, 'amount_hours')}
          text={this.state.workPeriod.amount_hours}
          ref={'amount_hours'}
          className={'amount-hours'}
          enabled={this.state.workPeriod.edit_hour_enabled}
          validate={this.hoursValidator}
          />
        </td>
        <td className="right aligned collapsing">
          <a href='#' onClick={this.onDeleteBtnClicked.bind(this)} className='ui red small button'>Delete</a>
        </td>
      </tr>
    );
  }
}