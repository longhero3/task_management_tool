var React = require('react')

export class WorkPeriodFilter extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      work_period: {
        start_date: '',
        end_date: '',
        user_id: ''
      }
    }
  }

  componentDidMount(){
    $('.start-date-filter').on('change', function(){
      this.handleChange('start_date');
    }.bind(this));

    $('.end-date-filter').on('change', function(){
      this.handleChange('end_date');
    }.bind(this));
  }

  handleChange(attr){
    this.state.work_period[attr] = this.refs[attr].value;
    this.props.onFilterChanged(this.state.work_period);
  }

  resetWorkPeriods(){
    $('.start-date-filter').val('');
    $('.end-date-filter').val('');

    this.props.onFilterChanged({
      start_date: null,
      end_date: null,
      user_id: null
    });
  }

  render(){
    return (
      <div className='ui grid'>
        <div className='four wide column'>
          <div className='ui fluid input'>
            <input className='datepicker start-date-filter'
              ref='start_date'
              placeholder='From ...'
            />
          </div>
        </div>

        <div className='four wide column'>
          <div className='ui fluid input'>
            <input className='datepicker end-date-filter'
              ref='end_date'
              placeholder='To ...'
            />
          </div>
        </div>

        <div className='two wide column'>
          <button onClick={this.resetWorkPeriods.bind(this)} className='ui fluid button green'>Reset</button>
        </div>
      </div>
    );
  }
}