var React = require('react');

export class LogHourButton extends React.Component{
  constructor(props){
    super(props);
    this.state = {
      href: this.props.href,
      task: this.props.task
    }
  }

  componentWillReceiveProps(nextProps){
    this.setState(nextProps);
  }

  render(){
    if(this.state.task.log_hours_enabled == true){
      return <a className='ui green button log-hour-btn' href={this.state.href}>Log Hours</a>;
    } else {
      return <div></div>;
    }
  }
}

LogHourButton.contextTypes = { currentUser: React.PropTypes.object }