var React = require('react');
export class TaskTreeMembers extends React.Component{
  constructor(props){
    super(props);
    this.state = {
      task: this.props.task,
      addMemberLabel: '+ Member',
      toggled: this.props.toggled,
      users:[]
    };
  }

  componentDidMount(){
    $.ajax({
      url: '/home/user_list.json',
      dataType: 'json',
      success: function(data) {
        this.setState({users: data.users});
      }.bind(this)
    });
  }

  /*componentDidUpdate() {
    var task_id = this.props.selected_task
    if(task_id != undefined) {
      var top = $(".task-id-"+ task_id).offset().top;
      var container = $(".task_info_col");
      container.scrollTop(
        top-100 - container.offset().top + container.scrollTop()
      );
    }
  }*/

  getFullName(member){
    if(member != null) {
      return member.first_name + ' ' + member.last_name;
    } else {
      return 'None Selected';
    }
  }

  componentWillReceiveProps(nextProps){
    this.setState({toggled: nextProps.toggled});
  }

  getClassName(){
    return 'task_' + this.state.task.id;
  }

  handleChange(task_role){
    var user_id;
    if(this.state.task[task_role]) {
      user_id = this.state.task[task_role].id;
    } else{
      user_id = null;
    }
    var data = {
      task_user: {
        user_id: user_id,
        task_id: this.state.task.id,
        switch_user_id: this.refs[task_role].getSelectedValue(),
        task_role: task_role
      }
    };

    $.ajax({
      url: '/task_users/switch_role.json',
      method: 'PUT',
      dataType: 'json',
      data: data,
      success: function(result) {
        /*this.props.updateTask(result)*/
        this.setState({task: result});
        this.context.reloadLogHourButton(result.log_hours_enabled);
      }.bind(this),
      error: function(xhr, status, err) {
        alert(err);
      }.bind(this)
    });
  }

  handleDeleteMember(taskUserID, event){
    event.preventDefault();
    $.ajax({
      url: '/task_users/' + taskUserID + '.json',
      method: 'DELETE',
      dataType: 'json',
      data: {
        id: taskUserID
      },
      success: function(result) {
        this.setState({task: result});
        this.context.reloadLogHourButton(result.log_hours_enabled);
      }.bind(this),
      error: function(xhr, status, err) {
        alert(err);
      }.bind(this)
    });
  }

  handleAddMember(){
    $.ajax({
      url: '/task_users.json',
      method: 'POST',
      dataType: 'json',
      data: {
        task_user: {
          user_id: this.refs.add_member.getSelectedValue(),
          task_id: this.state.task.id,
          task_role: 'member'
        }
      },
      success: function(result) {
        this.setState({task: result});
        this.context.reloadLogHourButton(result.log_hours_enabled);
        this.forceUpdate();
      }.bind(this),
      error: function(xhr, status, err) {
        alert(err);
      }.bind(this)
    });
  }

  taskMemberView(){
    return (
      <div className='description'>
        <div className='task-owner'>
          <EditableSearchDropdown
            valueName={'id'}
            keyName={'full_name'}
            options={this.state.users}
            onSave={this.handleChange.bind(this, 'task_owner')}
            ref={'task_owner'}
            text={this.getFullName(this.state.task.active_members.task_owner)}
            className={this.getClassName() + '_task_owner'}
          />
          (TM)
        </div>
        <div className='members'>
          {this.state.task.active_members.task_members.map(function(taskMember){
            return <div className='member' key={taskMember.id}>{this.getFullName(taskMember)} - &nbsp;
              <a href='#' className='add-member-btn' onClick={this.handleDeleteMember.bind(this, taskMember.task_user_id)}>Remove</a>
            </div>;
          }, this)}

          <EditableSearchDropdown
            valueName={'id'}
            keyName={'full_name'}
            options={this.state.users}
            onSave={this.handleAddMember.bind(this, 'add_member')}
            ref={'add_member'}
            text={this.state.addMemberLabel}
            className={this.getClassName() + '_add_member'}
            keepUnchanged={true}
          />
        </div>
        <div className='escalation-manager'>
          <EditableSearchDropdown
            valueName={'id'}
            keyName={'full_name'}
            options={this.state.users}
            onSave={this.handleChange.bind(this, 'escalation_manager')}
            ref={'escalation_manager'}
            text={this.getFullName(this.state.task.active_members.escalation_manager)}
            className={this.getClassName() + '_manager'}
          />
          (EM)
        </div>
      </div>
      );
  }

  render(){
    if(this.state.task != null && this.state.task.active_members != null && this.state.toggled){
      return this.taskMemberView();
    } else {
      return <div></div>;
    }
  }
}

TaskTreeMembers.contextTypes = {
  reloadLogHourButton: React.PropTypes.func,
  users: React.PropTypes.array
}