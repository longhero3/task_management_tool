var React = require('react');
var moment = require('moment');
export class TaskCard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      task: props.task,
      updateTreeView: false
    }
    this.params = {
      id: this.props.task.id,
      task: {}
    };
    this.update = this.update.bind(this)
    this.selectedParentId = this.selectedParentId.bind(this)
  }

  priorityClass() {
    var className = '';
    if(this.state.task.priority < 7 ) {
      className = 'orange';
    } else {
      className = 'red';
    }
    return 'warning ' + className + ' icon';
  }

  componentWillReceiveProps(nextProps){
    this.setState({task: nextProps.task});
  }

  update(){
    if(this.state.updateTreeView){
      this.props.updateTreeView(this.state.task.id)
    }
  }

  taskUrl(){
    return '/task/'+ this.state.task.id + '?show_modal=true';
  }

  getClassName(){
    return 'task_' + this.state.task.id;
  }

  selectedParentId(id){
    this.props.selectedParentId(id)
  }

  handleParentBudget(event){
    if(this.props.task.parent != undefined || this.props.task.level == 1){
      var result = confirm("Do you wish to decrease the parent’s task budget by the budget of the task we are moving?");
      this.params.task['decrease_old_parents_budget'] = result;
      this.checkParentBudget(event);
    }
  }

  checkParentBudget(event){
    var data = {
      id: this.refs.parent_id.state.parentID,
      budgeted_cost: this.state.task.budgeted_cost
    };
    $.ajax({
      url: '/tasks/check_task_budget.json',
      method: 'GET',
      dataType: 'json',
      data: data,
      error: function(xhr, status, err) {
        if(xhr.status == 406){
          var result = confirm("Do you wants to increase the budget by the amount of the task we are moving?");
           this.params.task['increase_new_parents_budget'] = result
        } else if(xhr.status == 409){
           var result = confirm("Do you wish to proceed before increasing the budget of the parent?");
           if(result==true)
            this.params.task['increase_new_parents_budget'] = false
           else
            this.params.task['increase_new_parents_budget'] = true
        }
        this.handleUpdate(event);
      }.bind(this)
    });
  }

  handleBudgetUpdate(event){
    var data = {
      id: this.props.task.id,
      task: {}
    };
    data.task['budgeted_cost'] = event;
    if(this.state.task.parent != undefined){
      $.ajax({
          url: '/tasks/' + this.state.task.id + '/check_budget.json',
          method: 'GET',
          dataType: 'json',
          data: data,
          success: function(result) {
            this.setState({task: result});
          }.bind(this),
          error: function(xhr, status, err) {
            if(xhr.status == 406){
              var result = confirm("You've set the budget greater than the parent's. Do you want to continue?");
              if(result == true){
                this.handleUpdate({budgeted_cost: 'budgeted_cost', value: event});
              } else {
                this.refs['budgeted_cost'].reset();
              }
            } else if(xhr.status == 409){
              var delta = parseInt(data.task['budgeted_cost']) - this.state.task.budgeted_cost;
              var result = false;
              if (delta < 1) {
                  result = confirm("Do you wish to decrease the budget of the parent tasks" + " by $" + Math.abs(delta) +" the same amount ?");
                  this.params.task['decrease_all_parents_budget'] = result;
                  if(result == true) {
                      var parent_attrs = {}
                      parent_attrs['amount'] = this.state.task.budgeted_cost - parseInt(data.task['budgeted_cost']);
                      parent_attrs['parent_id'] = this.props.task.parent.id;
                      this.params.task['decrease_parents_budget_attributes'] = parent_attrs;
                  }
              } else {
                  result = confirm("Do you wish to increase the budget of " + this.state.task.parent.title + " by $" + delta +"?");
                  this.params.task['increase_parents_budget'] = result;
                  if(result == true) {
                      var parent_attrs = {}
                      parent_attrs['amount'] = parseInt(data.task['budgeted_cost']) - this.state.task.budgeted_cost;
                      parent_attrs['parent_id'] = this.props.task.parent.id;
                      this.params.task['increase_parents_budget_attributes'] = parent_attrs;
                  }
              }
              this.handleUpdate({budgeted_cost: 'budgeted_cost', value: event});
            } else {
                this.refs['budgeted_cost'].reset();
            }
          }.bind(this)
        });
      }else{
        this.handleUpdate({budgeted_cost: 'budgeted_cost', value: event});
      }
  }

  handleTargetUpdate(event){
    var data = {
      id: this.props.task.id,
      task: {}
    };
    data.task[event] = this.refs[event].getText();

    $.ajax({
      url: '/tasks/' + this.state.task.id + '/check_task_date_range.json',
      method: 'PUT',
      dataType: 'json',
      data: data,
      success: function(result) {
        this.setState({task: result});
      }.bind(this),
      error: function(xhr, status, err) {
        if(xhr.status == 406){
          var result = confirm("You've set the date out of the parent. Do you want to continue?");
          if(result == true){
            this.handleUpdate(event);
          } else {
            this.refs[event].reset();
          }
        } else {
          alert(xhr.responseText);
          this.refs[event].reset();
        }
      }.bind(this)
    });
  }

  handleUpdate(event){
    if(event.budgeted_cost == "budgeted_cost"){
      var task = this.state.task
    this.params.id = this.props.task.id
    this.params.task[event.budgeted_cost] = event.value;
    }else{
    var task = this.state.task
    this.params.id = this.props.task.id
    this.params.task[event] = this.refs[event].getText();
    }
    $.ajax({
      url: '/tasks/' + this.state.task.id + '.json',
      method: 'PUT',
      dataType: 'json',
      data: this.params,
      success: function(result) {
        this.setState({task: result, updateTreeView: true});
        if(event == 'title'){
          this.context.onTaskSelected(this.state.task);
          return alertify.success("Task title is changed.");
        }
        if(event == 'parent_id'){
          this.setState({updateTreeView: true});
          this.context.onTaskSelected(this.state.task);
          this.update()
          return alertify.success("New Parent was assigned successfully.");
        }
        if(event.budgeted_cost == 'budgeted_cost'){
          return alertify.success("New Budget was assigned successfully.");
          this.setState({task: result, updateTreeView: true});
        }

      }.bind(this),
      error: function(xhr, status, err) {
        this.setState({updateTreeView: true})
        if(event.budgeted_cost == 'budgeted_cost'){
          this.setState({task: this.state.task})
          alert(xhr.responseText);
        }else if(event == 'parent_id'){
          this.setState({updateTreeView: true});
          this.context.onTaskSelected(this.state.task);
          this.update()
          return alertify.success("New Parent was assigned successfully.");
        }
      }.bind(this)
    });
  }

  render() {
    return (
      <div className='ui card task-card task_view_overflow width_task_card'>
        <div className='content lft_flxgrw'>
          <EditableField
            onSave={this.handleUpdate.bind(this, 'title')}
            text={this.state.task.title}
            ref={'title'}
            className={'header task-title'}
          />
        </div>
        <div className='content cstm_card_cntnt'>
          <div className='ui six row centered grid'></div>
          <div className='ui list'>
            <div>
              <div className="content">
                <div className='task-value'><b>Budget to be Allocated:</b>
                  {this.state.task.budget_to_be_allocated}
                </div>
                <div><b>Budget:</b>
                  <EditableBudgetField
                    onBudgetSave={this.handleBudgetUpdate.bind(this)}
                    text={this.state.task.budgeted_cost}
                    ref={'budgeted_cost'}
                    className={'task-value'}
                  />
                </div>
                <div className='task-value'><b>Budget Taken:</b>
                  {this.state.task.budget_taken}
                </div>
                <div className='task-value'><b>Budget Archived:</b>
                  {this.state.task.total_budget_of_archived_children}
                </div>
                <div className='task-value'><b>Actual Cost:</b>
                  {this.state.task.actual_cost}
                </div>
                <div className='task-value'><b>Completed:</b>
                  {this.state.task.task_completed}%
                </div>

              </div>
            </div>
          </div>
          <div className='ui list'>
            <div className='task-value'>
              <b>Task Code:</b>
              {this.state.task.task_code}
            </div>
            <div><b>Estimated Dates</b>
              <div>
                <div><b>Start:</b>
                  <EditableDatePicker
                    onSave={this.handleTargetUpdate.bind(this, 'target_start')}
                    text={moment(this.state.task.target_start).format('DD/MM/YYYY h:mm A')}
                    ref={'target_start'}
                    className={this.getClassName() + '_target_start task-value'}
                  />
                </div>
                <div><b>Finish:</b>
                  <EditableDatePicker
                    onSave={this.handleTargetUpdate.bind(this, 'target_finish')}
                    text={moment(this.state.task.target_finish).format('DD/MM/YYYY h:mm A')}
                    ref={'target_finish'}
                    className={this.getClassName() + '_target_finish task-value'}
                  />
                </div>
              </div>
            </div>
          </div>

          {this.state.task.level == 1 ? (
              <div className="row">
                <b>Project Code:</b>
                <EditableField
                  onSave={this.handleUpdate.bind(this, 'project_code')}
                  text={this.state.task.project_code}
                  ref={'project_code'}
                  className={'task-value'}
                 />
              </div>
            ) : null
          }

          <div className="row task-value">
            <b>Pay rate:</b>
            {this.state.task.pay_rate}
          </div>
          <div className="row">
            <b>Parent Task:</b>
            <ParentTaskChooser
              task={this.state.task}
              parentTask={this.state.task.parent}
              selectedParentId={this.selectedParentId}
              onParentTaskSelected={this.handleParentBudget.bind(this, 'parent_id')}
              ref={'parent_id'}
            />
          </div>
          <div className="pay-item row">
            <b>Pay Item:</b>
            <PayItemChooser
              task={this.state.task}
              onPayItemSelected={this.handleUpdate.bind(this, 'pay_item_attributes')}
              ref={'pay_item_attributes'}
            />
          </div>

          <div className="row">
            <b>Weekly report day:</b>
            <WeekDayChooser
              task={this.state.task}
              onWeekDaySelected={this.handleUpdate.bind(this, 'weekly_reports_start')}
              ref={'weekly_reports_start'}
             />
          </div>

          <div className="row">
            <b>Description: </b>(Type description here and press SHIFT + enter …)
            <EditableTextArea
              onSave={this.handleUpdate.bind(this, 'description')}
              text={this.state.task.description}
              ref={'description'}
              className={this.getClassName() + '_description'}
            />
          </div>
        </div>
      </div>
    );
  }
}

TaskCard.contextTypes = {
  onTaskSelected: React.PropTypes.func,
  reloadTask: React.PropTypes.func
}