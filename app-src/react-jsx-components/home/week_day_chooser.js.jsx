var React = require('react');
const weekDays = [{'weekId': 0, 'weekName': 'Sunday'}, {'weekId': 1, 'weekName': 'Monday'}, {'weekId': 2, 'weekName': 'Tuesday'}, {'weekId': 3, 'weekName': 'Wednesday'}, {'weekId': 4, 'weekName': 'Thursday'}, {'weekId': 5, 'weekName': 'Friday'}, {'weekId': 6, 'weekName': 'Saturday'}]
export class WeekDayChooser extends React.Component{
  constructor(props){
    super(props);
    this.state = {
      task: this.props.task,
      weekId: '',
      weekName: ''
    }
  }

  capitalizeFirstLetter(currentWeekName){
    return currentWeekName.charAt(0).toUpperCase()+ currentWeekName.slice(1);
  }

  weekName(){
    if(this.props.task != null && this.props.task.weekly_reports_start != null){
      return this.capitalizeFirstLetter(this.props.task.weekly_reports_start);
    } else {
      return weekDays[1].weekName;
    }
  }

  componentDidMount(){
    $('.week-day-item').dropdown({
      fullTextSearch: true,
      onChange: function(value, text) {
        this.setState({weekId: value, weekName: text});
        this.props.onWeekDaySelected();
      }.bind(this)
    })
  }

  getText(){
    return this.state.weekId;
  }

  render(){
    return(
      <div className="ui bottom left pointing dropdown labeled icon button week-day-item">
        <i className="icon dropdown" />
        <span className="text">{this.weekName()}</span>
        <div className="menu">
          <div className="scrolling menu">
            {weekDays.map(function(days){
              return (
                <div className="item" data-value={days.weekId} key={'week_days_' + days.weekId}>{days.weekName}</div>
              )}, this)}
          </div>
        </div>
      </div>
      );
  }
}

WeekDayChooser.contextTypes = {
  weekDays: React.PropTypes.array
}