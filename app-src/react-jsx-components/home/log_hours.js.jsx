var React = require('react');
var time_worked;
export class LogHours extends React.Component{
  constructor(props){
    super(props);
    this.state = {
      time_worked:'',
      validateTime:'',
      validatetaskProgress:'',
      validateDescription:'',
      description:'',
      task_progress:0,
      validateDate:''
    },
    this.handleSubmit = this.handleSubmit.bind(this);
    this._submitLogHours = this._submitLogHours.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.changeState = this.changeState.bind(this);
  }

  handleSubmit(e) {
    e.preventDefault();
  }

  handleChange(e){
    var numbers = /^[0-9]+$/;
    if(this.state.time_worked !=''){
      this.setState({
        validateTime:''
      })
    }
    if(this.state.description !=''){
      this.setState({
        validateDescription:''
      })
    }
    this.setState({
      [e.target.name]: e.target.value
    })
  }

  _validateInput(){
    var valid = true
    var numbers = /^-{0,1}\d*\.{0,1}\d+$/;
    if(this.state.time_worked ==''){
      this.setState({
        validateTime:<p className="ui basic red pointing prompt label transition visible fix_inpt">input cannot be blank</p>
      })
      valid=false
    }else if(!this.state.time_worked.match(numbers)){
      this.setState({
        validateTime:<p className="ui basic red pointing prompt label transition visible fix_inpt">Please fill numbers only</p>
      })
      valid=false
    }else{
      this.setState({
        validateTime:''
      })
    }
    if(this.state.description == ''){
      this.setState({
        validateDescription:<p className="ui basic red pointing prompt label transition visible fix_inpt">input cannot be blank</p>
      })
      valid=false
    }else{
      this.setState({
        validateDescription:''
      })
    }
    if(this.props.date == undefined || this.props.date == ''){
        alert("Date cannot be blank")
        this.setState({
        validateDate:<p className="ui basic red pointing prompt label transition visible date_valid fix_inpt">Please fill the date</p>
      })
      valid=false
    }else{
      this.setState({
        validateDate:''
      })
    }
    return valid;
  }

  changeState(){
    this.props.onclick()
  }

  _submitLogHours(e){
    var timeWorked = "time_worked"+this.props.task.id;
    var taskProgress = "task_progress"+this.props.task.id;
    var _description = "description"+this.props.task.id
    if(this._validateInput()){
      var _self = this
      var task_id = this.props.task.id;
      var amount_hours = this.state.time_worked;
      var date_worked = this.props.date;
      var progress = this.state.task_progress;
      var description = this.state.description;
      var values =  {
                      ['work_period[task_id]']:task_id,
                      ['work_period[amount_hours]']:amount_hours,
                      ['work_period[date_worked]']:date_worked,
                      ['work_period[progress]']:progress,
                      ['work_period[description]']:description
                    };
      return $.ajax({
        method: 'GET',
        url: '/work_periods/check_budget.json',
        data: values,
        success: function(xhr, status, err) {
          var result;
          if (xhr.code === "LOG-200") {
            return $.ajax({
              method: 'POST',
              url: '/work_periods.json',
              data: values,
              success: function(xhr, status, err) {
                _self.changeState()
                _self.state.task_progress = 0
                _self.state.description = ""
                _self.state.time_worked = ""
                document.getElementById(timeWorked).value = "";
                document.getElementById(taskProgress).value = "";
                document.getElementById(_description).value = "";
                return alertify.success(xhr.msg);
               },
            }) 
          }else if (xhr.code === "LOG-400") {
              result = confirm('You cannot log these hours as it would exceed the planned budget. Would you like to alert your Task Manager?');
            if (result) {
              $.ajax({
                method: 'POST',
                url: '/work_periods/send_exceeded_log_hours_notification.json',
                data: {
                  task_id: task_id,
                  amount_hours: amount_hours
                },
                success: function(xhr, status, err) {
                _self.state.task_progress = 0
                _self.state.description = ""
                _self.state.time_worked = ""
                document.getElementById(timeWorked).value = "";
                document.getElementById(taskProgress).value = "";
                document.getElementById(_description).value = "";
                  return alertify.success(xhr.msg);
                },
                   error: function() {
                     var result;
                    return result = confirm('something went wrong?');
                  }
                });
              }
            }
          }
        })
      }
  }

  render(){
    return(
			<form id="submission_form" onSubmit={this.handleSubmit}>
        {this.state.validateDate}
        <div id="main" className="main_task_col">
          <div className="time_col">
          </div>
          <div className="time_col">
            <input type="text" id={"time_worked"+this.props.task.id} name="time_worked" className="ipt_marg" onChange={this.handleChange} title="Time Worked(Hours)"/>
            {this.state.validateTime}
          </div>
          <div className="time_col">
            <input type="number" id={"task_progress"+this.props.task.id} className="ipt_marg" name="task_progress" onChange={this.handleChange} value={this.state.task_progress} title="Task progress(%)"/>
          </div>
          <div className="time_col">
            <input type="text" id={"description"+this.props.task.id} className="ipt_marg" name="description" onChange={this.handleChange} title="description" />
        	   {this.state.validateDescription}
          </div>
          <input type="submit" value="Log Hours" className="ui deny button submit_btn mg_top13" id="submit" onClick={this._submitLogHours}/>
      	</div>
    	</form>
    )
  }
}