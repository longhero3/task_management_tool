var React = require('react');
let selected_task_id
  export class TaskTreeItem extends React.Component{
    constructor(props){
      super(props);
      this.state = {
        loading: false,
        toggled: props.task.toggled
      }
      this.hoursAdded = this.hoursAdded.bind(this)
    }
    onTaskSelected(e){
      selected_task_id=this.props.task.id
      e.preventDefault();
      this.context.onTaskItemSelected(this.props.task);
      this.setState({ loading: false, toggled: !this.state.toggled });
    }
    hoursAdded(){
      this.props.onclick()
    }

    /*loadingClass() {
      var baseClass = 'ui inline mini loader';
      if(this.state.loading == true){
      }
    }*/

    render(){
      var _logHours
      if(this.state.loading){
      var baseClass = 'active_red';
      }
      if(this.props.task.children_count == 0){
        _logHours = <LogHours date={this.props.date} task={this.props.task} onclick={() => {this.hoursAdded()}} />
      }
      return(
        <div className={ 'ui list task-id-' + this.props.task.id }>
          <div className='item'>
            <div className={'content remv_padd ' + this.props.className}>
              <span className='task-level'>{this.props.task.level}</span>
              <i className='tasks icon remve_dsp_tblcll'>
              </i>
              <a href='#' className={'header task_view'} onClick={this.onTaskSelected.bind(this)}>
               <span className="task_title">{this.props.task.title}
               </span>
               <span>{this.props.task.hours_added}</span>
              </a>
              {_logHours}
            <TaskTreeMembers loading={this.state.loading} updateTask={this.props.updateTask} task={this.props.task} toggled={this.state.toggled} selected_task={selected_task_id}/>
              {this.props.children}
            </div>
          </div>
        </div>
      );
    }
  }

TaskTreeItem.contextTypes = { onTaskItemSelected: React.PropTypes.func }