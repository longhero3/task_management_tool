var React = require('react');
export class PayItemChooser extends React.Component{
  constructor(props){
    super(props);
    this.state = {
      task: this.props.task,
      payItemId: '',
      payItemName: '',
      payItems:[]
    }
  }

  payItemName(){
    if(this.props.task != null && this.props.task.pay_item != null){
      return this.props.task.pay_item.name;
    } else {
      return 'None Selected';
    }
  }

  componentDidMount(){
    $('.task-pay-item').dropdown({
      fullTextSearch: true,
      onChange: function(value, text) {
        this.setState({payItemId: value, payItemName: text});
        this.props.onPayItemSelected();
      }.bind(this)
    })
    $.ajax({
      url: '/home/pay_items.json',
      dataType: 'json',
      success: function(data) {
        this.setState({payItems: data.pay_items});
      }.bind(this)
    });
  }

  getText(){
    var hash = {
      earnings_rate_id: this.state.payItemId,
      name: this.state.payItemName
    };
    return hash;
  }

  render(){
    return(
      <div className="ui bottom left pointing dropdown labeled icon button task-pay-item">
        <i className="icon dropdown" />
        <span className="text">{this.payItemName()}</span>
        <div className="menu">
          <div className="ui icon search input">
            <i className="search icon" />
            <input type="text" placeholder="Search Pay Item..." />
          </div>
          <div className="scrolling menu">
            {this.state.payItems.map(function(payItem){
              return (
                <div className="item" data-value={payItem.earnings_rate_id} key={'pay_item_' + payItem.earnings_rate_id}>{payItem.name}</div>
              )}, this)}
          </div>
        </div>
      </div>
      );
  }
}

PayItemChooser.contextTypes = {
  payItems: React.PropTypes.array
}