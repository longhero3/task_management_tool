var React = require('react');
var ReactDOM = require('react-dom');

export class MainView extends React.Component{
  constructor(props){
    super(props);
    this.state = {
      data: '',
      total_tasks: 0,
      filter:'',
      tasks: [],
      levelHash: [],
      currentUser: null,
      currentFilterLevel: 1
    }
  }

  getChildContext(){
    return ({
      loadAllTasks: function(task){
        this.requestVitalTasks();
      }.bind(this),
      filterBranchTasks: function(data){
        this.filterBranchTasks(data)
      }.bind(this),
      filterPayitem: function(data){
        this.filterPayitem(data)
      }.bind(this),
      filterQueryTasks: function(data){
        this.filterQueryTasks(data)
      }.bind(this),
      currentUser: this.state.currentUser
    })
  }

  filterBranchTasks(data) {
    this.setState({data: data});
    $('#task_view_outer_wrapper .dimmer').dimmer('show');
    $.ajax({
      url: '/tasks/task_vital_tasks.json',
      dataType: 'json',
      data: data,
      success: function(data) {
        this.state.currentUser = data.current_user;
        this.setState({
          total_tasks: data.total_tasks_count,
          filter: 'filter_branch_tasks',
          tasks: data.tasks,
          levelHash: data.available_levels
        });
        $('#task_view_outer_wrapper .dimmer').dimmer('hide');
      }.bind(this),
      error: function(xhr, status, err) {
      }.bind(this)
    });
  }

  filterPayitem(data) {
    this.setState({data: data});
    $('#task_view_outer_wrapper .dimmer').dimmer('show');
    $.ajax({
      url: '/tasks/task_pay_item.json',
      dataType: 'json',
      data: data,
      success: function(data) {
        this.state.currentUser = data.current_user;
        this.setState({
          total_tasks: data.total_tasks_count,
          filter: 'filter_pay_item',
          tasks: data.tasks,
          levelHash: data.available_levels
       });
        $('#task_view_outer_wrapper .dimmer').dimmer('hide');
      }.bind(this),
      error: function(xhr, status, err) {
      }.bind(this)
    });
  }

  filterQueryTasks(data) {
    this.setState({data: data});
    $('#task_view_outer_wrapper .dimmer').dimmer('show');
    $.ajax({
      url: '/tasks/filter_tasks.json',
      dataType: 'json',
      data: data,
      success: function(data) {
        this.state.currentUser = data.current_user;
        this.setState({
          total_tasks: data.total_tasks_count,
          filter: 'filter_query_tasks',
          tasks: data.tasks,
          levelHash: data.available_levels
        });
        $('#task_view_outer_wrapper .dimmer').dimmer('hide');
      }.bind(this),
      error: function(xhr, status, err) {
      }.bind(this)
    });
  }

  componentWillMount(){
    this.requestVitalTasks();
  }

  requestVitalTasks(){
    $('#task_view_outer_wrapper .dimmer').dimmer('show');
    $.ajax({
      url: '/tasks/task_vital_tasks.json',
      dataType: 'json',
      data: {
        page: 1,
        per_page: 20
      },
      success: function(data) {
        $('#task_view_outer_wrapper .dimmer').removeClass('active');
        this.state.currentUser = data.current_user;
        this.setState({
          total_tasks: data.total_tasks_count,
          filter: 'request_vital_tasks',
          tasks: data.tasks,
          levelHash: data.available_levels
        });
        $('#task_view_outer_wrapper .dimmer').dimmer('hide');
      }.bind(this),
      error: function(xhr, status, err) {
      }.bind(this)
    });
  }

  render(){
    return(
      <div id='task_view_outer_wrapper' className='ui segment'>
        <VitalTasks tasks={this.state.tasks} levelHash={this.state.levelHash} ref='vital_tasks' currentFilterLevel={this.state.currentFilterLevel} total_tasks = {this.state.total_tasks} filter = {this.state.filter} data = {this.state.data} />
        <div className="ui active inverted dimmer">
          <div className="ui text loader">Loading Tasks ...</div>
        </div>
      </div>
      );
  }
}
MainView.childContextTypes = {
  loadAllTasks: React.PropTypes.func,
  filterQueryTasks: React.PropTypes.func,
  filterBranchTasks: React.PropTypes.func,
  filterPayitem: React.PropTypes.func,
  currentUser: React.PropTypes.object
}

var mountElement = document.getElementById('project_cards_container');
if(mountElement != null) {
  ReactDOM.render(<MainView/>, mountElement);
}
