var React = require('react');

export class DeleteTaskButton extends React.Component{
  constructor(props){
    super(props);
    this.state = {
      buttonShown: false
    }
  }

  componentDidMount(){
    var currentUser = this.props.task?this.props.task.current_user:''
    var role = this.props.task?this.props.task.current_user.role:''
    if(currentUser == null) return false;
    if(role != 'staff') {
      this.setState({buttonShown: true});
      return true;
    }
    this.setState({buttonShown: this.props.task.level > 2});
  }

  onClick(){
    this.props.onClick();
  }

  render(){
    if(this.state.buttonShown == true){
      return <div className='ui button red delete-task' onClick={this.onClick.bind(this)}><a href={'/'}>Archive Task</a></div>;
    } else {
      return <div></div>;
    }
  }
}

DeleteTaskButton.contextTypes = { currentUser: React.PropTypes.object }