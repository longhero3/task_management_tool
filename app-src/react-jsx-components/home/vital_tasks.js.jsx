var React = require('react');
var moment = require('moment');
var ReactDOM = require('react-dom');
var ReactPaginate = require('react-paginate');
var DEFAULT_USER = 0;

export class VitalTasks extends React.Component{
  constructor(props){
    super(props);
    this.state = {
      offset: 0,
      tasks: this.props.tasks,
      filteredTasks: this.props.tasks,
      levelHash: this.props.levelHash,
      task: null,
      currentFilterLevel: this.props.currentFilterLevel,
      currentFilterUser: DEFAULT_USER,
      currentTaskRole: '',
      users: [],
      current_taskId: null,
      payItems: [],
      current_task:{},
      statSchema: [
        {
          title: 'Task',
          value: 'title',
          className: 'title',
          order: 'asc'
        },
        {
          title: 'Due Date',
          value: 'target_finish_int',
          className: 'due-date',
          order: 'asc'
        },
        {
          title: '% Time Elapsed(TE)',
          value: 'time_elapsed',
          className: 'time-elapsed',
          order: 'asc'
        },
        {
          title: '% Task Competed(TC)',
          value: 'task_completed',
          className: 'task-completion',
          order: 'asc'
        },
        {
          title: '% Budget Used(BU)',
          value: 'budget_spent_ratio',
          className: 'budget-spent',
          order: 'asc'
        },
        {
          title: '% TC : % BU',
          value: 'task_completed_over_budget_consumed',
          className: 'task-budget-ratio',
          order: 'asc'
        },
        {
          title: '% TC by Me',
          value: 'task_completed_by_user',
          className: 'task-self-complete',
          order: 'asc'
        },
        {
          title: '% BU by Me',
          value: 'budget_spent_by_user',
          className: 'budget-self-spent',
          order: 'asc'
        },
        {
          title: 'My % TC : % BU',
          value: 'task_completed_over_budget_consumed_by_user',
          className: 'self-task-budget-ratio',
          order: 'asc'
        }
      ]
    }
    this.onTaskSelected = this.onTaskSelected.bind(this)
  }

  getChildContext(){
    return ({
      users: this.state.users,
      reloadTask: function(task){
        var taskRow = this.refs['task_row_' + task.id];
        if(taskRow != null) {
          taskRow.updateData(task);
        }
      }.bind(this)
    });
  }

  componentDidMount(){
    var taskId = window.location.pathname.split('/')[2] ? window.location.pathname.split('/')[2] : null;
    if(taskId) {
      $.ajax({
        url: 'tasks/'+ taskId + '.json',
        dataType: 'json',
        success: function(data) {
          this.onTaskSelected({task: data, modalShown: true});
        }.bind(this)
      });
    }
  }

  componentWillReceiveProps(newProps){
    this.setState({
      filteredTasks: newProps.tasks
    })
  }

  handleSort(key, direction){
    this.setState({
      filteredTasks: this.state.filteredTasks.sort(function(task1, task2){
        var value1 = task1[key];
        var value2 = task2[key];
        if(direction == 'desc') {
          [value1, value2] = [value2, value1]
        }
        if(value1 < value2){
          return -1;
        } else {
          return 1;
        }
      }.bind(this))
    })
  }

  componentWillUpdate(newProps, newStates){
    if(newStates.currentFilterLevel != this.state.currentFilterLevel
      || newStates.currentFilterUser != this.state.currentFilterUser
      || newStates.currentTaskRole != this.state.currentTaskRole){
      var queryParams = {task: { task_users: {}}};
      if(newStates.currentFilterLevel > 0) {
        queryParams.task.depth = newStates.currentFilterLevel - 1;
      }

      if(newStates.currentFilterLevel == 'Leaflet'){
        queryParams.task.is_leaf = true;
      }

      if(newStates.currentFilterUser > 0) {
        queryParams.task.task_users['user_id'] = newStates.currentFilterUser;
      }

      if(newStates.currentTaskRole != '') {
        queryParams.task.task_users['task_role'] = newStates.currentTaskRole;
      }

      this.context.filterQueryTasks(queryParams);
    }
  }

  handleLevelFilter(level){
    this.setState({
      currentFilterLevel: level
    });
  }

  handleStaffFilter(userID){
    this.setState({
      currentFilterUser: Number(userID)
    });
  }

  handleTaskRoleFilter(role){
    this.setState({
      currentTaskRole: role
    });
  }

  onTaskDeleted(deletedTask){
    var deletedIndex = -1;
    this.state.filteredTasks.some(function(task){
      if(task.id == deletedTask.id){
        deletedIndex = this.state.filteredTasks.indexOf(task);
        return true;
      }
    }.bind(this));

    if(deletedIndex > -1){
      this.state.filteredTasks.splice(deletedIndex, 1);
    }

    this.refs.task_modal.state.modalShown = false;
    $('.ui.modal.task-view').modal('hide');
    this.forceUpdate();
  }

  onTitleClicked(stat){
    this.handleSort(stat.value, stat.order);
    if(stat.order == 'asc') {
      stat.order = 'desc';
    } else {
      stat.order = 'asc';
    }
  }

  onTaskSelected(hash){
    this.refs.task_modal.setState(hash);
  }

  handlePageClick(data){
    let selected = data.selected;
    let offset = Math.ceil(selected);
    this.setState({
      offset: data.selected
    });
    if(this.props.filter == 'request_vital_tasks'){
      this.requestVitalTasks(offset);
    }else if(this.props.filter == 'filter_branch_tasks'){
      this.filterBranchTasks(offset);
    }else if(this.props.filter == 'filter_pay_item'){
      this.filterPayitem(offset);
    }else if(this.props.filter == 'filter_query_tasks'){
      this.filterQueryTasks(offset);
    }
  }

  filterBranchTasks(data) {
    $('#task_view_outer_wrapper .dimmer').dimmer('show');
    $.ajax({
      url: '/tasks/task_vital_tasks.json',
      dataType: 'json',
      data: {
        page:  data+1,
        per_page: 20
      },
      success: function(data) {
        this.state.currentUser = data.current_user;
        this.setState({
          tasks: data.tasks,
          filteredTasks: data.tasks,
          filter: 'filter_branch_tasks',
          levelHash: data.available_levels
        });
        $('#task_view_outer_wrapper .dimmer').dimmer('hide');
      }.bind(this),
      error: function(xhr, status, err) {
      }.bind(this)
    });
  }

  filterPayitem(data) {
    $('#task_view_outer_wrapper .dimmer').dimmer('show');
    $.ajax({
      url: '/tasks/task_pay_item.json',
      dataType: 'json',
      data: {
        page:  data+1,
        per_page: 20
      },
      success: function(data) {
        this.state.currentUser = data.current_user;
        this.setState({
          tasks: data.tasks,
          filteredTasks: data.tasks,
          filter: 'filter_pay_item',
          levelHash: data.available_levels
       });
        $('#task_view_outer_wrapper .dimmer').dimmer('hide');
      }.bind(this),
      error: function(xhr, status, err) {
      }.bind(this)
    });
  }

  filterQueryTasks(data) {
    $('#task_view_outer_wrapper .dimmer').dimmer('show');
    $.ajax({
      url: '/tasks/filter_tasks.json',
      dataType: 'json',
      data: {
        task: this.props.data.task,
        page:  data+1,
        per_page: 20
      },
      success: function(data) {
        this.state.currentUser = data.current_user;
        this.setState({
          tasks: data.tasks,
          filteredTasks: data.tasks,
          filter: 'filter_query_tasks',
          levelHash: data.available_levels
        });
        $('#task_view_outer_wrapper .dimmer').dimmer('hide');
      }.bind(this),
      error: function(xhr, status, err) {
      }.bind(this)
    });
  }

  requestVitalTasks(data){
    $('#task_view_outer_wrapper .dimmer').dimmer('show');
    $.ajax({
      url: '/tasks/task_vital_tasks.json',
      dataType: 'json',
      data: {
        page:  data+1,
        per_page: 20
      },
      success: function(data) {
        $('#task_view_outer_wrapper .dimmer').removeClass('active');
        this.state.currentUser = data.current_user;
        this.setState({
          tasks: data.tasks,
          filteredTasks: data.tasks,
        });
        $('#task_view_outer_wrapper .dimmer').dimmer('hide');
      }.bind(this),
      error: function(xhr, status, err) {
      }.bind(this)
    });
  }

  render(){
    return(
      <div id='task_view_wrapper'>
        <TaskModal task={this.state.task} ref={'task_modal'} currentId={this.state.current_taskId} onTaskDeleted={this.onTaskDeleted.bind(this)} onTaskSelected={this.onTaskSelected}/>
        <ToolBarView
          levelHash={this.props.levelHash}
          onLevelFilter={this.handleLevelFilter.bind(this)}
          onStaffFilter={this.handleStaffFilter.bind(this)}
          onTaskRoleFilter={this.handleTaskRoleFilter.bind(this)}
          onStatSelected={this.handleSort.bind(this)}
          ref='tool_bar'/>
          <ReactPaginate previousLabel={"previous"}
            nextLabel={"next"} breakLabel={<a href="javascript:void(0)">...</a>}
            breakClassName={"break-me"}
            pageCount={this.props.total_tasks ? Math.ceil(this.props.total_tasks / 20):1}
            marginPagesDisplayed={3}
            pageRangeDisplayed={2}
            onPageChange={this.handlePageClick.bind(this)}
            containerClassName={"pagination cursr_pointr"}
            subContainerClassName={"pages pagination"}
            activeClassName={"active"}
          />
        <table className="ui celled table selectable tasks-table">
          <thead>
            <tr>
            {this.state.statSchema.map(function(stat){
              return (
                <th className={stat.className}
                  key={'vital_header_' + stat.className}
                  onClick={this.onTitleClicked.bind(this, stat)}>
                    {stat.title}
                </th>
              )
            }, this)}
            </tr>
          </thead>
          <tbody>
            {this.state.filteredTasks.map(function(task){
              return(
                <TaskRow task={task} key={'task_row_' + task.id} ref={'task_row_' + task.id}  />
              )}, this)}
          </tbody>
        </table>
      </div>
    );
  }
}

VitalTasks.childContextTypes = {
  users: React.PropTypes.array,
  reloadTask: React.PropTypes.func,
  currentId: React.PropTypes.number
}

VitalTasks.contextTypes = {
  filterQueryTasks: React.PropTypes.func,
  loadAllTasks: React.PropTypes.func
}