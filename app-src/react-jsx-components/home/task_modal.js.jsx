var React = require('react');
var ReactDOM = require('react-dom');
export class TaskModal extends React.Component {
  constructor(props, context){
    super(props, context);
    this.state = {
      task: props.task,
      extension: 'html',
      modalShown: false,
      loader: false,
      showModal:false,
      scroll: false,
      _id:'',
      reportParams: {
        created_after: moment().format("DD-MM-YYYY , H:i, defaultTime:'00:00'"),
        created_before: new Date(),
        user_id: '',
        task_id: (window.location.pathname.split('/')[2] ? window.location.pathname.split('/')[2] : null),
        generate_pdf: true
      }
    };
    this.updateTask = this.updateTask.bind(this)
    this.taskData = this.taskData.bind(this)
    this.getId = this.getId.bind(this)
    this.updateTreeView = this.updateTreeView.bind(this)
    this.selectedParentId = this.selectedParentId.bind(this)
  }

  getChildContext(){
    return ({
      onTaskSelected: function(task){
        $.ajax({
          url: 'tasks/'+ task.id + '.json',
          dataType: 'json',
          success: function(data) {
            this.setState({task: data});
          }.bind(this)
        });
      }.bind(this),
      reloadLogHourButton: function(status) {
        this.state.task.log_hours_enabled = status;
        this.forceUpdate();
      }.bind(this)
    })
  }

  componentDidMount(){
    var new_id = window.location.pathname.split('/')[2] ? window.location.pathname.split('/')[2] : null;
    this.setState({loader: true});
    if(new_id){
      $.ajax({
        url: '/tasks/'+ new_id + '.json',
        dataType: 'json',
        success: function(data) {
          this.setState({task: data,id:''});
        }.bind(this)
      });
    }
  }

  taskData(){
    var new_id = window.location.pathname.split('/')[2] ? window.location.pathname.split('/')[2] : null;
    if(new_id){
      $.ajax({
        url: '/tasks/'+ new_id + '.json',
        dataType: 'json',
        success: function(data) {
          this.setState({task: data});
        }.bind(this)
      });
    }
  }

  componentDidUpdate() {
    if(this.state.scroll==true){
      var current_task_id = this.state.task?this.state.task.id:''
      // Id of the selected task
      var new_id = window.location.pathname.split('/')[2] ? window.location.pathname.split('/')[2] : null;

      // Scroll to selected task.

      if(this.state._id != '' && !this.state.scroll) {

        var top = $(".task-id-"+ this.state._id).offset().top;
        var container = $(".task_info_col");
        container.scrollTop(
            top-100 - container.offset().top + container.scrollTop()
        );
      }else if(this.state.task != undefined && new_id != current_task_id) {

        var top = $(".task-id-"+ this.state.task.id).offset().top;
        var container = $(".task_info_col");
        container.scrollTop(
            top-30 - container.offset().top + container.scrollTop()
        );
      }
    }
  }

  deleteTask(){
    var result = confirm('Do you want to delete this task?')
    if(result == true){
      $.ajax({
        url: '/tasks/'+ this.state.task.id + '/remove_task.json',
        method: 'PUT',
        type: 'json',
        data: {
          id: this.state.task.id
        },
        success: function(){
        }.bind(this)
      });
    }
  }

  setTask(task){
    if(task.level == 1){
        this.state.task.tasks['0'].active_members = task.active_members
    }
    task['tasks'] = task.child_tasks
    this.setState({task: task, tasks: task.child_tasks ? task.child_tasks : this.state.tasks, scroll: true});
  }

  updateTask(data){
    var myParam = window.location.pathname.split('/')[2] ? window.location.pathname.split('/')[2] : null;
      $.ajax({
        url: '/tasks/'+ myParam + '.json',
        dataType: 'json',
        success: function(data) {
          this.setState({task: data})
        }.bind(this)
      });
  }

  getId(id){
    this.state._id= id,
    this.state.scroll= false
  }

  downloadURL(){
    var str = [];
    var _params = this.state.reportParams;
    _params.user_id = localStorage.getItem('_id')
    for(var p in this.state.reportParams)
      if (this.state.reportParams.hasOwnProperty(p)) {
        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(this.state.reportParams[p]));
      }
    return '/reports/budget_reports.' + this.state.extension + '?' + str.join('&');
  }

  emptyModal(){
    return (
      <div className="ui modal task-view">
        <div className="header">
        </div>
      </div>
    );
  }

  selectedParentId(id){
    if(this.state.task.level == 1){
      window.location = '/tasks/' + id + '/show_task'
    }
  }

  updateTreeView(id){
    this.state._id= id,
    this.state.scroll= false
    if(this.state.task.level != 1){
      this.taskData()
    }
  }

  taskModal(){
    var _id = null
    if(this.state.task){
      _id = this.state.task.id
    }
    return (
      <div className="ui">
        <div className="header mtop40">
          {this.state.task?this.state.task.title:''} <span className='ui blue label'>{this.state.task?this.state.task.task_code:''}</span>
          <div className='ui button green delete-task' ><a target="_blank" href={`${this.downloadURL()}`}>Budget Report</a></div>
          <DeleteTaskButton onClick={this.deleteTask.bind(this)} task={this.state.task}></DeleteTaskButton>
        </div>
        <div className='content task-modal-content'>
        {this.state.task &&
          <div className='ui two column very relaxed stackable grid task_grid'>
            <div className='column mkting_col' id='taskcard-flow'>
              <TaskCard task={this.state.task} key={'task_' + _id} updateTreeView={this.updateTreeView} selectedParentId={this.selectedParentId}/>
            </div>
              <TaskTreeView _treeId = {this.getId} enable_logButton={this.state.task} updateTask={this.updateTask} task={this.state.task} key={'task_tree_' + this.state.task.id} onTaskChanged={this.setTask.bind(this)} loadHours = {() => {this.taskData()}}/>
          </div>
        }
        </div>
      </div>
      );
  }

  render(){
    try{
      localStorage.setItem("_id", this.state.task ? this.state.task.current_user ? Object.values(this.state.task.current_user)[0] : null : null);
    }catch(err){
      console.log(err)
    }
    var Id = window.location.pathname.split('/')[2] ? window.location.pathname.split('/')[2] : null
    if(window.location.pathname ==='/tasks/' + Id + '/show_task'){
      return this.taskModal();
    }else {
     return <b></b>;
   }
  }
}

TaskModal.childContextTypes = {
  reloadLogHourButton: React.PropTypes.func,
  onTaskSelected: React.PropTypes.func
}

TaskModal.contextTypes = {loadAllTasks: React.PropTypes.func}
TaskModal.PropTypes={onTaskSelected: React.PropTypes.func}

var mountElement = document.getElementById('show-task-container');
if(mountElement != null) {
ReactDOM.render(<TaskModal/>, mountElement);
}
