var React = require('react');
var ReactDOM = require('react-dom');
var task_path;
export class TaskRow extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      task: props.task,
      loading: false,
      showModal:false
    };
  }

  loadingClass() {
    var baseClass = 'ui inline mini loader';
    if(this.state.loading == true){
      baseClass = baseClass + ' active';
    }
    return baseClass;
  }

  updateData(taskData){
    this.setState({task: taskData});
  }

  /*onTaskClicked(taskID, e){
    e.preventDefault();
    this.setState({loading: true});
    $.ajax({
      url: 'tasks/'+ taskID + '.json',
      dataType: 'json',
      success: function(data) {
        localStorage.setItem('taskData', data);
        this.props.onTaskSelected({task: data, modalShown: true});
        this.setState({loading: false});
      }.bind(this)
    });
  }*/

  render(){
    return(
      <tr id="tasks_in">
        <td ><a className="main_id" href={'/tasks/' + this.state.task.id + '/show_task'}><div></div>{this.state.task.title}</a></td>
        <td className="collapsing" data-sort-value={this.state.task.target_finish_int}>{moment(this.state.task.target_finish).format('DD/MM/YYYY')}</td>
        <td className="collapsing">{this.state.task.time_elapsed}%</td>
        <td className="collapsing">{this.state.task.task_completed}%</td>
        <td className="collapsing">{this.state.task.budget_spent_ratio}%</td>
        <td>{this.state.task.task_completed_over_budget_consumed}</td>
        <td>{this.state.task.task_completed_by_user}%</td>
        <td>{this.state.task.budget_spent_ratio_by_user}%</td>
        <td>{this.state.task.task_completed_over_budget_consumed_by_user}</td>
      </tr>
    );
  }
}