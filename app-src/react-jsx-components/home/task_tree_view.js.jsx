var React = require('react');
var date_value = moment().format("DD-MM-YYYY , hh:mm:ss");
  export class TaskTreeView extends React.Component {
    constructor(props){
      super(props);
      this.state = {
        task: props.task,
        tasks: props.task.tasks,
        task_id: (window.location.pathname.split('/')[2] ? window.location.pathname.split('/')[2] : null),
      };
      this.handleChange = this.handleChange.bind(this)
      this.clickLogHours = this.clickLogHours.bind(this)
      this.setTask = this.setTask.bind(this)
    }

    componentDidMount(){
      var new_id = window.location.pathname.split('/')[2] ? window.location.pathname.split('/')[2] : null;
      if(new_id){
      var self = this
        $('.datepicker').datetimepicker({
          format: 'd/m/Y h:i',
          startDate: new Date,
          useCurrent: false,
          closeOnDateSelect: true
        });
      }
      $('.datepicker').change(function() {
      date_value = $(this).val();
      });
      this.setState({
        date:date_value
      })
    }
    componentWillReceiveProps(newProps){
      this.setState({
        task: newProps.task
      });
    }

    handleChange(e){
      this.setState({
        date:e.target.value
      })
    }

    changeDate(e){
      date_value=e.target.value
    }

    clickLogHours(id){
      this.props._treeId(id)
      date_value = moment().format("DD-MM-YYYY , hh:mm:ss");
      this.setState({
        date: moment().format("DD-MM-YYYY , hh:mm:ss")
      })
      /*this.props.loadHours();*/
      $.ajax({
          url: '/tasks/'+ this.state.task_id + '.json',
          dataType: 'json',
          success: function(data) {
            this.setState({task: data});
            this.setTask(data);
          }.bind(this)
        });
    }

    setTask(task){
      task['tasks'] = task.tasks
      this.setState({task: task,tasks:task.tasks});
    }

    getChildContext(){
      return ({
        onTaskItemSelected: function(task){
          $.ajax({
            url: '/tasks/'+ task.id + '/task_snapshot.json',
            dataType: 'json',
            success: function(data) {
              this.setState({task: data});

              this.props.onTaskChanged(data);
            }.bind(this)
          });
        }.bind(this)
      });
    }

    showTreeView(){
      this.setState({shown: true});
    }

    getSelectedClassName(id) {
      if(id == this.state.task.id) {
        return 'selected-task';
      } else {
        return '';
      }
    }

    hierarchyTasks(task){
      if(task.children_count == 0) {
        return <TaskTreeItem date={date_value} updateTask={this.props.updateTask} key={task.id} task={task} value={task.id} className={this.getSelectedClassName(task.id)} ref="task_tree" onclick={() => {this.clickLogHours(task.id)}}/>;
      } else {
        return <TaskTreeItem date={date_value} updateTask={this.props.updateTask} key={task.id} task={task} value={task.id} className={this.getSelectedClassName(task.id)} ref="task_tree" onclick={() => {this.clickLogHours(task.id)}}>
          {
            task.tasks.map(function(subTask){
              subTask['toggled'] = false;
              if(subTask.id == this.state.task.id){
                subTask['active_members'] = this.state.task['active_members'];
                subTask['toggled'] = true;
              }
              return this.hierarchyTasks(subTask);
            }, this)
          }
        </TaskTreeItem>;
      }
    }

    taskTree(){
      return (
        <div className='column task_pad0 pad_lf6 task_info_col'>
          <div className="for_seprte fixe_date_col">
            <span className="date_view edit_view"></span>
            <span className="task_title_col">
                <span className="task_title">{this.state.tasks[0].title}</span>
                <span>{this.state.tasks[0].hours_added}</span>
            </span>
            <span className="date_view clr_both">Date:</span>
              <input id="datepicker" type="text" className="ipt_width datepicker" onChange={this.changeDate} onSelect={this.handleChange} value={date_value}/>
          </div>
          {this.hierarchyTasks(this.state.tasks[0])}
        </div>
      );
    }

    render(){
      return this.taskTree();
    }
  }

  TaskTreeView.childContextTypes = {
    onTaskItemSelected: React.PropTypes.func
  }
