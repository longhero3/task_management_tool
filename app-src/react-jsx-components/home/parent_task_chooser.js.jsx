var React = require('react');
export class ParentTaskChooser extends React.Component{
  constructor(props){
    super(props);
    this.state = {
      parentTask: this.props.parentTask,
      task: this.props.task,
      parentID: 0
    }
  }

  parentTaskName(){
    if(this.props.parentTask != null){
      return this.props.parentTask.title;
    } else {
      return '(Level 1 Task)';
    }
  }

  getText(){
    return this.state.parentID;
  }

  componentDidMount(){
    $('.task-chooser').search({
      apiSettings: {
        url: '/tasks/search_tasks?term={query}'
      },
      fields: {
        results: 'tasks',
        title: 'title',
        id: 'id'
      },
      errors: {
        noResults: ''
      },
      onSelect: function(result, response){
        this.setState({parentID: result.id});
        this.props.onParentTaskSelected();
        this.props.selectedParentId(result.id)
      }.bind(this)
    });

    $('.task-chooser .prompt').blur();
    $('.task-chooser .prompt').focusout();
  }

  render(){
    return(
      <div className='task-parent-task'>
        <div className='ui search selection task-chooser'>
          <div className='ui icon input'>
            <input className='prompt' defaultValue={this.parentTaskName()}/>
            <i className="dropdown icon" />
          </div>
        </div>
      </div>
      );
  }
}