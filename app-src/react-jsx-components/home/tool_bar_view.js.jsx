var React = require('react');

export class ToolBarView extends React.Component {

  constructor(props){
    super(props);
    this.state = {
      levelHash: this.props.levelHash
    }
  }

  componentWillReceiveProps(newProps){
    this.setState({
      levelHash: newProps.levelHash
    });
  }

  componentDidMount(){
    var tablesort;

    $(".stats-dropdown, .order-dropdown").dropdown();

    $('.task-vital-stats').on('change', function() {
      var th = $('.task-vital-stats').val();
      var direction = $('.task-order-direction').val() || 'asc';
      this.props.onStatSelected(th, direction);
    }.bind(this));

    $('.task-order-direction').on('change', function() {
      var direction, selectedStat, th;
      selectedStat = $('.task-vital-stats').val();
      th = selectedStat ? selectedStat : 'title';
      direction = $('.task-order-direction').val();
      this.props.onStatSelected(th, direction);
    }.bind(this));

    $('.branch-filter').search({
      apiSettings: {
        url: '/tasks/search_tasks?term={query}'
      },
      fields: {
        results: 'tasks',
        title: 'title',
        description: 'description',
        id: 'id'
      },
      errors: {
        noResult: 'Result not found!'
      },
      onSelect: function(result, response){
        this.context.filterBranchTasks({
          id: result.id
        });
      }.bind(this)
    });
    $('.branch-filter .prompt').on('keyup', function(e){
      if($(e.target).val() == ''){
        this.context.loadAllTasks();
      }
    }.bind(this));

    $('.pay-item').search({
      apiSettings: {
        url: '/pay_item_categories/search_pay_items?term={query}'
      },
      fields: {
        results: 'pay_items',
        title: 'name',
        id: 'id'
      },
      errors: {
        noResult: 'Result not found!'
      },
      onSelect: function(result, response){
        this.context.filterPayitem({
          name: result.name
        });
      }.bind(this)
    });
    $('.branch-filter .prompt').on('keyup', function(e){
      if($(e.target).val() == ''){
        this.context.loadAllTasks();
      }
    }.bind(this));
  }

  componentDidUpdate(){
    $('.task-filter-dropdown').dropdown({
      onChange: function(value){
        this.props.onLevelFilter(value);
      }.bind(this)
    })

    $('.staff-filter-dropdown').dropdown({
      onChange: function(value){
        this.props.onStaffFilter(value);
      }.bind(this)
    })

    $('.task-role-filter-dropdown').dropdown({
      onChange: function(value){
        this.props.onTaskRoleFilter(value);
      }.bind(this)
    })
  }

  render(){
    return (
      <div className='ui stackable grid'>
        <div className= 'four wide column'>
          <div className="ui selection dropdown stats-dropdown fluid">
            <div className="default text">Vital stats</div>
            <input type="hidden" name="selected-vital-stat" className="ui task-vital-stats" />
            <i className="dropdown icon" />
            <div className="menu">
              <div className="item" data-value="target_finish_int">Due Date</div>
              <div className="item" data-value="time_elapsed">% Time Elapsed</div>
              <div className="item" data-value="task_completed">% Task Competed</div>
              <div className="item" data-value="budget_spent_ratio">% Budget Spent</div>
              <div className="item" data-value="task_completed_over_budget_consumed">% Task Completed : % Budget Spent</div>
              <div className="item" data-value="task_completed_by_user">% Task Competed by Me</div>
              <div className="item" data-value="budget_spent_by_user">% Budget Spent by Me</div>
            </div>
          </div>
        </div>

        <div className= 'three wide column'>
          <div className="ui selection dropdown order-dropdown fluid">
            <div className="default text">Order</div>
            <input type="hidden" name="selected-vital-order" className="task-order-direction" />
            <i className="dropdown icon" />
            <div className="menu">
              <div className="item" data-value="asc">Ascending</div>
              <div className="item" data-value="desc">Descending</div>
            </div>
          </div>
        </div>

        <div className= 'three wide column'>
          <div className="ui selection dropdown task-filter-dropdown fluid">
            <div className="default text">Filter by Level</div>
            <input type="hidden" name="selected-vital-order" className="selected-task-level" />
            <i className="dropdown icon" />
            <div className="menu">
              {this.props.levelHash.map(function(level){
                return <div className="item" data-value={level.value} key={'level_' + level.value}>{level.key}</div>
              }, this)}
            </div>
          </div>
        </div>

        <div className= 'three wide column'>
          <div className="ui selection search dropdown staff-filter-dropdown fluid">
            <i className="search icon" />
            <div className="default text">Filter by Staff...</div>
            <input type="hidden" name="selected-staff" className="selected-staff" />
            <div className="menu">
              <div className="item" data-value={0} key={'user_0'}>All Staff</div>
              {this.context.users.map(function(user){
                return <div className="item" data-value={user.id} key={'user_' + user.id}>{user.full_name}</div>
              }, this)}
            </div>
          </div>
        </div>

        <div className= 'three wide column'>
          <div className="ui selection dropdown task-role-filter-dropdown fluid">
            <div className="default text">Filter by Role</div>
            <input type="hidden" name="selected-task-role" className="selected-task-role" />
            <i className="dropdown icon" />
            <div className="menu">
                <div className="item" data-value="">Any</div>
                <div className="item" data-value="task_owner">TM</div>
                <div className="item" data-value="escalation_manager">EM</div>
                <div className="item" data-value="member">Member</div>
            </div>
          </div>
        </div>

        <div className= 'sixteen wide column'>
          <div className='ui fluid branch-row-filters'>
            <div className='ui fuild category search selection branch-filter'>
              <div className='ui icon input'>
                <input className='prompt' placeholder='Filter by Branch...'/>
                <i className="search icon" />
              </div>
            </div>
          </div>
        </div>

        <div className= 'four wide column'>
          <div className='ui fluid branch-row-filters'>
            <div className='ui fuild category search selection pay-item'>
              <div className='ui icon input'>
                <input className='prompt' placeholder='Filter by Pay item...'/>
                <i className="search icon" />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

ToolBarView.contextTypes = {
  users: React.PropTypes.array,
  filterBranchTasks: React.PropTypes.func,
  filterPayitem: React.PropTypes.func,
  loadAllTasks: React.PropTypes.func
}
