var React = require('react');
import { render } from 'react-dom'
import { Router, Route, Link, browserHistory, Redirect, IndexRoute, IndexRedirect } from 'react-router'

export class ReportApp extends React.Component {
  render(){
    return(
      <main className="app__main">
        {this.props.children}
      </main>
    );
  }
}

export class ReportMainView extends React.Component {
  render(){
    return(
      <div className='ui segment report_outer_wrapper'>
        <h1>Report List</h1>
        <table className="ui celled table report_list">
          <thead>
            <tr>
              <th>Name</th>
              <th>Description</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td><Link to="/reports/staff_hours_view">Staff Hours Report</Link></td>
              <td>Report containing all staff and the hours they have logged.</td>
            </tr>
            <tr>
              <td><Link to="/reports/budget_reports_view">Budget Report</Link></td>
              <td>Report containing a breakdown of spending within a task.</td>
            </tr>
          </tbody>
        </table>
      </div>
      );
  }
}

var mountElement = document.getElementById('reports_container');
if(mountElement != null) {
  render((
    <Router history={browserHistory}>
      <Route path="/reports" component={ReportApp}>
        <IndexRoute component={ReportMainView}/>
        <Route path="staff_hours_view" component={StaffHoursReport}></Route>
        <Route path="budget_reports_view" component={BudgetReport}></Route>
      </Route>
    </Router>
  ), mountElement)
}
