var React = require('react');

export class ReportFilter extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      report: {
        created_after: '',
        created_before: '',
        user_id: '',
        task_id: '',
        generate_pdf: true,
        pay:''
      }
    }
  }

  componentDidMount(){
    var self = this
    $('.datetimepicker').datetimepicker({
      format: 'd/m/Y H:i',
      closeOnDateSelect: true,
      defaultTime:'00:00'
    });
    $('.start-date-filter').on('change', function(e){
      this.handleChange('created_after', $(e.target).val());
    }.bind(this));

    $('.end-date-filter').on('change', function(e){
      this.handleChange('created_before', $(e.target).val());
    }.bind(this));

    $('.user-chooser').search({
      apiSettings: {
        url: '/users/search_users?term={query}'
      },
      fields: {
        results: 'users',
        title: 'full_name',
        id: 'id'
      },
      errors: {
        noResults: ''
      },
      onSelect: function(result, response){
        this.handleChange('user_id', result.id);
      }.bind(this)
    });

    $('.pay-item').search({
      apiSettings: {
        url: '/pay_item_categories/search_pay_items?term={query}'
      },
      fields: {
        results: 'pay_items',
        title: 'name',
        id: 'id'
      },
      errors: {
        noResult: 'Result not found!'
      },
      onSelect: function(result, response){
        this.handleChange('pay', result.name);
      }.bind(this)
    });

    $('.task-chooser').search({
      type: 'special',
      templates: {
        special: function(response){
          return self.customTemplateForArchiveTask(response);
        }
      },
      apiSettings: {
        url: '/tasks/search_tasks?term={query}'
      },
      fields: {
        results: 'tasks',
        title: 'title',
        id: 'id'
      },
      errors: {
        noResults: ''
      },
      onSelect: function(result, response){
        this.handleChange('task_id', result.id);
      }.bind(this)
    });

    $('.task-chooser .prompt').blur();
    $('.task-chooser .prompt').focusout();

    $('.task-chooser .prompt').on('keyup', function(e){
      if($(e.target).val() == ''){
        this.handleChange('task_id', '');
      }
    }.bind(this));
  }

  handleChange(name, value){
    this.state.report[name] = value;
    this.props.onFilterChanged(this.state.report);
  }

  customTemplateForArchiveTask(response){
    var _archieve
    var _tasks = []
    response.tasks.map((task,index)=>{
      if(task.status == "removed"){
        _archieve = "Archive Task"
      }else{
        _archieve = ""
      }
      _tasks.push(('<a class="result"><div class="content"><div class="title">' + task.title + '</div><div class="description">' + task.description + '</div><i>'+_archieve+'</i></div></div></a>'))
    })
    return _tasks
  }

  render(){
    return (
      <div className='ui grid segment'>
        <div className='row'>
          <div className='four wide column'>
            <div className='ui fluid input'>
              <input className='datetimepicker start-date-filter'
              ref='created_after'
              placeholder='From ...'
              />
              {this.props.validate_from}
            </div>
          </div>

          <div className='four wide column'>
            <div className='ui fluid input'>
              <input className='datetimepicker end-date-filter'
              ref='created_before'
              placeholder='To ...'
              />
              {this.props.validate_to}
            </div>
          </div>

          <div className="four wide column">
            <ReportExtensionComponent onExtensionSelected={(val)=>{this.props.onExtensionSelected(val)}}/>
          </div>
        </div>

        <div className='row'>
          <div className='four wide column'>
            <div className='ui search user-chooser'>
              <div className='ui fluid icon input'>
                <input className='prompt'
                ref='user_id'
                placeholder='User ...'
                />
                {this.props.validate_user}
                <i className="user icon"></i>
              </div>
            </div>
          </div>

          <div className='four wide column'>
            <div className='task-parent-task'>
              <div className='ui search selection task-chooser'>
                <div className='ui icon input fluid'>
                  <input className='prompt' placeholder='Task ...' ref='task_id' />
                  {this.props.validate_task}
                  <i className="dropdown icon" />
                </div>
              </div>
            </div>
          </div>

          <div className= 'four wide column'>
            <div className='ui fluid branch-row-filters'>
              <div className='ui fuild category search selection pay-item'>
                <div className='ui icon input fluid'>
                  <input className='prompt' placeholder='Filter by Pay item...' ref='pay'/>
                  <i className="search icon" />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      );
  }
}
