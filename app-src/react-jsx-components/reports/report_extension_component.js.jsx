var React = require('react');
import { Router, Route, Link, browserHistory } from 'react-router'

export class ReportExtensionComponent extends React.Component {

  componentDidMount(){
    $('.extension_dropdown').dropdown({
      onChange: function(value){
        this.props.onExtensionSelected(value);
      }.bind(this)
    })
  }

  render(){
    return(
      <div className='report_extension'>
        <div className="ui selection dropdown extension_dropdown fluid">
          <div className="default text">PDF</div>
          <input type="hidden" name="selected-task-role" className="selected-extension" />
          <i className="dropdown icon" />
          <div className="menu" ref ="extension">
            <div className="item" data-value="pdf">PDF</div>
            <div className="item" data-value="csv">CSV</div>
          </div>
        </div>
      </div>
      );
  }
}