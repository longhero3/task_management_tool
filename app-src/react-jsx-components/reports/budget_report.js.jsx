var React = require('react');

import { Router, Route, Link, browserHistory } from 'react-router'

export class BudgetReport extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      reportParams: {},
      extension: 'pdf',
      validateFrom:'',
      validateTo:'',
      validateTask:''
    }
  }

  handleFilter(data){
    Object.assign(this.state.reportParams, data);
    this.forceUpdate();
  }

  handleSelectedExtension(value){
    this.setState({extension: value});
  }

  resetFilters(){
    $('.start-date-filter').val('');
    $('.end-date-filter').val('');
    $('.user-chooser .prompt').val('');
    $('.selection .prompt').val('');
    $('.extension_dropdown').dropdown('restore defaults');

    this.handleFilter({
      created_after: '',
      created_before: '',
      user_id: '',
      task_id: ''
    });
  }

  downloadURL(){
    var str = [];
    for(var p in this.state.reportParams)
      if (this.state.reportParams.hasOwnProperty(p)) {
        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(this.state.reportParams[p]));
      }
    return '/reports/budget_reports.' + this.state.extension + '?' + str.join('&');
  }

  validate(e){
    var reportDetails=this.state.reportParams;
    if( reportDetails.created_after === undefined || reportDetails.created_before === undefined || reportDetails.task_id === undefined || reportDetails.created_after === '' || reportDetails.created_before === '' || reportDetails.task_id === '' ){
      e.preventDefault();
      if(reportDetails.created_after === undefined || reportDetails.created_after === ''){
        this.setState({
          validateFrom:<p className="ui basic red pointing prompt label transition visible fix_inpt">input cannot be blank</p>
        })
      }
      else if(reportDetails.created_before === undefined || reportDetails.created_before === '' ){
        this.setState({
          validateTo:<p className="ui basic red pointing prompt label transition visible fix_inpt">input cannot be blank</p>,
          validateFrom:''
        })
      }
      else{
        this.setState({
          validateTask:<p className="ui basic red pointing prompt label transition visible fix_inpt">input cannot be blank</p>,
          validateFrom:'',
          validateTo:''
        })
      }
      return false;
    }
    else{
      this.setState({
          validateFrom:'',
          validateTask:'',
          validateTo:''
      })
      return true;
    }
  }


  render(){
    return(
      <div className='budget_reports'>
        <h1>Budget Report</h1>
        <div className='right floated right'>
        Please ensure you select a task for the budget report.
        </div>
        <ReportFilter onFilterChanged={this.handleFilter.bind(this)} onExtensionSelected={(val)=>{this.handleSelectedExtension(val)}} ref='extension' validate_from={this.state.validateFrom} validate_to={this.state.validateTo} validate_task={this.state.validateTask}/>
        <div className="ui grid segment">
          <div className='two wide column'>
            <button onClick={this.resetFilters.bind(this)} className='ui fluid button green'>Reset</button>
          </div>
          <div className="four six wide column">
            <a target="_blank" href={`${this.downloadURL()}`} onClick={this.validate.bind(this)} className="ui button default fluid">Generate</a>
          </div>
        </div>
      </div>
    );
  }
}