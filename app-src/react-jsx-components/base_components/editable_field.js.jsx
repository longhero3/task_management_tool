var React = require('react');
var InlineEdit = require('react-edit-inline');
export class EditableField extends React.Component{
  constructor(props){
    super(props);
    var result = true;
    var validator = this.defaultValidator;
    if(this.props.enabled != null) {
      result = this.props.enabled;
    }

    if(this.props.validate != null) {
      validator = this.props.validate;
    }
    this.state = {
      text: this.props.text,
      className: this.props.className || '',
      enabled: result,
      validate: validator
    }
  }

  defaultValidator(text){
    return (text.length > 0 && text.length < 255);
  }

  onChange(params){
    this.state.text = params.changedText;
    this.props.onSave();
  }

  reset(){
    this.setState({text: this.props.text});
  }

  getText(){
    return this.state.text;
  }

  render(){
    if(this.state.enabled == true){
      return (
        <InlineEdit
          className={'ui input ' + this.state.className}
          text={this.state.text}
          paramName="changedText"
          change={this.onChange.bind(this)}
          validate={this.state.validate}
        />
      );
    } else {
      return (
        <label>{this.state.text}</label>
      );
    }
  }
}
