var React = require('react');
export class EditableTextArea extends React.Component{
  constructor(props){
    super(props);
    this.state = {
      text: this.props.text,
      editing: false,
      className: this.props.className
    }
  }

  componentDidUpdate(){
    if(this.state.editing == true){
      $('.'+ this.state.className).focus();
    }
  }

  enableEditing(e){
    e.preventDefault();
    this.setState({editing: true});
  }

  disableEditing(){
    this.setState({editing: false});
  }

  onCancel() {
    this.setState({
      editing: false,
      text: this.props.text
    });
  }

  onKeyUp(event){
    this.state.text = event.target.value;
    if(event.keyCode == 13 && event.shiftKey == true){
      this.props.onSave();
      this.setState({editing: false});
    }
  }

  getText(){
    return this.state.text;
  }

  labelView(){
    return (<label className='task-description-label' onClick={this.enableEditing.bind(this)}>
    {this.state.text}
    </label>);
  }

  editView(){
    return (
      <div className='ui form'>
        <div className='field'>
          <textarea type='text'
            onKeyUp={this.onKeyUp.bind(this)}
            defaultValue={this.state.text}
            className={this.state.className}
            onBlur={this.onCancel.bind(this)}>
          </textarea>
        </div>
      </div>
      );
  }

  render(){
    if(this.state.editing){
      return this.editView();
    } else {
      return this.labelView();
    }
  }
}