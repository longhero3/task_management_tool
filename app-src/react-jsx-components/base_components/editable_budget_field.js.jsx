var React = require('react');
export class EditableBudgetField extends React.Component { 
  constructor(props){
    super(props);
    this.state = {
      text: this.props.text,
      editable: false
    }
  }

  onChange(event){
    if(event.keyCode == 13){
      this.state.text = event.target.value;
      this.props.onBudgetSave(this.state.text);
      this.setState({
        editable: false
      })
    }  
  }

  onBlur(event){
    this.state.text = event.target.value;
      this.props.onBudgetSave(this.state.text);
      this.setState({
        editable: false
      })
  }

  onclick(){
    this.setState({
      editable: true
    })
  }

  render(){
    if(this.state.editable)
      return <input name="budgetCost" type="text" defaultValue={this.props.text} onKeyUp={this.onChange.bind(this)} onBlur={this.onBlur.bind(this)}/>
    else
      return <span onClick={this.onclick.bind(this)}>{this.props.text}</span>
  }
}
