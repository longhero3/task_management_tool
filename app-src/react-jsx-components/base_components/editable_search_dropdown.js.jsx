var React = require('react');
export class EditableSearchDropdown extends React.Component{
  constructor(props){
    super(props);
    this.state = {
      selectedText: this.props.text,
      options: this.props.options,
      keyName: this.props.keyName,
      valueName: this.props.valueName,
      className: this.props.className,
      editing: false,
      selectedValue: '',
      keepUnchanged: this.props.keepUnchanged || false
    }
  }

  componentDidUpdate(){
    if(this.props.options != []){
      $('.' + this.state.className).dropdown({
        onChange: function(value, text){
          this.onChange(value, text);
        }.bind(this),

        onHide: function(){
          this.setState({editing: false});
        }.bind(this)
      });
    }

    if(this.state.editing == true) {
      $('.' + this.state.className + ' .dropdown.icon').trigger('click');
    }
  }

  enableEditing(){
    this.setState({editing: true});
  }

  disableEditing(){
    this.setState({editing: false});
  }

  onCancel() {
    this.setState(this.props);
  }

  onChange(value, text){
    this.setState({
      selectedText: text,
      selectedValue: value
    });
    this.disableEditing();
    this.props.onSave();
  }

  getSelectedText(){
    return this.state.selectedText;
  }

  getSelectedValue(){
    return this.state.selectedValue;
  }

  labelView(){
    if(this.state.keepUnchanged == true){
      return (<label onClick={this.enableEditing.bind(this)}>{this.props.text}</label>);
    } else {
      return (<label onClick={this.enableEditing.bind(this)}>{this.state.selectedText}</label>);
    }
  }

  editView(){
    return (
      <div className={"ui fluid search selection dropdown " + this.state.className}>
        <input type="hidden" name="selected-option" value={this.state.selectedValue} onChange={this.onChange.bind(this)}/>
        <i className="dropdown icon" />
        <div className="default text">{this.state.selectedText}</div>
        <div className="menu">
          {this.props.options.map(function(option){
            return (<div className='item' key={option[this.state.valueName]} data-value={option[this.state.valueName]}>{option[this.state.keyName]}</div>);
          }.bind(this))}
        </div>
      </div>
    );
  }

  render(){
    if(this.state.editing){
      return this.editView();
    } else {
      return this.labelView();
    }
  }
}