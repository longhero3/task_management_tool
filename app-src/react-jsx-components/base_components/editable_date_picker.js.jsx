var React = require('react');
var moment = require('moment');
var DatePicker = require('react-datetime');
export class EditableDatePicker extends React.Component{
  constructor(props){
    super(props);
    this.state = {
      date: moment(this.props.text, 'DD/MM/YYYY h:mm A'),
      editing: false,
      className: this.props.className
    }
  }

  componentDidUpdate(){
    if(this.state.editing == true) {
      $('.'+ this.state.className.replace(' ', '.') + ' input').focus();
      $('.'+ this.state.className.replace(' ', '.') + ' input').on('blur', function(e){
        this.onChange(moment($(e.target).val(), 'DD/MM/YYYY h:mm A'));
      }.bind(this));
    }
  }

  enableEditing(){
    this.setState({editing: true});
  }

  disableEditing(text){
    this.setState({editing: false});
  }

  reset(){
    this.setState({
      date: moment(this.props.text, 'DD/MM/YYYY h:mm A')
    })
  }

  onChange(date){
    this.state.date = date;
    this.setState({date: date, editing: false});
    this.props.onSave();
  }

  getText(){
    var self = this;
    return self.state.date.format('llll');
  }

  labelView(){
    var self = this;
    return (<div onClick={this.enableEditing.bind(this)} className={'inline ' + this.state.className}>
    {self.state.date.format('DD/MM/YYYY h:mm A')}
    </div>);
  }

  editView(){
    return (
      <div className="datepicker-wrapper">
        <DatePicker value={this.state.date}
          onChange={this.onChange.bind(this)}
          dateFormat="DD/MM/YYYY"
          timeFormat="h:mm A"
          open={true}
          className={this.state.className}
        />
      </div>
    );
  }

  render(){
    if(this.state.editing){
      return this.editView();
    } else {
      return this.labelView();
    }
  }
}