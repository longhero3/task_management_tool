require 'clockwork'
require './config/boot'
require './config/environment'
module Clockwork

  handler do |job|
    puts "Running #{job}"
  end

  every(1.day, 'xero.sync_employees', :at => '03:30', tz: 'Melbourne') do
    Resque.enqueue(SyncEmployeesJob)
  end

  every(1.day, 'xero.sync_pay_items', :at => '04:00', tz: 'Melbourne') do
    Resque.enqueue(SyncPayItemsJob)
  end

  every(1.week, 'xero.create_pay_run', :at => 'Sunday 23:00', tz: 'Melbourne') do
    Resque.enqueue(CreatePayRunJob)
  end

  every(1.days, 'xero.sync_payroll_calendar', :at => '00:00', tz: 'Melbourne') do
    Resque.enqueue(SyncPayrollCalendarJob)
  end

  every(1.day, 'xero.create_timesheet_job', :at => '23:59', tz: 'Melbourne') do
    Resque.enqueue(CreateTimesheetJob)
  end

  every(1.day, 'xero.send_timesheet_job', :at => '0:15', tz: 'Melbourne') do
    Resque.enqueue(SendTimesheetJob)
  end

  every(1.day, 'budget_reports.daily', :at => '00:00', tz: 'Melbourne') do
    Task.send_budget_reports :daily
  end

  every(1.week, 'budget_reports.fortnightly', :at => ['Monday 0:00', 'Thursday 3:00'], tz: 'Melbourne') do
    pay_cycle_start = PayrollCalendar.latest_fortnight

    if (pay_cycle_start + 14.days).today? || (pay_cycle_start + 17.days).today?
      puts "Sending fortnightly budget reports"
      Task.send_budget_reports :fortnightly
    end
  end

  every(1.day, 'budget_reports.weekly', :at => '00:00', tz: 'Melbourne') do
    # every(1.week, 'budget_reports.weekly', :at => ['Monday 0:00'], tz: 'Melbourne') do
    Task.send_budget_reports :weekly
  end

  # commenting as it is called in schedular.rake

  # every(1.day, 'staff_hours_report.daily', :at => '00:00', tz: 'Melbourne') do
  #   Task.send_staff_hours_reports :daily
  # end

  every(1.day, 'run_reminder_callbacks.daily', :at => '03:00', tz: 'Melbourne') do
    RunReminderCallbacksJob.perform_later
  end
end
