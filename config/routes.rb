require 'resque_web'

resque_web_constraint = lambda do |request|
  current_user = request.env['warden'].user
  current_user.present? && current_user.admin?
end
Rails.application.routes.draw do
  use_doorkeeper
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'

  constraints resque_web_constraint do
    mount ResqueWeb::Engine => '/resque_web'
  end

  namespace :api do
    namespace :v1 do
      resources :users, only: [:index]
      resources :sprints, only: [:index, :create]
      resources :backlogs, only: [:index]
      resources :pay_items, only: [:index]
      resources :tasks, except: [:new, :edit, :destroy] do
        member do
          get  'work_periods' => :work_periods, :as => 'work_periods'
          post 'work_periods' => :log_hours, :as => 'log_hours'
          get :children
        end
      end
      get "tasks/:id/comments" => "tasks#comments"
      post "tasks/:id/comments" => "tasks#post_comment"
    end
  end

  resources :cost_types
  resources :costs
  resources :wages
  resources :users, only: [] do
    collection do
      get :search_users
    end
  end
  resources :work_periods do
    collection do
      get :filter_by
      get :check_budget
      post :send_exceeded_log_hours_notification
    end
  end
  resources :high_schools, only: [:index]
  resources :removed_tasks, only: [:index]
  resources :addresses do
    collection do
      get :find_place
    end
  end
  resources :tasks do
    member do
      get :show_task
    end
    collection do
      get :task_vital_tasks
      get :task_pay_item
      get :filter_tasks
      get :search_tasks
      get :check_task_budget
    end

    member do
      get :remaining_budget
      get :task_summary
      get :task_snapshot
      put :check_task_date_range
      match :check_budget, via: [:get, :put]
      put :remove_task
      put :recover_task
    end
  end
  resources :task_users do
    collection do
      put :switch_role
    end
  end

  resources :pay_item_categories, only: [] do
    collection do
      get :search_pay_items
    end
  end

  namespace :reports do
    resource :staff_hours, only: [:show]
    resource :budget_reports, only: [:show]
  end

  get 'home/index'
  get 'home/user_list'
  get 'home/pay_items'

  # reporting
  get 'reports' => :index, controller: :reports
  get 'reports/staff_hours_view' => :index, controller: :reports
  get 'reports/budget_reports_view' => :index, controller: :reports

  # timesheet
  get 'timesheet/current_timesheet', as: :current_timesheet
  post 'timesheet/send_timesheet', as: :send_timesheet

  #xero authentication
  get 'xero_session/new' => 'xero_session#new', as: :xero_new_session

  devise_for :users,
             controllers: {
                 registrations: 'users/registrations',
                 sessions: 'users/sessions',
                 passwords: 'users/passwords'
             }
  root 'home#index'
  get 'task/:id' => 'tasks#main', as: :task_id
end
