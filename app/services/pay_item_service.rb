# PayItemService
class PayItemService
  def initialize(id:)
    @task = PayItem.find(id)
  end

  def self.fetch_all
    PayItem.all
  end
end
