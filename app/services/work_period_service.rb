class WorkPeriodService
  def initialize(id:)
    @work_period = WorkPeriod.find(id)
    @task =	@work_period.task
  end

  def self.create(task_id:, user_id:, params: {})
    result, error = false, nil
    @work_period = WorkPeriod.new(params.merge(task_id: task_id, 
                                   user_id: user_id))
    if @work_period.date_worked &&
       !PayrollCalendar.log_hour_date_disabled(@work_period.date_worked)
      if @work_period.save
        result = @work_period
      else
        error = @work_period.errors.full_messages.join(', ')
      end
    else
      error = 'You cannot backlog hours to the previous pay period.'
    end
    [result, error]
  end
end
