# DateTimeService
class DateTimeService
  def self.formated_timestamp(time_stamp)
    Time.zone.at(time_stamp.to_i)
  end
end
