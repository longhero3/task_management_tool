# UserService
class UserService
  def initialize(id:)
    @task = User.find(id)
  end

  def self.fetch_all
    User.all
  end
end
