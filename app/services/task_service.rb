# TaskService
class TaskService
  def initialize(id:)
    @task = Task.find(id)
  end

  class << self
    def search(params = {})
      statuses = params[:status].class == 'Array' ?
                  params[:status] :
                  params[:status].split(',') if params[:status]
      priorities = params[:priority].class == 'Array' ?
                    params[:priority] :
                    params[:priority].split(',') if params[:priority]
      assignees = params[:assignee].class == 'Array' ?
                    [params[:assignee]] :
                    params[:assignee].split(',') if params[:assignee]

      tasks = Task.all
      tasks = tasks.where(status: statuses) if params[:status]
      tasks = tasks.where(status: priorities) if params[:priority]
      # TODO: FILTER ON PROJECT
      # tasks = tasks.where(status: projects) if params[:project]
      tasks = tasks.includes(task_users: :user).where("task_users.task_role = ?
        AND (LOWER(users.first_name) || ' ' || LOWER(users.last_name))
        IN (?)", TaskUser::TASK_ROLES[:member], assignees
        ).references(:task_users, :users) if params[:assignee]
      tasks
    end

    def create(user_id:, options: {})
      set_task_params(user_id, options)
      @task = Task.new(@task_params)
      @task.actual_cost = calc_actual(@task)
      if @task.save
        create_task_owner(user_id)
        create_task_escalation_manager(options[:escalation_manager])
      else
        @error = @task.errors.full_messages.join(', ')
      end
      [@task, @error]
    end

    private

    def create_task_owner(user_id)
      @task.task_users.create(
        user_id: user_id, isOwner: true,
        task_role: TaskUser::TASK_ROLES[:task_owner]
      )
    end

    def create_task_escalation_manager(user_id)
      @task.task_users.create(
        user_id: user_id,
        task_role: TaskUser::TASK_ROLES[:escalation_manager]
      )
    end

    def calc_actual(task)
      0 if task.children.count.zero?
    end

    def set_task_params(owner_id, options)
      @task_params = options.merge(user_ids: taks_members_ids(
        owner_id, options[:escalation_manager], options[:user_ids]
      ))
      chnage_task_params_time_format
      @task_params.delete(:escalation_manager)
      @task_params.to_h
    end

    def chnage_task_params_time_format
      @task_params[:target_start] = DateTimeService.formated_timestamp(
        @task_params[:target_start]
      )
      @task_params[:target_finish] = DateTimeService.formated_timestamp(
        @task_params[:target_finish]
      )
    end

    def taks_members_ids(owner_id, esc_manager_id, user_ids = [])
      member_ids = user_ids.reject do |i|
        i.empty? || i.to_i == owner_id || i.to_i == esc_manager_id
      end if user_ids.present?
      member_ids ||= []
      member_ids
    end
  end
end
