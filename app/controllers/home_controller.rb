class HomeController < ApplicationController
  before_action :authenticate_user!

  def index
    @tasks = Task.all
    @user = User.all
    @work_periods = WorkPeriod.all
    @task = Task.new
  end
end

def new
  @task = Task.new
end

def user_list
end

def create
  @task = Task.new(task_params)

  respond_to do |format|
    if @task.save
      format.html { redirect_to @task, notice: 'Task was successfully created.' }
      format.json { render :show, status: :created, location: @task }
    else
      format.html { render :new }
      format.json { render json: @task.errors, status: :unprocessable_entity }
    end
  end
end
