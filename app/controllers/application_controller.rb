
class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_action :configure_permitted_parameters, if: :devise_controller?
  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:first_name,:last_name,:date_of_birth,:pay_rate, :email, :password])
    devise_parameter_sanitizer.permit(:account_update, keys: [:first_name,:last_name,:date_of_birth,:pay_rate,:email, :password, :current_password])
    devise_parameter_sanitizer.permit(:sign_in, keys: [:first_name,:last_name,:date_of_birth,:pay_rate, :email])
  end

end





def current_user
  @current_user ||= User.find(session[:user_id]) if session[:user_id]
end