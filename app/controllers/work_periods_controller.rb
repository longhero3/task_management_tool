class WorkPeriodsController < ApplicationController
  before_action :authenticate_user!

  before_action :set_work_period, only: [:show, :edit, :update, :destroy]

  # GET /work_periods
  # GET /work_periods.json
  def index
    # or, use an explicit "per page" limit:

    @work_periods = WorkPeriod.all
    @work_periods = @work_periods.paginate(page: params[:page] || 1, per_page: params[:page] || 10)
  end

  def filter_by
    @work_periods = current_user.work_periods.order(created_at: :desc)

    unless work_period_params[:user_id].empty?
      user = User.find(work_period_params[:user_id])
      @work_periods = user.work_periods.order(created_at: :desc)
    end

    unless work_period_params[:start_date].empty?
      start_date = DateTime.parse(work_period_params[:start_date])
      @work_periods = @work_periods.where('work_periods.created_at >= ?', start_date)
    end

    unless work_period_params[:end_date].empty?
      end_date = DateTime.parse(work_period_params[:end_date])
      @work_periods = @work_periods.where('work_periods.created_at <= ?', end_date)
    end
    @work_periods = @work_periods.paginate(page: params[:page] || 1, per_page: params[:per_page] || 10)
    @total_count = @work_periods.count
  end

  # GET /work_periods/1
  # GET /work_periods/1.json
  def show
  end

  # GET /work_periods/new
  def new
    @work_period = WorkPeriod.new
  end

  # GET /work_periods/1/edit
  def edit
  end

  # POST /work_periods
  # POST /work_periods.json
  def create
    params[:work_period][:user_id] = current_user.id
    @task = Task.find(params[:work_period][:task_id])
    params[:work_period][:pay_rate] = @task.actual_pay_rate current_user
    hours_convert_time_to_decimal
    @work_period = WorkPeriod.new(work_period_params)
    respond_to do |format|
      if !PayrollCalendar.log_hour_date_disabled(@work_period.date_worked)
        if @work_period.save
          format.html { redirect_to "#{root_url}#task_id=#{@task.id}", notice: 'Work period was successfully created.' }
          #format.json { render :show, status: :created, location: @work_period }
          format.json { render json: {msg: 'Work period was successfully created.'}, status: :created }
        else
          format.html { redirect_to root_url, alert: @work_period.errors.full_messages.join(', ') }
          format.json { render json: @work_period.errors, status: :unprocessable_entity }
        end
      else
        format.html { redirect_to root_url, alert: 'You cannot backlog hours to the previous pay period.' }
        format.json { render json: {msg: 'You cannot backlog hours to the previous pay period.'}, status: :unprocessable_entity }
      end
    end
  end

  # GET /work_periods/check_budget
  # GET /work_periods/check_budget.json
  def check_budget
    task = Task.find(work_period_params[:task_id])
    pay_rate = task.try(:pay_item).try(:pay_rate).nil? ? "0.0".to_d : task.try(:pay_item).try(:pay_rate).to_d
    exceeding_amt = task.actual_cost + (work_period_params[:amount_hours].to_d * pay_rate) - task.budgeted_cost
    if exceeding_amt <= 0
      render json: {msg: "You can log these hours", code: "LOG-200"}, status: :ok
    else
      render json: {msg: "You log hours exceeding the budgeted cost so you cannot log these hours", code: "LOG-400"}, status: :ok
    end
  end

  def send_exceeded_log_hours_notification
    task = Task.find(params[:task_id])
    exceeding_amt = task.actual_cost + (params[:amount_hours].to_d * task.try(:pay_item).try(:pay_rate)) - task.budgeted_cost
    LogHoursMailer.exceeded_log_hours_notification(task, current_user, exceeding_amt).deliver_later
    render json: {msg: "Message Sent", redirect_location: "#{root_url}#task_id=#{task.id}"}, status: :ok
  end

  # PATCH/PUT /work_periods/1
  # PATCH/PUT /work_periods/1.json
  def update
    respond_to do |format|
      if @work_period.update(work_period_params)
        if current_user.admin? && @work_period.user != current_user
          WorkPeriodMailer.authorised_edit(current_user, @work_period).deliver_later
        end

        format.html { redirect_to @work_period, notice: 'Work period was successfully updated.' }
        format.json { render json: {}, status: :ok, location: @work_period }
      else
        format.html { render :edit }
        format.json { render json: @work_period.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /work_periods/1
  # DELETE /work_periods/1.json
  def destroy
    if current_user.admin? && @work_period.user != current_user
      WorkPeriodMailer.authorised_destroy(current_user, @work_period).deliver_later
    end

    @work_period.destroy
    respond_to do |format|
      format.html { redirect_to work_periods_url, notice: 'Work period was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_work_period
      @work_period = WorkPeriod.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def work_period_params
      params.require(:work_period).permit(:amount_hours, :date_worked, :start_time, :end_time, :description, :task_id, :pay_rate, :user_id, :progress, :start_date, :end_date)
    end

    # need to convert returned value from time input field into an hours decimal format
    # e.g. 1hr30min to 1.5 hr
    def hours_convert_time_to_decimal
      input = params[:work_period][:amount_hours].split(":")
      params[:work_period][:amount_hours] = input[0].to_f + (input[1].to_f / 60)
    end
end
