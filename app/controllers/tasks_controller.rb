class TasksController < ApplicationController
  before_action :authenticate_user!
  before_action :set_task, only: [:show, :edit, :update, :destroy,
                                  :task_summary, :task_snapshot,
                                  :check_task_date_range,
                                  :check_budget, :check_task_budget,
                                  :remove_task, :recover_task,
                                  :remaining_budget, :show_task]

  def main
    @task = Task.find_by_id(params[:id])
    @today = Time.now.strftime('%Y-%m-%d')
    @work_period = WorkPeriod.new
  end

  def task_vital_tasks
    if params[:id]
      selected_task = Task.find(params[:id])
      @tasks = selected_task.self_and_descendants.created
    else
      @tasks = Task.created.where(depth: 0)
    end
    @tasks = @tasks.paginate(page: params[:page] || 1, per_page: params[:per_page] || 20)
    @total_count = @tasks.count
  end

  def task_pay_item
    if params[:name]
      @tasks = Task.includes(:pay_item).where(pay_items: {name: params[:name]})
    end
    @tasks = @tasks.paginate(page: params[:page] || 1, per_page: params[:per_page] || 20)
    @total_count = @tasks.count
  end

  def filter_tasks
    if params[:task]
      @tasks = Task.created.includes(:task_users).where(vital_task_params)
    else
      @tasks = Task.created
    end
    @tasks = @tasks.paginate(page: params[:page] || 1, per_page: params[:per_page] || 20)
    @total_count = @tasks.count
  end

  def remaining_budget
    if @task.work_periods.count.nonzero?
      render json: { error: 'This parent task contains logged hours.
        Please remove them before continuing.' },
             status: :unprocessable_entity
    end
  end

  def search_tasks
    @tasks = if params[:term]
      task_code_match = Task.find_by_task_code(params[:term])
      if task_code_match
        Task.where(id: task_code_match.id)
      else
        Task.search_by_title(params[:term])
      end
    else
      Task.all
    end
  end
  def task_summary
  end

  def task_snapshot
  end

  # GET /tasks
  # GET /tasks.json
  def index
    @tasks = current_user.get_tasks
  end

  # GET /tasks/1
  # GET /tasks/1.json
  def show
  end

  # GET /tasks/new
  def new
    @task = Task.new
  end

  # GET /tasks/1/edit
  def edit
  end

  # POST /tasks
  # POST /tasks.json
  def create
    @user_ids = task_params[:user_ids].reject do |i|
      i.empty? || i.to_i == current_user.id ||
        i.to_i == task_params[:escalation_manager]
    end
    updated_params = task_params.merge(user_ids: @user_ids)
    updated_params.delete(:escalation_manager)
    @task = Task.new(updated_params)
    @task.actual_cost = calc_actual(@task)
    if params[:increase_parents_budget] == 'true'
      @p_task = Task.find(params[:task][:parent_id])
      new_budget = @p_task.budgeted_cost + @task.budgeted_cost.to_f
      @p_task.update(budgeted_cost: new_budget)
      @p_task.notify_increase_in_budget(current_user, @task.budgeted_cost.to_f, params[:task][:title], true)
    end

    e_manager = task_params[:escalation_manager].present? ? task_params[:escalation_manager] : find_escalation_manager

    respond_to do |format|
      if @task.save
        @task.task_users.create(user_id: e_manager,
                                task_role: TaskUser::TASK_ROLES[
                                  :escalation_manager])
        @task.task_users.create(user: current_user, isOwner: true,
                                task_role: TaskUser::TASK_ROLES[
                                  :task_owner])
        format.html do
          redirect_to "#{root_url}#task_id=#{@task.id}",
                      notice: 'Task was successfully created.'
        end
        format.json { render :show, status: :created, location: @task }
      else
        format.html { render :new }
        format.json { render json: @task.errors, status: :unprocessable_entity }
      end
    end
  end

  # @param [Task] task
  def calc_actual(task)
    0 if task.children.count.zero?
  end

  # PATCH/PUT /tasks/1
  # PATCH/PUT /tasks/1.json
  def update
    if params[:task][:parent_id].present? &&
       params[:task][:parent_id] != @task.parent_id
      assign_new_parent
    else
      update_task_info
    end
  end

  def find_escalation_manager
    escalation_manager = User.find_by(email: "michael.qiu@gradready.com.au")
    if escalation_manager.present?
      escalation_manager.id
    else
      e_manager = User.where(role: 'manager').first
      e_manager.id
    end
  end

  def check_task_date_range
    if @task.out_of_parent_date?(task_params[:target_start],
                                 task_params[:target_finish])
      render json: {}, status: 406
    else
      update_task_info
    end
  end

  def check_budget
    if request.method == 'PUT'
      update_task_info
    else
      if @task.out_of_parent_budget? task_params[:budgeted_cost]
        render json: { message: "Task budget is out of parent's budget" },
               status: 406
      else
        render json: { message: "Task budget is within parent's budget" },
               status: 409
      end
    end
  end

  def check_task_budget
    render json: 'Task no found', status: :unprocessable_entity unless @task
    if @task.child_out_of_budget?(params[:budgeted_cost])
      render json: { message: "Budget is out of task's budget" }, status: 406
    else
      render json: { message: "Budget is within task's budget" }, status: 409
    end
  end

  # DELETE /tasks/1
  # DELETE /tasks/1.json
  def destroy
    @task.destroy
    respond_to do |format|
      format.html do
        redirect_to tasks_url,
                    notice: 'Task was successfully destroyed.'
      end
      format.json { head :no_content }
    end
  end

  def remove_task
    @task.self_and_descendants.sort_by(&:depth).reverse.each do |task|
      c = task.actual_cost <= 0 ? 1 : task.actual_cost
      task.update(budgeted_cost: c, status: 'removed')
    end

    respond_to do |format|
      format.html do
        redirect_to tasks_url,
                    notice: 'Task was successfully destroyed.'
      end
      format.json { head :no_content }
    end
  end

  def recover_task
    @task.recover_task
    respond_to do |format|
      format.html do
        redirect_to removed_tasks_url,
                    notice: 'Task was successfully restored.'
      end
      format.json { head :no_content }
    end
  end

  def show_task
  end

  private

  def set_task
    @task = Task.find(params[:id])
  end

  def task_params
    params[:task][:weekly_reports_start] = params[:task][:weekly_reports_start].to_i if params[:task][:weekly_reports_start].present?
    params.require(:task).permit(:title, :description, :target_start,
                                 :actual_start, :target_finish, :actual_finish,
                                 :budgeted_cost, :actual_cost, :parent_id,
                                 :priority, :pay_rate, :escalation_manager,
                                 :project_code,
                                 :weekly_reports_start,
                                 pay_item_attributes: [:earnings_rate_id,
                                                       :name],
                                 user_ids: [])
  end

  def task_increase_parents_budget_params
    params.require(:task).permit(:increase_parents_budget,
                                 increase_parents_budget_attributes:
                                 [:parent_id, :amount])
  end

  def task_decrease_parents_budget_params
    params.require(:task).permit(:decrease_parents_budget,
                                 decrease_parents_budget_attributes:
                                 [:parent_id, :amount])
  end

  def task_decrease_all_parents_budget_params
    params.require(:task).permit(:decrease_all_parents_budget,
                                 decrease_parents_budget_attributes:
                                 [:parent_id, :amount])
  end

  def vital_task_params
    params.require(:task).permit(:depth, :is_leaf,
                                 task_users: [:user_id, :task_role])
  end

  def update_task_info
    @task.notify_out_of_parent_date if @task.out_of_parent_date?(
      task_params[:target_start], task_params[:target_finish]
    )
    if @task.out_of_parent_budget? task_params[:budgeted_cost]

      @task.notify_out_of_budget(current_user, task_params[:budgeted_cost])
      increase_parents_budget if task_increase_parents_budget_params[
        :increase_parents_budget] == 'true'

    end

    respond_to do |format|
      if @task.update(task_params)
        if task_decrease_all_parents_budget_params[
          :decrease_all_parents_budget] == 'true'
          @task.decrease_ancestors_budget(
            task_decrease_all_parents_budget_params[
            :decrease_parents_budget_attributes][:amount].to_d
          )
        end

        if @task.pay_item.earnings_rate_id.blank? 
          unassigned_item = PayItemCategory.find_by_name('Unassigned Item')
          @task.pay_item.update(name: unassigned_item.name, earnings_rate_id: unassigned_item.earnings_rate_id)
        end
        format.html do
          redirect_to home_index_path,
                      notice: 'Task was successfully updated.'
        end
        format.json { render :task_summary, status: :ok }
      else
        format.html { render :edit }
        format.json do
          render json: @task.errors.full_messages.join('\n'),
                 status: :unprocessable_entity
        end
      end
    end
  end

  def increase_parents_budget
    @c_task = Task.find(params[:id]) if(params[:id].present?)
    @p_task = Task.find(
      task_increase_parents_budget_params[
        :increase_parents_budget_attributes][:parent_id]
    )
    new_budget = @p_task.budgeted_cost +
                 task_increase_parents_budget_params[
                   :increase_parents_budget_attributes][:amount].to_f
    @p_task.update(budgeted_cost: new_budget)
    @p_task.notify_increase_in_budget(
      current_user, task_increase_parents_budget_params[
                      :increase_parents_budget_attributes][:amount].to_f, @c_task.title
    )
  end

  # def decrease_parents_budget
  #   @p_task = Task.find(
  #     task_decrease_parents_budget_params[
  #       :decrease_parents_budget_attributes][:parent_id]
  #   )
  #   new_budget = @p_task.budgeted_cost -
  #                task_decrease_parents_budget_params[
  #                 :decrease_parents_budget_attributes][:amount].to_f
  #   @p_task.update(budgeted_cost: new_budget)
  # end

  def assign_new_parent
    if params[:task][:decrease_old_parents_budget].blank? &&
       params[:task][:increase_new_parents_budget].blank?
      render(json: 'Invalid params', status: :unprocessable_entity) && return
    end
    result, error = @task.assign_new_parent(
      params[:task][:parent_id],
      params[:task][:decrease_old_parents_budget] == "true",
      params[:task][:increase_new_parents_budget] == "true"
    )
    respond_to do |format|
      if result
        format.html do
          redirect_to home_index_path,
                      notice: 'New Parent was assigned successfully.'
        end
        format.json { render :task_summary, status: :ok, task: @task }
      else
        format.html { render :edit }
        format.json { render json: error, status: :unprocessable_entity }
      end
    end
  end
end
