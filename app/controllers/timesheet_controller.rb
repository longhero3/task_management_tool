class TimesheetController < ApplicationController
  before_action :authenticate_user!
  before_action :set_fortnight

  require 'xero_client'

  def current_timesheet
  end

  private
    def set_fortnight
      @current_fortnight = PayrollCalendar.current_fortnight
      @previous_fortnight = PayrollCalendar.previous_fortnight
      @unassigned_items = PayItem.where(earnings_rate_id: '')
    end
end
