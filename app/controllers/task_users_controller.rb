class TaskUsersController < ApplicationController
  before_action :set_task, only: [:create, :update, :switch_role]
  before_action :set_task_user, only: [:show, :edit, :update, :destroy]

  # GET /task_users
  # GET /task_users.json
  def index
    @task_users = TaskUser.all
  end

  # GET /task_users/1
  # GET /task_users/1.json
  def show
  end

  # GET /task_users/new
  def new
    @task_user = Task User.new
  end

  # GET /task_users/1/edit
  def edit
  end

  # POST /task_users
  # POST /task_users.json
  def create
    @task_user = TaskUser.new(task_user_params)

    respond_to do |format|
      if @task_user.save
        format.html { redirect_to @task_user, notice: 'Task User was successfully created.' }
        format.json { render 'tasks/task_snapshot', status: :ok, location: @task }
      else
        format.html { render :new }
        format.json { render json: @task_user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /task_users/1
  # PATCH/PUT /task_users/1.json
  def update
    respond_to do |format|
      if @task_user.update(task_user_params)
        format.html { redirect_to @task_user, notice: 'Task User was successfully updated.' }
        format.json { render :show, status: :ok, location: @task_user }
      else
        format.html { render :edit }
        format.json { render json: @task_user.errors, status: :unprocessable_entity }
      end
    end
  end

  def switch_role
    @old_task_user = TaskUser.find_by(user_id: task_user_params[:user_id], task_id: task_user_params[:task_id])
    @new_task_user = TaskUser.find_by(user_id: task_user_params[:switch_user_id], task_id: task_user_params[:task_id])
    role_switched = TaskUser.switch_role(@old_task_user, @new_task_user, task_user_params)
    respond_to do |format|
      if role_switched
        task_owners  = @task.task_users.includes(:user).where.not(users: {id: task_user_params[:switch_user_id]}).where(task_role: TaskUser::TASK_ROLES[:task_owner])
        if task_owners.present? && task_user_params[:task_role] == "task_owner"
          ts_users = TaskUser.where(id: task_owners.ids)
          ts_users.update_all(task_role: 'member')
        end
        
        format.html { redirect_to @task_user, notice: 'Task User was successfully updated.' }
        format.json { render 'tasks/task_snapshot', status: :ok, location: @task }
      else
        format.html { render :edit }
        format.json { render json: @task_user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /task_users/1
  # DELETE /task_users/1.json
  def destroy
    @task = @task_user.task
    @task_user.delete
    respond_to do |format|
      format.html { redirect_to task_users_url, notice: 'Task User was successfully destroyed.' }
      format.json { render 'tasks/task_snapshot', status: :ok, location: @task }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_task_user
    @task_user = TaskUser.find(params[:id])
  end

  def set_task
    @task = Task.find(task_user_params[:task_id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def task_user_params
    params.require(:task_user).permit(:user_id, :task_id, :task_role, :switch_user_id)
  end
end
