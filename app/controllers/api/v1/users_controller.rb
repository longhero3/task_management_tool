module Api
  module V1
    # Users Controller
    class UsersController < ApiController
      def index
        @users = UserService.fetch_all
        render 'index.json.jbuilder', status: :ok
      end
    end
  end
end
