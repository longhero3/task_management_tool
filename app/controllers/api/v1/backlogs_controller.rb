# app/controller/api/v1/backlogs_controller.rb
module Api
  module V1
    # Backlogs CONTROLLER
    class BacklogsController < ApiController
      def index
        @backlogs = BacklogService.backlogs
        render 'backlogs.json.jbuilder', status: :ok
      end
    end
  end
end
