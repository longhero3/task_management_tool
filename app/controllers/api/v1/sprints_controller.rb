module Api
  module V1
    # SPRINTS CONTROLLER
    class SprintsController < ApiController
      def index
        @sprints = SprintService.search
        render 'index.json.jbuilder', status: :ok
      end

      def create
        @sprint = SprintService.create(user_id: current_user.id,
                                              options: create_sprint_params)
        render 'sprint.json.jbuilder', status: :ok
       
      end

      private

      def create_sprint_params
        params.permit(:start_date, :end_date, :actual_cost)
      end
    end
  end
end
