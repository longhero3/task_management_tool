module Api
  module V1
    # TASKS CONTROLLER
    class TasksController < ApiController
      before_action :set_task, except: [:index, :create]
      before_action :ensure_task_present, except: [:index, :create]

      def index
        @tasks = TaskService.search(task_search_params)
        render 'index.json.jbuilder', status: :ok
      end

      def create
        @task, error = TaskService.create(user_id: current_user.id,
                                          options: create_task_params)
        if error
          render json: error, status: :unprocessable_entity
        else
          render 'task.json.jbuilder', status: :ok
        end
      end

      def show
        render 'show.json.jbuilder', status: :ok
      end

      def update
        if @task.update(update_params)
          render 'task_summary.json.jbuilder', status: :ok
        else
          render json: { error: true, error_code: 'TASK-001',
                         message:  @task.errors.full_messages.join(', ') }
        end
      end

      def destroy
        @task.destroy
        head :no_content
      end

      def comments
        @comments = @task.comments
        render 'comments.json.jbuilder', status: :ok
      end

      def post_comment
        @comment = @task.comments.build(user_id: current_user.id,
                                        comment: params[:body])
        if @comment.save
          render 'comment.json.jbuilder', status: :ok
        else
          render json: { error: true, error_code: 'COMMENT-001',
                         message:  @comment.errors.full_messages.join(', ') }
        end
      end

      def work_periods
        @work_periods = @task.filter_work_periods(work_periods_filter_params)
        # NO RESPONSE BOBY PROVIDED IN APIARRY DOC
        head :no_content, status: :ok
      end

      def log_hours
        @work_period, error = WorkPeriodService.create(task_id: @task.id,
                                                       user_id: current_user.id,
                                                       params: log_hours_params)
        if error
          render json: error, status: :unprocessable_entity
        else
          # render 'work_period.json.jbuilder'
          # NO RESPONSE BOBY PROVIDED IN APIARRY DOC
          head :no_content, status: :ok
        end
      end

      def children
        @tasks = @task.children
        render 'index.json.jbuilder', status: :ok
      end

      private

      def set_task
        @task = Task.find_by(id: params[:id])
      end

      def update_params
        params.permit(:title, :description, :pay_rate)
      end

      def ensure_task_present
        render json: { error: true, error_code: 'TASK-404',
                       message: 'Task not Found' } unless @task && return
      end

      def work_periods_filter_params
        params.permit(:start_time, :amount_hours, :description)
      end

      def log_hours_params
        params.permit(:start_time, :amount_hours, :description,
                      :end_time, :date_worked, :progress, :start_date,
                      :end_date)
      end

      def task_search_params
        params.permit(:status, :assignee, :project, :priority)
      end

      def create_task_params
        # AS GIVEN IS APIARRAY DOC
        # params.permit(:status, :assignee, :project, :priority)
        # TODO
        params.permit(:title, :description, :target_start, :actual_start,
                      :target_finish, :actual_finish, :actual_finish,
                      :budgeted_cost, :actual_cost, :priority, :parent_id,
                      :pay_rate, :escalation_manager,
                      pay_item_attributes: [:earnings_rate_id, :name],
                      user_ids: [])
      end
    end
  end
end
