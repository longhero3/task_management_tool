module Api
  module V1
    # THIS IS API BASE CONTROLLER
    class ApiController < ActionController::API
      respond_to :json
      before_action :doorkeeper_authorize!

      private

      def current_user
        if doorkeeper_token
          @current_user ||= User.find(doorkeeper_token.resource_owner_id)
        end
      end
    end
  end
end
