module Api
  module V1
    # PayItems Controller
    class PayItemsController < ApiController
      # before_action :doorkeeper_authorize!, scopes: [:api]

      def index
        @pay_items = PayItemService.fetch_all
        render 'index.json.jbuilder', status: :ok
      end
    end
  end
end
