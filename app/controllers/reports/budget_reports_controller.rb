class Reports::BudgetReportsController < Reports::WorkPeriodReportsController
  def show
    return render plain: 'Please specify a task', content_type: 'text/plain' if @task.nil?

    @created_after  ||= 1.day.ago.iso8601
    @created_before ||= (Time.parse(@created_after.to_s) + 1.day).iso8601
    @report_data = BudgetReport.new(@task, @work_periods, @created_after, @created_before)
    if (params[:generate_pdf] == "true")
      respond_to do |format|
        format.html { render 'show_budget.pdf',layout: 'layouts/pdf' }
        format.pdf do
          render pdf: "file_name.pdf", orientation: 'Landscape'
        end
      end
    else
      respond_to do |format|
        format.html { render 'show.pdf', layout: 'layouts/pdf' }
        format.pdf do
          render pdf: "file_name.pdf", orientation: 'Landscape'
        end
      end
    end
  end
end
