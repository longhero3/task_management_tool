class Reports::StaffHoursController < Reports::WorkPeriodReportsController
  def show
    @report_data = StaffHoursReport.new(@work_periods, @created_after, @created_before)
    if params[:generate_pdf] == "true"
      respond_to do |format|
        format.html { render 'show_staff_hour.pdf', layout: 'layouts/pdf' }
        format.pdf do
          render pdf: "file_name.pdf", orientation: 'Landscape'
        end
        format.csv { render csv: @report_data.csv_data, filename: 'staff_hours' }
      end
    else
      respond_to do |format|
        format.html { render 'show.pdf', layout: 'layouts/pdf' }
        format.pdf do
          render pdf: "file_name.pdf", orientation: 'Landscape'
        end
        format.csv { render csv: @report_data.csv_data, filename: 'staff_hours' }
      end
    end
  end
end
