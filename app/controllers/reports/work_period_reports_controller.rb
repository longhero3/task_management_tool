class Reports::WorkPeriodReportsController < ApplicationController
  before_action :set_task, only: [ :show ]
  before_action :set_work_periods, only: [ :show ]

  def new
  end

  protected
  def parse_time_or_nil(time_string)
    begin
      (Time.parse(time_string))
    rescue ArgumentError
      nil
    rescue TypeError
      nil
    end
  end

  def set_task
    @task = Task.find_by_id(params[:task_id]) unless params[:task_id].blank?
  end

  def set_work_periods
    # Attempt to parse dates and return nil if invalid
    @created_after = parse_time_or_nil params[:created_after]
    @created_before = parse_time_or_nil params[:created_before]
    @work_periods = WorkPeriod.all

    @work_periods = @work_periods.where(
      'work_periods.created_at >= ?', @created_after
    ) unless @created_after.blank?
    @work_periods = @work_periods.where(
      'work_periods.created_at <= ?', @created_before
    ) unless @created_before.blank?
    @work_periods = @work_periods.where(
      task_id: @task.self_and_descendants.ids
    ) unless @task.nil?
    @work_periods = @work_periods.where(
      user_id: params[:user_id]
    ) unless params[:user_id].blank?
  end
end
