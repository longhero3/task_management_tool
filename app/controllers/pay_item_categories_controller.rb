class PayItemCategoriesController < ApplicationController
  def search_pay_items
    if params[:term]
      @pay_item_categories = PayItemCategory.where("UPPER(pay_item_categories.name) LIKE UPPER(?)", "%#{params[:term]}%")
    else
      @pay_item_categories = PayItemCategory.all
    end
  end
end
