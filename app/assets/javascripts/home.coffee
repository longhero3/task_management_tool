# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://
$ ->

  $(".message .close").on "click", ->
    $(this).closest(".message").transition "fade", ->
      $(this).remove()

  $('#add_task').on "click", ->
    $('.new_task').modal 'show'

  $('#submit_task_form').on 'click', ->
    remain_budget = Number($('.remaining_budget_value').text())
    expected_budget = Number($('#task_budgeted_cost').val())
    if(remain_budget < expected_budget && $('#task_parent_id').val())
      result = confirm('The budgetted cost has exeeded remaining budget. Do you want to continue?')
      if (result == true)
        check_budgetedCost_with_parent()
    else
        check_budgetedCost_with_parent()
          
  check_budgetedCost_with_parent = ->
    if($('#task_budgeted_cost').val() && $('#task_parent_id').val())
      $.ajax
        url: "/tasks/check_task_budget.json"
        method: 'GET'
        data: { budgeted_cost:$('#task_budgeted_cost').val(),id:$('#task_parent_id').val()},
        error: (data) ->
          if(data.status==406)
            result = confirm("You've set the budget greater than the parent's. Do you wish to proceed and update the budget for parent task");
            if(result == true)
              input = $('<input>').attr('type', 'hidden').attr('name', 'increase_parents_budget').val('true') 
              $('form').append(input).submit()
            else 
              input = $('<input>').attr('type', 'hidden').attr('name', 'increase_parents_budget').val('false') 
              $('form').append(input).submit()
          else if(data.status==409)
            result = confirm("Do you want to increase the budget of " + $("#task_parent").val() + "?");
            if(result == true)
              input = $('<input>').attr('type', 'hidden').attr('name', 'increase_parents_budget').val('true') 
              $('form').append(input).submit()
            else 
              input = $('<input>').attr('type', 'hidden').attr('name', 'increase_parents_budget').val('false') 
              $('form').append(input).submit()
    else
      $('form').submit()
                     
  $('#task_parent_id').on 'change', ->
    $.ajax
      url: "/tasks/#{$(this).val()}/remaining_budget.json"
      method: 'GET'
      success: (data) ->
        $('.remaining_budget_value').text(data.remaining_budget)
        $('#submit_task_form').attr('disabled', false);
      error: (data) ->
        $('#submit_task_form').attr('disabled', true);
        alert(data.responseJSON.error);

    $.ajax
      url: "/tasks/#{$(this).val()}/task_summary.json"
      method: 'GET'
      success: (data) ->
        $('#task_pay_item').val(data.pay_item.name);
        $('#task_pay_item_attributes_name').val(data.pay_item.name);
        $('#task_pay_item_attributes_earnings_rate_id').val(data.pay_item.earnings_rate_id);
      error: (data) ->
        console.log('Error when setting payment item')

  $('.add-task-parent-chooser').search(
    apiSettings:
      url: '/tasks/search_tasks?term={query}'
    fields:
      results: 'tasks',
      title: 'title',
      id: 'id'
    errors:
      noResults: ''
    onSelect: (result, response) ->
      $('#task_parent_id').val(result.id).trigger('change')
  )

  $('.pay-item-chooser').search(
    apiSettings:
      url: '/pay_item_categories/search_pay_items?term={query}'
    fields:
      results: 'pay_items',
      title: 'name',
      id: 'earnings_rate_id'
    errors:
      noResults: ''
    onSelect: (result, response) ->
      $('#task_pay_item_attributes_name').val(result.name)
      $('#task_pay_item_attributes_earnings_rate_id').val(result.earnings_rate_id)
  )

  $('.popup-control').popup()

  $('.add-task-parent-chooser .prompt').on 'change', ->
    if $(this).val() == ''
      $('#task_parent_id').val('')
      $('.remaining_budget_value').text('0')
      $('#submit_task_form').attr('disabled', false)

  $.fn.form.settings.rules.lesserThan = (value, field) ->
    validatedDate = moment(value, 'DD/MM/YYYY HH:mm')
    specifiedDate = moment($("##{field}").val(), 'DD/MM/YYYY HH:mm')
    specifiedDate.isAfter(validatedDate)

  $.fn.form.settings.rules.greaterThanZero = (value, field) ->
    number = parseFloat(value)
    number >= 0

  $('.create-task-form').form
    inline: true
    fields:
      task_title:
        identifier: 'task_title'
        rules: [
          {
            type: 'empty',
            prompt: 'Please enter task title'
          }
        ]
      task_description:
        identifier: 'task_description'
        rules: [
          {
            type: 'empty',
            prompt: 'Please enter task description'
          }
        ]
      task_pay_item:
        identifier: 'task_pay_item'
        rules: [
          {
            type: 'empty',
            prompt: 'Please select pay item'
          }
        ]
      task_target_start:
        identifier: 'task_target_start'
        rules: [
          {
            type: 'empty',
            prompt: 'Please enter task target start date'
          },
          {
            type: 'lesserThan[task_target_finish]',
            prompt: 'Target start must be before target finish'
          }
        ]
      task_target_finish:
        identifier: 'task_target_finish'
        rules: [
          {
            type: 'empty',
            prompt: 'Please enter task target finish date'
          }
        ]
      task_budgeted_cost:
        identifier: 'task_budgeted_cost'
        rules: [
          {
            type: 'empty',
            prompt: 'Please enter task target budget'
          },
          {
            type: 'number',
            prompt: 'The task budget must be numeric'
          },
          {
            type: 'greaterThanZero[task_budgeted_cost]',
            prompt: 'The task budget must not be negative'
          }
        ]
      task_pay_rate:
        identifier: 'task_pay_rate'
        rules: [
          {
            type: 'empty',
            prompt: 'Please enter task pay rate'
          },
          {
            type: 'number',
            prompt: 'The pay rate must be numeric'
          }
        ]

$ -> $('#edit_task').on 'click', ->
  $('.card').animate()

$ -> $(document).ready ->
  $('.datetimepicker').datetimepicker({format: 'd/m/Y H:i', defaultTime: '00:00', closeOnDateSelect: true});
  $('.datepicker').datetimepicker({timepicker: false, format: 'd/m/Y', closeOnDateSelect: true});
  $('.dropdown').dropdown()

  $ -> $('.item').on 'click', ->
    $('.search').val("");

  $ -> $('#task_pay_item').on 'change', ->
    earnings_rate_id = $(this).val();
    name = $(this).find("option:selected").text();
    $('#task_pay_item_attributes_name').val(name)
    $('#task_pay_item_attributes_earnings_rate_id').val(earnings_rate_id)