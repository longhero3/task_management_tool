$ ->
  $('#img_prev').hide()
  $('#user_profile_image_attributes_avatar').on 'change', ->
    $('#img_prev').show()
    readURL $(this)[0]
    return

  $('.registration-form').form
    fields:
      user_first_name: 'empty'
      user_last_name: 'empty'
      user_grad_job_title: 'empty'
      user_profile_image_attributes_avatar: 'empty'
      user_email: 'empty'
      user_second_email: 'empty'
      user_date_of_birth: 'empty'
      user_contact_number: 'empty'
      user_address_attributes_street: 'empty'
      user_address_attributes_suburb: 'empty'
      user_address_attributes_state: 'empty'
      user_grad_year: 'empty'
      user_high_school: 'empty'
      user_user_payment_attributes_pay_rate: 'empty'
      user_user_payment_attributes_extra_pay_rate: 'empty'
      user_password: 'empty'
      user_password_confirmation: 'empty'

  $('.user-high-school').search
    apiSettings:
      url: '/high_schools?term={query}'
    fields:
      results: 'high_schools'
      title: 'name'
    errors:
      noResult: 'This high school will be added to the database!'

  readURL = (input) ->
    if input.files and input.files[0]
      reader = new FileReader

      reader.onload = (e) ->
        $('#img_prev').attr('src', e.target.result).width(150).height 200
        return

      reader.readAsDataURL input.files[0]
    return
