'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var React = require('react');
var moment = require('moment');
var DatePicker = require('react-datetime');

var EditableDatePicker = exports.EditableDatePicker = function (_React$Component) {
  _inherits(EditableDatePicker, _React$Component);

  function EditableDatePicker(props) {
    _classCallCheck(this, EditableDatePicker);

    var _this = _possibleConstructorReturn(this, (EditableDatePicker.__proto__ || Object.getPrototypeOf(EditableDatePicker)).call(this, props));

    _this.state = {
      date: moment(_this.props.text, 'DD/MM/YYYY h:mm A'),
      editing: false,
      className: _this.props.className
    };
    return _this;
  }

  _createClass(EditableDatePicker, [{
    key: 'componentDidUpdate',
    value: function componentDidUpdate() {
      if (this.state.editing == true) {
        $('.' + this.state.className.replace(' ', '.') + ' input').focus();
        $('.' + this.state.className.replace(' ', '.') + ' input').on('blur', function (e) {
          this.onChange(moment($(e.target).val(), 'DD/MM/YYYY h:mm A'));
        }.bind(this));
      }
    }
  }, {
    key: 'enableEditing',
    value: function enableEditing() {
      this.setState({ editing: true });
    }
  }, {
    key: 'disableEditing',
    value: function disableEditing(text) {
      this.setState({ editing: false });
    }
  }, {
    key: 'reset',
    value: function reset() {
      this.setState({
        date: moment(this.props.text, 'DD/MM/YYYY h:mm A')
      });
    }
  }, {
    key: 'onChange',
    value: function onChange(date) {
      this.state.date = date;
      this.setState({ date: date, editing: false });
      this.props.onSave();
    }
  }, {
    key: 'getText',
    value: function getText() {
      var self = this;
      return self.state.date.format('llll');
    }
  }, {
    key: 'labelView',
    value: function labelView() {
      var self = this;
      return React.createElement(
        'div',
        { onClick: this.enableEditing.bind(this), className: 'inline ' + this.state.className },
        self.state.date.format('DD/MM/YYYY h:mm A')
      );
    }
  }, {
    key: 'editView',
    value: function editView() {
      return React.createElement(
        'div',
        { className: 'datepicker-wrapper' },
        React.createElement(DatePicker, { value: this.state.date,
          onChange: this.onChange.bind(this),
          dateFormat: 'DD/MM/YYYY',
          timeFormat: 'h:mm A',
          open: true,
          className: this.state.className
        })
      );
    }
  }, {
    key: 'render',
    value: function render() {
      if (this.state.editing) {
        return this.editView();
      } else {
        return this.labelView();
      }
    }
  }]);

  return EditableDatePicker;
}(React.Component);

'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var React = require('react');
var InlineEdit = require('react-edit-inline');

var EditableField = exports.EditableField = function (_React$Component) {
  _inherits(EditableField, _React$Component);

  function EditableField(props) {
    _classCallCheck(this, EditableField);

    var _this = _possibleConstructorReturn(this, (EditableField.__proto__ || Object.getPrototypeOf(EditableField)).call(this, props));

    var result = true;
    var validator = _this.defaultValidator;
    if (_this.props.enabled != null) {
      result = _this.props.enabled;
    }

    if (_this.props.validate != null) {
      validator = _this.props.validate;
    }
    _this.state = {
      text: _this.props.text,
      className: _this.props.className || '',
      enabled: result,
      validate: validator
    };
    return _this;
  }

  _createClass(EditableField, [{
    key: 'defaultValidator',
    value: function defaultValidator(text) {
      return text.length > 0 && text.length < 255;
    }
  }, {
    key: 'onChange',
    value: function onChange(params) {
      this.state.text = params.changedText;
      this.props.onSave();
    }
  }, {
    key: 'reset',
    value: function reset() {
      this.setState({ text: this.props.text });
    }
  }, {
    key: 'getText',
    value: function getText() {
      return this.state.text;
    }
  }, {
    key: 'render',
    value: function render() {
      if (this.state.enabled == true) {
        return React.createElement(InlineEdit, {
          className: 'ui input ' + this.state.className,
          text: this.state.text,
          paramName: 'changedText',
          change: this.onChange.bind(this),
          validate: this.state.validate
        });
      } else {
        return React.createElement(
          'label',
          null,
          this.state.text
        );
      }
    }
  }]);

  return EditableField;
}(React.Component);

'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var React = require('react');

var EditableSearchDropdown = exports.EditableSearchDropdown = function (_React$Component) {
  _inherits(EditableSearchDropdown, _React$Component);

  function EditableSearchDropdown(props) {
    _classCallCheck(this, EditableSearchDropdown);

    var _this = _possibleConstructorReturn(this, (EditableSearchDropdown.__proto__ || Object.getPrototypeOf(EditableSearchDropdown)).call(this, props));

    _this.state = {
      selectedText: _this.props.text,
      options: _this.props.options,
      keyName: _this.props.keyName,
      valueName: _this.props.valueName,
      className: _this.props.className,
      editing: false,
      selectedValue: '',
      keepUnchanged: _this.props.keepUnchanged || false
    };
    return _this;
  }

  _createClass(EditableSearchDropdown, [{
    key: 'componentDidUpdate',
    value: function componentDidUpdate() {
      if (this.props.options != []) {
        $('.' + this.state.className).dropdown({
          onChange: function (value, text) {
            this.onChange(value, text);
          }.bind(this),

          onHide: function () {
            this.setState({ editing: false });
          }.bind(this)
        });
      }

      if (this.state.editing == true) {
        $('.' + this.state.className + ' .dropdown.icon').trigger('click');
      }
    }
  }, {
    key: 'enableEditing',
    value: function enableEditing() {
      this.setState({ editing: true });
    }
  }, {
    key: 'disableEditing',
    value: function disableEditing() {
      this.setState({ editing: false });
    }
  }, {
    key: 'onCancel',
    value: function onCancel() {
      this.setState(this.props);
    }
  }, {
    key: 'onChange',
    value: function onChange(value, text) {
      this.setState({
        selectedText: text,
        selectedValue: value
      });
      this.disableEditing();
      this.props.onSave();
    }
  }, {
    key: 'getSelectedText',
    value: function getSelectedText() {
      return this.state.selectedText;
    }
  }, {
    key: 'getSelectedValue',
    value: function getSelectedValue() {
      return this.state.selectedValue;
    }
  }, {
    key: 'labelView',
    value: function labelView() {
      if (this.state.keepUnchanged == true) {
        return React.createElement(
          'label',
          { onClick: this.enableEditing.bind(this) },
          this.props.text
        );
      } else {
        return React.createElement(
          'label',
          { onClick: this.enableEditing.bind(this) },
          this.state.selectedText
        );
      }
    }
  }, {
    key: 'editView',
    value: function editView() {
      return React.createElement(
        'div',
        { className: "ui fluid search selection dropdown " + this.state.className },
        React.createElement('input', { type: 'hidden', name: 'selected-option', value: this.state.selectedValue, onChange: this.onChange.bind(this) }),
        React.createElement('i', { className: 'dropdown icon' }),
        React.createElement(
          'div',
          { className: 'default text' },
          this.state.selectedText
        ),
        React.createElement(
          'div',
          { className: 'menu' },
          this.props.options.map(function (option) {
            return React.createElement(
              'div',
              { className: 'item', key: option[this.state.valueName], 'data-value': option[this.state.valueName] },
              option[this.state.keyName]
            );
          }.bind(this))
        )
      );
    }
  }, {
    key: 'render',
    value: function render() {
      if (this.state.editing) {
        return this.editView();
      } else {
        return this.labelView();
      }
    }
  }]);

  return EditableSearchDropdown;
}(React.Component);

'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var React = require('react');

var EditableTextArea = exports.EditableTextArea = function (_React$Component) {
  _inherits(EditableTextArea, _React$Component);

  function EditableTextArea(props) {
    _classCallCheck(this, EditableTextArea);

    var _this = _possibleConstructorReturn(this, (EditableTextArea.__proto__ || Object.getPrototypeOf(EditableTextArea)).call(this, props));

    _this.state = {
      text: _this.props.text,
      editing: false,
      className: _this.props.className
    };
    return _this;
  }

  _createClass(EditableTextArea, [{
    key: 'componentDidUpdate',
    value: function componentDidUpdate() {
      if (this.state.editing == true) {
        $('.' + this.state.className).focus();
      }
    }
  }, {
    key: 'enableEditing',
    value: function enableEditing(e) {
      e.preventDefault();
      this.setState({ editing: true });
    }
  }, {
    key: 'disableEditing',
    value: function disableEditing() {
      this.setState({ editing: false });
    }
  }, {
    key: 'onCancel',
    value: function onCancel() {
      this.setState({
        editing: false,
        text: this.props.text
      });
    }
  }, {
    key: 'onKeyUp',
    value: function onKeyUp(event) {
      this.state.text = event.target.value;
      if (event.keyCode == 13 && event.shiftKey == true) {
        this.props.onSave();
        this.setState({ editing: false });
      }
    }
  }, {
    key: 'getText',
    value: function getText() {
      return this.state.text;
    }
  }, {
    key: 'labelView',
    value: function labelView() {
      return React.createElement(
        'label',
        { className: 'task-description-label', onClick: this.enableEditing.bind(this) },
        this.state.text
      );
    }
  }, {
    key: 'editView',
    value: function editView() {
      return React.createElement(
        'div',
        { className: 'ui form' },
        React.createElement(
          'div',
          { className: 'field' },
          React.createElement('textarea', { type: 'text',
            onKeyUp: this.onKeyUp.bind(this),
            defaultValue: this.state.text,
            className: this.state.className,
            onBlur: this.onCancel.bind(this) })
        )
      );
    }
  }, {
    key: 'render',
    value: function render() {
      if (this.state.editing) {
        return this.editView();
      } else {
        return this.labelView();
      }
    }
  }]);

  return EditableTextArea;
}(React.Component);

'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var React = require('react');

var TaskTreeMembers = exports.TaskTreeMembers = function (_React$Component) {
  _inherits(TaskTreeMembers, _React$Component);

  function TaskTreeMembers(props) {
    _classCallCheck(this, TaskTreeMembers);

    var _this = _possibleConstructorReturn(this, (TaskTreeMembers.__proto__ || Object.getPrototypeOf(TaskTreeMembers)).call(this, props));

    _this.state = {
      task: _this.props.task,
      addMemberLabel: '+ Member',
      toggled: _this.props.toggled,
      users: []
    };
    return _this;
  }

  _createClass(TaskTreeMembers, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      $.ajax({
        url: '/home/user_list.json',
        dataType: 'json',
        success: function (data) {
          this.setState({ users: data.users });
        }.bind(this)
      });
    }

    /*componentDidUpdate() {
      var task_id = this.props.selected_task
      if(task_id != undefined) {
        var top = $(".task-id-"+ task_id).offset().top;
        var container = $(".task_info_col");
        container.scrollTop(
          top-100 - container.offset().top + container.scrollTop()
        );
      }
    }*/

  }, {
    key: 'getFullName',
    value: function getFullName(member) {
      if (member != null) {
        return member.first_name + ' ' + member.last_name;
      } else {
        return 'None Selected';
      }
    }
  }, {
    key: 'componentWillReceiveProps',
    value: function componentWillReceiveProps(nextProps) {
      this.setState({ toggled: nextProps.toggled });
    }
  }, {
    key: 'getClassName',
    value: function getClassName() {
      return 'task_' + this.state.task.id;
    }
  }, {
    key: 'handleChange',
    value: function handleChange(task_role) {
      var user_id;
      if (this.state.task[task_role]) {
        user_id = this.state.task[task_role].id;
      } else {
        user_id = null;
      }
      var data = {
        task_user: {
          user_id: user_id,
          task_id: this.state.task.id,
          switch_user_id: this.refs[task_role].getSelectedValue(),
          task_role: task_role
        }
      };

      $.ajax({
        url: '/task_users/switch_role.json',
        method: 'PUT',
        dataType: 'json',
        data: data,
        success: function (result) {
          /*this.props.updateTask(result)*/
          this.setState({ task: result });
          this.context.reloadLogHourButton(result.log_hours_enabled);
        }.bind(this),
        error: function (xhr, status, err) {
          alert(err);
        }.bind(this)
      });
    }
  }, {
    key: 'handleDeleteMember',
    value: function handleDeleteMember(taskUserID, event) {
      event.preventDefault();
      $.ajax({
        url: '/task_users/' + taskUserID + '.json',
        method: 'DELETE',
        dataType: 'json',
        data: {
          id: taskUserID
        },
        success: function (result) {
          this.setState({ task: result });
          this.context.reloadLogHourButton(result.log_hours_enabled);
        }.bind(this),
        error: function (xhr, status, err) {
          alert(err);
        }.bind(this)
      });
    }
  }, {
    key: 'handleAddMember',
    value: function handleAddMember() {
      $.ajax({
        url: '/task_users.json',
        method: 'POST',
        dataType: 'json',
        data: {
          task_user: {
            user_id: this.refs.add_member.getSelectedValue(),
            task_id: this.state.task.id,
            task_role: 'member'
          }
        },
        success: function (result) {
          this.setState({ task: result });
          this.context.reloadLogHourButton(result.log_hours_enabled);
          this.forceUpdate();
        }.bind(this),
        error: function (xhr, status, err) {
          alert(err);
        }.bind(this)
      });
    }
  }, {
    key: 'taskMemberView',
    value: function taskMemberView() {
      return React.createElement(
        'div',
        { className: 'description' },
        React.createElement(
          'div',
          { className: 'task-owner' },
          React.createElement(EditableSearchDropdown, {
            valueName: 'id',
            keyName: 'full_name',
            options: this.state.users,
            onSave: this.handleChange.bind(this, 'task_owner'),
            ref: 'task_owner',
            text: this.getFullName(this.state.task.active_members.task_owner),
            className: this.getClassName() + '_task_owner'
          }),
          '(TM)'
        ),
        React.createElement(
          'div',
          { className: 'members' },
          this.state.task.active_members.task_members.map(function (taskMember) {
            return React.createElement(
              'div',
              { className: 'member', key: taskMember.id },
              this.getFullName(taskMember),
              ' - \xA0',
              React.createElement(
                'a',
                { href: '#', className: 'add-member-btn', onClick: this.handleDeleteMember.bind(this, taskMember.task_user_id) },
                'Remove'
              )
            );
          }, this),
          React.createElement(EditableSearchDropdown, {
            valueName: 'id',
            keyName: 'full_name',
            options: this.state.users,
            onSave: this.handleAddMember.bind(this, 'add_member'),
            ref: 'add_member',
            text: this.state.addMemberLabel,
            className: this.getClassName() + '_add_member',
            keepUnchanged: true
          })
        ),
        React.createElement(
          'div',
          { className: 'escalation-manager' },
          React.createElement(EditableSearchDropdown, {
            valueName: 'id',
            keyName: 'full_name',
            options: this.state.users,
            onSave: this.handleChange.bind(this, 'escalation_manager'),
            ref: 'escalation_manager',
            text: this.getFullName(this.state.task.active_members.escalation_manager),
            className: this.getClassName() + '_manager'
          }),
          '(EM)'
        )
      );
    }
  }, {
    key: 'render',
    value: function render() {
      if (this.state.task != null && this.state.task.active_members != null && this.state.toggled) {
        return this.taskMemberView();
      } else {
        return React.createElement('div', null);
      }
    }
  }]);

  return TaskTreeMembers;
}(React.Component);

TaskTreeMembers.contextTypes = {
  reloadLogHourButton: React.PropTypes.func,
  users: React.PropTypes.array
};

'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var React = require('react');
var selected_task_id = void 0;

var TaskTreeItem = exports.TaskTreeItem = function (_React$Component) {
  _inherits(TaskTreeItem, _React$Component);

  function TaskTreeItem(props) {
    _classCallCheck(this, TaskTreeItem);

    var _this = _possibleConstructorReturn(this, (TaskTreeItem.__proto__ || Object.getPrototypeOf(TaskTreeItem)).call(this, props));

    _this.state = {
      loading: false,
      toggled: props.task.toggled
    };
    _this.hoursAdded = _this.hoursAdded.bind(_this);
    return _this;
  }

  _createClass(TaskTreeItem, [{
    key: 'onTaskSelected',
    value: function onTaskSelected(e) {
      selected_task_id = this.props.task.id;
      e.preventDefault();
      this.context.onTaskItemSelected(this.props.task);
      this.setState({ loading: false, toggled: !this.state.toggled });
    }
  }, {
    key: 'hoursAdded',
    value: function hoursAdded() {
      this.props.onclick();
    }

    /*loadingClass() {
      var baseClass = 'ui inline mini loader';
      if(this.state.loading == true){
      }
    }*/

  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      var _logHours;
      if (this.state.loading) {
        var baseClass = 'active_red';
      }
      if (this.props.task.children_count == 0) {
        _logHours = React.createElement(LogHours, { date: this.props.date, task: this.props.task, onclick: function onclick() {
            _this2.hoursAdded();
          } });
      }
      return React.createElement(
        'div',
        { className: 'ui list task-id-' + this.props.task.id },
        React.createElement(
          'div',
          { className: 'item' },
          React.createElement(
            'div',
            { className: 'content remv_padd ' + this.props.className },
            React.createElement(
              'span',
              { className: 'task-level' },
              this.props.task.level
            ),
            React.createElement('i', { className: 'tasks icon remve_dsp_tblcll' }),
            React.createElement(
              'a',
              { href: '#', className: 'header task_view', onClick: this.onTaskSelected.bind(this) },
              React.createElement(
                'span',
                { className: 'task_title' },
                this.props.task.title
              ),
              React.createElement(
                'span',
                null,
                this.props.task.hours_added
              )
            ),
            _logHours,
            React.createElement(TaskTreeMembers, { loading: this.state.loading, updateTask: this.props.updateTask, task: this.props.task, toggled: this.state.toggled, selected_task: selected_task_id }),
            this.props.children
          )
        )
      );
    }
  }]);

  return TaskTreeItem;
}(React.Component);

TaskTreeItem.contextTypes = { onTaskItemSelected: React.PropTypes.func };

'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var React = require('react');

var LogHourButton = exports.LogHourButton = function (_React$Component) {
  _inherits(LogHourButton, _React$Component);

  function LogHourButton(props) {
    _classCallCheck(this, LogHourButton);

    var _this = _possibleConstructorReturn(this, (LogHourButton.__proto__ || Object.getPrototypeOf(LogHourButton)).call(this, props));

    _this.state = {
      href: _this.props.href,
      task: _this.props.task
    };
    return _this;
  }

  _createClass(LogHourButton, [{
    key: 'componentWillReceiveProps',
    value: function componentWillReceiveProps(nextProps) {
      this.setState(nextProps);
    }
  }, {
    key: 'render',
    value: function render() {
      if (this.state.task.log_hours_enabled == true) {
        return React.createElement(
          'a',
          { className: 'ui green button log-hour-btn', href: this.state.href },
          'Log Hours'
        );
      } else {
        return React.createElement('div', null);
      }
    }
  }]);

  return LogHourButton;
}(React.Component);

LogHourButton.contextTypes = { currentUser: React.PropTypes.object };

'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var React = require('react');

var DeleteTaskButton = exports.DeleteTaskButton = function (_React$Component) {
  _inherits(DeleteTaskButton, _React$Component);

  function DeleteTaskButton(props) {
    _classCallCheck(this, DeleteTaskButton);

    var _this = _possibleConstructorReturn(this, (DeleteTaskButton.__proto__ || Object.getPrototypeOf(DeleteTaskButton)).call(this, props));

    _this.state = {
      buttonShown: false
    };
    return _this;
  }

  _createClass(DeleteTaskButton, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      var currentUser = this.props.task ? this.props.task.current_user : '';
      var role = this.props.task ? this.props.task.current_user.role : '';
      if (currentUser == null) return false;
      if (role != 'staff') {
        this.setState({ buttonShown: true });
        return true;
      }
      this.setState({ buttonShown: this.props.task.level > 2 });
    }
  }, {
    key: 'onClick',
    value: function onClick() {
      this.props.onClick();
    }
  }, {
    key: 'render',
    value: function render() {
      if (this.state.buttonShown == true) {
        return React.createElement(
          'div',
          { className: 'ui button red delete-task', onClick: this.onClick.bind(this) },
          React.createElement(
            'a',
            { href: '/' },
            'Archive Task'
          )
        );
      } else {
        return React.createElement('div', null);
      }
    }
  }]);

  return DeleteTaskButton;
}(React.Component);

DeleteTaskButton.contextTypes = { currentUser: React.PropTypes.object };

'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var React = require('react');

var ParentTaskChooser = exports.ParentTaskChooser = function (_React$Component) {
  _inherits(ParentTaskChooser, _React$Component);

  function ParentTaskChooser(props) {
    _classCallCheck(this, ParentTaskChooser);

    var _this = _possibleConstructorReturn(this, (ParentTaskChooser.__proto__ || Object.getPrototypeOf(ParentTaskChooser)).call(this, props));

    _this.state = {
      parentTask: _this.props.parentTask,
      task: _this.props.task,
      parentID: 0
    };
    return _this;
  }

  _createClass(ParentTaskChooser, [{
    key: 'parentTaskName',
    value: function parentTaskName() {
      if (this.props.parentTask != null) {
        return this.props.parentTask.title;
      } else {
        return '(Level 1 Task)';
      }
    }
  }, {
    key: 'getText',
    value: function getText() {
      return this.state.parentID;
    }
  }, {
    key: 'componentDidMount',
    value: function componentDidMount() {
      $('.task-chooser').search({
        apiSettings: {
          url: '/tasks/search_tasks?term={query}'
        },
        fields: {
          results: 'tasks',
          title: 'title',
          id: 'id'
        },
        errors: {
          noResults: ''
        },
        onSelect: function (result, response) {
          this.setState({ parentID: result.id });
          this.props.onParentTaskSelected();
          this.props.selectedParentId(result.id);
        }.bind(this)
      });

      $('.task-chooser .prompt').blur();
      $('.task-chooser .prompt').focusout();
    }
  }, {
    key: 'render',
    value: function render() {
      return React.createElement(
        'div',
        { className: 'task-parent-task' },
        React.createElement(
          'div',
          { className: 'ui search selection task-chooser' },
          React.createElement(
            'div',
            { className: 'ui icon input' },
            React.createElement('input', { className: 'prompt', defaultValue: this.parentTaskName() }),
            React.createElement('i', { className: 'dropdown icon' })
          )
        )
      );
    }
  }]);

  return ParentTaskChooser;
}(React.Component);

'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var React = require('react');

var PayItemChooser = exports.PayItemChooser = function (_React$Component) {
  _inherits(PayItemChooser, _React$Component);

  function PayItemChooser(props) {
    _classCallCheck(this, PayItemChooser);

    var _this = _possibleConstructorReturn(this, (PayItemChooser.__proto__ || Object.getPrototypeOf(PayItemChooser)).call(this, props));

    _this.state = {
      task: _this.props.task,
      payItemId: '',
      payItemName: '',
      payItems: []
    };
    return _this;
  }

  _createClass(PayItemChooser, [{
    key: 'payItemName',
    value: function payItemName() {
      if (this.props.task != null && this.props.task.pay_item != null) {
        return this.props.task.pay_item.name;
      } else {
        return 'None Selected';
      }
    }
  }, {
    key: 'componentDidMount',
    value: function componentDidMount() {
      $('.task-pay-item').dropdown({
        fullTextSearch: true,
        onChange: function (value, text) {
          this.setState({ payItemId: value, payItemName: text });
          this.props.onPayItemSelected();
        }.bind(this)
      });
      $.ajax({
        url: '/home/pay_items.json',
        dataType: 'json',
        success: function (data) {
          this.setState({ payItems: data.pay_items });
        }.bind(this)
      });
    }
  }, {
    key: 'getText',
    value: function getText() {
      var hash = {
        earnings_rate_id: this.state.payItemId,
        name: this.state.payItemName
      };
      return hash;
    }
  }, {
    key: 'render',
    value: function render() {
      return React.createElement(
        'div',
        { className: 'ui bottom left pointing dropdown labeled icon button task-pay-item' },
        React.createElement('i', { className: 'icon dropdown' }),
        React.createElement(
          'span',
          { className: 'text' },
          this.payItemName()
        ),
        React.createElement(
          'div',
          { className: 'menu' },
          React.createElement(
            'div',
            { className: 'ui icon search input' },
            React.createElement('i', { className: 'search icon' }),
            React.createElement('input', { type: 'text', placeholder: 'Search Pay Item...' })
          ),
          React.createElement(
            'div',
            { className: 'scrolling menu' },
            this.state.payItems.map(function (payItem) {
              return React.createElement(
                'div',
                { className: 'item', 'data-value': payItem.earnings_rate_id, key: 'pay_item_' + payItem.earnings_rate_id },
                payItem.name
              );
            }, this)
          )
        )
      );
    }
  }]);

  return PayItemChooser;
}(React.Component);

PayItemChooser.contextTypes = {
  payItems: React.PropTypes.array
};

'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var React = require('react');
var moment = require('moment');

var TaskCard = exports.TaskCard = function (_React$Component) {
  _inherits(TaskCard, _React$Component);

  function TaskCard(props) {
    _classCallCheck(this, TaskCard);

    var _this = _possibleConstructorReturn(this, (TaskCard.__proto__ || Object.getPrototypeOf(TaskCard)).call(this, props));

    _this.state = {
      task: props.task,
      updateTreeView: false
    };
    _this.params = {
      id: _this.props.task.id,
      task: {}
    };
    _this.update = _this.update.bind(_this);
    _this.selectedParentId = _this.selectedParentId.bind(_this);
    return _this;
  }

  _createClass(TaskCard, [{
    key: 'priorityClass',
    value: function priorityClass() {
      var className = '';
      if (this.state.task.priority < 7) {
        className = 'orange';
      } else {
        className = 'red';
      }
      return 'warning ' + className + ' icon';
    }
  }, {
    key: 'componentWillReceiveProps',
    value: function componentWillReceiveProps(nextProps) {
      this.setState({ task: nextProps.task });
    }
  }, {
    key: 'update',
    value: function update() {
      if (this.state.updateTreeView) {
        this.props.updateTreeView(this.state.task.id);
      }
    }
  }, {
    key: 'taskUrl',
    value: function taskUrl() {
      return '/task/' + this.state.task.id + '?show_modal=true';
    }
  }, {
    key: 'getClassName',
    value: function getClassName() {
      return 'task_' + this.state.task.id;
    }
  }, {
    key: 'selectedParentId',
    value: function selectedParentId(id) {
      this.props.selectedParentId(id);
    }
  }, {
    key: 'handleParentBudget',
    value: function handleParentBudget(event) {
      if (this.props.task.parent != undefined || this.props.task.level == 1) {
        var result = confirm("Do you wish to decrease the parent’s task budget by the budget of the task we are moving?");
        this.params.task['decrease_old_parents_budget'] = result;
        this.checkParentBudget(event);
      }
    }
  }, {
    key: 'checkParentBudget',
    value: function checkParentBudget(event) {
      var data = {
        id: this.refs.parent_id.state.parentID,
        budgeted_cost: this.state.task.budgeted_cost
      };
      $.ajax({
        url: '/tasks/check_task_budget.json',
        method: 'GET',
        dataType: 'json',
        data: data,
        error: function (xhr, status, err) {
          if (xhr.status == 406) {
            var result = confirm("Do you wants to increase the budget by the amount of the task we are moving?");
            this.params.task['increase_new_parents_budget'] = result;
          } else if (xhr.status == 409) {
            var result = confirm("Do you wish to proceed before increasing the budget of the parent?");
            if (result == true) this.params.task['increase_new_parents_budget'] = false;else this.params.task['increase_new_parents_budget'] = true;
          }
          this.handleUpdate(event);
        }.bind(this)
      });
    }
  }, {
    key: 'handleBudgetUpdate',
    value: function handleBudgetUpdate(event) {
      var data = {
        id: this.props.task.id,
        task: {}
      };
      data.task['budgeted_cost'] = event;
      if (this.state.task.parent != undefined) {
        $.ajax({
          url: '/tasks/' + this.state.task.id + '/check_budget.json',
          method: 'GET',
          dataType: 'json',
          data: data,
          success: function (result) {
            this.setState({ task: result });
          }.bind(this),
          error: function (xhr, status, err) {
            if (xhr.status == 406) {
              var result = confirm("You've set the budget greater than the parent's. Do you want to continue?");
              if (result == true) {
                this.handleUpdate({ budgeted_cost: 'budgeted_cost', value: event });
              } else {
                this.refs['budgeted_cost'].reset();
              }
            } else if (xhr.status == 409) {
              var delta = parseInt(data.task['budgeted_cost']) - this.state.task.budgeted_cost;
              var result = false;
              if (delta < 1) {
                result = confirm("Do you wish to decrease the budget of the parent tasks" + " by $" + Math.abs(delta) + " the same amount ?");
                this.params.task['decrease_all_parents_budget'] = result;
                if (result == true) {
                  var parent_attrs = {};
                  parent_attrs['amount'] = this.state.task.budgeted_cost - parseInt(data.task['budgeted_cost']);
                  parent_attrs['parent_id'] = this.props.task.parent.id;
                  this.params.task['decrease_parents_budget_attributes'] = parent_attrs;
                }
              } else {
                result = confirm("Do you wish to increase the budget of " + this.state.task.parent.title + " by $" + delta + "?");
                this.params.task['increase_parents_budget'] = result;
                if (result == true) {
                  var parent_attrs = {};
                  parent_attrs['amount'] = parseInt(data.task['budgeted_cost']) - this.state.task.budgeted_cost;
                  parent_attrs['parent_id'] = this.props.task.parent.id;
                  this.params.task['increase_parents_budget_attributes'] = parent_attrs;
                }
              }
              this.handleUpdate({ budgeted_cost: 'budgeted_cost', value: event });
            } else {
              this.refs['budgeted_cost'].reset();
            }
          }.bind(this)
        });
      } else {
        this.handleUpdate({ budgeted_cost: 'budgeted_cost', value: event });
      }
    }
  }, {
    key: 'handleTargetUpdate',
    value: function handleTargetUpdate(event) {
      var data = {
        id: this.props.task.id,
        task: {}
      };
      data.task[event] = this.refs[event].getText();

      $.ajax({
        url: '/tasks/' + this.state.task.id + '/check_task_date_range.json',
        method: 'PUT',
        dataType: 'json',
        data: data,
        success: function (result) {
          this.setState({ task: result });
        }.bind(this),
        error: function (xhr, status, err) {
          if (xhr.status == 406) {
            var result = confirm("You've set the date out of the parent. Do you want to continue?");
            if (result == true) {
              this.handleUpdate(event);
            } else {
              this.refs[event].reset();
            }
          } else {
            alert(xhr.responseText);
            this.refs[event].reset();
          }
        }.bind(this)
      });
    }
  }, {
    key: 'handleUpdate',
    value: function handleUpdate(event) {
      if (event.budgeted_cost == "budgeted_cost") {
        var task = this.state.task;
        this.params.id = this.props.task.id;
        this.params.task[event.budgeted_cost] = event.value;
      } else {
        var task = this.state.task;
        this.params.id = this.props.task.id;
        this.params.task[event] = this.refs[event].getText();
      }
      $.ajax({
        url: '/tasks/' + this.state.task.id + '.json',
        method: 'PUT',
        dataType: 'json',
        data: this.params,
        success: function (result) {
          this.setState({ task: result, updateTreeView: true });
          if (event == 'title') {
            this.context.onTaskSelected(this.state.task);
            return alertify.success("Task title is changed.");
          }
          if (event == 'parent_id') {
            this.setState({ updateTreeView: true });
            this.context.onTaskSelected(this.state.task);
            this.update();
            return alertify.success("New Parent was assigned successfully.");
          }
          if (event.budgeted_cost == 'budgeted_cost') {
            return alertify.success("New Budget was assigned successfully.");
            this.setState({ task: result, updateTreeView: true });
          }
        }.bind(this),
        error: function (xhr, status, err) {
          this.setState({ updateTreeView: true });
          if (event.budgeted_cost == 'budgeted_cost') {
            this.setState({ task: this.state.task });
            alert(xhr.responseText);
          } else if (event == 'parent_id') {
            this.setState({ updateTreeView: true });
            this.context.onTaskSelected(this.state.task);
            this.update();
            return alertify.success("New Parent was assigned successfully.");
          }
        }.bind(this)
      });
    }
  }, {
    key: 'render',
    value: function render() {
      return React.createElement(
        'div',
        { className: 'ui card task-card task_view_overflow width_task_card' },
        React.createElement(
          'div',
          { className: 'content lft_flxgrw' },
          React.createElement(EditableField, {
            onSave: this.handleUpdate.bind(this, 'title'),
            text: this.state.task.title,
            ref: 'title',
            className: 'header task-title'
          })
        ),
        React.createElement(
          'div',
          { className: 'content cstm_card_cntnt' },
          React.createElement('div', { className: 'ui six row centered grid' }),
          React.createElement(
            'div',
            { className: 'ui list' },
            React.createElement(
              'div',
              null,
              React.createElement(
                'div',
                { className: 'content' },
                React.createElement(
                  'div',
                  { className: 'task-value' },
                  React.createElement(
                    'b',
                    null,
                    'Budget to be Allocated:'
                  ),
                  this.state.task.budget_to_be_allocated
                ),
                React.createElement(
                  'div',
                  null,
                  React.createElement(
                    'b',
                    null,
                    'Budget:'
                  ),
                  React.createElement(EditableBudgetField, {
                    onBudgetSave: this.handleBudgetUpdate.bind(this),
                    text: this.state.task.budgeted_cost,
                    ref: 'budgeted_cost',
                    className: 'task-value'
                  })
                ),
                React.createElement(
                  'div',
                  { className: 'task-value' },
                  React.createElement(
                    'b',
                    null,
                    'Budget Taken:'
                  ),
                  this.state.task.budget_taken
                ),
                React.createElement(
                  'div',
                  { className: 'task-value' },
                  React.createElement(
                    'b',
                    null,
                    'Budget Archived:'
                  ),
                  this.state.task.total_budget_of_archived_children
                ),
                React.createElement(
                  'div',
                  { className: 'task-value' },
                  React.createElement(
                    'b',
                    null,
                    'Actual Cost:'
                  ),
                  this.state.task.actual_cost
                ),
                React.createElement(
                  'div',
                  { className: 'task-value' },
                  React.createElement(
                    'b',
                    null,
                    'Completed:'
                  ),
                  this.state.task.task_completed,
                  '%'
                )
              )
            )
          ),
          React.createElement(
            'div',
            { className: 'ui list' },
            React.createElement(
              'div',
              { className: 'task-value' },
              React.createElement(
                'b',
                null,
                'Task Code:'
              ),
              this.state.task.task_code
            ),
            React.createElement(
              'div',
              null,
              React.createElement(
                'b',
                null,
                'Estimated Dates'
              ),
              React.createElement(
                'div',
                null,
                React.createElement(
                  'div',
                  null,
                  React.createElement(
                    'b',
                    null,
                    'Start:'
                  ),
                  React.createElement(EditableDatePicker, {
                    onSave: this.handleTargetUpdate.bind(this, 'target_start'),
                    text: moment(this.state.task.target_start).format('DD/MM/YYYY h:mm A'),
                    ref: 'target_start',
                    className: this.getClassName() + '_target_start task-value'
                  })
                ),
                React.createElement(
                  'div',
                  null,
                  React.createElement(
                    'b',
                    null,
                    'Finish:'
                  ),
                  React.createElement(EditableDatePicker, {
                    onSave: this.handleTargetUpdate.bind(this, 'target_finish'),
                    text: moment(this.state.task.target_finish).format('DD/MM/YYYY h:mm A'),
                    ref: 'target_finish',
                    className: this.getClassName() + '_target_finish task-value'
                  })
                )
              )
            )
          ),
          this.state.task.level == 1 ? React.createElement(
            'div',
            { className: 'row' },
            React.createElement(
              'b',
              null,
              'Project Code:'
            ),
            React.createElement(EditableField, {
              onSave: this.handleUpdate.bind(this, 'project_code'),
              text: this.state.task.project_code,
              ref: 'project_code',
              className: 'task-value'
            })
          ) : null,
          React.createElement(
            'div',
            { className: 'row task-value' },
            React.createElement(
              'b',
              null,
              'Pay rate:'
            ),
            this.state.task.pay_rate
          ),
          React.createElement(
            'div',
            { className: 'row' },
            React.createElement(
              'b',
              null,
              'Parent Task:'
            ),
            React.createElement(ParentTaskChooser, {
              task: this.state.task,
              parentTask: this.state.task.parent,
              selectedParentId: this.selectedParentId,
              onParentTaskSelected: this.handleParentBudget.bind(this, 'parent_id'),
              ref: 'parent_id'
            })
          ),
          React.createElement(
            'div',
            { className: 'pay-item row' },
            React.createElement(
              'b',
              null,
              'Pay Item:'
            ),
            React.createElement(PayItemChooser, {
              task: this.state.task,
              onPayItemSelected: this.handleUpdate.bind(this, 'pay_item_attributes'),
              ref: 'pay_item_attributes'
            })
          ),
          React.createElement(
            'div',
            { className: 'row' },
            React.createElement(
              'b',
              null,
              'Weekly report day:'
            ),
            React.createElement(WeekDayChooser, {
              task: this.state.task,
              onWeekDaySelected: this.handleUpdate.bind(this, 'weekly_reports_start'),
              ref: 'weekly_reports_start'
            })
          ),
          React.createElement(
            'div',
            { className: 'row' },
            React.createElement(
              'b',
              null,
              'Description: '
            ),
            '(Type description here and press SHIFT + enter \u2026)',
            React.createElement(EditableTextArea, {
              onSave: this.handleUpdate.bind(this, 'description'),
              text: this.state.task.description,
              ref: 'description',
              className: this.getClassName() + '_description'
            })
          )
        )
      );
    }
  }]);

  return TaskCard;
}(React.Component);

TaskCard.contextTypes = {
  onTaskSelected: React.PropTypes.func,
  reloadTask: React.PropTypes.func
};

'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var React = require('react');
var date_value = moment().format("DD-MM-YYYY , hh:mm:ss");

var TaskTreeView = exports.TaskTreeView = function (_React$Component) {
  _inherits(TaskTreeView, _React$Component);

  function TaskTreeView(props) {
    _classCallCheck(this, TaskTreeView);

    var _this = _possibleConstructorReturn(this, (TaskTreeView.__proto__ || Object.getPrototypeOf(TaskTreeView)).call(this, props));

    _this.state = {
      task: props.task,
      tasks: props.task.tasks,
      task_id: window.location.pathname.split('/')[2] ? window.location.pathname.split('/')[2] : null
    };
    _this.handleChange = _this.handleChange.bind(_this);
    _this.clickLogHours = _this.clickLogHours.bind(_this);
    _this.setTask = _this.setTask.bind(_this);
    return _this;
  }

  _createClass(TaskTreeView, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      var new_id = window.location.pathname.split('/')[2] ? window.location.pathname.split('/')[2] : null;
      if (new_id) {
        var self = this;
        $('.datepicker').datetimepicker({
          format: 'd/m/Y h:i',
          startDate: new Date(),
          useCurrent: false,
          closeOnDateSelect: true
        });
      }
      $('.datepicker').change(function () {
        date_value = $(this).val();
      });
      this.setState({
        date: date_value
      });
    }
  }, {
    key: 'componentWillReceiveProps',
    value: function componentWillReceiveProps(newProps) {
      this.setState({
        task: newProps.task
      });
    }
  }, {
    key: 'handleChange',
    value: function handleChange(e) {
      this.setState({
        date: e.target.value
      });
    }
  }, {
    key: 'changeDate',
    value: function changeDate(e) {
      date_value = e.target.value;
    }
  }, {
    key: 'clickLogHours',
    value: function clickLogHours(id) {
      this.props._treeId(id);
      date_value = moment().format("DD-MM-YYYY , hh:mm:ss");
      this.setState({
        date: moment().format("DD-MM-YYYY , hh:mm:ss")
      });
      /*this.props.loadHours();*/
      $.ajax({
        url: '/tasks/' + this.state.task_id + '.json',
        dataType: 'json',
        success: function (data) {
          this.setState({ task: data });
          this.setTask(data);
        }.bind(this)
      });
    }
  }, {
    key: 'setTask',
    value: function setTask(task) {
      task['tasks'] = task.tasks;
      this.setState({ task: task, tasks: task.tasks });
    }
  }, {
    key: 'getChildContext',
    value: function getChildContext() {
      return {
        onTaskItemSelected: function (task) {
          $.ajax({
            url: '/tasks/' + task.id + '/task_snapshot.json',
            dataType: 'json',
            success: function (data) {
              this.setState({ task: data });

              this.props.onTaskChanged(data);
            }.bind(this)
          });
        }.bind(this)
      };
    }
  }, {
    key: 'showTreeView',
    value: function showTreeView() {
      this.setState({ shown: true });
    }
  }, {
    key: 'getSelectedClassName',
    value: function getSelectedClassName(id) {
      if (id == this.state.task.id) {
        return 'selected-task';
      } else {
        return '';
      }
    }
  }, {
    key: 'hierarchyTasks',
    value: function hierarchyTasks(task) {
      var _this2 = this;

      if (task.children_count == 0) {
        return React.createElement(TaskTreeItem, { date: date_value, updateTask: this.props.updateTask, key: task.id, task: task, value: task.id, className: this.getSelectedClassName(task.id), ref: 'task_tree', onclick: function onclick() {
            _this2.clickLogHours(task.id);
          } });
      } else {
        return React.createElement(
          TaskTreeItem,
          { date: date_value, updateTask: this.props.updateTask, key: task.id, task: task, value: task.id, className: this.getSelectedClassName(task.id), ref: 'task_tree', onclick: function onclick() {
              _this2.clickLogHours(task.id);
            } },
          task.tasks.map(function (subTask) {
            subTask['toggled'] = false;
            if (subTask.id == this.state.task.id) {
              subTask['active_members'] = this.state.task['active_members'];
              subTask['toggled'] = true;
            }
            return this.hierarchyTasks(subTask);
          }, this)
        );
      }
    }
  }, {
    key: 'taskTree',
    value: function taskTree() {
      return React.createElement(
        'div',
        { className: 'column task_pad0 pad_lf6 task_info_col' },
        React.createElement(
          'div',
          { className: 'for_seprte fixe_date_col' },
          React.createElement('span', { className: 'date_view edit_view' }),
          React.createElement(
            'span',
            { className: 'task_title_col' },
            React.createElement(
              'span',
              { className: 'task_title' },
              this.state.tasks[0].title
            ),
            React.createElement(
              'span',
              null,
              this.state.tasks[0].hours_added
            )
          ),
          React.createElement(
            'span',
            { className: 'date_view clr_both' },
            'Date:'
          ),
          React.createElement('input', { id: 'datepicker', type: 'text', className: 'ipt_width datepicker', onChange: this.changeDate, onSelect: this.handleChange, value: date_value })
        ),
        this.hierarchyTasks(this.state.tasks[0])
      );
    }
  }, {
    key: 'render',
    value: function render() {
      return this.taskTree();
    }
  }]);

  return TaskTreeView;
}(React.Component);

TaskTreeView.childContextTypes = {
  onTaskItemSelected: React.PropTypes.func
};

'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var React = require('react');
var ReactDOM = require('react-dom');

var TaskModal = exports.TaskModal = function (_React$Component) {
  _inherits(TaskModal, _React$Component);

  function TaskModal(props, context) {
    _classCallCheck(this, TaskModal);

    var _this = _possibleConstructorReturn(this, (TaskModal.__proto__ || Object.getPrototypeOf(TaskModal)).call(this, props, context));

    _this.state = {
      task: props.task,
      extension: 'html',
      modalShown: false,
      loader: false,
      showModal: false,
      scroll: false,
      _id: '',
      reportParams: {
        created_after: moment().format("DD-MM-YYYY , H:i, defaultTime:'00:00'"),
        created_before: new Date(),
        user_id: '',
        task_id: window.location.pathname.split('/')[2] ? window.location.pathname.split('/')[2] : null,
        generate_pdf: true
      }
    };
    _this.updateTask = _this.updateTask.bind(_this);
    _this.taskData = _this.taskData.bind(_this);
    _this.getId = _this.getId.bind(_this);
    _this.updateTreeView = _this.updateTreeView.bind(_this);
    _this.selectedParentId = _this.selectedParentId.bind(_this);
    return _this;
  }

  _createClass(TaskModal, [{
    key: 'getChildContext',
    value: function getChildContext() {
      return {
        onTaskSelected: function (task) {
          $.ajax({
            url: 'tasks/' + task.id + '.json',
            dataType: 'json',
            success: function (data) {
              this.setState({ task: data });
            }.bind(this)
          });
        }.bind(this),
        reloadLogHourButton: function (status) {
          this.state.task.log_hours_enabled = status;
          this.forceUpdate();
        }.bind(this)
      };
    }
  }, {
    key: 'componentDidMount',
    value: function componentDidMount() {
      var new_id = window.location.pathname.split('/')[2] ? window.location.pathname.split('/')[2] : null;
      this.setState({ loader: true });
      if (new_id) {
        $.ajax({
          url: '/tasks/' + new_id + '.json',
          dataType: 'json',
          success: function (data) {
            this.setState({ task: data, id: '' });
          }.bind(this)
        });
      }
    }
  }, {
    key: 'taskData',
    value: function taskData() {
      var new_id = window.location.pathname.split('/')[2] ? window.location.pathname.split('/')[2] : null;
      if (new_id) {
        $.ajax({
          url: '/tasks/' + new_id + '.json',
          dataType: 'json',
          success: function (data) {
            this.setState({ task: data });
          }.bind(this)
        });
      }
    }
  }, {
    key: 'componentDidUpdate',
    value: function componentDidUpdate() {
      if (this.state.scroll == true) {
        var current_task_id = this.state.task ? this.state.task.id : '';
        // Id of the selected task
        var new_id = window.location.pathname.split('/')[2] ? window.location.pathname.split('/')[2] : null;

        // Scroll to selected task.

        if (this.state._id != '' && !this.state.scroll) {

          var top = $(".task-id-" + this.state._id).offset().top;
          var container = $(".task_info_col");
          container.scrollTop(top - 100 - container.offset().top + container.scrollTop());
        } else if (this.state.task != undefined && new_id != current_task_id) {

          var top = $(".task-id-" + this.state.task.id).offset().top;
          var container = $(".task_info_col");
          container.scrollTop(top - 30 - container.offset().top + container.scrollTop());
        }
      }
    }
  }, {
    key: 'deleteTask',
    value: function deleteTask() {
      var result = confirm('Do you want to delete this task?');
      if (result == true) {
        $.ajax({
          url: '/tasks/' + this.state.task.id + '/remove_task.json',
          method: 'PUT',
          type: 'json',
          data: {
            id: this.state.task.id
          },
          success: function () {}.bind(this)
        });
      }
    }
  }, {
    key: 'setTask',
    value: function setTask(task) {
      if (task.level == 1) {
        this.state.task.tasks['0'].active_members = task.active_members;
      }
      task['tasks'] = task.child_tasks;
      this.setState({ task: task, tasks: task.child_tasks ? task.child_tasks : this.state.tasks, scroll: true });
    }
  }, {
    key: 'updateTask',
    value: function updateTask(data) {
      var myParam = window.location.pathname.split('/')[2] ? window.location.pathname.split('/')[2] : null;
      $.ajax({
        url: '/tasks/' + myParam + '.json',
        dataType: 'json',
        success: function (data) {
          this.setState({ task: data });
        }.bind(this)
      });
    }
  }, {
    key: 'getId',
    value: function getId(id) {
      this.state._id = id, this.state.scroll = false;
    }
  }, {
    key: 'downloadURL',
    value: function downloadURL() {
      var str = [];
      var _params = this.state.reportParams;
      _params.user_id = localStorage.getItem('_id');
      for (var p in this.state.reportParams) {
        if (this.state.reportParams.hasOwnProperty(p)) {
          str.push(encodeURIComponent(p) + "=" + encodeURIComponent(this.state.reportParams[p]));
        }
      }return '/reports/budget_reports.' + this.state.extension + '?' + str.join('&');
    }
  }, {
    key: 'emptyModal',
    value: function emptyModal() {
      return React.createElement(
        'div',
        { className: 'ui modal task-view' },
        React.createElement('div', { className: 'header' })
      );
    }
  }, {
    key: 'selectedParentId',
    value: function selectedParentId(id) {
      if (this.state.task.level == 1) {
        window.location = '/tasks/' + id + '/show_task';
      }
    }
  }, {
    key: 'updateTreeView',
    value: function updateTreeView(id) {
      this.state._id = id, this.state.scroll = false;
      if (this.state.task.level != 1) {
        this.taskData();
      }
    }
  }, {
    key: 'taskModal',
    value: function taskModal() {
      var _this2 = this;

      var _id = null;
      if (this.state.task) {
        _id = this.state.task.id;
      }
      return React.createElement(
        'div',
        { className: 'ui' },
        React.createElement(
          'div',
          { className: 'header mtop40' },
          this.state.task ? this.state.task.title : '',
          ' ',
          React.createElement(
            'span',
            { className: 'ui blue label' },
            this.state.task ? this.state.task.task_code : ''
          ),
          React.createElement(
            'div',
            { className: 'ui button green delete-task' },
            React.createElement(
              'a',
              { target: '_blank', href: '' + this.downloadURL() },
              'Budget Report'
            )
          ),
          React.createElement(DeleteTaskButton, { onClick: this.deleteTask.bind(this), task: this.state.task })
        ),
        React.createElement(
          'div',
          { className: 'content task-modal-content' },
          this.state.task && React.createElement(
            'div',
            { className: 'ui two column very relaxed stackable grid task_grid' },
            React.createElement(
              'div',
              { className: 'column mkting_col', id: 'taskcard-flow' },
              React.createElement(TaskCard, { task: this.state.task, key: 'task_' + _id, updateTreeView: this.updateTreeView, selectedParentId: this.selectedParentId })
            ),
            React.createElement(TaskTreeView, { _treeId: this.getId, enable_logButton: this.state.task, updateTask: this.updateTask, task: this.state.task, key: 'task_tree_' + this.state.task.id, onTaskChanged: this.setTask.bind(this), loadHours: function loadHours() {
                _this2.taskData();
              } })
          )
        )
      );
    }
  }, {
    key: 'render',
    value: function render() {
      try {
        localStorage.setItem("_id", this.state.task ? this.state.task.current_user ? Object.values(this.state.task.current_user)[0] : null : null);
      } catch (err) {
        console.log(err);
      }
      var Id = window.location.pathname.split('/')[2] ? window.location.pathname.split('/')[2] : null;
      if (window.location.pathname === '/tasks/' + Id + '/show_task') {
        return this.taskModal();
      } else {
        return React.createElement('b', null);
      }
    }
  }]);

  return TaskModal;
}(React.Component);

TaskModal.childContextTypes = {
  reloadLogHourButton: React.PropTypes.func,
  onTaskSelected: React.PropTypes.func
};

TaskModal.contextTypes = { loadAllTasks: React.PropTypes.func };
TaskModal.PropTypes = { onTaskSelected: React.PropTypes.func };

var mountElement = document.getElementById('show-task-container');
if (mountElement != null) {
  ReactDOM.render(React.createElement(TaskModal, null), mountElement);
}

;'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var React = require('react');

var ToolBarView = exports.ToolBarView = function (_React$Component) {
  _inherits(ToolBarView, _React$Component);

  function ToolBarView(props) {
    _classCallCheck(this, ToolBarView);

    var _this = _possibleConstructorReturn(this, (ToolBarView.__proto__ || Object.getPrototypeOf(ToolBarView)).call(this, props));

    _this.state = {
      levelHash: _this.props.levelHash
    };
    return _this;
  }

  _createClass(ToolBarView, [{
    key: 'componentWillReceiveProps',
    value: function componentWillReceiveProps(newProps) {
      this.setState({
        levelHash: newProps.levelHash
      });
    }
  }, {
    key: 'componentDidMount',
    value: function componentDidMount() {
      var tablesort;

      $(".stats-dropdown, .order-dropdown").dropdown();

      $('.task-vital-stats').on('change', function () {
        var th = $('.task-vital-stats').val();
        var direction = $('.task-order-direction').val() || 'asc';
        this.props.onStatSelected(th, direction);
      }.bind(this));

      $('.task-order-direction').on('change', function () {
        var direction, selectedStat, th;
        selectedStat = $('.task-vital-stats').val();
        th = selectedStat ? selectedStat : 'title';
        direction = $('.task-order-direction').val();
        this.props.onStatSelected(th, direction);
      }.bind(this));

      $('.branch-filter').search({
        apiSettings: {
          url: '/tasks/search_tasks?term={query}'
        },
        fields: {
          results: 'tasks',
          title: 'title',
          description: 'description',
          id: 'id'
        },
        errors: {
          noResult: 'Result not found!'
        },
        onSelect: function (result, response) {
          this.context.filterBranchTasks({
            id: result.id
          });
        }.bind(this)
      });
      $('.branch-filter .prompt').on('keyup', function (e) {
        if ($(e.target).val() == '') {
          this.context.loadAllTasks();
        }
      }.bind(this));

      $('.pay-item').search({
        apiSettings: {
          url: '/pay_item_categories/search_pay_items?term={query}'
        },
        fields: {
          results: 'pay_items',
          title: 'name',
          id: 'id'
        },
        errors: {
          noResult: 'Result not found!'
        },
        onSelect: function (result, response) {
          this.context.filterPayitem({
            name: result.name
          });
        }.bind(this)
      });
      $('.branch-filter .prompt').on('keyup', function (e) {
        if ($(e.target).val() == '') {
          this.context.loadAllTasks();
        }
      }.bind(this));
    }
  }, {
    key: 'componentDidUpdate',
    value: function componentDidUpdate() {
      $('.task-filter-dropdown').dropdown({
        onChange: function (value) {
          this.props.onLevelFilter(value);
        }.bind(this)
      });

      $('.staff-filter-dropdown').dropdown({
        onChange: function (value) {
          this.props.onStaffFilter(value);
        }.bind(this)
      });

      $('.task-role-filter-dropdown').dropdown({
        onChange: function (value) {
          this.props.onTaskRoleFilter(value);
        }.bind(this)
      });
    }
  }, {
    key: 'render',
    value: function render() {
      return React.createElement(
        'div',
        { className: 'ui stackable grid' },
        React.createElement(
          'div',
          { className: 'four wide column' },
          React.createElement(
            'div',
            { className: 'ui selection dropdown stats-dropdown fluid' },
            React.createElement(
              'div',
              { className: 'default text' },
              'Vital stats'
            ),
            React.createElement('input', { type: 'hidden', name: 'selected-vital-stat', className: 'ui task-vital-stats' }),
            React.createElement('i', { className: 'dropdown icon' }),
            React.createElement(
              'div',
              { className: 'menu' },
              React.createElement(
                'div',
                { className: 'item', 'data-value': 'target_finish_int' },
                'Due Date'
              ),
              React.createElement(
                'div',
                { className: 'item', 'data-value': 'time_elapsed' },
                '% Time Elapsed'
              ),
              React.createElement(
                'div',
                { className: 'item', 'data-value': 'task_completed' },
                '% Task Competed'
              ),
              React.createElement(
                'div',
                { className: 'item', 'data-value': 'budget_spent_ratio' },
                '% Budget Spent'
              ),
              React.createElement(
                'div',
                { className: 'item', 'data-value': 'task_completed_over_budget_consumed' },
                '% Task Completed : % Budget Spent'
              ),
              React.createElement(
                'div',
                { className: 'item', 'data-value': 'task_completed_by_user' },
                '% Task Competed by Me'
              ),
              React.createElement(
                'div',
                { className: 'item', 'data-value': 'budget_spent_by_user' },
                '% Budget Spent by Me'
              )
            )
          )
        ),
        React.createElement(
          'div',
          { className: 'three wide column' },
          React.createElement(
            'div',
            { className: 'ui selection dropdown order-dropdown fluid' },
            React.createElement(
              'div',
              { className: 'default text' },
              'Order'
            ),
            React.createElement('input', { type: 'hidden', name: 'selected-vital-order', className: 'task-order-direction' }),
            React.createElement('i', { className: 'dropdown icon' }),
            React.createElement(
              'div',
              { className: 'menu' },
              React.createElement(
                'div',
                { className: 'item', 'data-value': 'asc' },
                'Ascending'
              ),
              React.createElement(
                'div',
                { className: 'item', 'data-value': 'desc' },
                'Descending'
              )
            )
          )
        ),
        React.createElement(
          'div',
          { className: 'three wide column' },
          React.createElement(
            'div',
            { className: 'ui selection dropdown task-filter-dropdown fluid' },
            React.createElement(
              'div',
              { className: 'default text' },
              'Filter by Level'
            ),
            React.createElement('input', { type: 'hidden', name: 'selected-vital-order', className: 'selected-task-level' }),
            React.createElement('i', { className: 'dropdown icon' }),
            React.createElement(
              'div',
              { className: 'menu' },
              this.props.levelHash.map(function (level) {
                return React.createElement(
                  'div',
                  { className: 'item', 'data-value': level.value, key: 'level_' + level.value },
                  level.key
                );
              }, this)
            )
          )
        ),
        React.createElement(
          'div',
          { className: 'three wide column' },
          React.createElement(
            'div',
            { className: 'ui selection search dropdown staff-filter-dropdown fluid' },
            React.createElement('i', { className: 'search icon' }),
            React.createElement(
              'div',
              { className: 'default text' },
              'Filter by Staff...'
            ),
            React.createElement('input', { type: 'hidden', name: 'selected-staff', className: 'selected-staff' }),
            React.createElement(
              'div',
              { className: 'menu' },
              React.createElement(
                'div',
                { className: 'item', 'data-value': 0, key: 'user_0' },
                'All Staff'
              ),
              this.context.users.map(function (user) {
                return React.createElement(
                  'div',
                  { className: 'item', 'data-value': user.id, key: 'user_' + user.id },
                  user.full_name
                );
              }, this)
            )
          )
        ),
        React.createElement(
          'div',
          { className: 'three wide column' },
          React.createElement(
            'div',
            { className: 'ui selection dropdown task-role-filter-dropdown fluid' },
            React.createElement(
              'div',
              { className: 'default text' },
              'Filter by Role'
            ),
            React.createElement('input', { type: 'hidden', name: 'selected-task-role', className: 'selected-task-role' }),
            React.createElement('i', { className: 'dropdown icon' }),
            React.createElement(
              'div',
              { className: 'menu' },
              React.createElement(
                'div',
                { className: 'item', 'data-value': '' },
                'Any'
              ),
              React.createElement(
                'div',
                { className: 'item', 'data-value': 'task_owner' },
                'TM'
              ),
              React.createElement(
                'div',
                { className: 'item', 'data-value': 'escalation_manager' },
                'EM'
              ),
              React.createElement(
                'div',
                { className: 'item', 'data-value': 'member' },
                'Member'
              )
            )
          )
        ),
        React.createElement(
          'div',
          { className: 'sixteen wide column' },
          React.createElement(
            'div',
            { className: 'ui fluid branch-row-filters' },
            React.createElement(
              'div',
              { className: 'ui fuild category search selection branch-filter' },
              React.createElement(
                'div',
                { className: 'ui icon input' },
                React.createElement('input', { className: 'prompt', placeholder: 'Filter by Branch...' }),
                React.createElement('i', { className: 'search icon' })
              )
            )
          )
        ),
        React.createElement(
          'div',
          { className: 'four wide column' },
          React.createElement(
            'div',
            { className: 'ui fluid branch-row-filters' },
            React.createElement(
              'div',
              { className: 'ui fuild category search selection pay-item' },
              React.createElement(
                'div',
                { className: 'ui icon input' },
                React.createElement('input', { className: 'prompt', placeholder: 'Filter by Pay item...' }),
                React.createElement('i', { className: 'search icon' })
              )
            )
          )
        )
      );
    }
  }]);

  return ToolBarView;
}(React.Component);

ToolBarView.contextTypes = {
  users: React.PropTypes.array,
  filterBranchTasks: React.PropTypes.func,
  filterPayitem: React.PropTypes.func,
  loadAllTasks: React.PropTypes.func
};

'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var React = require('react');
var ReactDOM = require('react-dom');
var task_path;

var TaskRow = exports.TaskRow = function (_React$Component) {
  _inherits(TaskRow, _React$Component);

  function TaskRow(props) {
    _classCallCheck(this, TaskRow);

    var _this = _possibleConstructorReturn(this, (TaskRow.__proto__ || Object.getPrototypeOf(TaskRow)).call(this, props));

    _this.state = {
      task: props.task,
      loading: false,
      showModal: false
    };
    return _this;
  }

  _createClass(TaskRow, [{
    key: 'loadingClass',
    value: function loadingClass() {
      var baseClass = 'ui inline mini loader';
      if (this.state.loading == true) {
        baseClass = baseClass + ' active';
      }
      return baseClass;
    }
  }, {
    key: 'updateData',
    value: function updateData(taskData) {
      this.setState({ task: taskData });
    }

    /*onTaskClicked(taskID, e){
      e.preventDefault();
      this.setState({loading: true});
      $.ajax({
        url: 'tasks/'+ taskID + '.json',
        dataType: 'json',
        success: function(data) {
          localStorage.setItem('taskData', data);
          this.props.onTaskSelected({task: data, modalShown: true});
          this.setState({loading: false});
        }.bind(this)
      });
    }*/

  }, {
    key: 'render',
    value: function render() {
      return React.createElement(
        'tr',
        { id: 'tasks_in' },
        React.createElement(
          'td',
          null,
          React.createElement(
            'a',
            { className: 'main_id', href: '/tasks/' + this.state.task.id + '/show_task' },
            React.createElement('div', null),
            this.state.task.title
          )
        ),
        React.createElement(
          'td',
          { className: 'collapsing', 'data-sort-value': this.state.task.target_finish_int },
          moment(this.state.task.target_finish).format('DD/MM/YYYY')
        ),
        React.createElement(
          'td',
          { className: 'collapsing' },
          this.state.task.time_elapsed,
          '%'
        ),
        React.createElement(
          'td',
          { className: 'collapsing' },
          this.state.task.task_completed,
          '%'
        ),
        React.createElement(
          'td',
          { className: 'collapsing' },
          this.state.task.budget_spent_ratio,
          '%'
        ),
        React.createElement(
          'td',
          null,
          this.state.task.task_completed_over_budget_consumed
        ),
        React.createElement(
          'td',
          null,
          this.state.task.task_completed_by_user,
          '%'
        ),
        React.createElement(
          'td',
          null,
          this.state.task.budget_spent_ratio_by_user,
          '%'
        ),
        React.createElement(
          'td',
          null,
          this.state.task.task_completed_over_budget_consumed_by_user
        )
      );
    }
  }]);

  return TaskRow;
}(React.Component);

'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var React = require('react');
var moment = require('moment');
var ReactDOM = require('react-dom');
var ReactPaginate = require('react-paginate');
var DEFAULT_USER = 0;

var VitalTasks = exports.VitalTasks = function (_React$Component) {
  _inherits(VitalTasks, _React$Component);

  function VitalTasks(props) {
    _classCallCheck(this, VitalTasks);

    var _this = _possibleConstructorReturn(this, (VitalTasks.__proto__ || Object.getPrototypeOf(VitalTasks)).call(this, props));

    _this.state = {
      offset: 0,
      tasks: _this.props.tasks,
      filteredTasks: _this.props.tasks,
      levelHash: _this.props.levelHash,
      task: null,
      currentFilterLevel: _this.props.currentFilterLevel,
      currentFilterUser: DEFAULT_USER,
      currentTaskRole: '',
      users: [],
      current_taskId: null,
      payItems: [],
      current_task: {},
      statSchema: [{
        title: 'Task',
        value: 'title',
        className: 'title',
        order: 'asc'
      }, {
        title: 'Due Date',
        value: 'target_finish_int',
        className: 'due-date',
        order: 'asc'
      }, {
        title: '% Time Elapsed(TE)',
        value: 'time_elapsed',
        className: 'time-elapsed',
        order: 'asc'
      }, {
        title: '% Task Competed(TC)',
        value: 'task_completed',
        className: 'task-completion',
        order: 'asc'
      }, {
        title: '% Budget Used(BU)',
        value: 'budget_spent_ratio',
        className: 'budget-spent',
        order: 'asc'
      }, {
        title: '% TC : % BU',
        value: 'task_completed_over_budget_consumed',
        className: 'task-budget-ratio',
        order: 'asc'
      }, {
        title: '% TC by Me',
        value: 'task_completed_by_user',
        className: 'task-self-complete',
        order: 'asc'
      }, {
        title: '% BU by Me',
        value: 'budget_spent_by_user',
        className: 'budget-self-spent',
        order: 'asc'
      }, {
        title: 'My % TC : % BU',
        value: 'task_completed_over_budget_consumed_by_user',
        className: 'self-task-budget-ratio',
        order: 'asc'
      }]
    };
    _this.onTaskSelected = _this.onTaskSelected.bind(_this);
    return _this;
  }

  _createClass(VitalTasks, [{
    key: 'getChildContext',
    value: function getChildContext() {
      return {
        users: this.state.users,
        reloadTask: function (task) {
          var taskRow = this.refs['task_row_' + task.id];
          if (taskRow != null) {
            taskRow.updateData(task);
          }
        }.bind(this)
      };
    }
  }, {
    key: 'componentDidMount',
    value: function componentDidMount() {
      var taskId = window.location.pathname.split('/')[2] ? window.location.pathname.split('/')[2] : null;
      if (taskId) {
        $.ajax({
          url: 'tasks/' + taskId + '.json',
          dataType: 'json',
          success: function (data) {
            this.onTaskSelected({ task: data, modalShown: true });
          }.bind(this)
        });
      }
    }
  }, {
    key: 'componentWillReceiveProps',
    value: function componentWillReceiveProps(newProps) {
      this.setState({
        filteredTasks: newProps.tasks
      });
    }
  }, {
    key: 'handleSort',
    value: function handleSort(key, direction) {
      this.setState({
        filteredTasks: this.state.filteredTasks.sort(function (task1, task2) {
          var value1 = task1[key];
          var value2 = task2[key];
          if (direction == 'desc') {
            var _ref = [value2, value1];
            value1 = _ref[0];
            value2 = _ref[1];
          }
          if (value1 < value2) {
            return -1;
          } else {
            return 1;
          }
        }.bind(this))
      });
    }
  }, {
    key: 'componentWillUpdate',
    value: function componentWillUpdate(newProps, newStates) {
      if (newStates.currentFilterLevel != this.state.currentFilterLevel || newStates.currentFilterUser != this.state.currentFilterUser || newStates.currentTaskRole != this.state.currentTaskRole) {
        var queryParams = { task: { task_users: {} } };
        if (newStates.currentFilterLevel > 0) {
          queryParams.task.depth = newStates.currentFilterLevel - 1;
        }

        if (newStates.currentFilterLevel == 'Leaflet') {
          queryParams.task.is_leaf = true;
        }

        if (newStates.currentFilterUser > 0) {
          queryParams.task.task_users['user_id'] = newStates.currentFilterUser;
        }

        if (newStates.currentTaskRole != '') {
          queryParams.task.task_users['task_role'] = newStates.currentTaskRole;
        }

        this.context.filterQueryTasks(queryParams);
      }
    }
  }, {
    key: 'handleLevelFilter',
    value: function handleLevelFilter(level) {
      this.setState({
        currentFilterLevel: level
      });
    }
  }, {
    key: 'handleStaffFilter',
    value: function handleStaffFilter(userID) {
      this.setState({
        currentFilterUser: Number(userID)
      });
    }
  }, {
    key: 'handleTaskRoleFilter',
    value: function handleTaskRoleFilter(role) {
      this.setState({
        currentTaskRole: role
      });
    }
  }, {
    key: 'onTaskDeleted',
    value: function onTaskDeleted(deletedTask) {
      var deletedIndex = -1;
      this.state.filteredTasks.some(function (task) {
        if (task.id == deletedTask.id) {
          deletedIndex = this.state.filteredTasks.indexOf(task);
          return true;
        }
      }.bind(this));

      if (deletedIndex > -1) {
        this.state.filteredTasks.splice(deletedIndex, 1);
      }

      this.refs.task_modal.state.modalShown = false;
      $('.ui.modal.task-view').modal('hide');
      this.forceUpdate();
    }
  }, {
    key: 'onTitleClicked',
    value: function onTitleClicked(stat) {
      this.handleSort(stat.value, stat.order);
      if (stat.order == 'asc') {
        stat.order = 'desc';
      } else {
        stat.order = 'asc';
      }
    }
  }, {
    key: 'onTaskSelected',
    value: function onTaskSelected(hash) {
      this.refs.task_modal.setState(hash);
    }
  }, {
    key: 'handlePageClick',
    value: function handlePageClick(data) {
      var selected = data.selected;
      var offset = Math.ceil(selected);
      this.setState({
        offset: data.selected
      });
      if (this.props.filter == 'request_vital_tasks') {
        this.requestVitalTasks(offset);
      } else if (this.props.filter == 'filter_branch_tasks') {
        this.filterBranchTasks(offset);
      } else if (this.props.filter == 'filter_pay_item') {
        this.filterPayitem(offset);
      } else if (this.props.filter == 'filter_query_tasks') {
        this.filterQueryTasks(offset);
      }
    }
  }, {
    key: 'filterBranchTasks',
    value: function filterBranchTasks(data) {
      $('#task_view_outer_wrapper .dimmer').dimmer('show');
      $.ajax({
        url: '/tasks/task_vital_tasks.json',
        dataType: 'json',
        data: {
          page: data + 1,
          per_page: 20
        },
        success: function (data) {
          this.state.currentUser = data.current_user;
          this.setState({
            tasks: data.tasks,
            filteredTasks: data.tasks,
            filter: 'filter_branch_tasks',
            levelHash: data.available_levels
          });
          $('#task_view_outer_wrapper .dimmer').dimmer('hide');
        }.bind(this),
        error: function (xhr, status, err) {}.bind(this)
      });
    }
  }, {
    key: 'filterPayitem',
    value: function filterPayitem(data) {
      $('#task_view_outer_wrapper .dimmer').dimmer('show');
      $.ajax({
        url: '/tasks/task_pay_item.json',
        dataType: 'json',
        data: {
          page: data + 1,
          per_page: 20
        },
        success: function (data) {
          this.state.currentUser = data.current_user;
          this.setState({
            tasks: data.tasks,
            filteredTasks: data.tasks,
            filter: 'filter_pay_item',
            levelHash: data.available_levels
          });
          $('#task_view_outer_wrapper .dimmer').dimmer('hide');
        }.bind(this),
        error: function (xhr, status, err) {}.bind(this)
      });
    }
  }, {
    key: 'filterQueryTasks',
    value: function filterQueryTasks(data) {
      $('#task_view_outer_wrapper .dimmer').dimmer('show');
      $.ajax({
        url: '/tasks/filter_tasks.json',
        dataType: 'json',
        data: {
          task: this.props.data.task,
          page: data + 1,
          per_page: 20
        },
        success: function (data) {
          this.state.currentUser = data.current_user;
          this.setState({
            tasks: data.tasks,
            filteredTasks: data.tasks,
            filter: 'filter_query_tasks',
            levelHash: data.available_levels
          });
          $('#task_view_outer_wrapper .dimmer').dimmer('hide');
        }.bind(this),
        error: function (xhr, status, err) {}.bind(this)
      });
    }
  }, {
    key: 'requestVitalTasks',
    value: function requestVitalTasks(data) {
      $('#task_view_outer_wrapper .dimmer').dimmer('show');
      $.ajax({
        url: '/tasks/task_vital_tasks.json',
        dataType: 'json',
        data: {
          page: data + 1,
          per_page: 20
        },
        success: function (data) {
          $('#task_view_outer_wrapper .dimmer').removeClass('active');
          this.state.currentUser = data.current_user;
          this.setState({
            tasks: data.tasks,
            filteredTasks: data.tasks
          });
          $('#task_view_outer_wrapper .dimmer').dimmer('hide');
        }.bind(this),
        error: function (xhr, status, err) {}.bind(this)
      });
    }
  }, {
    key: 'render',
    value: function render() {
      return React.createElement(
        'div',
        { id: 'task_view_wrapper' },
        React.createElement(TaskModal, { task: this.state.task, ref: 'task_modal', currentId: this.state.current_taskId, onTaskDeleted: this.onTaskDeleted.bind(this), onTaskSelected: this.onTaskSelected }),
        React.createElement(ToolBarView, {
          levelHash: this.props.levelHash,
          onLevelFilter: this.handleLevelFilter.bind(this),
          onStaffFilter: this.handleStaffFilter.bind(this),
          onTaskRoleFilter: this.handleTaskRoleFilter.bind(this),
          onStatSelected: this.handleSort.bind(this),
          ref: 'tool_bar' }),
        React.createElement(ReactPaginate, { previousLabel: "previous",
          nextLabel: "next", breakLabel: React.createElement(
            'a',
            { href: 'javascript:void(0)' },
            '...'
          ),
          breakClassName: "break-me",
          pageCount: this.props.total_tasks ? Math.ceil(this.props.total_tasks / 20) : 1,
          marginPagesDisplayed: 3,
          pageRangeDisplayed: 2,
          onPageChange: this.handlePageClick.bind(this),
          containerClassName: "pagination cursr_pointr",
          subContainerClassName: "pages pagination",
          activeClassName: "active"
        }),
        React.createElement(
          'table',
          { className: 'ui celled table selectable tasks-table' },
          React.createElement(
            'thead',
            null,
            React.createElement(
              'tr',
              null,
              this.state.statSchema.map(function (stat) {
                return React.createElement(
                  'th',
                  { className: stat.className,
                    key: 'vital_header_' + stat.className,
                    onClick: this.onTitleClicked.bind(this, stat) },
                  stat.title
                );
              }, this)
            )
          ),
          React.createElement(
            'tbody',
            null,
            this.state.filteredTasks.map(function (task) {
              return React.createElement(TaskRow, { task: task, key: 'task_row_' + task.id, ref: 'task_row_' + task.id });
            }, this)
          )
        )
      );
    }
  }]);

  return VitalTasks;
}(React.Component);

VitalTasks.childContextTypes = {
  users: React.PropTypes.array,
  reloadTask: React.PropTypes.func,
  currentId: React.PropTypes.number
};

VitalTasks.contextTypes = {
  filterQueryTasks: React.PropTypes.func,
  loadAllTasks: React.PropTypes.func
};

'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var React = require('react');
var ReactDOM = require('react-dom');

var MainView = exports.MainView = function (_React$Component) {
  _inherits(MainView, _React$Component);

  function MainView(props) {
    _classCallCheck(this, MainView);

    var _this = _possibleConstructorReturn(this, (MainView.__proto__ || Object.getPrototypeOf(MainView)).call(this, props));

    _this.state = {
      data: '',
      total_tasks: 0,
      filter: '',
      tasks: [],
      levelHash: [],
      currentUser: null,
      currentFilterLevel: 1
    };
    return _this;
  }

  _createClass(MainView, [{
    key: 'getChildContext',
    value: function getChildContext() {
      return {
        loadAllTasks: function (task) {
          this.requestVitalTasks();
        }.bind(this),
        filterBranchTasks: function (data) {
          this.filterBranchTasks(data);
        }.bind(this),
        filterPayitem: function (data) {
          this.filterPayitem(data);
        }.bind(this),
        filterQueryTasks: function (data) {
          this.filterQueryTasks(data);
        }.bind(this),
        currentUser: this.state.currentUser
      };
    }
  }, {
    key: 'filterBranchTasks',
    value: function filterBranchTasks(data) {
      this.setState({ data: data });
      $('#task_view_outer_wrapper .dimmer').dimmer('show');
      $.ajax({
        url: '/tasks/task_vital_tasks.json',
        dataType: 'json',
        data: data,
        success: function (data) {
          this.state.currentUser = data.current_user;
          this.setState({
            total_tasks: data.total_tasks_count,
            filter: 'filter_branch_tasks',
            tasks: data.tasks,
            levelHash: data.available_levels
          });
          $('#task_view_outer_wrapper .dimmer').dimmer('hide');
        }.bind(this),
        error: function (xhr, status, err) {}.bind(this)
      });
    }
  }, {
    key: 'filterPayitem',
    value: function filterPayitem(data) {
      this.setState({ data: data });
      $('#task_view_outer_wrapper .dimmer').dimmer('show');
      $.ajax({
        url: '/tasks/task_pay_item.json',
        dataType: 'json',
        data: data,
        success: function (data) {
          this.state.currentUser = data.current_user;
          this.setState({
            total_tasks: data.total_tasks_count,
            filter: 'filter_pay_item',
            tasks: data.tasks,
            levelHash: data.available_levels
          });
          $('#task_view_outer_wrapper .dimmer').dimmer('hide');
        }.bind(this),
        error: function (xhr, status, err) {}.bind(this)
      });
    }
  }, {
    key: 'filterQueryTasks',
    value: function filterQueryTasks(data) {
      this.setState({ data: data });
      $('#task_view_outer_wrapper .dimmer').dimmer('show');
      $.ajax({
        url: '/tasks/filter_tasks.json',
        dataType: 'json',
        data: data,
        success: function (data) {
          this.state.currentUser = data.current_user;
          this.setState({
            total_tasks: data.total_tasks_count,
            filter: 'filter_query_tasks',
            tasks: data.tasks,
            levelHash: data.available_levels
          });
          $('#task_view_outer_wrapper .dimmer').dimmer('hide');
        }.bind(this),
        error: function (xhr, status, err) {}.bind(this)
      });
    }
  }, {
    key: 'componentWillMount',
    value: function componentWillMount() {
      this.requestVitalTasks();
    }
  }, {
    key: 'requestVitalTasks',
    value: function requestVitalTasks() {
      $('#task_view_outer_wrapper .dimmer').dimmer('show');
      $.ajax({
        url: '/tasks/task_vital_tasks.json',
        dataType: 'json',
        data: {
          page: 1,
          per_page: 20
        },
        success: function (data) {
          $('#task_view_outer_wrapper .dimmer').removeClass('active');
          this.state.currentUser = data.current_user;
          this.setState({
            total_tasks: data.total_tasks_count,
            filter: 'request_vital_tasks',
            tasks: data.tasks,
            levelHash: data.available_levels
          });
          $('#task_view_outer_wrapper .dimmer').dimmer('hide');
        }.bind(this),
        error: function (xhr, status, err) {}.bind(this)
      });
    }
  }, {
    key: 'render',
    value: function render() {
      return React.createElement(
        'div',
        { id: 'task_view_outer_wrapper', className: 'ui segment' },
        React.createElement(VitalTasks, { tasks: this.state.tasks, levelHash: this.state.levelHash, ref: 'vital_tasks', currentFilterLevel: this.state.currentFilterLevel, total_tasks: this.state.total_tasks, filter: this.state.filter, data: this.state.data }),
        React.createElement(
          'div',
          { className: 'ui active inverted dimmer' },
          React.createElement(
            'div',
            { className: 'ui text loader' },
            'Loading Tasks ...'
          )
        )
      );
    }
  }]);

  return MainView;
}(React.Component);

MainView.childContextTypes = {
  loadAllTasks: React.PropTypes.func,
  filterQueryTasks: React.PropTypes.func,
  filterBranchTasks: React.PropTypes.func,
  filterPayitem: React.PropTypes.func,
  currentUser: React.PropTypes.object
};

var mountElement = document.getElementById('project_cards_container');
if (mountElement != null) {
  ReactDOM.render(React.createElement(MainView, null), mountElement);
}

;'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var React = require('react');
var moment = require('moment');

var WorkPeriodRow = exports.WorkPeriodRow = function (_React$Component) {
  _inherits(WorkPeriodRow, _React$Component);

  function WorkPeriodRow(props) {
    _classCallCheck(this, WorkPeriodRow);

    var _this = _possibleConstructorReturn(this, (WorkPeriodRow.__proto__ || Object.getPrototypeOf(WorkPeriodRow)).call(this, props));

    _this.state = {
      workPeriod: props.workPeriod
    };
    return _this;
  }

  _createClass(WorkPeriodRow, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      $('.popup-control').popup();
    }
  }, {
    key: 'onDeleteBtnClicked',
    value: function onDeleteBtnClicked(e) {
      e.preventDefault();
      if (this.state.workPeriod.edit_hour_enabled) {
        this.props.onWorkPeriodDelete();
      } else {
        alert('You cannot delete these hours because they are in the locked previous pay cycle.');
      }
    }
  }, {
    key: 'onUpdate',
    value: function onUpdate(key) {
      if (key == 'amount_hours') {
        var preBudget = (parseFloat(this.refs[key].state.text) - parseFloat(this.state.workPeriod.amount_hours)) * parseFloat(this.state.workPeriod.pay_rate) + parseFloat(this.state.workPeriod.actual_cost);
        if (preBudget > parseFloat(this.state.workPeriod.budgeted_cost)) {
          var result = confirm('The edited hours logged will exceed the task budget. Continue?');
          if (result == true) {
            this.updateWorkPeriod(key);
          } else {
            this.refs[key].state.text = this.state.workPeriod.amount_hours;
            this.forceUpdate();
          }
        } else {
          this.updateWorkPeriod(key);
        }
      } else {
        this.updateWorkPeriod(key);
      }
    }
  }, {
    key: 'updateWorkPeriod',
    value: function updateWorkPeriod(key) {
      var hash = {
        id: this.state.workPeriod.id,
        work_period: {}
      };
      hash.work_period[key] = this.refs[key].state.text;
      this.setState({ work_period: { "#{key}": parseInt(this.refs[key].state.text) } });
      this.props.onWorkPeriodUpdate(hash);
    }
  }, {
    key: 'hoursValidator',
    value: function hoursValidator(text) {
      return isNaN(text) == false && parseInt('0' + text, 10) > 0;
    }
  }, {
    key: 'render',
    value: function render() {
      return React.createElement(
        'tr',
        { className: 'popup-control', 'data-content': this.state.workPeriod.description },
        React.createElement(
          'td',
          null,
          this.state.workPeriod.task_title
        ),
        React.createElement(
          'td',
          null,
          moment(this.state.workPeriod.date_worked).format('DD/MM/YYYY')
        ),
        React.createElement(
          'td',
          null,
          React.createElement(EditableField, {
            onSave: this.onUpdate.bind(this, 'amount_hours'),
            text: this.state.workPeriod.amount_hours,
            ref: 'amount_hours',
            className: 'amount-hours',
            enabled: this.state.workPeriod.edit_hour_enabled,
            validate: this.hoursValidator
          })
        ),
        React.createElement(
          'td',
          { className: 'right aligned collapsing' },
          React.createElement(
            'a',
            { href: '#', onClick: this.onDeleteBtnClicked.bind(this), className: 'ui red small button' },
            'Delete'
          )
        )
      );
    }
  }]);

  return WorkPeriodRow;
}(React.Component);

'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var React = require('react');

var WorkPeriodFilter = exports.WorkPeriodFilter = function (_React$Component) {
  _inherits(WorkPeriodFilter, _React$Component);

  function WorkPeriodFilter(props) {
    _classCallCheck(this, WorkPeriodFilter);

    var _this = _possibleConstructorReturn(this, (WorkPeriodFilter.__proto__ || Object.getPrototypeOf(WorkPeriodFilter)).call(this, props));

    _this.state = {
      work_period: {
        start_date: '',
        end_date: '',
        user_id: ''
      }
    };
    return _this;
  }

  _createClass(WorkPeriodFilter, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      $('.start-date-filter').on('change', function () {
        this.handleChange('start_date');
      }.bind(this));

      $('.end-date-filter').on('change', function () {
        this.handleChange('end_date');
      }.bind(this));
    }
  }, {
    key: 'handleChange',
    value: function handleChange(attr) {
      this.state.work_period[attr] = this.refs[attr].value;
      this.props.onFilterChanged(this.state.work_period);
    }
  }, {
    key: 'resetWorkPeriods',
    value: function resetWorkPeriods() {
      $('.start-date-filter').val('');
      $('.end-date-filter').val('');

      this.props.onFilterChanged({
        start_date: null,
        end_date: null,
        user_id: null
      });
    }
  }, {
    key: 'render',
    value: function render() {
      return React.createElement(
        'div',
        { className: 'ui grid' },
        React.createElement(
          'div',
          { className: 'four wide column' },
          React.createElement(
            'div',
            { className: 'ui fluid input' },
            React.createElement('input', { className: 'datepicker start-date-filter',
              ref: 'start_date',
              placeholder: 'From ...'
            })
          )
        ),
        React.createElement(
          'div',
          { className: 'four wide column' },
          React.createElement(
            'div',
            { className: 'ui fluid input' },
            React.createElement('input', { className: 'datepicker end-date-filter',
              ref: 'end_date',
              placeholder: 'To ...'
            })
          )
        ),
        React.createElement(
          'div',
          { className: 'two wide column' },
          React.createElement(
            'button',
            { onClick: this.resetWorkPeriods.bind(this), className: 'ui fluid button green' },
            'Reset'
          )
        )
      );
    }
  }]);

  return WorkPeriodFilter;
}(React.Component);

'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var React = require('react');

var WorkPeriods = exports.WorkPeriods = function (_React$Component) {
  _inherits(WorkPeriods, _React$Component);

  function WorkPeriods(props) {
    _classCallCheck(this, WorkPeriods);

    var _this = _possibleConstructorReturn(this, (WorkPeriods.__proto__ || Object.getPrototypeOf(WorkPeriods)).call(this, props));

    _this.state = {
      workPeriods: props.workPeriods
    };
    return _this;
  }

  _createClass(WorkPeriods, [{
    key: 'componentWillReceiveProps',
    value: function componentWillReceiveProps(newProps) {
      this.state = newProps;
      this.forceUpdate();
    }
  }, {
    key: 'deleteWorkPeriod',
    value: function deleteWorkPeriod(workPeriodID, arrayIndex) {
      var result = confirm('Do you want to delete this work period?');
      if (result == true) {
        $.ajax({
          url: 'work_periods/' + workPeriodID + '.json',
          method: 'DELETE',
          type: 'json',
          data: {
            id: workPeriodID
          },
          success: function () {
            this.state.workPeriods.splice(arrayIndex, 1);
            this.forceUpdate();
          }.bind(this)
        });
      }
    }
  }, {
    key: 'handleUpdate',
    value: function handleUpdate(data) {
      $.ajax({
        url: 'work_periods/' + data.id + '.json',
        method: 'PUT',
        type: 'json',
        data: data,
        success: function () {}.bind(this)
      });
    }
  }, {
    key: 'render',
    value: function render() {
      return React.createElement(
        'table',
        { className: 'ui celled table' },
        React.createElement(
          'thead',
          null,
          React.createElement(
            'tr',
            null,
            React.createElement(
              'th',
              null,
              'Task'
            ),
            React.createElement(
              'th',
              null,
              'Date'
            ),
            React.createElement(
              'th',
              null,
              'Hour Logged'
            ),
            React.createElement(
              'th',
              null,
              'Actions'
            )
          )
        ),
        React.createElement(
          'tbody',
          null,
          this.state.workPeriods.map(function (workPeriod, index) {
            return React.createElement(WorkPeriodRow, { workPeriod: workPeriod, key: 'work_period_' + workPeriod.id,
              onWorkPeriodDelete: this.deleteWorkPeriod.bind(this, workPeriod.id, index),
              onWorkPeriodUpdate: this.handleUpdate.bind(this)
            });
          }, this)
        )
      );
    }
  }]);

  return WorkPeriods;
}(React.Component);

'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var React = require('react');
var ReactDOM = require('react-dom');
var ReactPaginate = require('react-paginate');

var WorkPeriodIndex = exports.WorkPeriodIndex = function (_React$Component) {
  _inherits(WorkPeriodIndex, _React$Component);

  function WorkPeriodIndex(props) {
    _classCallCheck(this, WorkPeriodIndex);

    var _this = _possibleConstructorReturn(this, (WorkPeriodIndex.__proto__ || Object.getPrototypeOf(WorkPeriodIndex)).call(this, props));

    _this.state = {
      workPeriods: [],
      currentUser: null,
      offset: 0,
      total_pages_count: 0
    };
    return _this;
  }

  _createClass(WorkPeriodIndex, [{
    key: 'getChildContext',
    value: function getChildContext() {
      return {
        getWorkPeriods: function () {
          this.handleFilter({ start_date: '', end_date: '' });
        }.bind(this),
        currentUser: this.state.currentUser
      };
    }
  }, {
    key: 'handleFilter',
    value: function handleFilter(data) {
      $.ajax({
        url: '/work_periods/filter_by.json',
        dataType: 'json',
        data: {
          work_period: data,
          page: this.state.offset + 1,
          per_page: 10
        },
        success: function (data) {
          $('#work_periods_view_wrapper .dimmer').removeClass('active');
          this.state.currentUser = data.current_user;
          this.setState({
            workPeriods: data.work_periods,
            total_pages_count: Math.ceil(data.total_work_periods_count / 10)
          });
        }.bind(this),
        error: function (xhr, status, err) {}.bind(this)
      });
    }
  }, {
    key: 'handlePageClick',
    value: function handlePageClick(data) {
      var selected = data.selected;
      var offset = Math.ceil(selected);

      this.setState({ offset: offset }, function () {

        this.handleFilter({ start_date: '', end_date: '', user_id: '' });
      });
    }
  }, {
    key: 'componentWillMount',
    value: function componentWillMount() {
      this.handleFilter({ start_date: '', end_date: '', user_id: '' });
    }
  }, {
    key: 'render',
    value: function render() {
      return React.createElement(
        'div',
        { id: 'work_periods_view_wrapper', className: 'ui segment' },
        React.createElement(WorkPeriodFilter, { onFilterChanged: this.handleFilter.bind(this) }),
        React.createElement(
          'div',
          { className: 'paginate_center' },
          React.createElement(ReactPaginate, { previousLabel: "previous",
            nextLabel: "next", breakLabel: React.createElement(
              'a',
              { href: '' },
              '...'
            ),
            breakClassName: "break-me",
            pageCount: this.state.total_pages_count, marginPagesDisplayed: 3,
            pageRangeDisplayed: 2, onPageChange: this.handlePageClick.bind(this),
            containerClassName: "pagination", subContainerClassName: "pages pagination",
            activeClassName: "active"
          })
        ),
        React.createElement(WorkPeriods, { workPeriods: this.state.workPeriods, ref: 'work_periods' }),
        React.createElement(
          'div',
          { className: 'ui active inverted dimmer' },
          React.createElement(
            'div',
            { className: 'ui text loader' },
            'Loading Hours Logged ...'
          )
        )
      );
    }
  }]);

  return WorkPeriodIndex;
}(React.Component);

WorkPeriodIndex.childContextTypes = {
  getWorkPeriods: React.PropTypes.func,
  currentUser: React.PropTypes.object
};

var mountElement = document.getElementById('work_periods_container');
if (mountElement != null) {
  ReactDOM.render(React.createElement(WorkPeriodIndex, null), mountElement);
}

;'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var React = require('react');
var moment = require('moment');

var AdminWorkPeriodRow = exports.AdminWorkPeriodRow = function (_React$Component) {
  _inherits(AdminWorkPeriodRow, _React$Component);

  function AdminWorkPeriodRow(props) {
    _classCallCheck(this, AdminWorkPeriodRow);

    var _this = _possibleConstructorReturn(this, (AdminWorkPeriodRow.__proto__ || Object.getPrototypeOf(AdminWorkPeriodRow)).call(this, props));

    _this.state = {
      workPeriod: props.workPeriod
    };
    return _this;
  }

  _createClass(AdminWorkPeriodRow, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      $('.popup-control').popup();
    }
  }, {
    key: 'onDeleteBtnClicked',
    value: function onDeleteBtnClicked(e) {
      e.preventDefault();
      this.props.onWorkPeriodDelete();
    }
  }, {
    key: 'onUpdate',
    value: function onUpdate(key) {
      if (key == 'amount_hours') {
        var preBudget = (parseFloat(this.refs[key].state.text) - parseFloat(this.state.workPeriod.amount_hours)) * parseFloat(this.state.workPeriod.pay_rate) + parseFloat(this.state.workPeriod.actual_cost);
        if (preBudget > parseFloat(this.state.workPeriod.budgeted_cost)) {
          var result = confirm('The edited hours logged will exceed the task budget. Continue?');
          if (result == true) {
            this.updateWorkPeriod(key);
          } else {
            this.refs[key].state.text = this.state.workPeriod.amount_hours;
            this.forceUpdate();
          }
        } else {
          this.updateWorkPeriod(key);
        }
      } else {
        this.updateWorkPeriod(key);
      }
    }
  }, {
    key: 'updateWorkPeriod',
    value: function updateWorkPeriod(key) {
      var hash = {
        id: this.state.workPeriod.id,
        work_period: {}
      };
      hash.work_period[key] = this.refs[key].state.text;
      this.setState({ work_period: { "#{key}": parseInt(this.refs[key].state.text) } });
      this.props.onWorkPeriodUpdate(hash);
    }
  }, {
    key: 'hoursValidator',
    value: function hoursValidator(text) {
      return isNaN(text) == false && parseInt('0' + text, 10) > 0;
    }
  }, {
    key: 'render',
    value: function render() {
      return React.createElement(
        'tr',
        { className: 'popup-control', 'data-content': this.state.workPeriod.description },
        React.createElement(
          'td',
          null,
          this.state.workPeriod.task_title
        ),
        React.createElement(
          'td',
          null,
          moment(this.state.workPeriod.date_worked).format('DD/MM/YYYY')
        ),
        React.createElement(
          'td',
          null,
          React.createElement(EditableField, {
            onSave: this.onUpdate.bind(this, 'amount_hours'),
            text: this.state.workPeriod.amount_hours,
            ref: 'amount_hours',
            className: 'amount-hours',
            enabled: true,
            validate: this.hoursValidator
          })
        ),
        React.createElement(
          'td',
          { className: 'right aligned collapsing' },
          React.createElement(
            'a',
            { href: '#', onClick: this.onDeleteBtnClicked.bind(this), className: 'ui red small button' },
            'Delete'
          )
        )
      );
    }
  }]);

  return AdminWorkPeriodRow;
}(React.Component);

'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var React = require('react');

var AdminWorkPeriods = exports.AdminWorkPeriods = function (_React$Component) {
  _inherits(AdminWorkPeriods, _React$Component);

  function AdminWorkPeriods(props) {
    _classCallCheck(this, AdminWorkPeriods);

    var _this = _possibleConstructorReturn(this, (AdminWorkPeriods.__proto__ || Object.getPrototypeOf(AdminWorkPeriods)).call(this, props));

    _this.state = {
      workPeriods: props.workPeriods
    };
    return _this;
  }

  _createClass(AdminWorkPeriods, [{
    key: 'componentWillReceiveProps',
    value: function componentWillReceiveProps(newProps) {
      this.state = newProps;
      this.forceUpdate();
    }
  }, {
    key: 'deleteWorkPeriod',
    value: function deleteWorkPeriod(workPeriodID, arrayIndex) {
      var result = confirm('Do you want to delete this work period? The user who logged these hours will be notified.');
      if (result == true) {
        $.ajax({
          url: 'work_periods/' + workPeriodID + '.json',
          method: 'DELETE',
          type: 'json',
          data: {
            id: workPeriodID
          },
          success: function () {
            this.state.workPeriods.splice(arrayIndex, 1);
            this.forceUpdate();
          }.bind(this)
        });
      }
    }
  }, {
    key: 'handleUpdate',
    value: function handleUpdate(data) {
      $.ajax({
        url: 'work_periods/' + data.id + '.json',
        method: 'PUT',
        type: 'json',
        data: data,
        success: function () {}.bind(this)
      });
    }
  }, {
    key: 'render',
    value: function render() {
      return React.createElement(
        'table',
        { className: 'ui celled table' },
        React.createElement(
          'thead',
          null,
          React.createElement(
            'tr',
            null,
            React.createElement(
              'th',
              null,
              'Task'
            ),
            React.createElement(
              'th',
              null,
              'Date'
            ),
            React.createElement(
              'th',
              null,
              'Hour Logged'
            ),
            React.createElement(
              'th',
              null,
              'Actions'
            )
          )
        ),
        React.createElement(
          'tbody',
          null,
          this.state.workPeriods.map(function (workPeriod, index) {
            return React.createElement(AdminWorkPeriodRow, { workPeriod: workPeriod, key: 'work_period_' + workPeriod.id,
              onWorkPeriodDelete: this.deleteWorkPeriod.bind(this, workPeriod.id, index),
              onWorkPeriodUpdate: this.handleUpdate.bind(this)
            });
          }, this)
        )
      );
    }
  }]);

  return AdminWorkPeriods;
}(React.Component);

'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var React = require('react');

var AdminWorkPeriodFilter = exports.AdminWorkPeriodFilter = function (_React$Component) {
  _inherits(AdminWorkPeriodFilter, _React$Component);

  function AdminWorkPeriodFilter(props) {
    _classCallCheck(this, AdminWorkPeriodFilter);

    var _this = _possibleConstructorReturn(this, (AdminWorkPeriodFilter.__proto__ || Object.getPrototypeOf(AdminWorkPeriodFilter)).call(this, props));

    _this.state = {
      work_period: {
        start_date: '',
        end_date: '',
        user_id: ''
      }
    };
    return _this;
  }

  _createClass(AdminWorkPeriodFilter, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      $('.start-date-filter').on('change', function (e) {
        this.handleChange('start_date', $(e.target).val());
      }.bind(this));

      $('.end-date-filter').on('change', function (e) {
        this.handleChange('end_date', $(e.target).val());
      }.bind(this));

      $('.user-chooser').search({
        apiSettings: {
          url: '/users/search_users?term={query}'
        },
        fields: {
          results: 'users',
          title: 'full_name',
          id: 'id'
        },
        errors: {
          noResults: ''
        },
        onSelect: function (result, response) {
          this.handleChange('user_id', result.id);
        }.bind(this)
      });
    }
  }, {
    key: 'handleChange',
    value: function handleChange(attr, value) {
      this.state.work_period[attr] = value;
      this.props.onFilterChanged(this.state.work_period);
    }
  }, {
    key: 'resetWorkPeriods',
    value: function resetWorkPeriods() {
      $('.start-date-filter').val('');
      $('.end-date-filter').val('');
      $('.user-chooser .prompt').val('');

      this.props.onFilterChanged({
        start_date: null,
        end_date: null,
        user_id: null
      });
    }
  }, {
    key: 'render',
    value: function render() {
      return React.createElement(
        'div',
        { className: 'ui grid' },
        React.createElement(
          'div',
          { className: 'six wide column' },
          React.createElement(
            'div',
            { className: 'ui search user-chooser' },
            React.createElement(
              'div',
              { className: 'ui fluid icon input' },
              React.createElement('input', { className: 'prompt',
                ref: 'user_id',
                placeholder: 'User ...'
              }),
              React.createElement('i', { className: 'user icon' })
            )
          )
        ),
        React.createElement(
          'div',
          { className: 'four wide column' },
          React.createElement(
            'div',
            { className: 'ui fluid input' },
            React.createElement('input', { className: 'datepicker start-date-filter',
              ref: 'start_date',
              placeholder: 'From ...'
            })
          )
        ),
        React.createElement(
          'div',
          { className: 'four wide column' },
          React.createElement(
            'div',
            { className: 'ui fluid input' },
            React.createElement('input', { className: 'datepicker end-date-filter',
              ref: 'end_date',
              placeholder: 'To ...'
            })
          )
        ),
        React.createElement(
          'div',
          { className: 'two wide column' },
          React.createElement(
            'button',
            { onClick: this.resetWorkPeriods.bind(this), className: 'ui fluid button green' },
            'Reset'
          )
        )
      );
    }
  }]);

  return AdminWorkPeriodFilter;
}(React.Component);

'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var React = require('react');
var ReactDOM = require('react-dom');

var AdminWorkPeriodIndex = exports.AdminWorkPeriodIndex = function (_React$Component) {
  _inherits(AdminWorkPeriodIndex, _React$Component);

  function AdminWorkPeriodIndex(props) {
    _classCallCheck(this, AdminWorkPeriodIndex);

    var _this = _possibleConstructorReturn(this, (AdminWorkPeriodIndex.__proto__ || Object.getPrototypeOf(AdminWorkPeriodIndex)).call(this, props));

    _this.state = {
      workPeriods: [],
      currentUser: null
    };
    return _this;
  }

  _createClass(AdminWorkPeriodIndex, [{
    key: 'getChildContext',
    value: function getChildContext() {
      return {
        getWorkPeriods: function () {
          this.handleFilter({ start_date: '', end_date: '', user_id: '' });
        }.bind(this),
        currentUser: this.state.currentUser
      };
    }
  }, {
    key: 'handleFilter',
    value: function handleFilter(data) {
      $.ajax({
        url: '/work_periods/filter_by.json',
        dataType: 'json',
        data: {
          work_period: data
        },
        success: function (data) {
          $('#work_periods_view_wrapper .dimmer').removeClass('active');
          this.state.currentUser = data.current_user;
          this.setState({ workPeriods: data.work_periods });
        }.bind(this),
        error: function (xhr, status, err) {}.bind(this)
      });
    }
  }, {
    key: 'componentWillMount',
    value: function componentWillMount() {
      this.handleFilter({ start_date: '', end_date: '', user_id: '' });
    }
  }, {
    key: 'render',
    value: function render() {
      return React.createElement(
        'div',
        { id: 'work_periods_view_wrapper', className: 'ui segment' },
        React.createElement(AdminWorkPeriodFilter, { onFilterChanged: this.handleFilter.bind(this) }),
        React.createElement(AdminWorkPeriods, { workPeriods: this.state.workPeriods, ref: 'work_periods' }),
        React.createElement(
          'div',
          { className: 'ui active inverted dimmer' },
          React.createElement(
            'div',
            { className: 'ui text loader' },
            'Loading Hours Logged ...'
          )
        )
      );
    }
  }]);

  return AdminWorkPeriodIndex;
}(React.Component);

AdminWorkPeriodIndex.childContextTypes = {
  getWorkPeriods: React.PropTypes.func,
  currentUser: React.PropTypes.object
};

var mountElement = document.getElementById('admin_work_periods_container');
if (mountElement != null) {
  ReactDOM.render(React.createElement(AdminWorkPeriodIndex, null), mountElement);
}

;'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ReportExtensionComponent = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _reactRouter = require('react-router');

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var React = require('react');

var ReportExtensionComponent = exports.ReportExtensionComponent = function (_React$Component) {
  _inherits(ReportExtensionComponent, _React$Component);

  function ReportExtensionComponent() {
    _classCallCheck(this, ReportExtensionComponent);

    return _possibleConstructorReturn(this, (ReportExtensionComponent.__proto__ || Object.getPrototypeOf(ReportExtensionComponent)).apply(this, arguments));
  }

  _createClass(ReportExtensionComponent, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      $('.extension_dropdown').dropdown({
        onChange: function (value) {
          this.props.onExtensionSelected(value);
        }.bind(this)
      });
    }
  }, {
    key: 'render',
    value: function render() {
      return React.createElement(
        'div',
        { className: 'report_extension' },
        React.createElement(
          'div',
          { className: 'ui selection dropdown extension_dropdown fluid' },
          React.createElement(
            'div',
            { className: 'default text' },
            'PDF'
          ),
          React.createElement('input', { type: 'hidden', name: 'selected-task-role', className: 'selected-extension' }),
          React.createElement('i', { className: 'dropdown icon' }),
          React.createElement(
            'div',
            { className: 'menu', ref: 'extension' },
            React.createElement(
              'div',
              { className: 'item', 'data-value': 'pdf' },
              'PDF'
            ),
            React.createElement(
              'div',
              { className: 'item', 'data-value': 'csv' },
              'CSV'
            )
          )
        )
      );
    }
  }]);

  return ReportExtensionComponent;
}(React.Component);

'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var React = require('react');

var ReportFilter = exports.ReportFilter = function (_React$Component) {
  _inherits(ReportFilter, _React$Component);

  function ReportFilter(props) {
    _classCallCheck(this, ReportFilter);

    var _this = _possibleConstructorReturn(this, (ReportFilter.__proto__ || Object.getPrototypeOf(ReportFilter)).call(this, props));

    _this.state = {
      report: {
        created_after: '',
        created_before: '',
        user_id: '',
        task_id: '',
        generate_pdf: true,
        pay: ''
      }
    };
    return _this;
  }

  _createClass(ReportFilter, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      var self = this;
      $('.datetimepicker').datetimepicker({
        format: 'd/m/Y H:i',
        closeOnDateSelect: true,
        defaultTime: '00:00'
      });
      $('.start-date-filter').on('change', function (e) {
        this.handleChange('created_after', $(e.target).val());
      }.bind(this));

      $('.end-date-filter').on('change', function (e) {
        this.handleChange('created_before', $(e.target).val());
      }.bind(this));

      $('.user-chooser').search({
        apiSettings: {
          url: '/users/search_users?term={query}'
        },
        fields: {
          results: 'users',
          title: 'full_name',
          id: 'id'
        },
        errors: {
          noResults: ''
        },
        onSelect: function (result, response) {
          this.handleChange('user_id', result.id);
        }.bind(this)
      });

      $('.pay-item').search({
        apiSettings: {
          url: '/pay_item_categories/search_pay_items?term={query}'
        },
        fields: {
          results: 'pay_items',
          title: 'name',
          id: 'id'
        },
        errors: {
          noResult: 'Result not found!'
        },
        onSelect: function (result, response) {
          this.handleChange('pay', result.name);
        }.bind(this)
      });

      $('.task-chooser').search({
        type: 'special',
        templates: {
          special: function special(response) {
            return self.customTemplateForArchiveTask(response);
          }
        },
        apiSettings: {
          url: '/tasks/search_tasks?term={query}'
        },
        fields: {
          results: 'tasks',
          title: 'title',
          id: 'id'
        },
        errors: {
          noResults: ''
        },
        onSelect: function (result, response) {
          this.handleChange('task_id', result.id);
        }.bind(this)
      });

      $('.task-chooser .prompt').blur();
      $('.task-chooser .prompt').focusout();

      $('.task-chooser .prompt').on('keyup', function (e) {
        if ($(e.target).val() == '') {
          this.handleChange('task_id', '');
        }
      }.bind(this));
    }
  }, {
    key: 'handleChange',
    value: function handleChange(name, value) {
      this.state.report[name] = value;
      this.props.onFilterChanged(this.state.report);
    }
  }, {
    key: 'customTemplateForArchiveTask',
    value: function customTemplateForArchiveTask(response) {
      var _archieve;
      var _tasks = [];
      response.tasks.map(function (task, index) {
        if (task.status == "removed") {
          _archieve = "Archive Task";
        } else {
          _archieve = "";
        }
        _tasks.push('<a class="result"><div class="content"><div class="title">' + task.title + '</div><div class="description">' + task.description + '</div><i>' + _archieve + '</i></div></div></a>');
      });
      return _tasks;
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      return React.createElement(
        'div',
        { className: 'ui grid segment' },
        React.createElement(
          'div',
          { className: 'row' },
          React.createElement(
            'div',
            { className: 'four wide column' },
            React.createElement(
              'div',
              { className: 'ui fluid input' },
              React.createElement('input', { className: 'datetimepicker start-date-filter',
                ref: 'created_after',
                placeholder: 'From ...'
              }),
              this.props.validate_from
            )
          ),
          React.createElement(
            'div',
            { className: 'four wide column' },
            React.createElement(
              'div',
              { className: 'ui fluid input' },
              React.createElement('input', { className: 'datetimepicker end-date-filter',
                ref: 'created_before',
                placeholder: 'To ...'
              }),
              this.props.validate_to
            )
          ),
          React.createElement(
            'div',
            { className: 'four wide column' },
            React.createElement(ReportExtensionComponent, { onExtensionSelected: function onExtensionSelected(val) {
                _this2.props.onExtensionSelected(val);
              } })
          )
        ),
        React.createElement(
          'div',
          { className: 'row' },
          React.createElement(
            'div',
            { className: 'four wide column' },
            React.createElement(
              'div',
              { className: 'ui search user-chooser' },
              React.createElement(
                'div',
                { className: 'ui fluid icon input' },
                React.createElement('input', { className: 'prompt',
                  ref: 'user_id',
                  placeholder: 'User ...'
                }),
                this.props.validate_user,
                React.createElement('i', { className: 'user icon' })
              )
            )
          ),
          React.createElement(
            'div',
            { className: 'four wide column' },
            React.createElement(
              'div',
              { className: 'task-parent-task' },
              React.createElement(
                'div',
                { className: 'ui search selection task-chooser' },
                React.createElement(
                  'div',
                  { className: 'ui icon input fluid' },
                  React.createElement('input', { className: 'prompt', placeholder: 'Task ...', ref: 'task_id' }),
                  this.props.validate_task,
                  React.createElement('i', { className: 'dropdown icon' })
                )
              )
            )
          ),
          React.createElement(
            'div',
            { className: 'four wide column' },
            React.createElement(
              'div',
              { className: 'ui fluid branch-row-filters' },
              React.createElement(
                'div',
                { className: 'ui fuild category search selection pay-item' },
                React.createElement(
                  'div',
                  { className: 'ui icon input fluid' },
                  React.createElement('input', { className: 'prompt', placeholder: 'Filter by Pay item...', ref: 'pay' }),
                  React.createElement('i', { className: 'search icon' })
                )
              )
            )
          )
        )
      );
    }
  }]);

  return ReportFilter;
}(React.Component);

'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.StaffHoursReport = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _reactRouter = require('react-router');

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var React = require('react');

var StaffHoursReport = exports.StaffHoursReport = function (_React$Component) {
  _inherits(StaffHoursReport, _React$Component);

  function StaffHoursReport(props) {
    _classCallCheck(this, StaffHoursReport);

    var _this = _possibleConstructorReturn(this, (StaffHoursReport.__proto__ || Object.getPrototypeOf(StaffHoursReport)).call(this, props));

    _this.state = {
      reportParams: {},
      extension: 'pdf',
      validateFrom: '',
      validateTo: '',
      validateUser: ''
    };
    return _this;
  }

  _createClass(StaffHoursReport, [{
    key: 'handleFilter',
    value: function handleFilter(data) {
      Object.assign(this.state.reportParams, data);
      this.forceUpdate();
    }
  }, {
    key: 'handleSelectedExtension',
    value: function handleSelectedExtension(value) {
      this.setState({ extension: value });
    }
  }, {
    key: 'resetFilters',
    value: function resetFilters() {
      $('.start-date-filter').val('');
      $('.end-date-filter').val('');
      $('.user-chooser .prompt').val('');
      $('.selection .prompt').val('');
      $('.extension_dropdown').dropdown('restore defaults');

      this.handleFilter({
        created_after: '',
        created_before: '',
        user_id: '',
        task_id: ''
      });
    }
  }, {
    key: 'downloadURL',
    value: function downloadURL() {
      var str = [];
      for (var p in this.state.reportParams) {
        if (this.state.reportParams.hasOwnProperty(p)) {
          str.push(encodeURIComponent(p) + "=" + encodeURIComponent(this.state.reportParams[p]));
        }
      }return '/reports/staff_hours.' + this.state.extension + '?' + str.join('&');
    }
  }, {
    key: 'validate',
    value: function validate(e) {
      var reportDetails = this.state.reportParams;
      if (reportDetails.created_after === undefined || reportDetails.created_before === undefined || reportDetails.user_id === undefined || reportDetails.created_after === '' || reportDetails.created_before === '' || reportDetails.user_id === '') {
        e.preventDefault();
        if (reportDetails.created_after === undefined || reportDetails.created_after === '') {
          this.setState({
            validateFrom: React.createElement(
              'p',
              { className: 'ui basic red pointing prompt label transition visible fix_inpt' },
              'input cannot be blank'
            )
          });
        } else if (reportDetails.created_before === undefined || reportDetails.created_before === '') {
          this.setState({
            validateTo: React.createElement(
              'p',
              { className: 'ui basic red pointing prompt label transition visible fix_inpt' },
              'input cannot be blank'
            ),
            validateFrom: ''
          });
        } else {
          this.setState({
            validateUser: React.createElement(
              'p',
              { className: 'ui basic red pointing prompt label transition visible fix_inpt' },
              'input cannot be blank'
            ),
            validateFrom: '',
            validateTo: ''
          });
        }
        return false;
      } else {
        this.setState({
          validateFrom: '',
          validateUser: '',
          validateTo: ''
        });
        return true;
      }
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      return React.createElement(
        'div',
        { className: 'staff_hours_report' },
        React.createElement(
          'h1',
          null,
          'Staff Hours Report'
        ),
        React.createElement('div', { className: 'right floated right' }),
        React.createElement(ReportFilter, { onFilterChanged: this.handleFilter.bind(this), onExtensionSelected: function onExtensionSelected(val) {
            _this2.handleSelectedExtension(val);
          }, ref: 'extension', validate_from: this.state.validateFrom, validate_to: this.state.validateTo, validate_user: this.state.validateUser }),
        React.createElement(
          'div',
          { className: 'ui grid segment' },
          React.createElement(
            'div',
            { className: 'two wide column' },
            React.createElement(
              'button',
              { onClick: this.resetFilters.bind(this), className: 'ui fluid button green' },
              'Reset'
            )
          ),
          React.createElement(
            'div',
            { className: 'four six wide column' },
            React.createElement(
              'a',
              { target: '_blank', href: '' + this.downloadURL(), onClick: this.validate.bind(this), className: 'ui button default fluid' },
              'Generate'
            )
          )
        )
      );
    }
  }]);

  return StaffHoursReport;
}(React.Component);

"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var React = require('react');

var EditableBudgetField = exports.EditableBudgetField = function (_React$Component) {
  _inherits(EditableBudgetField, _React$Component);

  function EditableBudgetField(props) {
    _classCallCheck(this, EditableBudgetField);

    var _this = _possibleConstructorReturn(this, (EditableBudgetField.__proto__ || Object.getPrototypeOf(EditableBudgetField)).call(this, props));

    _this.state = {
      text: _this.props.text,
      editable: false
    };
    return _this;
  }

  _createClass(EditableBudgetField, [{
    key: "onChange",
    value: function onChange(event) {
      if (event.keyCode == 13) {
        this.state.text = event.target.value;
        this.props.onBudgetSave(this.state.text);
        this.setState({
          editable: false
        });
      }
    }
  }, {
    key: "onBlur",
    value: function onBlur(event) {
      this.state.text = event.target.value;
      this.props.onBudgetSave(this.state.text);
      this.setState({
        editable: false
      });
    }
  }, {
    key: "onclick",
    value: function onclick() {
      this.setState({
        editable: true
      });
    }
  }, {
    key: "render",
    value: function render() {
      if (this.state.editable) return React.createElement("input", { name: "budgetCost", type: "text", defaultValue: this.props.text, onKeyUp: this.onChange.bind(this), onBlur: this.onBlur.bind(this) });else return React.createElement(
        "span",
        { onClick: this.onclick.bind(this) },
        this.props.text
      );
    }
  }]);

  return EditableBudgetField;
}(React.Component);

'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var React = require('react');
var time_worked;

var LogHours = exports.LogHours = function (_React$Component) {
  _inherits(LogHours, _React$Component);

  function LogHours(props) {
    _classCallCheck(this, LogHours);

    var _this = _possibleConstructorReturn(this, (LogHours.__proto__ || Object.getPrototypeOf(LogHours)).call(this, props));

    _this.state = {
      time_worked: '',
      validateTime: '',
      validatetaskProgress: '',
      validateDescription: '',
      description: '',
      task_progress: 0,
      validateDate: ''
    }, _this.handleSubmit = _this.handleSubmit.bind(_this);
    _this._submitLogHours = _this._submitLogHours.bind(_this);
    _this.handleChange = _this.handleChange.bind(_this);
    _this.changeState = _this.changeState.bind(_this);
    return _this;
  }

  _createClass(LogHours, [{
    key: 'handleSubmit',
    value: function handleSubmit(e) {
      e.preventDefault();
    }
  }, {
    key: 'handleChange',
    value: function handleChange(e) {
      var numbers = /^[0-9]+$/;
      if (this.state.time_worked != '') {
        this.setState({
          validateTime: ''
        });
      }
      if (this.state.description != '') {
        this.setState({
          validateDescription: ''
        });
      }
      this.setState(_defineProperty({}, e.target.name, e.target.value));
    }
  }, {
    key: '_validateInput',
    value: function _validateInput() {
      var valid = true;
      var numbers = /^-{0,1}\d*\.{0,1}\d+$/;
      if (this.state.time_worked == '') {
        this.setState({
          validateTime: React.createElement(
            'p',
            { className: 'ui basic red pointing prompt label transition visible fix_inpt' },
            'input cannot be blank'
          )
        });
        valid = false;
      } else if (!this.state.time_worked.match(numbers)) {
        this.setState({
          validateTime: React.createElement(
            'p',
            { className: 'ui basic red pointing prompt label transition visible fix_inpt' },
            'Please fill numbers only'
          )
        });
        valid = false;
      } else {
        this.setState({
          validateTime: ''
        });
      }
      if (this.state.description == '') {
        this.setState({
          validateDescription: React.createElement(
            'p',
            { className: 'ui basic red pointing prompt label transition visible fix_inpt' },
            'input cannot be blank'
          )
        });
        valid = false;
      } else {
        this.setState({
          validateDescription: ''
        });
      }
      if (this.props.date == undefined || this.props.date == '') {
        alert("Date cannot be blank");
        this.setState({
          validateDate: React.createElement(
            'p',
            { className: 'ui basic red pointing prompt label transition visible date_valid fix_inpt' },
            'Please fill the date'
          )
        });
        valid = false;
      } else {
        this.setState({
          validateDate: ''
        });
      }
      return valid;
    }
  }, {
    key: 'changeState',
    value: function changeState() {
      this.props.onclick();
    }
  }, {
    key: '_submitLogHours',
    value: function _submitLogHours(e) {
      var timeWorked = "time_worked" + this.props.task.id;
      var taskProgress = "task_progress" + this.props.task.id;
      var _description = "description" + this.props.task.id;
      if (this._validateInput()) {
        var _values;

        var _self = this;
        var task_id = this.props.task.id;
        var amount_hours = this.state.time_worked;
        var date_worked = this.props.date;
        var progress = this.state.task_progress;
        var description = this.state.description;
        var values = (_values = {}, _defineProperty(_values, 'work_period[task_id]', task_id), _defineProperty(_values, 'work_period[amount_hours]', amount_hours), _defineProperty(_values, 'work_period[date_worked]', date_worked), _defineProperty(_values, 'work_period[progress]', progress), _defineProperty(_values, 'work_period[description]', description), _values);
        return $.ajax({
          method: 'GET',
          url: '/work_periods/check_budget.json',
          data: values,
          success: function success(xhr, status, err) {
            var result;
            if (xhr.code === "LOG-200") {
              return $.ajax({
                method: 'POST',
                url: '/work_periods.json',
                data: values,
                success: function success(xhr, status, err) {
                  _self.changeState();
                  _self.state.task_progress = 0;
                  _self.state.description = "";
                  _self.state.time_worked = "";
                  document.getElementById(timeWorked).value = "";
                  document.getElementById(taskProgress).value = "";
                  document.getElementById(_description).value = "";
                  return alertify.success(xhr.msg);
                }
              });
            } else if (xhr.code === "LOG-400") {
              result = confirm('You cannot log these hours as it would exceed the planned budget. Would you like to alert your Task Manager?');
              if (result) {
                $.ajax({
                  method: 'POST',
                  url: '/work_periods/send_exceeded_log_hours_notification.json',
                  data: {
                    task_id: task_id,
                    amount_hours: amount_hours
                  },
                  success: function success(xhr, status, err) {
                    _self.state.task_progress = 0;
                    _self.state.description = "";
                    _self.state.time_worked = "";
                    document.getElementById(timeWorked).value = "";
                    document.getElementById(taskProgress).value = "";
                    document.getElementById(_description).value = "";
                    return alertify.success(xhr.msg);
                  },
                  error: function error() {
                    var result;
                    return result = confirm('something went wrong?');
                  }
                });
              }
            }
          }
        });
      }
    }
  }, {
    key: 'render',
    value: function render() {
      return React.createElement(
        'form',
        { id: 'submission_form', onSubmit: this.handleSubmit },
        this.state.validateDate,
        React.createElement(
          'div',
          { id: 'main', className: 'main_task_col' },
          React.createElement('div', { className: 'time_col' }),
          React.createElement(
            'div',
            { className: 'time_col' },
            React.createElement('input', { type: 'text', id: "time_worked" + this.props.task.id, name: 'time_worked', className: 'ipt_marg', onChange: this.handleChange, title: 'Time Worked(Hours)' }),
            this.state.validateTime
          ),
          React.createElement(
            'div',
            { className: 'time_col' },
            React.createElement('input', { type: 'number', id: "task_progress" + this.props.task.id, className: 'ipt_marg', name: 'task_progress', onChange: this.handleChange, value: this.state.task_progress, title: 'Task progress(%)' })
          ),
          React.createElement(
            'div',
            { className: 'time_col' },
            React.createElement('input', { type: 'text', id: "description" + this.props.task.id, className: 'ipt_marg', name: 'description', onChange: this.handleChange, title: 'description' }),
            this.state.validateDescription
          ),
          React.createElement('input', { type: 'submit', value: 'Log Hours', className: 'ui deny button submit_btn mg_top13', id: 'submit', onClick: this._submitLogHours })
        )
      );
    }
  }]);

  return LogHours;
}(React.Component);

'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var React = require('react');
var weekDays = [{ 'weekId': 0, 'weekName': 'Sunday' }, { 'weekId': 1, 'weekName': 'Monday' }, { 'weekId': 2, 'weekName': 'Tuesday' }, { 'weekId': 3, 'weekName': 'Wednesday' }, { 'weekId': 4, 'weekName': 'Thursday' }, { 'weekId': 5, 'weekName': 'Friday' }, { 'weekId': 6, 'weekName': 'Saturday' }];

var WeekDayChooser = exports.WeekDayChooser = function (_React$Component) {
  _inherits(WeekDayChooser, _React$Component);

  function WeekDayChooser(props) {
    _classCallCheck(this, WeekDayChooser);

    var _this = _possibleConstructorReturn(this, (WeekDayChooser.__proto__ || Object.getPrototypeOf(WeekDayChooser)).call(this, props));

    _this.state = {
      task: _this.props.task,
      weekId: '',
      weekName: ''
    };
    return _this;
  }

  _createClass(WeekDayChooser, [{
    key: 'capitalizeFirstLetter',
    value: function capitalizeFirstLetter(currentWeekName) {
      return currentWeekName.charAt(0).toUpperCase() + currentWeekName.slice(1);
    }
  }, {
    key: 'weekName',
    value: function weekName() {
      if (this.props.task != null && this.props.task.weekly_reports_start != null) {
        return this.capitalizeFirstLetter(this.props.task.weekly_reports_start);
      } else {
        return weekDays[1].weekName;
      }
    }
  }, {
    key: 'componentDidMount',
    value: function componentDidMount() {
      $('.week-day-item').dropdown({
        fullTextSearch: true,
        onChange: function (value, text) {
          this.setState({ weekId: value, weekName: text });
          this.props.onWeekDaySelected();
        }.bind(this)
      });
    }
  }, {
    key: 'getText',
    value: function getText() {
      return this.state.weekId;
    }
  }, {
    key: 'render',
    value: function render() {
      return React.createElement(
        'div',
        { className: 'ui bottom left pointing dropdown labeled icon button week-day-item' },
        React.createElement('i', { className: 'icon dropdown' }),
        React.createElement(
          'span',
          { className: 'text' },
          this.weekName()
        ),
        React.createElement(
          'div',
          { className: 'menu' },
          React.createElement(
            'div',
            { className: 'scrolling menu' },
            weekDays.map(function (days) {
              return React.createElement(
                'div',
                { className: 'item', 'data-value': days.weekId, key: 'week_days_' + days.weekId },
                days.weekName
              );
            }, this)
          )
        )
      );
    }
  }]);

  return WeekDayChooser;
}(React.Component);

WeekDayChooser.contextTypes = {
  weekDays: React.PropTypes.array
};

'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.BudgetReport = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _reactRouter = require('react-router');

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var React = require('react');

var BudgetReport = exports.BudgetReport = function (_React$Component) {
  _inherits(BudgetReport, _React$Component);

  function BudgetReport(props) {
    _classCallCheck(this, BudgetReport);

    var _this = _possibleConstructorReturn(this, (BudgetReport.__proto__ || Object.getPrototypeOf(BudgetReport)).call(this, props));

    _this.state = {
      reportParams: {},
      extension: 'pdf',
      validateFrom: '',
      validateTo: '',
      validateTask: ''
    };
    return _this;
  }

  _createClass(BudgetReport, [{
    key: 'handleFilter',
    value: function handleFilter(data) {
      Object.assign(this.state.reportParams, data);
      this.forceUpdate();
    }
  }, {
    key: 'handleSelectedExtension',
    value: function handleSelectedExtension(value) {
      this.setState({ extension: value });
    }
  }, {
    key: 'resetFilters',
    value: function resetFilters() {
      $('.start-date-filter').val('');
      $('.end-date-filter').val('');
      $('.user-chooser .prompt').val('');
      $('.selection .prompt').val('');
      $('.extension_dropdown').dropdown('restore defaults');

      this.handleFilter({
        created_after: '',
        created_before: '',
        user_id: '',
        task_id: ''
      });
    }
  }, {
    key: 'downloadURL',
    value: function downloadURL() {
      var str = [];
      for (var p in this.state.reportParams) {
        if (this.state.reportParams.hasOwnProperty(p)) {
          str.push(encodeURIComponent(p) + "=" + encodeURIComponent(this.state.reportParams[p]));
        }
      }return '/reports/budget_reports.' + this.state.extension + '?' + str.join('&');
    }
  }, {
    key: 'validate',
    value: function validate(e) {
      var reportDetails = this.state.reportParams;
      if (reportDetails.created_after === undefined || reportDetails.created_before === undefined || reportDetails.task_id === undefined || reportDetails.created_after === '' || reportDetails.created_before === '' || reportDetails.task_id === '') {
        e.preventDefault();
        if (reportDetails.created_after === undefined || reportDetails.created_after === '') {
          this.setState({
            validateFrom: React.createElement(
              'p',
              { className: 'ui basic red pointing prompt label transition visible fix_inpt' },
              'input cannot be blank'
            )
          });
        } else if (reportDetails.created_before === undefined || reportDetails.created_before === '') {
          this.setState({
            validateTo: React.createElement(
              'p',
              { className: 'ui basic red pointing prompt label transition visible fix_inpt' },
              'input cannot be blank'
            ),
            validateFrom: ''
          });
        } else {
          this.setState({
            validateTask: React.createElement(
              'p',
              { className: 'ui basic red pointing prompt label transition visible fix_inpt' },
              'input cannot be blank'
            ),
            validateFrom: '',
            validateTo: ''
          });
        }
        return false;
      } else {
        this.setState({
          validateFrom: '',
          validateTask: '',
          validateTo: ''
        });
        return true;
      }
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      return React.createElement(
        'div',
        { className: 'budget_reports' },
        React.createElement(
          'h1',
          null,
          'Budget Report'
        ),
        React.createElement(
          'div',
          { className: 'right floated right' },
          'Please ensure you select a task for the budget report.'
        ),
        React.createElement(ReportFilter, { onFilterChanged: this.handleFilter.bind(this), onExtensionSelected: function onExtensionSelected(val) {
            _this2.handleSelectedExtension(val);
          }, ref: 'extension', validate_from: this.state.validateFrom, validate_to: this.state.validateTo, validate_task: this.state.validateTask }),
        React.createElement(
          'div',
          { className: 'ui grid segment' },
          React.createElement(
            'div',
            { className: 'two wide column' },
            React.createElement(
              'button',
              { onClick: this.resetFilters.bind(this), className: 'ui fluid button green' },
              'Reset'
            )
          ),
          React.createElement(
            'div',
            { className: 'four six wide column' },
            React.createElement(
              'a',
              { target: '_blank', href: '' + this.downloadURL(), onClick: this.validate.bind(this), className: 'ui button default fluid' },
              'Generate'
            )
          )
        )
      );
    }
  }]);

  return BudgetReport;
}(React.Component);

'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ReportMainView = exports.ReportApp = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _reactDom = require('react-dom');

var _reactRouter = require('react-router');

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var React = require('react');

var ReportApp = exports.ReportApp = function (_React$Component) {
  _inherits(ReportApp, _React$Component);

  function ReportApp() {
    _classCallCheck(this, ReportApp);

    return _possibleConstructorReturn(this, (ReportApp.__proto__ || Object.getPrototypeOf(ReportApp)).apply(this, arguments));
  }

  _createClass(ReportApp, [{
    key: 'render',
    value: function render() {
      return React.createElement(
        'main',
        { className: 'app__main' },
        this.props.children
      );
    }
  }]);

  return ReportApp;
}(React.Component);

var ReportMainView = exports.ReportMainView = function (_React$Component2) {
  _inherits(ReportMainView, _React$Component2);

  function ReportMainView() {
    _classCallCheck(this, ReportMainView);

    return _possibleConstructorReturn(this, (ReportMainView.__proto__ || Object.getPrototypeOf(ReportMainView)).apply(this, arguments));
  }

  _createClass(ReportMainView, [{
    key: 'render',
    value: function render() {
      return React.createElement(
        'div',
        { className: 'ui segment report_outer_wrapper' },
        React.createElement(
          'h1',
          null,
          'Report List'
        ),
        React.createElement(
          'table',
          { className: 'ui celled table report_list' },
          React.createElement(
            'thead',
            null,
            React.createElement(
              'tr',
              null,
              React.createElement(
                'th',
                null,
                'Name'
              ),
              React.createElement(
                'th',
                null,
                'Description'
              )
            )
          ),
          React.createElement(
            'tbody',
            null,
            React.createElement(
              'tr',
              null,
              React.createElement(
                'td',
                null,
                React.createElement(
                  _reactRouter.Link,
                  { to: '/reports/staff_hours_view' },
                  'Staff Hours Report'
                )
              ),
              React.createElement(
                'td',
                null,
                'Report containing all staff and the hours they have logged.'
              )
            ),
            React.createElement(
              'tr',
              null,
              React.createElement(
                'td',
                null,
                React.createElement(
                  _reactRouter.Link,
                  { to: '/reports/budget_reports_view' },
                  'Budget Report'
                )
              ),
              React.createElement(
                'td',
                null,
                'Report containing a breakdown of spending within a task.'
              )
            )
          )
        )
      );
    }
  }]);

  return ReportMainView;
}(React.Component);

var mountElement = document.getElementById('reports_container');
if (mountElement != null) {
  (0, _reactDom.render)(React.createElement(
    _reactRouter.Router,
    { history: _reactRouter.browserHistory },
    React.createElement(
      _reactRouter.Route,
      { path: '/reports', component: ReportApp },
      React.createElement(_reactRouter.IndexRoute, { component: ReportMainView }),
      React.createElement(_reactRouter.Route, { path: 'staff_hours_view', component: StaffHoursReport }),
      React.createElement(_reactRouter.Route, { path: 'budget_reports_view', component: BudgetReport })
    )
  ), mountElement);
}

;/* jshint ignore:start */
(function() {
  var WebSocket = window.WebSocket || window.MozWebSocket;
  var br = window.brunch = (window.brunch || {});
  var ar = br['auto-reload'] = (br['auto-reload'] || {});
  if (!WebSocket || ar.disabled) return;

  var cacheBuster = function(url){
    var date = Math.round(Date.now() / 1000).toString();
    url = url.replace(/(\&|\\?)cacheBuster=\d*/, '');
    return url + (url.indexOf('?') >= 0 ? '&' : '?') +'cacheBuster=' + date;
  };

  var browser = navigator.userAgent.toLowerCase();
  var forceRepaint = ar.forceRepaint || browser.indexOf('chrome') > -1;

  var reloaders = {
    page: function(){
      window.location.reload(true);
    },

    stylesheet: function(){
      [].slice
        .call(document.querySelectorAll('link[rel=stylesheet]'))
        .filter(function(link) {
          var val = link.getAttribute('data-autoreload');
          return link.href && val != 'false';
        })
        .forEach(function(link) {
          link.href = cacheBuster(link.href);
        });

      // Hack to force page repaint after 25ms.
      if (forceRepaint) setTimeout(function() { document.body.offsetHeight; }, 25);
    }
  };
  var port = ar.port || 9485;
  var host = br.server || window.location.hostname || 'localhost';

  var connect = function(){
    var connection = new WebSocket('ws://' + host + ':' + port);
    connection.onmessage = function(event){
      if (ar.disabled) return;
      var message = event.data;
      var reloader = reloaders[message] || reloaders.page;
      reloader();
    };
    connection.onerror = function(){
      if (connection.readyState) connection.close();
    };
    connection.onclose = function(){
      window.setTimeout(connect, 1000);
    };
  };
  connect();
})();
/* jshint ignore:end */
;
//# sourceMappingURL=compiled_components.js.map