# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
$ ->
  $('#time_submission_form .form').form
    inline: 'true'
    fields:
      work_period_date_worked:
        identifier: 'work_period_date_worked'
        rules: [
          {
            type: 'empty',
            prompt: 'Please enter the work period date'
          }
        ]
      work_period_progress:
        identifier: 'work_period_progress'
        rules: [
          {
            type: 'empty',
            prompt: 'Please enter the task progress'
          }
        ]
      work_period_amount_hours:
        identifier: 'work_period_amount_hours'
        rules: [
          {
            type: 'empty',
            prompt: 'Please enter the time worked hours.'
          },
          {
            type: 'regExp[/^\\d+(\.\\d{1,2}[0]*)?$/]'
            prompt: 'Please enter hours in numeric with 2 decimal places.'
          }
        ]
  return

$(document).on 'click', '#submit_work_period', (e)->
  e.preventDefault()
  $(e.target).attr('disabled','disabled')
  form = $('#time_submission_form .form')
  values = { work_period: {} }
  $.each form.serializeArray(), (i, field) ->
    values[field.name] = field.value
    return

  $.ajax
    method: 'GET'
    url: '/work_periods/check_budget.json'
    data: values
    success: (xhr, status, err) ->
      if xhr.code == "LOG-200"
        form.submit()
      else if xhr.code == "LOG-400"
        result = confirm('You cannot log these hours as it would exceed the planned budget. Would you like to alert your Task Manager?')
        if result
          $.ajax
            method: 'POST'
            url: '/work_periods/send_exceeded_log_hours_notification.json'
            data: {
              task_id: values['work_period[task_id]'],
              amount_hours: values['work_period[amount_hours]']
            }
            success: (xhr, status, err) ->
              window.location = xhr.redirect_location;
              alertify.success(xhr.msg);
        $('.add-time-modal').modal('hide')
    error: ->
      result = confirm('something went wrong?')
