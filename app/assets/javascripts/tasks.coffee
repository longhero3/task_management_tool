# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$ ->
  url = window.location.href
  if url.indexOf('show_modal=true') != -1 and url.indexOf('task') != -1
    $('.add-time-modal').modal('show')
  $('#add_time').on "click", ->
    $('.add-time-modal').modal('show')