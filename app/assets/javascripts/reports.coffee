# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
$ ->
  $('.staff-report-form').form
    fields:
      user_id: 'empty'
      start_date: 'empty'
      end_date: 'empty'