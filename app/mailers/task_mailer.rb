class TaskMailer < ApplicationMailer
  def out_of_date_range(task)
    @task = task
    mail(subject: "#{task.task_code} - #{task.title}'s date range has been out of parent's task bound.",
         bcc: task.users.map(&:email)
    )
  end

  def out_of_budget(task, user, increased_budget)
    @task = task
    @user = user
    @increase_budget = increase_budget

    to_email = @task.task_owner.try(:email)
    cc_emails = @task.ancestors_escalation_managers.pluck(:email)
    cc_emails.delete(to_email)

    mail(subject: "#{task.task_code} - #{task.title}'s budgeted has exceeded the parent's", to: to_email, cc: cc_emails)
  end

  def out_of_actual_budget(task, user, logged_hours)
    @task = task
    @user = user
    @logged_hours = logged_hours

    to_email = @task.task_owner.try(:email)
    cc_emails = @task.ancestors_escalation_managers.pluck(:email)
    cc_emails.delete(to_email)

    mail(subject: "#{task.task_code} - #{task.title}'s actual budget has exceeded the planned budget", to: to_email, cc: cc_emails)
  end

  def increase_in_budget(task, user, increase_budget, child_title = nil, new_child_added)
    @task = task
    @user = user
    @increase_budget = increase_budget
    @child_title = child_title
    @new_child_added = new_child_added
    mail(subject: "#{task.task_code} - #{task.title}'s budget has been increased", to: @task.task_owner.try(:email), cc: @task.escalation_manager.try(:email))
  end

  def decrease_in_budget(task, user, decreased_budget)
    @task = task
    @user = user
    @decreased_budget = decreased_budget
    mail(subject: "#{task.task_code} - #{task.title}'s budget has been decreased", to: @task.task_owner.try(:email), cc: @task.escalation_manager.try(:email))
  end
end
