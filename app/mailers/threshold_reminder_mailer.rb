class ThresholdReminderMailer < ApplicationMailer
  def send_change_record_notification(u, reminder)
    @user = u
    @reminder = reminder
    mail(
      to: @user.email,
      subject: "There is a change in record",
      body: "Hi, there is a change in record threshhold"
    )
  end
end
