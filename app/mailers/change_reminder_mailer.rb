class ChangeReminderMailer < ApplicationMailer
  def send_change_record_notification(u, record)
    @user = u
    @record = record
    mail(
      to: @user.email,
      subject: "There is a change in record"
    )
  end
end
