class LogHoursMailer < ApplicationMailer
  def exceeded_log_hours_notification(task, user, increased_budget)
    @task = task
    @user = user
    @increased_budget = increased_budget
    mail(subject: "Attempt to increase the budget of #{task.task_code} - #{task.title}'s", to: @task.task_owner.try(:email), cc: @task.escalation_manager.try(:email))
  end
end
