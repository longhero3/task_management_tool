class WorkPeriodMailer < ApplicationMailer
  def authorised_edit(user, work_period)
    @user = user
    @work_period = work_period
    @task = work_period.task
    mail(
      to: @work_period.user.email,
      subject: "#{@user.full_name} has edited your hours"
    )
  end

  def authorised_destroy(user, work_period)
    @user = user
    @work_period = work_period
    @task = work_period.task
    mail(
      to: @work_period.user.email,
      subject: "#{@user.full_name} has deleted your hours"
    )
  end
end
