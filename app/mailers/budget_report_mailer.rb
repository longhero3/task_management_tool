class BudgetReportMailer < ApplicationMailer
  include ActionView::Helpers::DateHelper

  def report(recipient, task, period_from, period_to, pdf)
    attachments[pdf[:filename]] = pdf[:document]

    mail(to: recipient, subject: "Budget Report For #{task.title} - #{distance_of_time_in_words(period_from, period_to)}")
  end
end
