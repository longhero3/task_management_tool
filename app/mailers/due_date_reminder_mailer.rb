class DueDateReminderMailer < ApplicationMailer
  def send_due_date_notification(u, reminder)
    @user = u
    @reminder = reminder
    mail(
      to: @user.email,
      subject: "There is a change in record",
      body: "Hi, due date is less than #{reminder.due_remind_days} days."
    )
  end
end
