class UserMailer < ApplicationMailer
  def xero_connect(user)
    @user = user
    mail(
    to: Rails.configuration.x.email_addresses.human_resources,
    subject: "#{@user.full_name} on TMT requiring Xero account connection"
    )
  end
end
