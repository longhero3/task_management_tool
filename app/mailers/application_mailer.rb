class ApplicationMailer < ActionMailer::Base
  default from: "no-reply@gradready.com.au"
  layout 'mailer'
end
