class StaffHoursMailer < ApplicationMailer
  include ActionView::Helpers::DateHelper

  def report(recipient, period_from, period_to, pdf)
    attachments[pdf[:filename]] = pdf[:document]

    mail(to: recipient, subject: "Staff Hours Report - #{distance_of_time_in_words(period_from, period_to)}")
  end
end
