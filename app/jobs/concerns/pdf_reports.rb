module PdfReports
  extend ActiveSupport::Concern

  # TODO: http://blog.arkency.com/2014/03/pdf-in-rails-without-controllers/
  def render_pdf_to_string(template, options = {})
    ac = ApplicationController.new()
    options.merge!({ template: template, pdf: 'report', orientation: 'Landscape' })
    ac.render_to_string(options)
  end
end