class CreatePayRunJob < XeroBaseJob
  @queue = :xero

  def self.perform
    if PayrollCalendar.end_of_pay_cycle_date?
      XeroClient.new.create_pay_run
    end
  end
end