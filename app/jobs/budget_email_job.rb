class BudgetEmailJob < ActiveJob::Base
  include PdfReports

  queue_as :default

  def perform(task, from_time, to_time)
    @task = task
    return if email_recipients.empty?

    from_time = Time.parse from_time
    to_time = Time.parse to_time
    work_periods = WorkPeriod.where(task_id: task.self_and_descendants.ids).where(created_at: from_time .. to_time)

    report_data = BudgetReport.new task, work_periods, from_time, to_time

    BudgetReportMailer.report(
       email_recipients,
       task,
       from_time,
       to_time,
       filename: "BudgetReport_#{from_time.to_date.to_s}.pdf",
       document: render_pdf_to_string('reports/budget_reports/show', locals: { :@report_data => report_data })
     ).deliver_now
  end

  def email_recipients
    recipients = @task.task_subscribers.map { |task_user| task_user.user.email }
    recipients.push @task.escalation_manager.email unless @task.escalation_manager.nil?
    recipients.push @task.task_owner.email unless @task.task_owner.nil?
    recipients.uniq
  end
end
