class CreateTimesheetJob < XeroBaseJob
  @queue = :xero

  def self.perform
    if PayrollCalendar.create_timesheet_date?
      XeroTimesheet.delete_all
      TimesheetSnapshot.delete_all
      User.xero_integrated.each do |user|
        user.create_timesheet_snapshot
      end
    end
  end
end