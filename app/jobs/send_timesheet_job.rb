class SendTimesheetJob < XeroBaseJob
  @queue = :xero

  def self.perform
    if PayrollCalendar.sending_timesheet_date?
      TimesheetSnapshot.all.each do |snapshot|
        snapshot.send_to_xero
        sleep 2
      end
    end
  end
end