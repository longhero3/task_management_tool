class SyncEmployeesJob < XeroBaseJob
  @queue = :xero

  def self.perform
    xero_client = XeroClient.new
    xero_client.sync_employees
    sleep 2
    xero_client.sync_xero_account_pay_template
  end
end