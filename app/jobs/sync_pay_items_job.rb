class SyncPayItemsJob < XeroBaseJob
  @queue = :xero

  def self.perform
    Rails.logger.info "==========================================="
    Rails.logger.info "perform"
    Rails.logger.info "==========================================="
    XeroClient.new.sync_pay_items
  end
end
