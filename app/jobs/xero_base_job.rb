class XeroBaseJob
  require 'xero_client'

  def self.enqueue(*args)
    self.perform(*args)
  end
end