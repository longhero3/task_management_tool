class StaffHoursEmailJob < ActiveJob::Base
  include PdfReports

  queue_as :default

  def perform(from_time, to_time)
    @from_time = Time.parse from_time
    @to_time = Time.parse to_time
    @work_periods = WorkPeriod.includes(:task, :user).where(created_at: @from_time .. @to_time)
    recipients = @work_periods.map { |wp| task_recipients wp.task.root }.flatten.uniq
    return if recipients.blank?
    report_data = StaffHoursReport.new @work_periods, @from_time, @to_time

    StaffHoursMailer.report(
      recipients,
      @from_time,
      @to_time,
      filename: "StaffHoursReport_#{@from_time.to_date.to_s}.pdf",
      document: render_pdf_to_string('reports/staff_hours/show', locals: { :@report_data => report_data })
    ).deliver_now
  end

  def task_recipients(task)
    recipients = task.task_subscribers.map { |task_user| task_user.user.email }
    recipients.push task.escalation_manager.email unless task.escalation_manager.nil?
    recipients.push task.task_owner.email unless task.task_owner.nil?
    recipients
  end
end
