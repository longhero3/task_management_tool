class RunReminderCallbacksJob < ApplicationJob
  queue_as :default
  @queue = :default

  def perform(*args)
    Reminder.all.each(&:run)
    ChangeReminder.cleanup
  end
end
