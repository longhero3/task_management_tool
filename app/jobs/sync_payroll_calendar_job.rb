class SyncPayrollCalendarJob < XeroBaseJob
  @queue = :xero

  def self.perform
    XeroClient.new.sync_payroll_calendar
  end
end