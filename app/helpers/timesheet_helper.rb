module TimesheetHelper
  def format_day_month_year date
    date.strftime('%d/%m/%Y')
  end

  def format_day_month date
    date.class == String ? '' : date.strftime('%d/%m')
  end

  def format_day date
    date.class == String ? date : date.strftime('%a')
  end

  def timesheet_hash user, start_date
    timesheet_data = {}
    simple_data = {}
    if start_date
      start_time = Time.parse start_date.iso8601
      end_time = Time.parse (start_date.end_of_day + 13.days).iso8601
      work_periods = user.work_periods.where(created_at: start_time .. end_time)
      if work_periods.count > 0
        work_periods.includes(:pay_item).group_by(&:pay_item).each do |pay_item, grouped_work_periods|
          next if !pay_item || pay_item.name.empty?
          grouped_relation = WorkPeriod.where(id: grouped_work_periods.map(&:id)).current_sprint_time_sheet_data(start_date).to_a
          if simple_data[pay_item.earnings_rate_id]
            grouped_relation = grouped_relation.each_with_index.map do |element, index|
              [element[0], element[1] + simple_data[pay_item.earnings_rate_id][index]]
            end
          end
          grouped_relation += [['Total', grouped_relation.inject(0) { |sum, time_set| sum + time_set[1] }]]
          timesheet_data[pay_item.name] = grouped_relation
          simple_data[pay_item.earnings_rate_id] = grouped_relation.map { |timeset| timeset[1] }
        end
      end
    end
    timesheet_data['Total'] = simple_data.to_a.inject([]) do |array, time_set|
      if array.count > 0
        time_set[1].each_with_index { |element, index| array[index] += element }
        array
      else
        array = time_set[1]
      end
    end.map { |element| ['Total', element]}
    timesheet_data
  end
end
