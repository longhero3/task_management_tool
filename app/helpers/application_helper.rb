module ApplicationHelper
  def available_states
    au = Carmen::Country.coded('AU')
    au.subregions.map { |subregion| subregion.code }
  end

  def available_high_schools
    HighSchool.pluck(:name)
  end

  def available_job_titles
    [['Type text here','']] + JobTitle.all.map do |title|
      [title.name, title.name]
    end
  end

  def sentry_tag
    return "" if ENV['SENTRY_DSN'].blank?

    html_out = javascript_include_tag 'https://cdn.ravenjs.com/3.0.4/raven.min.js'

    # Don't expose private secrets
    # Sentry for Javascript should not require a secret
    dsn_uri = URI(ENV['SENTRY_DSN'])
    dsn_uri.password = nil

    # Extract URI without a secret
    public_dsn = dsn_uri.to_s

    html_out += javascript_tag "Raven.config('#{escape_javascript public_dsn}').install();"

    html_out += javascript_tag """
    Raven.setUserContext({
      email: '#{escape_javascript current_user.email}',
      id: #{current_user.id}
    });
    """ unless current_user.nil?

    html_out
  end
end
