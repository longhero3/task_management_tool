module ReportHelper
  def number_to_hours(hours)
    hours.round(1).to_s
  end
end
