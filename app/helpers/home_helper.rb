module HomeHelper
  def user_options
    users = User.all.sort_by(&:first_name)
    [['Type text here', '', '']] + users.map{|u| [u.full_name,u.id,{:first_name => u.first_name}]}
  end
  def available_tasks
    [['Type text here', '']] + Task.created.all.map{|t| [t.title,t.id]}
  end

  def pay_item_options
    [['Type text here', '', '']] + PayItemCategory.all.map{|p| [p.name,p.earnings_rate_id]}
  end
end
