json.extract! sprint, :id

json.start_date sprint.target_start
json.end_date sprint.target_end
json.actual_cost sprint.actual_cost.to_f
