json.extract! task, :title, :id
json.budget task.budgeted_cost.to_d
json.actual_cost task.actual_cost.to_d
json.estimated_hours task.budgeted_hours
json.progress task.task_completed
json.start_date task.target_start
json.end_date task.target_finish
json.pay_rate task.pay_rate.to_d

json.pay_item do
  json.name task.pay_item.try(:name)
  json.earnings_rate_id task.pay_item.try(:earnings_rate_id)
end
json.task_manager task.task_owner.try(:full_name)
json.esc_manager task.escalation_manager.try(:full_name)
json.assignee do
  json.array! task.assignees_full_names
end
json.extract! task, :description
json.extract! task, :priority
json.project 'XXXXXX'
json.extract! task, :status
json.watching do
  json.array! task.watchers_full_names
end
json.extract! task, :sprint_id
json.extract! task, :parent_id
json.child_ids do
  json.array! task.children.ids
end
