json.extract! comment, :id
json.body comment.comment
json.author comment.user.full_name
json.created comment.created_at
