json.pay_items do
  json.array!(@pay_item_categories) do |pay_item_category|
    json.extract! pay_item_category, :earnings_rate_id, :name
  end
end
