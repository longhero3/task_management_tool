json.first_name @task_user.user.first_name
json.last_name @task_user.user.last_name
json.full_name @task_user.user.full_name
json.id @task_user.user.id
json.task_user_id @task_user.id
json.log_hours_enabled @task_user.task.log_hours_enabled current_user