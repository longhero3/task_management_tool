json.current_user do
  json.extract! current_user, :id, :email, :role
end
json.available_levels do
  levels = Task.available_levels + ['Leaflet']
  json.array! levels do |level|
    key = level == 0 ? 'All' : level
    json.key key
    json.value level
  end
end
json.tasks do
  json.array!(tasks) do |task|
    json.extract! task, :id, :title, :description, :target_finish, :priority
    json.target_finish_int task.target_finish.to_i
    json.url task_url(task, format: :json)
    json.leaflet task.leaflet?
    json.level task.depth + 1
    json.user_ids task.users.pluck(:id)
    json.time_elapsed task.time_elapsed.to_i
    json.task_completed task.task_summary.task_completed.to_i rescue nil
    json.task_completed_by_user task.task_completed_by_user(current_user.id).to_i
    json.budget_spent_ratio task.task_summary.budget_spent_ratio.to_i rescue nil
    json.task_completed_over_budget_consumed task.task_summary.task_completed_over_budget_consumed.round(2).to_s rescue nil
    json.budget_spent_by_user task.budget_spent_by_user(current_user.id).to_i
    json.budget_spent_ratio_by_user task.budget_spent_ratio_by_user(current_user.id).to_i
    json.task_completed_over_budget_consumed_by_user task.task_completed_over_budget_consumed_by_user(current_user.id).round(2).to_s
  end
end
