json.array!(@tasks) do |task|
  json.extract! task, :id, :title, :description, :target_start, :target_finish, :actual_start, :actual_finish, :budgeted_cost, :actual_cost, :priority, :pay_rate
  json.url task_url(task, format: :json)
  json.task_owner task.task_owner
  json.task_members task.task_members
  json.escalation_manager task.escalation_manager
  json.tasks task.children
end
