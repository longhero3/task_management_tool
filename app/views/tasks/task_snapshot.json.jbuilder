json.extract! @task, :id, :title, :description, :target_start, :target_finish, :actual_start, :actual_finish, :priority, :total_budget_of_archived_children, :budget_to_be_allocated, :budgeted_cost
json.url task_url(@task, format: :json)
json.budgeted_cost @task.budgeted_cost.round(2)
json.actual_cost @task.actual_cost.round(2)
json.task_completed @task.task_summary.task_completed.round(0) rescue(0)
json.budget_taken @task.budget_taken.round(2)

json.total_budget_of_archived_children number_with_precision(@task.total_budget_of_archived_children, precision: 2)
json.budget_to_be_allocated @task.budget_to_be_allocated.round(2)
json.budgeted_cost @task.budgeted_cost.round(2).to_s
json.level @task.depth + 1
json.pay_item @task.pay_item
json.pay_rate @task.try(:pay_item).try(:pay_rate)
json.log_hours_enabled @task.log_hours_enabled current_user
json.project_code @task.project_code
json.task_code @task.task_code
json.parent do
  if @task.parent
    json.extract! @task.parent, :id, :title
  else
    nil
  end
end

json.active_members do
  json.task_owner @task.task_owner
  json.task_members do
    json.array! @task.task_members do |task_user|
      json.first_name task_user.user.try(:first_name)
      json.last_name task_user.user.try(:last_name)
      json.full_name task_user.user.try(:full_name)
      json.id task_user.user.try(:id)
      json.task_user_id task_user.id
    end
  end
  json.escalation_manager @task.escalation_manager
end
json.children_count @task.root.children.count
json.child_tasks do
  json.array! [@task.root], partial: 'tasks/task', as: :task
end