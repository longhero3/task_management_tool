json.extract! @task, :id, :title, :description, :target_start, :target_finish, :actual_start, :actual_finish, :priority
json.budgeted_cost number_with_precision(@task.budgeted_cost, precision: 2)
json.url task_url(@task, format: :json)
json.actual_cost @task.actual_cost.round(2)
json.total_budget_of_archived_children number_with_precision(@task.total_budget_of_archived_children, precision: 2)
json.remaining_budget @task.remaining_budget.to_f.to_s
json.weekly_reports_start @task.weekly_reports_start
json.budget_to_be_allocated number_with_precision(@task.budget_to_be_allocated, precision: 2)
json.task_completed @task.task_completed.round(0)
json.level @task.depth + 1
json.pay_item @task.pay_item
json.log_hours_enabled @task.log_hours_enabled current_user
json.pay_rate @task.try(:pay_item).try(:pay_rate)
json.task_code @task.task_code
json.current_user current_user
json.project_code @task.project_code
json.hours_added @task.hours_added_in24
json.parent do
  if @task.parent
    json.extract! @task.parent, :id, :title
  else
    nil
  end
end
json.active_members do
  json.task_owner @task.task_owner
  json.task_members do
    json.array! @task.task_members do |task_user|
      json.first_name task_user.user.try(:first_name)
      json.last_name task_user.user.try(:last_name)
      json.full_name task_user.user.try(:full_name)
      json.id task_user.user.try(:id)
      json.task_user_id task_user.id
    end
  end
  json.escalation_manager @task.escalation_manager
end
json.children_count @task.root.children.count
json.tasks do
  json.array! [@task.root], partial: 'tasks/task', as: :task
end