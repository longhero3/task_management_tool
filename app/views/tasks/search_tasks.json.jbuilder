json.tasks do
  json.array!(@tasks) do |task|
    json.extract! task, :id
    json.title task.unique_title
    json.description "Parent task: #{task.parent.try(:title) || 'Root task'}"
    json.status task.status
    json.pay_item do
      json.name task.pay_item.try(:name)
      json.name task.pay_item.try(:earnings_rate_id)
    end
  end
end
