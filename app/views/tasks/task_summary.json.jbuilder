json.extract! @task, :id, :title, :description, :target_start, :target_finish, :actual_start, :actual_finish, :priority, :total_budget_of_archived_children, :budget_to_be_allocated, :budget_taken
json.budgeted_cost number_with_precision(@task.budgeted_cost, precision: 2)
json.target_finish_int @task.target_finish.to_i
json.weekly_reports_start @task.weekly_reports_start
json.url task_url(@task, format: :json)
json.actual_cost @task.actual_cost.round(2)
json.budget_taken @task.budget_taken.round(2)
json.total_budget_of_archived_children number_with_precision(@task.total_budget_of_archived_children, precision: 2)
json.budget_to_be_allocated @task.budget_to_be_allocated.round(2)
json.task_completed @task.task_summary.task_completed.round(0) rescue nil
json.leaflet @task.leaflet?
json.level @task.depth + 1
json.user_ids @task.users.pluck(:id)
json.time_elapsed @task.time_elapsed.round(2)
json.task_completed @task.task_completed.round(2)
json.task_completed_by_user @task.task_completed_by_user(current_user.id).to_i
json.budget_spent_ratio @task.task_summary.budget_spent_ratio.to_i rescue nil
json.task_completed_over_budget_consumed @task.task_summary.task_completed_over_budget_consumed.round(2).to_s rescue nil
json.budget_spent_by_user @task.budget_spent_by_user(current_user.id).to_i
json.budget_spent_ratio_by_user @task.budget_spent_ratio_by_user(current_user.id).to_i
json.task_completed_over_budget_consumed_by_user @task.task_completed_over_budget_consumed_by_user(current_user.id).round(2).to_s
json.log_hours_enabled @task.log_hours_enabled current_user
json.pay_rate @task.try(:pay_item).try(:pay_rate)
json.parent do
  if @task.parent
    json.extract! @task.parent, :id, :title
  else
    nil
  end
end
json.pay_item do
  json.name @task.pay_item.try(:name)
  json.earnings_rate_id @task.pay_item.try(:earnings_rate_id)
end