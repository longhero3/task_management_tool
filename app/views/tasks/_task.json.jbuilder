json.extract! task, :id, :title, :description, :target_start, :target_finish, :actual_start, :actual_finish, :priority, :pay_rate, :budget_taken
json.url task_url(task, format: :json)
json.actual_cost task.actual_cost.to_i.to_s
json.level task.depth + 1
json.hours_added task.hours_added_in24
if @task == task
  json.active_members do
    json.task_owner task.task_owner
    json.task_members do
      json.array! task.task_members do |task_user|
        json.first_name task_user.user.try(:first_name)
        json.last_name task_user.user.try(:last_name)
        json.full_name task_user.user.try(:full_name)
        json.id task_user.user.try(:id)
        json.task_user_id task_user.id
      end
    end
    json.escalation_manager task.escalation_manager

    json.task_subscribers do
      json.array! task.task_subscribers do |task_user|
        json.first_name task_user.user.first_name
        json.last_name task_user.user.last_name
        json.full_name task_user.user.full_name
        json.id task_user.user.id
        json.task_user_id task_user.id
      end
    end
  end
end

json.task_subscribers do
  json.array! task.task_subscribers do |task_user|
    json.first_name task_user.user.first_name
    json.last_name task_user.user.last_name
    json.full_name task_user.user.full_name
    json.id task_user.user.id
    json.task_user_id task_user.id
  end
end

json.escalation_manager task.escalation_manager
json.children_count task.children.count
json.tasks do
  json.array! task.children.created, partial: 'tasks/task', as: :task
end