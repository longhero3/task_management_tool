json.high_schools do
  json.array!(@high_schools) do |high_school|
    json.extract! high_school, :name
  end
end
