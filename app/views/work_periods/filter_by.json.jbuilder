json.work_periods do
  json.array!(@work_periods) do |work_period|
    json.extract! work_period, :id, :amount_hours, :date_worked, :description
    json.task_title work_period.task.title
    json.url work_period_url(work_period, format: :json)
    json.edit_hour_enabled !PayrollCalendar.log_hour_date_disabled(work_period.date_worked)
    json.actual_cost work_period.task.actual_cost
    json.budgeted_cost work_period.task.budgeted_cost
    json.pay_rate work_period.task.pay_rate
  end
end
json.total_work_periods_count @total_count
json.current_user current_user
