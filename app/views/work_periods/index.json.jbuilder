json.work_periods do
  json.array!(@work_periods) do |work_period|
    json.extract! work_period, :id, :amount_hours, :date_worked
    json.task_title work_period.task.title
    json.url work_period_url(work_period, format: :json)
  end
end
json.current_user current_user
json.edit_hour_enabled true

