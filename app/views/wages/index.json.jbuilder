json.array!(@wages) do |wage|
  json.extract! wage, :id, :is_salary, :rate, :description
  json.url wage_url(wage, format: :json)
end
