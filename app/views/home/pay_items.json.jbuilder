json.pay_items do
  json.array! PayItemCategory.order(:name).to_a do |pay_item|
    json.earnings_rate_id pay_item.earnings_rate_id
    json.name pay_item.name
  end
end