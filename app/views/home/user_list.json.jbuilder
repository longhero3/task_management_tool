json.users do
  json.array! User.order(:first_name, :last_name).all do |user|
    json.full_name user.full_name
    json.id user.id
  end
end