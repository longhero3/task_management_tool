class Comment < ApplicationRecord
  belongs_to :user
  belongs_to :task

  validates :comment, presence: { message: "body can't be blank" }
end
