class Task < ActiveRecord::Base
  include CsvExportable
  include AASM

  aasm column: 'status' do
    state :created, initial: true
    state :removed
    event :archive, after: :archive_task do
      transitions from: :created, to: :removed
    end
  end

  acts_as_nested_set order_column: :title
  accepts_nested_attributes_for :children

  has_many   :work_periods, dependent: :destroy
  has_many   :tasks, dependent: :destroy, foreign_key: :parent_id
  belongs_to :task
  has_many   :users, through: :task_users
  has_many   :task_users, dependent: :destroy
  has_many   :costs, dependent: :destroy
  has_many   :change_records, as: :remindable, dependent: :destroy
  has_many   :comments, dependent: :destroy
  has_one    :pay_item, dependent: :destroy
  belongs_to :sprint
  has_one    :task_summary, dependent: :destroy
  accepts_nested_attributes_for :tasks, :pay_item
  after_commit  :update_max_depth, on: :create
  after_commit  :update_over_budget, on: :create
  after_commit :create_summary, on: :create
  after_commit :update_summary, on: :update
  after_commit  :update_is_leaf, on: [:create, :update]
  after_commit  :update_over_budget_for_log_hours, on: :update
  after_update  :record_changed_attributes # Make sure this is at the top in after update callbacks
  after_update  :update_target_dates
  before_update :update_budget
  # before_update :update_parent

  validates :target_start, :target_finish, :budgeted_cost, :title, :description, :pay_rate, presence: true
  validates :budgeted_cost, numericality: { greater_than_or_equal_to: 0 }
  validates_datetime :target_start, before: :target_finish, before_message: I18n.t('errors.target_start_before_finish')
  validates :pay_rate, numericality: { greater_than: 0 }
  validate :must_have_valid_budget
  scope :created, -> { where(status: 'created').order('LOWER(tasks.title)') }
  scope :removed, -> { where(status: 'removed') }
  scope :pay_item_unassigned, -> { joins('LEFT JOIN pay_items on tasks.id = pay_items.task_id').where('pay_items.task_id IS NULL') }
  scope :search_by_title, -> (term) { basic_search(term).all }
  extend Enumerize
  enumerize :status, in: [:created, :removed]
  enum weekly_reports_start: [:sunday, :monday, :tuesday, :wednesday, :thursday, :friday, :saturday]

  def update_max_depth
    root = self.root
    root.max_depth = depth if depth > root.max_depth
    root.save
  end

  def archive_task
    update(budgeted_cost: actual_cost)
    self_and_descendants.update_all(status: 'removed')
  end

  def recover_task
    self_and_descendants.update_all(status: :created)
  end

  def must_have_valid_budget
    errors.add(:base, 'Budgeted cost is less than sum of child costs') if budgeted_cost < children.created.sum(:budgeted_cost)
  end

  def update_budget
    if budgeted_cost_changed?
      diff = (budgeted_cost - budgeted_cost_was)
      if parent && parent.remaining_budget < diff
        diff -= parent.remaining_budget
        parent.update(budgeted_cost:  parent.budgeted_cost + diff)
      end
    end
  end

  def update_over_budget_for_log_hours
    if remaining_actual.negative?
      reload.self_and_ancestors.update_all(budgeted_cost: budgeted_cost - remaining_actual)
    end
  end

  def update_over_budget
    if parent && parent.remaining_budget.negative?
      reload.ancestors.update_all("budgeted_cost = budgeted_cost - #{parent.remaining_budget}")
    end
  end

  def update_target_dates
    reload.descendants.where('tasks.target_start < ?', target_start).update_all(target_start: target_start)
    reload.descendants.where('tasks.target_finish > ?', target_finish).update_all(target_finish: target_finish)
    reload.ancestors.where('tasks.target_start > ?', target_start).update_all(target_start: target_start)
    reload.ancestors.where('tasks.target_finish < ?', target_finish).update_all(target_finish: target_finish)
  end

  def assign_new_parent(new_parent_id, decrease_old_parents_budget,
                        increase_new_parents_budget)
    result = false
    error = nil
    old_parent = parent
    new_parent = Task.find(new_parent_id)
    self.parent_id = new_parent.id
    if valid?
      if !increase_new_parents_budget && new_parent.child_out_of_budget?(
        budgeted_cost
      )
        error = 'You must permit to increase parents budget as your budgets is out of parents budget'
      else
        old_parent.decrease_self_and_ancestors_budget(
          budgeted_cost
        ) if old_parent.present? && decrease_old_parents_budget
        save && reload
        result, error = increase_parent_budget(budgeted_cost) if increase_new_parents_budget
      end
    else
      error = errors.full_messages.join(', ')
    end
    [result, error]
  end

  def increase_parent_budget(amount)
    new_budget = parent.budgeted_cost + amount
    if parent.update(budgeted_cost: new_budget)
      return true, nil
    else
      return nil, parent.errors.full_messages.join(', ')
    end
  end

  def decrease_self_and_ancestors_budget(amount)
    self_and_ancestors.sort_by(&:depth).reverse_each do |task|
      new_budget = task.budgeted_cost - amount.to_f
      task.update(budgeted_cost: new_budget)
    end
  end

  def decrease_ancestors_budget(amount)
    ancestors.sort_by(&:depth).reverse_each do |task|
      new_budget = task.budgeted_cost - amount.to_f
      task.update(budgeted_cost: new_budget)
    end
  end

  # def update_parent
  #   if parent_id_changed?
  #     if parent_id_was
  #       old_task = Task.find(parent_id_was)
  #       old_task.increment(:actual_cost, by = self.actual_cost)
  #     end

  #     if parent_id
  #       new_parent = Task.find(parent_id)
  #       new_parent.self_and_ancestors.update_all("budgeted_cost = budgeted_cost + #{budgeted_cost}")
  #     end
  #   end
  # end

  def create_summary
    create_task_summary
  end

  def update_summary
    if task_summary
      task_summary.update_columns(task_completed: task_completed, budget_spent_ratio: budget_spent_ratio,
                          task_completed_over_budget_consumed: task_completed_over_budget_consumed)
    end
  end

  def update_is_leaf
    update_columns(is_leaf: reload.children.count.zero?)
  end

  def actual_cost
    # Calulates the total amount of money that has been spent on this task or children of this task, represents budget used
    WorkPeriod.actual_cost_by_task self
  end

  def actual_cost=(cost)
    self[:actual_cost] = cost
  end

  def task_owner
    task_users.where(task_role: TaskUser::TASK_ROLES[:task_owner]).last.try(:user)
  end

  def escalation_manager
    task_users.where(task_role: TaskUser::TASK_ROLES[:escalation_manager]).last.try(:user)
  end

  def task_subscribers
    task_users.where(task_role: TaskUser::TASK_ROLES[:subscriber])
  end

  def task_members
    task_users.where(task_role: TaskUser::TASK_ROLES[:member])
  end

  def time_elapsed
    (DateTime.now.to_f - target_start.to_f) / (target_finish.to_f - target_start.to_f) * 100
  end

  def task_completed
    if children.count.positive?
      return 0 if budgeted_cost.zero?
      leaves.sum("tasks.total_progress * tasks.budgeted_cost/#{budgeted_cost}")
    else
      total_progress
    end
  end

  def task_completed_by_user(user_id)
    return 0 if budgeted_cost.to_i.zero?
    if children.count.positive?
      return 0 if budgeted_cost.zero?
      leaves.joins(:work_periods).where(work_periods: { user_id: user_id }).sum("tasks.total_progress * tasks.budgeted_cost/#{budgeted_cost}")
    else
      total_progress
    end
    work_periods.where(user_id: user_id).order(date_worked: :desc).first.try(:progress) || 0
  end

  def budget_spent_ratio
    return 0 if budgeted_cost.to_i.zero?
    (actual_cost / budgeted_cost) * 100
  end

  def task_completed_over_budget_consumed
    return 0 if budget_spent_ratio.zero?
    (task_completed / budget_spent_ratio)
  end

  def task_completed_over_budget_consumed_by_user(user_id)
    ratio = budget_spent_ratio_by_user(user_id)
    return 0 if ratio.zero?
    task_completed_by_user(user_id) / ratio
  end

  def budget_spent_by_user(user_id)
    work_periods.where(user_id: user_id).sum('work_periods.amount_hours * work_periods.pay_rate')
  end

  def budget_spent_ratio_by_user(user_id)
    return 0 if budgeted_cost.to_i.zero?
    (budget_spent_by_user(user_id) / budgeted_cost) * 100
  end

  def budget_spent_by_user(user_id)
    work_periods.where(user_id: user_id).sum(:amount_hours) * pay_rate
  end

  def budget_spent_ratio_by_user(user_id)
    return 0 if budgeted_cost.to_i == 0
    (budget_spent_by_user(user_id) / budgeted_cost) * 100
  end

  def out_of_parent_date?(target_start, target_finish)
    return false unless parent
    (target_start && parent.target_start > target_start) || (target_finish && parent.target_finish < target_finish)
  end

  def out_of_parent_budget?(new_budgeted_cost)
    return false unless parent
    (new_budgeted_cost && parent.budgeted_cost < siblings.sum(:budgeted_cost) + new_budgeted_cost.to_d)
  end

  def child_out_of_budget?(new_child_budget) # Check Budget with a new budget of a new child
    new_parent = self
    # new_parent.budgeted_cost  < new_parent.children.sum(:budgeted_cost) + new_child_budget.to_d
    new_parent.remaining_budget < new_child_budget.to_f
  end

  def notify_out_of_parent_date
    TaskMailer.out_of_date_range(parent).deliver_later
  end

  def notify_out_of_budget(user, increased_budget)
    TaskMailer.out_of_budget(self, user, increased_budget).deliver_later
  end

  def notify_increase_in_budget(user, increased_budget, child_title = nil, new_child_added = false)
    TaskMailer.increase_in_budget(self, user, increased_budget, child_title, new_child_added).deliver_later
  end

  def notify_decrease_in_budget(user, decreased_budget)
    TaskMailer.decrease_in_budget(self, user, decreased_budget).deliver_later
  end

  def self.available_levels
    return [0] if Task.count.zero?
    (0..Task.maximum(:depth) + 1).to_a
  end

  def budget_to_be_allocated
    if children.count == 0
      (budgeted_cost - actual_cost)
    else
      (budgeted_cost - children.sum(:budgeted_cost))
    end
  end

  def remaining_budget
    budgeted_cost - children.sum(:budgeted_cost)
  end

  def remaining_actual
    budgeted_cost - WorkPeriod.actual_cost_by_task(self)
  end

  def total_budget_of_archived_children
    children.removed.sum(:budgeted_cost)
  end

  def hours_added_in24
    wps = WorkPeriod.includes(:task).where(tasks: { id: self_and_descendants.ids }).where('work_periods.created_at BETWEEN ? AND ?', Time.zone.now.to_date.beginning_of_day, Time.zone.now.to_date.end_of_day)
    if wps.present?
      wps.sum(:amount_hours)
    else
      0
    end
  end

  def budget_taken
    # Amount of money of budget that has been allocated
    if children.count == 0
      actual_cost
    else
      children.sum(:budgeted_cost)
    end
  end

  def leaflet?
    leaf?
  end

  def hours_logged_by_staff(options)
    work_periods.where(user_id: options[:user_id])
                .where('work_periods.date_worked >= ? AND work_periods.date_worked <= ?', DateTime.parse(options[:created_after]).iso8601, DateTime.parse(options[:created_before]).iso8601)
                .sum(:amount_hours)
  end

  def log_hours_enabled(user)
    return false if depth.zero?
    return false if children.count.positive?
    return true if user.manager?
    users.include? user
  end

  def self.send_budget_reports(budget_type)
    case budget_type
    when :daily
      from_time = 1.days.ago
      to_time = Time.now
    when :fortnightly
      from_time = PayrollCalendar.latest_fortnight
      to_time = from_time + 14.days
    when :weekly
      from_time = Time.now - 1.week
      to_time = Time.now
    else
      raise "Unknown budget report type: #{budget_type}"
    end

    send_staff_hours_reports(budget_type)
    roots.created.each do |level1_task|
      if(budget_type != :weekly || (budget_type == :weekly &&  Time.zone.today.wday == weekly_reports_starts[level1_task.weekly_reports_start]))
        BudgetEmailJob.perform_later level1_task, from_time.iso8601, to_time.iso8601
      end
    end
  end

  def self.send_staff_hours_reports(report_type)
    case report_type
    when :daily
      from_time = (1.days.ago).beginning_of_day
      to_time = (Time.zone.now).beginning_of_day
    when :fortnightly
      from_time = PayrollCalendar.latest_fortnight
      to_time = from_time + 14.days
    when :weekly
      from_time = (1.week.ago).beginning_of_day
      to_time = (Time.zone.now).beginning_of_day
    else
      raise "Unknown staff hours report type: #{report_type}"
    end
    StaffHoursEmailJob.perform_later(from_time.iso8601, to_time.iso8601)
  end

  def unique_title
    "#{title} (Level #{depth + 1})"
  end

  def ancestors_escalation_managers
    User.includes(:task_users).where('task_users.task_id IN(?)',
                                     ancestors.ids).where(
                                       'task_users.task_role = ?',
                                       TaskUser::TASK_ROLES[:escalation_manager]
                                     ).references(:task_users).uniq
  end

  def budgeted_hours
    (target_finish - target_start)/1.hours
  end

  def assignees_full_names
    task_members.includes(:user).pluck("users.first_name || ' ' ||  users.last_name")
  end

  def watchers_full_names
    task_subscribers.includes(:user).pluck("users.first_name || ' ' ||  users.last_name")
  end

  def filter_work_periods(params = {})
    wps = work_periods
    wps = wps.where(start_time:
      params[:start_time].to_time) if params[:start_time]
    wps = wps.where(amount_hours:
      params[:amount_hours].to_d) if params[:amount_hours]
    wps = wps.where(description:
      params[:description]) if params[:description]
    wps
  end

  def actual_pay_rate user=nil
    if user && user.xero_account
      pay_template = user.pay_templates.find_by(earnings_rate_id: self.pay_item.earnings_rate_id)
      rate = pay_template.try(:pay_rate)
      rate ? rate : self.try(:pay_item).try(:pay_rate) || 1
    else
      self.try(:pay_item).try(:pay_rate) || 1
    end
  end

  def record_changed_attributes
    changes.each do |name, changed_values|
      rec = change_records.create_with(
        old_value: changed_values.first
      ).find_or_initialize_by(name: name)
      rec.new_value = changed_values.last
      rec.save
    end
  end

  def self.searchable_columns
    [:title, :project_code, :id]
  end

  def project_code
    if self.root == self
      if self[:project_code].nil?
        # If there is no project code, use a default value
        # of the first few characters of the title
        self.update_column :project_code, title.split.first.upcase
        self.reload
      end

      self[:project_code]
    else
      self.root.project_code
    end
  end

  def task_code
    "#{project_code}-#{id}"
  end

  def self.find_by_task_code(task_code)
    return nil if task_code.nil?
    task_code.upcase!

    task_id = task_code.split('-').last
    if task_id.nil?
      nil
    else
      task = self.find_by_id(task_id)
      (!task.nil? && task.task_code == task_code) ? task : nil
    end
  end
end
