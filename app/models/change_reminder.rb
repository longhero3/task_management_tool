class ChangeReminder < Reminder
  validates :attribute_name, presence: true

  def run
    record = ChangeRecord.find_by(name: attribute_name, remindable: remindable)
    if record
      users.each do |u|
        ChangeReminderMailer.send_change_record_notification(u, record).deliver_later
      end
    end
  end

  def self.cleanup
    ChangeRecord.delete_all
  end
end
