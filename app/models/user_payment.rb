class UserPayment < ActiveRecord::Base
  belongs_to :user, inverse_of: :user_payment
  validates :pay_rate, :extra_pay_rate, presence: true, numericality: true
end
