class ChangeRecord < ApplicationRecord
  belongs_to :remindable, polymorphic: true

  validates_presence_of :name, :old_value, :new_value
end
