class PayItem < ActiveRecord::Base
  belongs_to :task

  def pay_rate
    category = PayItemCategory.find_by_earnings_rate_id(self.earnings_rate_id)
    category.try(:pay_rate) || 1
  end
end
