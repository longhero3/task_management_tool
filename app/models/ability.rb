class Ability
  include CanCan::Ability

  def initialize(user)
    role = user ? user.role.try(:to_sym) : nil
    can :access, :rails_admin
    can :dashboard

    # admin user can read all info in the dashboard
    # can :read, :all
    case role
      when :super_admin
        can :manage, User
        can [:read, :destroy], Task
        common_rights
      when :admin
        can :read, User
        can [:read, :destroy], Task
        common_rights
        can [:create, :update, :destroy], User do |u|
          !u.admin?
        end
      when :manager
        can :read, User
        can [:read, :destroy], Task
        common_rights
        can [:create, :update, :destroy], User do |u|
          !u.manager?
        end
      when :staff
        can :read, User
        can [:update], User do |u|
          u == user
        end
      else
        cannot :manage, :all
    end
  end

  def common_rights
    can :manage, HighSchool
    can :manage, JobTitle
  end
end
