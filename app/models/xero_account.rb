class XeroAccount < ActiveRecord::Base
  belongs_to :user, inverse_of: :xero_account
  has_many :pay_templates, dependent: :destroy

  validates :employee_id, presence: true

  rails_admin do
    field :employee_id, :enum do
      label 'Xero Employee Name'
      enum do
        Employee.all.map { |employee| [employee.first_name + ' ' + employee.last_name, employee.employee_id]}
      end
    end
  end
end
