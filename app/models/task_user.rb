class TaskUser < ActiveRecord::Base
  belongs_to :task
  belongs_to :user

  extend Enumerize
  TASK_ROLES = {
    task_owner: 'task_owner',
    escalation_manager: 'escalation_manager',
    member: 'member',
    subscriber: 'subscriber'
  }
  enumerize :task_role, in: [:task_owner, :escalation_manager, :member, :subscriber]
  validates :task_role, presence: true

  def self.switch_role old_task_user, new_task_user, params
    unless new_task_user
      new_task_user = TaskUser.create(user_id: params[:switch_user_id], task_id: params[:task_id], task_role: TASK_ROLES[:member])
    end
    ActiveRecord::Base.transaction do
      begin
        if old_task_user
          old_task_user.update_attributes(task_role: 'member')
        end
        new_task_user.update_attributes(task_role: params[:task_role])
        true
      rescue ActiveRecord::StatementInvalid
        false
      end
    end
  end
end