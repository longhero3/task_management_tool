require 'active_support/concern'
module CsvExportable extend ActiveSupport::Concern
  included do
  end

  class_methods do
    def hours_logged_by_staff_to_csv(options = {})
      user = User.find(options[:user_id])
      CSV.generate({}) do |csv|
        csv << ['Staff:', user.try(:full_name)]
        csv << ['From:', options[:created_after]]
        csv << ['To:', options[:created_before]]
        csv << ['Task Title', 'Hours Logged']
        all.each do |task|
          csv << [task.title, task.hours_logged_by_staff(options)]
        end
      end
    end
  end
end