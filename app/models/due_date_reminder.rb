class DueDateReminder < Reminder
  validates :due_remind_days, presence: true

  def run
    days = (remindable.target_finish - Time.zone.now).days
    if( days >= 0 && days < due_remind_days)
      users.each do |u|
        DueDateReminderMailer.send_due_date_notification(u, self).deliver_later
      end
    end
  end
end
