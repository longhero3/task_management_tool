class Sprint < ApplicationRecord
  has_many :tasks

  def actual_cost
    tasks.sum(:actual_cost)
  end
end
