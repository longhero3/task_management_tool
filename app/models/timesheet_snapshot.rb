class TimesheetSnapshot < ActiveRecord::Base
  belongs_to :user

  def send_to_xero
    XeroClient.new.send_timesheet_snapshot self
  end
end
