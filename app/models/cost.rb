class Cost < ActiveRecord::Base
  belongs_to :cost_type
  belongs_to :user
  belongs_to :task
end
