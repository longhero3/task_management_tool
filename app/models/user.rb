class User < ActiveRecord::Base
  require 'xero_client'

  has_many :work_periods, dependent: :destroy
  has_many :tasks, through: :task_users
  has_many :task_users, dependent: :destroy
  has_many :xero_timesheets, dependent: :destroy
  has_many :timesheet_snapshots, dependent: :destroy
  belongs_to :wage
  has_many :costs
  has_many :pay_templates, through: :xero_account
  has_one :profile_image, inverse_of: :user, dependent: :destroy
  has_one :user_payment, inverse_of: :user, dependent: :destroy
  has_one :address, inverse_of: :user, dependent: :destroy
  has_one :xero_account, inverse_of: :user, dependent: :destroy
  has_many :subject_fields, inverse_of: :user, dependent: :destroy
  has_many :exam_fields, inverse_of: :user, dependent: :destroy

  accepts_nested_attributes_for :profile_image, :user_payment, :subject_fields, :exam_fields, :address, :xero_account,
                                reject_if: :all_blank, allow_destroy: true

  scope :xero_integrated, -> { joins(:xero_account).where('xero_accounts.user_id IS NOT NULL') }
  scope :search_by_full_name, -> (term) { fuzzy_search(term).take(6) }

  after_initialize :init

  def init
    self.role
  end

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  ROLES = %w[super_admin admin manager staff]

  extend Enumerize
  enumerize :role, in: [:super_admin, :admin, :manager, :staff]
  validates :role, :first_name, :last_name, :grad_job_title, :email, :second_email, :contact_number, :high_school,
            :grad_year, :user_payment, :address, presence: true
  validates :second_email, uniqueness: true


validates :exam_fields, :subject_fields, presence: true, unless: -> {grad_job_title == "Software Developer" || grad_job_title == "Software Manager"}

  validates :grad_year,
            inclusion: { in: 1900...100.years.from_now.year },
            format: {
              with: /(19|20|21)\d{2}/i,
              message: "should be a four-digit year"
            }

  def role?(base_role)
    return false if !ROLES.include?(base_role) || !ROLES.include?(self.role)
    ROLES.index(base_role.to_s) >= ROLES.index(self.role)
  end

  def method_missing(method, *args, &block)
    return self.role?(method.to_s.gsub("?", "")) if ROLES.include?(method.to_s.gsub("?", ""))
    super
  end

  def full_name
    self.first_name + ' ' + self.last_name
  end

  def get_tasks
    self.manager? ? Task.all : self.tasks
  end

  def self.roles_by_user user
    role = user.role.to_sym
    case role
      when :super_admin
        ROLES.to_a
      when :staff
        ['staff']
      else
        ROLES.to_a.reject { |role_name| ROLES.index(user.role) >= ROLES.index(role_name) }
    end
  end

  def send_xero_connect_email
    UserMailer.xero_connect(self).deliver_later
  end

  def create_timesheet_snapshot
    latest_fortnight = PayrollCalendar.latest_fortnight
    xml_timesheet_data = {}
    if latest_fortnight
      start_time = Time.parse latest_fortnight.iso8601
      end_time = Time.parse (latest_fortnight.end_of_day + 13.days).iso8601
      work_periods = self.work_periods.where(created_at: start_time .. end_time)
      if work_periods.count > 0
        work_periods.includes(:pay_item).group_by(&:pay_item).each do |pay_item, grouped_work_periods|
          next if !pay_item || pay_item.name.empty?
          grouped_relation = WorkPeriod.where(id: grouped_work_periods.map(&:id)).current_sprint_time_sheet_data(latest_fortnight).to_a
          if xml_timesheet_data["#{pay_item.earnings_rate_id}"]
            grouped_relation = grouped_relation.each_with_index.map do |element, index|
              [element[0], element[1] + xml_timesheet_data["#{pay_item.earnings_rate_id}"][index]]
            end
          end
          xml_timesheet_data["#{pay_item.earnings_rate_id}"] = grouped_relation.map { |time_set| time_set[1] }
        end
      end
    end

    unless xml_timesheet_data.empty?
      content = XeroClient.new.generate_timesheet_xml self, xml_timesheet_data
      TimesheetSnapshot.create(content: content, user: self)
    end
  end

  rails_admin do
    list do
      field :email
      field :first_name
      field :last_name
      field :state do
        formatted_value{ bindings[:object].address.state if bindings[:object].address.present? }
      end
      field :role
    end

    create do
      field :first_name
      field :last_name
      field :grad_job_title, :enum do
        label 'Job Title'
        enum do
          JobTitle.pluck(:name)
        end
      end
      field :role, :enum do
        label 'Site Access Level'
        enum do
          current_user = bindings[:view]._current_user
          User.roles_by_user current_user
        end
      end
      field :email do
        label do
          'Company Email'
        end
      end
      field :second_email do
        label 'Personal Email'
      end
      field :contact_number
      field :address
      field :grad_year do
        label 'Year of University Graduation'
      end
      field :high_school, :enum do
        enum do
          HighSchool.pluck(:name)
        end
      end
      field :user_payment do
        label 'Payment'
      end
      field :password
      field :subject_fields do
        label 'Subjects Taught'
      end
      field :exam_fields do
        label 'Exam Scores'
      end
    end

    edit do
      field :first_name
      field :last_name
      field :grad_job_title, :enum do
        label 'Job Title'
        enum do
          JobTitle.pluck(:name)
        end
      end
      field :role, :enum do
        label 'Site Access Level'
        enum do
          current_user = bindings[:view]._current_user
          User.roles_by_user current_user
        end
      end
      field :email do
        label do
          'Company Email'
        end
      end
      field :second_email do
        label 'Personal Email'
      end
      field :xero_account
      field :contact_number
      field :address do
        # render do
        #   address = bindings[:object].address || Address.new(user: bindings[:object])
        #   bindings[:view].render :partial => 'user_address', :locals => {:field => self, :form => bindings[:form], address: address}
        # end
      end
      field :grad_year do
        label 'Year of University Graduation'
      end
      field :high_school, :enum do
        enum do
          HighSchool.pluck(:name)
        end
      end
      field :password
      field :subject_fields do
        label 'Subjects Taught'
      end
      field :exam_fields do
        label 'Exam Scores'
      end
    end
  end
end
