class RemovedTask < Task
  default_scope { where(status: 'removed') }
end