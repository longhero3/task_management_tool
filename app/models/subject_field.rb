class SubjectField < ActiveRecord::Base
  belongs_to :user, inverse_of: :subject_fields

  validates :name, presence: true

  rails_admin do
    configure :name, :enum do
      enum do
        I18n.t(:subjects).invert
      end
    end
  end
end
