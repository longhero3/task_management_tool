class WorkPeriod < ActiveRecord::Base
  attribute :pay_rate, :decimal, default: 0

  belongs_to :task
  belongs_to :user

  has_one :pay_item, through: :task

  after_create :update_task_total_progress

  after_commit :update_budget, on: [:create, :update]

  validates_presence_of :task_id, :user_id, :pay_rate

  validate :hours_can_be_logged

  def update_task_total_progress
    task.total_progress = progress
    task.save
  end

  def update_budget
    if task.actual_cost > task.budgeted_cost
      task.update(budgeted_cost: task.actual_cost)
      TaskMailer.out_of_actual_budget(task,
                                      user, amount_hours.to_f).deliver_later
    end
  end

  def self.actual_cost_by_task(task)
    joins(:task).where(task_id: task.self_and_descendants.pluck(:id))
                .sum('work_periods.amount_hours * work_periods.pay_rate')
  end

  def self.current_sprint_time_sheet_data(start_date)
    if start_date
      start_time = Time.parse start_date.iso8601
      end_time = Time.parse (start_date.end_of_day + 13.days).iso8601
      self.group_by_day(:created_at, range: start_time .. end_time, timezone: 'Melbourne').sum(:amount_hours)
    else
      nil
    end
  end

  def can_log_hours?
    task && !task.children.present?
  end

  private

  def hours_can_be_logged
    errors.add(
      :invalid_task,
      'Hours cannot be logged to this task.
      Task is having some children either archived or unarchived'
    ) unless can_log_hours?
  end
end
