class ThresholdReminder < Reminder
  validates :attribute_name, :threshold_comparator, :threshold_value, presence: true

  enum threshold_comparator: [:greater_than, :less_than]

  def run
    case threshold_comparator
    when "greater_than"
      flag = remindable[attribute_name] > threshold_value
    when "less_than"
      flag = remindable[attribute_name] < threshold_value
    else
      flag = false
    end
    if flag.present?
      users.each do |u|
        ThresholdReminderMailer.send_change_record_notification(u, self).deliver_later
      end
    end
  end
end
