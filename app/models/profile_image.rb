class ProfileImage < ActiveRecord::Base
  belongs_to :user, inverse_of: :profile_image

  has_attached_file :avatar, styles: { medium: "300x300>", :small  => "150x150>", thumb: "100x100>" }, default_url: "/images/:style/missing.png"
  validates_attachment_content_type :avatar, content_type: %w{image/jpeg image/png image/gif}
  attr_accessor :asset_delete, :small, :asset_cache
  before_validation { self.asset.clear if self.asset_delete == '1' }

  rails_admin do
    configure :avatar do
      thumb_method :small

      delete_method :asset_delete     # don't forget to whitelist if you use :attr_accessible

      cache_method :asset_cache
    end
  end
end
