class ExamScore < ActiveRecord::Base
  belongs_to :exam_field, inverse_of: :exam_scores

  validates :score, presence: true, numericality: true
end
