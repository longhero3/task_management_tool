class Address < ActiveRecord::Base
  belongs_to :user, inverse_of: :address

  validates :street, :suburb, :state, presence: true

  rails_admin do
    field :street
    field :suburb
    field :state, :enum do
      enum do
        au = Carmen::Country.coded('AU')
        au.subregions.map { |subregion| subregion.code}
      end
    end
  end
end
