class ExamField < ActiveRecord::Base
  belongs_to :user, inverse_of: :exam_fields
  has_many :exam_scores, inverse_of: :exam_field, dependent: :destroy
  accepts_nested_attributes_for :exam_scores

  validates :name, :exam_scores, presence: true

  rails_admin do
    configure :name, :enum do
      enum do
        I18n.t(:exams).invert
      end
    end
  end
end
