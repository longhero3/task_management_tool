class PayrollCalendar < ActiveRecord::Base

  FORTNIGHT = 'FORTNIGHTLY'
  DEADLINE_DAYS_PUSHED = 3
  DISABLE_DAYS_PUSHED = 0
  CREATE_TIMESHEET_DAYS_PUSHED = 0
  SENDING_TIMESHEET_DAYS_PUSHED = 1
  PAY_CYCLE_DAYS = 13

  def self.latest_fortnight
    current_fortnight = self.current_fortnight_calendar
    if current_fortnight
      if Time.zone.now.to_date - current_fortnight.start_date.to_date < DEADLINE_DAYS_PUSHED
        current_fortnight.start_date.beginning_of_day - 14.days
      else
        current_fortnight.start_date.beginning_of_day
      end
    else
      nil
    end
  end

  def self.in_lock_period? date
    current_calendar_date = self.current_fortnight_calendar.start_date
    current_calendar_date.beginning_of_day <= date && (current_calendar_date).end_of_day >= date
  end

  def self.current_fortnight_calendar
    return nil if PayrollCalendar.count == 0
    fortnight_calendars = PayrollCalendar.where(calendar_type: FORTNIGHT)
    return nil if fortnight_calendars.count == 0
    fortnight_calendars.order(start_date: :desc).first
  end

  def self.current_fortnight
    return nil unless self.current_fortnight_calendar
    self.current_fortnight_calendar.start_date.change({hour: 0})
  end

  def self.previous_fortnight
    return nil unless self.current_fortnight_calendar
    self.current_fortnight_calendar.start_date.change({hour: 0}) - 14.days
  end

  def self.log_hour_date_disabled date
    return true if CONFIG.edit_hours_lock
    current_fortnight = self.current_fortnight_calendar
    if current_fortnight
      date < current_fortnight.start_date.beginning_of_day
    else
      false
    end
  end

  def self.create_timesheet_date?
    current_fortnight = self.current_fortnight_calendar
    if current_fortnight
      Time.zone.now.to_date - current_fortnight.start_date.to_date == CREATE_TIMESHEET_DAYS_PUSHED
    else
      false
    end
  end

  def self.sending_timesheet_date?
    current_fortnight = self.current_fortnight_calendar
    if current_fortnight
      Time.zone.now.to_date - current_fortnight.start_date.to_date == SENDING_TIMESHEET_DAYS_PUSHED
    else
      false
    end
  end

  def self.end_of_pay_cycle_date?
    current_calendar_date = self.current_fortnight_calendar.start_date.to_date
    Time.zone.now.to_date - current_calendar_date == PAY_CYCLE_DAYS ? true : false
  end
end
