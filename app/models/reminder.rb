class Reminder < ApplicationRecord
  belongs_to :remindable, polymorphic: true
  has_many :reminder_users
  has_many :users, through: :reminder_users

  def run
  end

  def self.cleanup
  end
end
