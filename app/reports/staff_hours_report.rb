class StaffHoursReport < ApplicationReport
  def initialize(records, period_start = nil, period_end = nil)
    @records = records
    @period_start = period_start.in_time_zone.to_formatted_s(:long_ordinal) if period_start.present?
    @period_end = period_end.in_time_zone.to_formatted_s(:long_ordinal) if period_start.present?
  end

  def period_start(status=false)
    if status == true
      @period_start = @period_start.present? ? @period_start.to_time.strftime('%a, %d %b %Y %H:%M:%S') : records.order(created_at: :asc).first.try(:created_at) || Time.zone.now
    else
      @period_start ||= records.order(created_at: :asc).first.try(:created_at).in_time_zone.to_formatted_s(:long_ordinal) || Time.zone.now
    end
  end

  def period_end(status=false)
    if status == true
      @period_end = @period_end.present? ? @period_end.to_time.strftime('%a, %d %b %Y %H:%M:%S') : records.order(created_at: :asc).first.try(:created_at) || Time.zone.now
    else
      @period_end ||= records.order(created_at: :desc).first.try(:created_at).in_time_zone.to_formatted_s(:long_ordinal) || Time.zone.now
    end
  end

  def total_expenditure
    @total_expenditure ||= records.sum('work_periods.amount_hours * work_periods.pay_rate')
  end

  def pay_data
    if @pay_data.nil?
      @pay_data = {}
      @records.includes(:user, :task).each do |wp|

        # Create new user entry if not exist already
        @pay_data[wp.user_id] ||= {
          id: wp.user_id,
          name: wp.user.full_name,
          pay_items: {},
          total_hours: BigDecimal.new(0),
          total_spend: BigDecimal.new(0)
        }
        # Create a new pay item if not exist already
        @pay_data[wp.user_id][:pay_items][wp.task_id] ||= {
          name: wp.task.title,
          pay_rate: wp.task.actual_pay_rate,
          hours: BigDecimal.new(0),
          individual_hours: [],
          backlogged: false,
          descriptions: [],
          total_spend: BigDecimal.new(0)
        }
        # Add logged hours
        @pay_data[wp.user_id][:pay_items][wp.task_id][:hours] += wp.amount_hours
        @pay_data[wp.user_id][:pay_items][wp.task_id][:descriptions] << wp.description
        @pay_data[wp.user_id][:pay_items][wp.task_id][:individual_hours] << wp.amount_hours
        @pay_data[wp.user_id][:total_hours] += wp.amount_hours

        @pay_data[wp.user_id][:pay_items][wp.task_id][:total_spend] += wp.amount_hours * (wp.pay_rate ? wp.pay_rate : 0)
        @pay_data[wp.user_id][:total_spend] += wp.amount_hours * (wp.pay_rate ? wp.pay_rate : 0)

        @pay_data[wp.user_id][:pay_items][wp.task_id][:backlogged] ||= wp.date_worked > period_end
      end

      # Flatten keys
      @pay_data = @pay_data.values.sort_by { |user| user[:name] }

      # Flatten pay items
      @pay_data = @pay_data.each do |user|
        pay_items = user[:pay_items].values
        user[:pay_items] = pay_items.sort_by { |task| task[:name] }
      end
    end

    @pay_data
  end

  def csv_data
    data = pay_data
    start_time = period_start
    end_time = period_end

    CSV.generate({}) do |csv|
      data.each do |user|
        csv << ['Staff:', user[:name]]
        csv << ['From:', start_time]
        csv << ['To:', end_time]
        csv << ['Task Title', 'Hours Logged', 'Root Task']
        task_records = Task.includes(:work_periods).where(id: @records.pluck(:task_id).uniq).where(work_periods: { user_id: user[:id]})
        task_records.all.each do |task|
          csv << [task.title, task.hours_logged_by_staff({created_after: start_time.to_s, created_before: end_time.to_s, user_id: user[:id]}), task.root.title]
        end
        csv << ['Total', user[:total_hours]]
        csv << ['']
      end
    end
  end
end
