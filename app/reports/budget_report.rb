class BudgetReport < ApplicationReport
  attr_accessor :staff_hours_report

  def initialize(task, work_periods, from_time, to_time)
    @task  = task
    @staff_hours_report = StaffHoursReport.new(work_periods, from_time, to_time)
    @from_time = from_time
    @to_time = to_time
  end

  def period_start(status=false)
    if status == true
      @from_time.to_time.strftime('%a, %d %b %Y %H:%M:%S')
    else
      @from_time.in_time_zone.to_formatted_s(:long_ordinal)
    end
  end

  def period_end(status=false)
    if status == true
      @to_time.to_time.strftime('%a, %d %b %Y %H:%M:%S')
    else
      @to_time.in_time_zone.to_formatted_s(:long_ordinal)
    end
  end

  def budget_data
    @budget_data ||= summarise_children(@task)
  end

  def pay_data
    @pay_data ||= summarise_work_periods(@task)
  end

  def summarise_children(task)
    children_budgets = task.children.map { |child| summarise_children child }.compact
    children_budgets.sort_by! { |child| child[:title].downcase }

    child_task_ids = task.self_and_descendants.pluck(:id)
    wp_periods = WorkPeriod.where(task_id: child_task_ids).where('created_at BETWEEN ? AND ?', @from_time, @to_time).to_a

    if (@from_time.to_time.beginning_of_week.to_date < @from_time.to_time.to_date) && (@from_time.to_time.end_of_week.to_date<=@from_time.to_time.to_date)
      from_time = @from_time.to_time.next_week
      to_time = from_time.end_of_week
    else
      from_time = @from_time.to_time.beginning_of_week
      to_time = @from_time.to_time.end_of_week
    end

    weeekly_wp_periods = WorkPeriod.where(task_id: task.self_and_descendants.ids).where(created_at: from_time .. to_time)

    delta_week_hours = weeekly_wp_periods.sum { |wp| wp.amount_hours }
    delta_week_cost = weeekly_wp_periods.sum { |wp| wp.amount_hours * ( wp.pay_rate ? wp.pay_rate : 0) }

    # daily_from_time = DateTime.now.beginning_of_day
    # daily_to_time = DateTime.now.end_of_day

    # today_wp_periods = WorkPeriod.where(task_id: child_task_ids).where('created_at BETWEEN ? AND ?', daily_from_time, daily_to_time).to_a
    delta_hours = wp_periods.sum { |wp| wp.amount_hours }
    delta_cost = wp_periods.sum { |wp| wp.amount_hours * ( wp.pay_rate ? wp.pay_rate : 0) }

    total_hours = task.work_periods.sum(:amount_hours)
    total_cost = total_hours * task.pay_rate

    return nil if children_budgets.count < 1 && task.status == "removed"  && !task.parent.nil? && !(delta_hours > 0)

    {
      title: task.title,
      new_task: (task.created_at > @from_time),
      percent_te: task.time_elapsed,
      percent_tc: task.task_completed,
      percent_bu: task.budget_spent_ratio,
      total_spending: task.actual_cost,
      total_hours: total_hours,
      delta_hours: delta_hours,
      delta_week_cost: delta_week_cost,
      delta_cost: delta_cost,
      budget_remaining: task.budgeted_cost - task.actual_cost,
      children: children_budgets,
      colour_tcte: task.colour_tcte,
      removed: task.status == "removed"
    }
  end

  def summarise_work_periods(task)
    pay_data = {}
    task_ids = task.self_and_descendants.pluck(:id)
    work_periods = WorkPeriod.includes(:task, :user).where('created_at BETWEEN ? AND ?', @from_time, @to_time).where(task_id: task_ids)
    work_periods.each do |wp|
      # Create new user entry if not exist already
      pay_data[wp.user_id] ||= {
        name: wp.user.full_name,
        pay_items: {}
      }
      # Create a new pay item if not exist already
      pay_data[wp.user_id][:pay_items][wp.task_id] ||= {
        name: wp.task.title,
        pay_rate: wp.pay_rate,
        hours: BigDecimal.new(0),
        date_worked: wp.date_worked
      }
      # Add logged hours
      pay_data[wp.user_id][:pay_items][wp.task_id][:hours] += wp.amount_hours
    end
		pay_data
  end

  def total_expenditure
		budget_data[:children].sum{|child| child[:delta_cost]}
  end

  def flatten_budget_data
    depth_first_budget(budget_data)
  end

  def depth_first_budget(budget_data)
    flatten = []
    stack = []
    budget_data[:level] = -1
    stack.push(budget_data)
    while(!stack.empty?)
      v = stack.pop()
      flatten.push(v)
      v[:children].reverse.each do |child|
        child[:level] = v[:level] + 1
        stack.push(child)
      end
    end
    flatten
  end
end
