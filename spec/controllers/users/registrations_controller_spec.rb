require 'rails_helper'

RSpec.describe Users::RegistrationsController, type: :controller do
  before do
    @request.env["devise.mapping"] = Devise.mappings[:user]
  end

  let(:valid_attributes) do
    {
      user:  {
        first_name: "John",
        last_name: "Smith",
        grad_job_title: "asdf",
        email: "example@gradready.com.au",
        second_email: "example@example.com",
        date_of_birth: "1995-12-12",
        contact_number: "0412345678",
        address_attributes: {
          street: "Street",
          suburb: "Suburb",
          state: "VIC"
        },
        profile_image_attributes: {
          avatar: fixture_file_upload('user.png', 'image/png')
        },
        grad_year: "2016",
        high_school: "Highschool",
        user_payment_attributes: {
          pay_rate: "20",
          extra_pay_rate: "20"
        },
        password: "password1",
        password_confirmation: "password1",
        subject_fields_attributes: {
          "0" => {
            name: "gamsat_biology",
            assessed_for_proficiency: "1",
          }
        },
        exam_fields_attributes: {
          "0" => {
            name: "gamsat",
            exam_scores_attributes: {
              "0" => {
                score: "10"
              }
            }
          },
        }
      }
    }
  end

  describe '#create' do
    it 'should create user when valid' do
      expect { post :create, params: valid_attributes }.to change(User, :count).by(1)
    end

    context 'with email to hr' do
      it 'should send an email' do
        attributes = valid_attributes
        attributes[:auto_contact_hr] = "on"

        expect {
          post :create, params: attributes
        }.to change { ActionMailer::Base.deliveries.count }.by(1)
      end
    end

    context 'without email to hr' do
      it 'should not send an email' do
        expect {
          post :create, params: valid_attributes
        }.to change { ActionMailer::Base.deliveries.count }.by(0)
      end
    end
  end
end