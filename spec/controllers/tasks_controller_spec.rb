require 'rails_helper'

RSpec.describe TasksController, type: :controller do
  before do
    @current_user = FactoryGirl.create(:user, role: 'admin')
    sign_in @current_user
  end
  describe 'GET index' do
    let(:task) { create(:task) }

    it 'should list all the task' do
      get :index, params: {}
      expect(assigns(:tasks)).to eq [task]
    end

  end

  describe 'GET main' do
    let(:task) { create(:task) }

    it 'should get the specified task' do
      get :main, params: { id: task.id }
      expect(assigns(:task)).to eq task
    end
  end

  describe 'POST create' do

    let!(:user) { create(:user) }
    let(:task_params) { attributes_for(:task).merge!(user_ids: [user.id]) }
    let!(:manager) { create(:user, role: 'manager') }

    context 'tasked created successfully' do
      it 'should change the number of tasks to 1' do
        expect{
          post :create, params: { task: task_params }
        }.to change{Task.count}.by(1)
      end

      it 'should redirect to task view' do
        post :create, params: { task: task_params }
        expect(response).to redirect_to("#{root_url}#task_id=#{Task.last.id}")
      end
    end
  end

  describe 'PUT update' do
    let(:task) { create(:task) }
    let(:task_params) { attributes_for(:task).merge!(title: 'Test Title') }

    context 'the task is updated successfully' do
      it 'redirects to the home page' do
        put :update, params: { task: task_params, id: task.id }
        expect(response).to redirect_to home_index_path
      end

      it 'changes the title of the task' do
        put :update, params: { task: task_params, id: task.id }
        expect(task.reload.title).to eq task_params[:title]
      end
    end
  end

  describe 'DELETE destroy' do
    let!(:task) { create(:task) }

    context 'the task is deleted successfully' do
      it 'deletes the Task from the database' do
        expect{
          delete :destroy, params: { id: task.id }
        }.to change{Task.count}.by(-1)
      end

      it 'redirects to the task index page' do
        delete :destroy, params: { id: task.id }
        expect(response).to redirect_to tasks_url
      end
    end
  end
end
