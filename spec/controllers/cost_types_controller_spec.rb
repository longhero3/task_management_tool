require 'rails_helper'

RSpec.describe CostTypesController, type: :controller do
  describe 'GET index' do
    let(:cost_type) { create(:cost_type) }

    it 'should list all the work period' do
      get :index, params: {}
      expect(assigns(:cost_types)).to eq [cost_type]
    end

  end

  describe 'POST create' do

    let(:cost_type_params) { attributes_for(:cost_type) }

    context 'cost type created successfully' do
      it 'should change the number of cost to 1' do
        expect{
          post :create, params: { cost_type: cost_type_params }
        }.to change{CostType.count}.by(1)
      end

      it 'should redirect to cost type view' do
        post :create, params: { cost_type: cost_type_params }
        expect(response).to redirect_to(CostType.first)
      end
    end
  end

  describe 'PUT update' do
    let(:cost_type) { create(:cost_type) }
    let(:cost_type_params) { attributes_for(:cost_type).merge!(cost_type: 'New Cost type') }

    context 'the work period is updated successfully' do
      it 'redirects to the updated cost' do
        put :update, params: { cost_type: cost_type_params, id: cost_type.id }
        expect(response).to redirect_to(cost_type)
      end

      it 'changes the amount of the cost' do
        put :update, params: { cost_type: cost_type_params, id: cost_type.id }
        expect(cost_type.reload.cost_type).to eq cost_type_params[:cost_type]
      end
    end
  end

  describe 'DELETE destroy' do
    let!(:cost_type) { create(:cost_type) }

    context 'the cost type is deleted successfully' do
      it 'deletes the cost type record from the database' do
        expect{
          delete :destroy, params: { id: cost_type.id }
        }.to change{CostType.count}.by(-1)
      end

      it 'redirects to the work period index page' do
        delete :destroy, params: { id: cost_type.id }
        expect(response).to redirect_to cost_types_url
      end
    end
  end
end
