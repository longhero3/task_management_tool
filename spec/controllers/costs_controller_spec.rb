require 'rails_helper'

RSpec.describe CostsController, type: :controller do
  describe 'GET index' do
    let(:cost) { create(:cost) }

    it 'should list all the work period' do
      get :index, params: {}
      expect(assigns(:costs)).to eq [cost]
    end

  end

  describe 'POST create' do

    let(:cost_params) { attributes_for(:cost) }

    context 'cost created successfully' do
      it 'should change the number of cost to 1' do
        expect{
          post :create, params: { cost: cost_params }
        }.to change{Cost.count}.by(1)
      end

      it 'should redirect to cost view' do
        post :create, params: { cost: cost_params }
        expect(response).to redirect_to(Cost.first)
      end
    end
  end

  describe 'PUT update' do
    let(:cost) { create(:cost) }
    let(:cost_params) { attributes_for(:cost).merge!(amount: 2) }

    context 'the work period is updated successfully' do
      it 'redirects to the updated cost' do
        put :update, params: { cost: cost_params, id: cost.id }
        expect(response).to redirect_to(cost)
      end

      it 'changes the amount of the cost' do
        put :update, params: { cost: cost_params, id: cost.id }
        expect(cost.reload.amount).to eq cost_params[:amount]
      end
    end
  end

  describe 'DELETE destroy' do
    let!(:cost) { create(:cost) }

    context 'the work period is deleted successfully' do
      it 'deletes the Task from the database' do
        expect{
          delete :destroy, params: { id: cost.id }
        }.to change{Cost.count}.by(-1)
      end

      it 'redirects to the work period index page' do
        delete :destroy, params: { id: cost.id }
        expect(response).to redirect_to costs_url
      end
    end
  end
end
