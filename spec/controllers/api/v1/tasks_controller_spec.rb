require 'rails_helper'

RSpec.describe Api::V1::TasksController, type: :controller do
  let(:user) { FactoryGirl.create(:user, role: 'admin') }
  let(:token) { double acceptable?: true }
  let(:task) { FactoryGirl.create(:task) }
  before do
    allow(controller).to receive(:doorkeeper_token) { token }
    allow(controller).to receive(:current_user) { user }
  end

  describe 'GET #index' do
    context 'when authorization has succeeded' do
      it 'responds with 200' do
        get :index, format: :json
        expect(response.status).to eq 200
      end

      it 'call to search service of TaskService' do
        expect(TaskService).to receive(:search)
        get :index, params: {}, format: :json
      end

      it 'returns tasks as json' do
        get :index, params: {}, format: :json
        expect(assigns(:tasks)).to eq []
      end
    end

    context 'when authorization has failed' do
      before do
        token = double(:token, acceptable?: false, accessible?: false)
        allow(controller).to receive(:doorkeeper_token) { token }
      end
      it 'returns the error response' do
        get :index, format: :json

        expect(response.status).to eq 401
        expect(response.headers['WWW-Authenticate']).to match(/Bearer/)
      end
    end
  end

  describe 'POST #create' do
    context 'when authorization has succeeded' do
      let(:user1) { FactoryGirl.create(:user, role: 'staff') }
      let(:user2) { FactoryGirl.create(:user, role: 'staff') }

      context 'with valid params' do
        let(:valid_task_params) do
          { title: 'Test', description: 'Test', target_start: 12_213_123,
            actual_start: 123_123_123, target_finish: 123_123_123,
            actual_finish: 12_312_312, budgeted_cost: 234_234,
            actual_cost: 0, priority: 0, parent_id: nil, pay_rate: 1,
            escalation_manager: 23, pay_item_attributes: {
              earnings_rate_id: 'asdasdw', name: 'test123'
            },
            user_ids: [user1.id, user2.id] }
        end

        it 'call to create service of TaskService' do
          expect(TaskService).to receive(:create)
          post :create, params: valid_task_params
        end

        it 'should increase the number of tasks by 1' do
          expect do
            post :create, params: valid_task_params
          end.to change { Task.count }.by(1)
        end

        it 'responds with 200' do
          post :create, params: valid_task_params
          expect(response.status).to eq 200
        end
      end

      context 'with invalid params' do
        let(:invalid_task_params) do
          { title: 'Test' }
        end

        it 'should not increase the number of tasks' do
          expect do
            post :create, params: invalid_task_params
          end.to change { Task.count }.by(0)
        end

        it 'responds with 422 unprocessable entity' do
          post :create, params: invalid_task_params
          expect(response.status).to eq 422
        end
      end
    end

    context 'when authorization has failed' do
      before do
        token = double(:token, acceptable?: false, accessible?: false)
        allow(controller).to receive(:doorkeeper_token) { token }
      end

      it 'returns the error response' do
        post :create, params: {}

        expect(response.status).to eq 401
        expect(response.headers['WWW-Authenticate']).to match(/Bearer/)
      end
    end
  end

  describe 'GET #show' do
    context 'when authorization has succeeded' do
      context 'with valid id' do
        it 'returns task as json' do
          get :show, params: { id: task.id }
          expect(assigns(:task)).to eq task
        end

        it 'responds with 200' do
          get :show, params: { id: task.id }
          expect(response.status).to eq 200
        end
      end

      context 'with invalid id' do
        it 'responds with error_code TASK-404' do
          get :show, params: { id: 'xyz' }
          expect(response.status).to eq 200
          expect(JSON.parse(@response.body)['error_code']).to eq 'TASK-404'
        end
      end
    end

    context 'when authorization has failed' do
      before do
        token = double(:token, acceptable?: false, accessible?: false)
        allow(controller).to receive(:doorkeeper_token) { token }
      end
      it 'returns the error response' do
        get :show, params: { id: 'xyz' }

        expect(response.status).to eq 401
        expect(response.headers['WWW-Authenticate']).to match(/Bearer/)
      end
    end
  end

  describe 'PUT #update' do
    context 'when authorization has succeeded' do
      context 'with valid params' do
        let(:valid_task_params) do
          { id: task.id, title: 'Test112' }
        end

        it 'should chnage the title' do
          put :update, params: valid_task_params
          expect(task.reload.title).to eq valid_task_params[:title]
        end

        it 'responds with 200' do
          put :update, params: valid_task_params
          expect(response.status).to eq 200
        end
      end

      context 'with invalid params' do
        let(:invalid_task_params) do
          { id: task.id, title: '' }
        end

        it 'should not increase the number of tasks' do
          put :update, params: invalid_task_params
          expect(JSON.parse(@response.body)['error_code']).to eq 'TASK-001'
        end
      end
    end

    context 'when authorization has failed' do
      before do
        token = double(:token, acceptable?: false, accessible?: false)
        allow(controller).to receive(:doorkeeper_token) { token }
      end

      it 'returns the error response' do
        put :update, params: { id: task.id }

        expect(response.status).to eq 401
        expect(response.headers['WWW-Authenticate']).to match(/Bearer/)
      end
    end
  end

  describe 'GET #comments' do
    context 'when authorization has succeeded' do
      it 'responds with 200' do
        get :comments, params: { id: task.id }, format: :json
        expect(response.status).to eq 200
      end
    end

    context 'when authorization has failed' do
      before do
        token = double(:token, acceptable?: false, accessible?: false)
        allow(controller).to receive(:doorkeeper_token) { token }
      end
      it 'returns the error response' do
        get :comments, params: { id: task.id }, format: :json

        expect(response.status).to eq 401
        expect(response.headers['WWW-Authenticate']).to match(/Bearer/)
      end
    end
  end

  describe 'POST post_comment' do
    context 'when authorization has succeeded' do
      context 'with valid params' do
        it 'responds with status 204 no content' do
          post :post_comment, params: { id: task.id }
          expect(response.status).to eq 200
        end

        it 'should increase the count by 1' do
          expect do
            post :post_comment, params: {id: task.id, body: "test comment 1234"}
          end.to change { Comment.count }.by(1)
        end
      end
    end

    context 'when authorization has failed' do
      before do
        token = double(:token, acceptable?: false, accessible?: false)
        allow(controller).to receive(:doorkeeper_token) { token }
      end

      it 'returns the error response' do
        post :post_comment, params: { id: task.id }

        expect(response.status).to eq 401
        expect(response.headers['WWW-Authenticate']).to match(/Bearer/)
      end
    end
  end

  describe 'GET #work_periods' do
    context 'when authorization has succeeded' do
      let(:work_period) { FactoryGirl.create(:work_period, task_id: task.id) }
      # it 'call to filter_work_periods of Task' do
      #   get :work_periods, params: {id: task.id}, format: :json
      #   expect(task).to receive(:filter_work_periods)
      # end

      it 'returns work_periods as json' do
        get :work_periods, params: { id: task.id }, format: :json
        expect(assigns(:work_periods)).to eq [work_period]
      end

      it 'responds with 204' do
        get :work_periods, params: { id: task.id }, format: :json
        expect(response.status).to eq 204
      end
    end

    context 'when authorization has failed' do
      before do
        token = double(:token, acceptable?: false, accessible?: false)
        allow(controller).to receive(:doorkeeper_token) { token }
      end
      it 'returns the error response' do
        get :work_periods, params: { id: task.id }, format: :json

        expect(response.status).to eq 401
        expect(response.headers['WWW-Authenticate']).to match(/Bearer/)
      end
    end
  end

  describe 'POST #log_hours' do
    context 'when authorization has succeeded' do
      let(:work_period) { FactoryGirl.create(:work_period, task_id: task.id) }
      # it 'call to filter_log_hours of Task' do
      #   get :log_hours, params: {id: task.id}, format: :json
      #   expect(task).to receive(:filter_log_hours)
      # end
      context 'with valid params' do
        let(:valid_log_params) do
          { id: task.id, start_time: (DateTime.now + 1.minutes).to_i,
            amount_hours: 1, description: 'xyz',
            end_time: (DateTime.now + 10.minutes).to_i,
            date_worked: DateTime.now + 1.minutes,
            progress: 0 }
        end

        it 'should increase the number of work_periods by 1' do
          expect do
            post :log_hours, params: valid_log_params
          end.to change { WorkPeriod.count }.by(1)
        end

        it 'responds with 204' do
          post :log_hours, params: valid_log_params
          expect(response.status).to eq 204
        end
      end

      context 'with invalid params' do
        let(:invalid_log_params) do
          { id: task.id, start_time: (DateTime.now + 11.minutes).to_i,
            amount_hours: 1, description: 'xyz',
            end_time: (DateTime.now + 10.minutes).to_i, date_worked: nil,
            progress: 0 }
        end

        it 'should not increase the number of work_periods' do
          expect do
            post :log_hours, params: invalid_log_params
          end.to change { WorkPeriod.count }.by(0)
        end

        it 'responds with 422 unprocessable entity' do
          post :log_hours, params: invalid_log_params
          expect(response.status).to eq 422
        end
      end
    end

    context 'when authorization has failed' do
      before do
        token = double(:token, acceptable?: false, accessible?: false)
        allow(controller).to receive(:doorkeeper_token) { token }
      end
      it 'returns the error response' do
        post :log_hours, params: { id: task.id }
        expect(response.status).to eq 401
        expect(response.headers['WWW-Authenticate']).to match(/Bearer/)
      end
    end
  end
end
