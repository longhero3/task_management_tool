require 'rails_helper'

RSpec.describe Api::V1::SprintsController, type: :controller do
  let!(:user) { FactoryGirl.create(:user, role: 'admin') }
  let!(:token) { double acceptable?: true }
  let!(:task) { FactoryGirl.create(:task) }
  before do
    allow(controller).to receive(:doorkeeper_token) { token }
    allow(controller).to receive(:current_user) { user }
  end

  describe 'GET #index' do
    context 'when authorization has succeeded' do
      it 'responds with 200' do
        get :index
        expect(response.status).to eq 200
      end

      it 'call to search service of SprintService' do
        expect(SprintService).to receive(:search)
        get :index
      end

      it 'returns sprints as json' do
        get :index
        expect(assigns(:sprints)).to eq Sprint.all
      end
    end

    context 'when authorization has failed' do
      before do
        token = double(:token, acceptable?: false, accessible?: false)
        allow(controller).to receive(:doorkeeper_token) { token }
      end
      it 'returns the error response' do
        get :index

        expect(response.status).to eq 401
        expect(response.headers['WWW-Authenticate']).to match(/Bearer/)
      end
    end
  end

  describe 'POST #create' do
    context 'when authorization has succeeded' do
      context 'with valid params' do
        let(:valid_sprint_params) do
          { start_date: 1231231, end_date: 2312312313,
            actual_cost: 12
          }
        end

        it 'call to create service of SprintService' do
          expect(SprintService).to receive(:create)
          post :create, params: valid_sprint_params
        end

        it 'assigns sprint with newly created sprint' do
          post :create, params: valid_sprint_params
          expect(assigns(:sprint)).to eq 'Test Sprint'
        end

        it 'responds with 200' do
          post :create, params: valid_sprint_params
          expect(response.status).to eq 200
        end
      end
    end

    context 'when authorization has failed' do
      before do
        token = double(:token, acceptable?: false, accessible?: false)
        allow(controller).to receive(:doorkeeper_token) { token }
      end

      it 'returns the error response' do
        post :create, params: {}

        expect(response.status).to eq 401
        expect(response.headers['WWW-Authenticate']).to match(/Bearer/)
      end
    end
  end

end
