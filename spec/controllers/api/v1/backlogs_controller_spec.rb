require 'rails_helper'

RSpec.describe Api::V1::BacklogsController, type: :controller do
  let!(:user) { FactoryGirl.create(:user, role: 'admin') }
  let!(:token) { double acceptable?: true }
  let!(:task) { FactoryGirl.create(:task) }
  before do
    allow(controller).to receive(:doorkeeper_token) { token }
    allow(controller).to receive(:current_user) { user }
  end

  describe 'GET #index' do
    context 'when authorization has succeeded' do
      it 'responds with 200' do
        get :index
        expect(response.status).to eq 200
      end

      it 'call to backlogs service of BacklogService' do
        expect(BacklogService).to receive(:backlogs)
        get :index
      end

      it 'returns tasks as json' do
        get :index
        expect(assigns(:backlogs)).to eq Task.limit(10)
      end
    end

    context 'when authorization has failed' do
      before do
        token = double(:token, acceptable?: false, accessible?: false)
        allow(controller).to receive(:doorkeeper_token) { token }
      end
      it 'returns the error response' do
        get :index

        expect(response.status).to eq 401
        expect(response.headers['WWW-Authenticate']).to match(/Bearer/)
      end
    end
  end
end
