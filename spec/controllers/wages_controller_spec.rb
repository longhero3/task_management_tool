require 'rails_helper'

RSpec.describe WagesController, type: :controller do
  describe 'GET index' do
    let(:wage) { create(:wage) }

    it 'should list all the work period' do
      get :index, params: {}
      expect(assigns(:wages)).to eq [wage]
    end

  end

  describe 'POST create' do

    let(:wage_params) { attributes_for(:wage) }

    context 'wage created successfully' do
      it 'should change the number of wage to 1' do
        expect{
          post :create, params: { wage: wage_params }
        }.to change{Wage.count}.by(1)
      end

      it 'should redirect to wage view' do
        post :create, params: { wage: wage_params }
        expect(response).to redirect_to(Wage.first)
      end
    end
  end

  describe 'PUT update' do
    let(:wage) { create(:wage) }
    let(:wage_params) { attributes_for(:wage).merge!(description: 'New Description') }

    context 'the work period is updated successfully' do
      it 'redirects to the updated wage' do
        put :update, params: { wage: wage_params, id: wage.id }
        expect(response).to redirect_to(wage)
      end

      it 'changes the amount of the wage' do
        put :update, params: { wage: wage_params, id: wage.id }
        expect(wage.reload.description).to eq wage_params[:description]
      end
    end
  end

  describe 'DELETE destroy' do
    let!(:wage) { create(:wage) }

    context 'the work period is deleted successfully' do
      it 'deletes the Task from the database' do
        expect{
          delete :destroy, params: { id: wage.id }
        }.to change{Wage.count}.by(-1)
      end

      it 'redirects to the work period index page' do
        delete :destroy, params: { id: wage.id }
        expect(response).to redirect_to wages_url
      end
    end
  end
end
