require 'rails_helper'

RSpec.describe WorkPeriodsController, type: :controller do
  let(:user) { create(:user) }
  let(:task) { create(:task) }
  before do
    sign_in user
  end

  describe 'GET index' do
    let(:work_period) { create(:work_period) }

    it 'should list all the work periods' do
      get :index, params: {}
      expect(assigns(:work_periods)).to eq [work_period]
    end

  end

  describe 'POST create' do
    before do
      @current_user = FactoryGirl.create(:user)
      sign_in @current_user
    end

    let(:work_period_params) { attributes_for(:work_period).merge!(task_id: task.id) }

    context 'work period created successfully' do
      it 'should change the number of work period to 1' do
        expect{
          post :create, params: { work_period: work_period_params }
        }.to change{WorkPeriod.count}.by(1)
      end

      it 'should redirect to work period view' do
        post :create, params: { work_period: work_period_params }
        expect(response).to redirect_to("#{root_url}#task_id=#{task.id}")
      end
    end
  end

  describe 'send_exceeded_log_hours_notification' do
    let!(:task_manager) { FactoryGirl.create(:task_user, :to, task: task)}
    context 'with valid params' do
      it 'should notify manager' do
        post :send_exceeded_log_hours_notification, params: { task_id: task.id, amount_hours: 99999 }
        expect(ActionMailer::Base.deliveries.last.to).to eq([task.task_owner.try(:email)])
      end
    end
  end

  describe 'PUT update' do
    let(:work_period) { create(:work_period, user: user) }
    let(:work_period_params) { attributes_for(:work_period).merge!(amount_hours: 3) }

    context 'the work period is updated successfully' do
      it 'redirects to the updated work period' do
        put :update, params: { work_period: work_period_params, id: work_period.id }
        expect(response).to redirect_to(work_period)
      end

      it 'changes the amount hour of the work period' do
        put :update, params: { work_period: work_period_params, id: work_period.id }
        expect(work_period.reload.amount_hours).to eq work_period_params[:amount_hours]
      end
    end

    context 'current user edits the hours' do
      before do
        sign_in user
      end

      it 'should not send the notify email' do
        put :update, params: { work_period: work_period_params, id: work_period.id }
        expect(ActionMailer::Base.deliveries.last.to).not_to eq [user.email]
      end
    end

    context 'admin edits the hours' do
      let(:admin) { create(:user, role: 'admin') }
      before do
        sign_in admin
      end

      it 'sends the email to the work log owner' do
        put :update, params: { work_period: work_period_params, id: work_period.id }
        expect(ActionMailer::Base.deliveries.last.to).to eq [work_period.user.email]
      end
    end

  end

  describe 'DELETE destroy' do
    let!(:work_period) { create(:work_period, user: user) }

    context 'the task is deleted successfully' do
      it 'deletes the work period from the database' do
        expect{
          delete :destroy, params: { id: work_period.id }
        }.to change{WorkPeriod.count}.by(-1)
      end

      it 'redirects to the work period index page' do
        delete :destroy, params: { id: work_period.id }
        expect(response).to redirect_to work_periods_url
      end
    end

    context 'current user destroy the hours' do
      before do
        sign_in user
      end

      it 'should not send the notify email' do
        delete :destroy, params: { id: work_period.id }
        expect(ActionMailer::Base.deliveries.last.to).not_to eq [user.email]
      end
    end

    context 'admin destroy the hours' do
      let(:admin) { create(:user, role: 'admin') }
      before do
        sign_in admin
      end

      it 'sends the email to the work log owner' do
        delete :destroy, params: { id: work_period.id }
        expect(ActionMailer::Base.deliveries.last.to).to eq [work_period.user.email]
      end
    end
  end
end
