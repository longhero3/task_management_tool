require 'rails_helper'

RSpec.describe BudgetEmailJob, type: :job do
  describe Clockwork do
    after(:each) { Clockwork::Test.clear! }
    before(:each) { allow(PayrollCalendar).to receive(:latest_fortnight).and_return(Time.parse "2016-07-25T10:00:00+10:00") }

    describe 'budget_reports.fortnightly' do
      context 'is the day of report' do
        it 'sends fortnightly' do
          start_time = (PayrollCalendar.latest_fortnight + 14.days).beginning_of_day
          end_time = start_time + 2.hours

          Clockwork::Test.run(start_time: start_time, end_time: end_time, tick_speed: 1.minute)

          expect(Clockwork::Test.ran_job?("budget_reports.fortnightly")).to be_truthy
        end
      end

      context 'is not day of report' do
        it 'does not send' do
          start_time = (PayrollCalendar.latest_fortnight + 13.days).beginning_of_day + 2.hours
          end_time = start_time + 2.hours

          Clockwork::Test.run(start_time: start_time, end_time: end_time, tick_speed: 1.minute)

          expect(Clockwork::Test.ran_job?("budget_reports.fortnightly")).not_to be_truthy
        end
      end
    end

    describe 'budget_reports.daily' do
      it 'sends daily' do
        start_time = Time.zone.today
        end_time = start_time + 2.hours

        Clockwork::Test.run(start_time: start_time, end_time: end_time, tick_speed: 1.minute)

        expect(Clockwork::Test.ran_job?("budget_reports.daily")).to be_truthy
      end
    end

    describe 'budget_reports.weekly' do
      context 'on Monday' do
        it 'sends budget_reports Monday midnight' do
          start_time = Time.zone.now.monday
          end_time = start_time + 2.hours

          Clockwork::Test.run(start_time: start_time, end_time: end_time, tick_speed: 1.minute)

          expect(Clockwork::Test.ran_job?("budget_reports.weekly")).to be_truthy
        end
      end
    end
  end

  describe '#perform' do
    context 'with ems' do
      let(:task_user) { FactoryGirl.create(:task_user, :em) }

      it 'should send an email' do
        expect {
          BudgetEmailJob.new(task_user.task, 1.day.ago.iso8601, Time.now.iso8601).perform_now
        }.to change { ActionMailer::Base.deliveries.count }.by(1)
      end
    end

    context 'with ems and to' do
      let!(:task) { FactoryGirl.create(:task) }
      let!(:task_em) { FactoryGirl.create(:task_user, :em, task: task) }
      let!(:task_owner) { FactoryGirl.create(:task_user, :to, task: task) }

      it 'should send email to task_owner and escalation manager' do
        BudgetEmailJob.new(task, 1.day.ago.iso8601, Time.now.iso8601).perform_now
        expect(ActionMailer::Base.deliveries.last.to).to eq([task_em.user.email, task_owner.user.email])
      end
    end

    context 'without ems' do
      let(:task_user) { FactoryGirl.create(:task_user) }

      it 'should not send an email' do
        expect {
          BudgetEmailJob.new(task_user.task, 1.day.ago.iso8601, Time.now.iso8601).perform_now
        }.to change { ActionMailer::Base.deliveries.count }.by(0)
      end
    end
  end
end
