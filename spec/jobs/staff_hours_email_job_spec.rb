require 'rails_helper'

RSpec.describe StaffHoursEmailJob, type: :job do
  describe '#perform' do
    # TODO

    let(:task_user) { FactoryGirl.create(:task_user, :em) }
    xit 'should send an email to an admin' do
      admin = FactoryGirl.create(:user, role: 'admin')
      StaffHoursEmailJob.new(1.day.ago.iso8601, Time.now.iso8601).perform_now
      expect(
        ActionMailer::Base.deliveries.last.to.include?(admin.email)
      ).to be_truthy
    end

    xit 'should send an email to a superadmin' do
      superadmin = FactoryGirl.create(:user, role: 'super_admin')
      StaffHoursEmailJob.new(1.day.ago.iso8601, Time.now.iso8601).perform_now
      expect(
        ActionMailer::Base.deliveries.last.to.include?(superadmin.email)
      ).to be_truthy
    end

    xit 'should not send an email to staff' do
      staff = FactoryGirl.create(:user, role: 'staff')
      StaffHoursEmailJob.new(1.day.ago.iso8601, Time.now.iso8601).perform_now
      expect(
        ActionMailer::Base.deliveries.last.to.include?(staff.email)
      ).to be_falsy
    end

    it 'should not raise error' do
      task_user = FactoryGirl.create :task_user, :em
      FactoryGirl.create :work_period, task: task_user.task, user: task_user.user
      expect { StaffHoursEmailJob.perform_now(1.week.ago.iso8601, 1.week.since.iso8601) }.not_to raise_error
    end
  end
end
