FactoryGirl.define do
  factory :wage do
    is_salary true
    rate 30
    description { Faker::Lorem.words(8) }
  end
end