FactoryGirl.define do
  factory :threshold_reminder do
    association :remindable, factory: :task

    type "ThresholdReminder"
    attribute_name "budgeted_cost"
    threshold_value 10
    threshold_comparator 0
  end
end
