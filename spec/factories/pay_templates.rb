FactoryGirl.define do
  factory :pay_template do
    earnings_rate_id Faker::Code.ean
    pay_rate Faker::Number.decimal(2)
    xero_account
  end
end