FactoryGirl.define do
  factory :subject_field do
    name { Faker::Name.name }
  end
end