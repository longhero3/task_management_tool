FactoryGirl.define do
  factory :pay_item_category do
    earnings_rate_id Faker::Code.ean
    pay_rate Faker::Number.decimal(2)
  end
end