FactoryGirl.define do
  factory :task do
    title { "#{Faker::Lorem.characters(4).upcase} - #{Faker::Lorem.characters(10)}" }
    description { Faker::Lorem.characters(15) }
    target_start { Faker::Date.between(2.days.from_now, 4.days.from_now) }
    target_finish { Faker::Date.between(5.days.from_now, 6.days.from_now) }
    actual_start { Faker::Date.between(5.days.from_now, 6.days.from_now) }
    actual_finish { Faker::Date.between(6.days.from_now, 10.days.from_now) }
    budgeted_cost 1000
    actual_cost 2000
    pay_rate 30
    priority 2
    association :pay_item, factory: :pay_item
  end
end