FactoryGirl.define do
  factory :xero_account do
    employee_id Faker::Code.ean
    user
  end
end