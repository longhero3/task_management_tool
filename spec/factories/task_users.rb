FactoryGirl.define do
  factory :task_user do
    isOwner false
    task_role TaskUser::TASK_ROLES[:member]
    pay_rate 10
    extra_pay_rate 5
    user
    task
  end

  trait :em do
    task_role TaskUser::TASK_ROLES[:escalation_manager]
  end

  trait :to do
    task_role TaskUser::TASK_ROLES[:task_owner]
  end
end