FactoryGirl.define do
  factory :pay_item do
    earnings_rate_id Faker::Code.ean
    name Faker::Commerce.product_name
  end
end