FactoryGirl.define do
  factory :user do
    first_name { Faker::Name.first_name }
    last_name { Faker::Name.last_name }
    email { Faker::Internet.email }
    second_email { Faker::Internet.email }
    grad_job_title { Faker::Lorem.words(3) }
    contact_number { Faker::PhoneNumber.phone_number }
    password '12345678'
    date_of_birth { Faker::Date.between(20.years.ago, 30.years.ago) }
    high_school { Faker::Name.name }
    grad_year 1991
    address
    user_payment
    profile_image
    exam_fields { create_list(:exam_field, 1) }
    subject_fields { create_list(:subject_field, 1) }

    role 'admin'
  end
end