FactoryGirl.define do
  factory :address do
    street { Faker::Address.street_name }
    suburb { Faker::Address.city }
    state { Faker::Address.state }
  end
end