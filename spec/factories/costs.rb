FactoryGirl.define do
  factory :cost do
    amount 10
    user
    cost_type
    task
  end
end