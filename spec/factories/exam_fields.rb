FactoryGirl.define do
  factory :exam_field do
    name { Faker::Name.name }
    exam_scores { create_list(:exam_score, 1) }
  end
end