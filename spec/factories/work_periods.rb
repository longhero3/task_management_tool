FactoryGirl.define do
  factory :work_period do
    amount_hours 0
    date_worked 2.days.ago
    start_time 3.days.ago
    end_time 2.days.from_now
    pay_rate { BigDecimal.new(30) }
    description { Faker::Lorem.words(8) }
    progress 0
    task
    user
  end
end