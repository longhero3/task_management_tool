FactoryGirl.define do
  factory :reminder_user do
    association :user, factory: :user
    association :reminder, factory: :reminder
  end
end
