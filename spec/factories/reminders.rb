FactoryGirl.define do
  factory :reminder do
    association :remindable, factory: :task

    type "chnageReminder"
    attribute_name "title"

    trait :change_reminder do
      type "chnageReminder"
      attribute_name "title"
    end

    trait :threshold_reminder do
      type "ThresholdReminder"
      attribute_name "budgeted_cost"
      threshold_value 10
      threshold_comparator 0
    end

    trait :due_date_reminder do
      type "DueDateReminder"
      due_remind_days 10
    end
  end
end
