require 'rails_helper'

RSpec.describe Reminder, type: :model do
  it 'should belong to remindable' do
    expect(Reminder.reflect_on_association(:remindable).macro).to eq :belongs_to
  end

  it 'should have many reminder users' do
    expect(Reminder.reflect_on_association(:reminder_users).macro).to eq :has_many
  end

  it 'should have many users' do
    expect(Reminder.reflect_on_association(:users).macro).to eq :has_many
  end

end
