require 'rails_helper'

RSpec.describe ReminderUser, type: :model do
  it 'should belong to user' do
    expect(ReminderUser.reflect_on_association(:user).macro).to eq :belongs_to
  end

  it 'should belong to reminder' do
    expect(ReminderUser.reflect_on_association(:reminder).macro).to eq :belongs_to
  end
end
