require 'rails_helper'

RSpec.describe ChangeRecord, type: :model do

  it 'should belong to remindable' do
    expect(ChangeRecord.reflect_on_association(:remindable).macro).to eq :belongs_to
  end

  it { should validate_presence_of(:name) }
  it { should validate_presence_of(:old_value) }
  it { should validate_presence_of(:new_value) }
end

