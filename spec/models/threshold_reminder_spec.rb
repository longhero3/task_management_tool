require 'rails_helper'

RSpec.describe ThresholdReminder, type: :model do
  it { should validate_presence_of(:attribute_name) }
  it { should validate_presence_of(:threshold_comparator) }
  it { should validate_presence_of(:threshold_value) }
end

