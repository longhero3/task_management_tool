require 'rails_helper'

RSpec.describe Task, type: :model do
  let(:task) { create(:task, pay_rate: 20) }
  let(:user) { create(:user) }

  describe '.created' do
    let!(:task_1) { create(:task, title: 'b') }
    let!(:task_2) { create(:task, title: 'a') }

    it 'returns the task in alphabetical order' do
      expect(Task.created.to_a).to eq [task_2, task_1]
    end
  end

  describe '.searchable_columns' do
    it 'should include project code' do
      expect(Task.searchable_columns).to include(:project_code)
    end
  end

  describe '.find_by_task_code' do
    let(:task) { FactoryGirl.create(:task) }

    it 'can find own task code' do
      found_task = Task.find_by_task_code(task.task_code)
      expect(found_task.id).to eq task.id
    end

    it 'should only match exact projects' do
      found_task = Task.find_by_task_code('A' + task.task_code)
      expect(found_task).to be_nil
    end

    it 'should return nil when not found' do
      expect(Task.find_by_task_code('INVALID-2147483647')).to be_nil
    end

    it 'should return nil when nil is the argument' do
      expect(Task.find_by_task_code(nil)).to be_nil
    end
  end

  describe '#project_code' do
    let!(:task) { FactoryGirl.create(:task) }

    it 'generates project code appropriately' do
      expect { task.project_code }.to change { task[:project_code] }.from(nil)
    end

    it 'should never be nil' do
      expect(task.project_code).not_to be_nil
    end
  end

  describe '#budgeted_cost' do
    let(:parent_task) { create(:task, pay_rate: 20, budgeted_cost: 15) }
    let(:child_task) { create(:task, parent: parent_task, pay_rate: 20, budgeted_cost: 10) }

    context 'decrease parent budget below child sum' do
      it 'should not be valid' do
        child_task
        expect(parent_task.children.count).to eq(1)
        expect(parent_task.reload).to be_valid
        parent_task.budgeted_cost = 5
        expect(parent_task).not_to be_valid
      end
    end

    context 'should not be negative' do
      before do
        child_task.budgeted_cost = -1
      end

      it 'should not be valid' do
        expect(child_task).to_not be_valid
      end
    end

    context 'decrease parent budget to child sum' do
      it 'should be valid' do
        parent_task.budgeted_cost = 10
        expect(parent_task).to be_valid
      end
    end

    context 'increase parent budget' do
      it 'should not modify child' do
        expect do
          parent_task.update(budgeted_cost: 12)
        end.to change { child_task.reload.budgeted_cost }.by(0)
      end
    end

    context 'the child task is deleted' do
      before do
        child_task.archive
      end

      it 'should modify the parent task' do
        expect do
          parent_task.update(budgeted_cost: 1)
        end.to change { parent_task.reload.budgeted_cost }.by(-14)
      end

      it 'equals to the actual cost' do
        expect(child_task.budgeted_cost).to eq child_task.actual_cost
      end

      it 'should remove the task and its childrens' do
        expect(child_task).to be_removed
      end
    end

    context 'the child budget is increased' do
      it 'should change the parent remaining budget' do
        child_task
        expect do
          child_task.update(budgeted_cost: 11)
        end.to change { parent_task.reload.remaining_budget }.by(-1)
      end

      it 'should not change the parent budget' do
        child_task
        expect do
          child_task.update(budgeted_cost: 11)
        end.to change { parent_task.reload.budgeted_cost }.by(0)
      end

      context 'above the parent budget' do
        it 'increases the parent budget' do
          child_task
          expect do
            child_task.update(budgeted_cost: 16)
          end.to change { parent_task.reload.budgeted_cost }.by(1)
        end
      end
    end

    context 'the child is moved' do
      let(:move_task) { create(:task, pay_rate: 20, budgeted_cost: 15) }
      it 'should not change the parent budget' do
        expect do
          child_task.update(parent_id: move_task.id)
        end.to change { parent_task.reload.budgeted_cost }.by(0)
      end
      it 'should not change the parent budget remaining' do
        expect do
          child_task.update(parent_id: move_task.id)
        end.to change { parent_task.reload.remaining_budget }.by(0)
      end
    end
  end

  describe '#actual_cost' do
    context 'there is no working hours logged' do
      it 'should return 0' do
        expect(task.actual_cost).to eq 0
      end
    end

    context 'there is working hours logged into that task' do
      let!(:task_user) { create(:task_user, user: user, task: task) }
      let!(:work_period) { create(:work_period, user: user, task: task, amount_hours: 2, pay_rate: 20) }

      it 'should return the actual cost of the task' do
        expect(task.actual_cost).to eq 40
      end
    end
  end

  describe '#task_owner' do
    context 'there is one task owner in the task' do
      let!(:task_user) { create(:task_user, task: task, user: user, task_role: TaskUser::TASK_ROLES[:task_owner]) }

      it 'should return user as the task owner' do
        expect(task.task_owner).to eq(user)
      end
    end
  end

  describe '#escalation_manager' do
    context 'there is no escalation manager in the system' do
      it 'should return nil' do
        expect(task.escalation_manager).to be_nil
      end
    end

    context 'there is escalation manager in the task' do
      let!(:task_user) { create(:task_user, task: task, user: user, task_role: TaskUser::TASK_ROLES[:escalation_manager]) }

      it 'should return the user as escalation manager' do
        expect(task.escalation_manager).to eq user
      end
    end
  end

  describe '#task_members' do
    let!(:task_user) { create(:task_user, task: task, user: user) }
    context 'there are task members in the task' do
      it 'returns the task members in the task' do
        expect(task.task_members).to eq [task_user]
      end
    end
  end

  describe '#task_completed_by_user' do
    let!(:work_period1) { create(:work_period, progress: 20, user: user, task: task, date_worked: 1.day.ago) }
    let!(:work_period2) { create(:work_period, progress: 10, user: user, task: task, date_worked: 1.day.from_now) }

    it 'should calculate the right amount of the task' do
      expect(task.task_completed_by_user(user.id)).to eq 10
    end
  end

  describe '#budget_spent_ratio' do
    let!(:work_period1) { create(:work_period, progress: 20, user: user, task: task) }
    let!(:work_period2) { create(:work_period, progress: 10, user: user, task: task) }

    it 'should calculate the right ratio' do
      expect(task.budget_spent_ratio).to eq(task.actual_cost / task.budgeted_cost * 100)
    end
  end

  describe 'update target dates' do
    let!(:task) { create(:task, target_start: 2.days.ago, target_finish: 7.days.from_now) }
    let!(:task_1) { create(:task, parent: task, target_start: 3.days.from_now, target_finish: 6.days.from_now) }
    let!(:task_11) { create(:task, parent: task_1, target_start: 3.days.from_now, target_finish: 5.days.from_now, budgeted_cost: 500) }
    let!(:task_12) { create(:task, parent: task_1, target_start: 2.days.from_now, target_finish: 3.days.from_now, budgeted_cost: 500) }
    let!(:task_user1) { create(:task_user, user: user, task: task_1, task_role: TaskUser::TASK_ROLES[:task_owner]) }
    let!(:task_user2) { create(:task_user, user: user, task: task_11, task_role: TaskUser::TASK_ROLES[:task_owner]) }
    let!(:task_user3) { create(:task_user, user: user, task: task_12, task_role: TaskUser::TASK_ROLES[:task_owner]) }

    context 'update root task' do
      before do
        task.update_attributes(target_start: 3.days.from_now, target_finish: 4.days.from_now)
      end

      it 'should update the descendant task' do
        expect(task_1.reload.target_start.to_date).to eq(3.days.from_now.to_date)
        expect(task_1.reload.target_finish.to_date).to eq(4.days.from_now.to_date)
        expect(task_11.reload.target_start.to_date).to eq(3.days.from_now.to_date)
        expect(task_11.reload.target_finish.to_date).to eq(4.days.from_now.to_date)
        expect(task_12.reload.target_start.to_date).to eq(3.days.from_now.to_date)
        expect(task_12.reload.target_finish.to_date).to eq(3.days.from_now.to_date)
      end
    end

    context 'update middle task' do
      before do
        task_1.update_attributes(target_start: 1.day.from_now, target_finish: 7.days.from_now)
      end

      it 'should update the ancestors task' do
        expect(task.reload.target_start.to_date).to eq(2.days.ago.to_date)
        expect(task.reload.target_finish.to_date).to eq(7.days.from_now.to_date)
      end
    end

    context 'Update task completion' do
      let(:task_1_main) { create(:task, budgeted_cost: 100) }
      let!(:task_1_1) { create(:task, parent: task_1_main, budgeted_cost: 20) }
      let!(:task_1_2) { create(:task, parent: task_1_main, budgeted_cost: 80) }
      let!(:task_user1) { create(:task_user, user: user, task: task_1_main, task_role: TaskUser::TASK_ROLES[:task_owner]) }
      let!(:task_user2) { create(:task_user, user: user, task: task_1_1, task_role: TaskUser::TASK_ROLES[:task_owner]) }
      let!(:task_user3) { create(:task_user, user: user, task: task_1_2, task_role: TaskUser::TASK_ROLES[:task_owner]) }
      let!(:work_period_1_1a) { create(:work_period, task: task_1_1, user: user, progress: 50) }
      let!(:work_period_1_1b) { create(:work_period, task: task_1_1, user: user, progress: 70) }
      let!(:work_period_1_2a) { create(:work_period, task: task_1_2, user: user, progress: 80) }
      let!(:work_period_1_2b) { create(:work_period, task: task_1_2, user: user, progress: 90) }

      context 'there is only level 1 and level 2 task' do
        it 'should update the task total progress' do
          expect(task_1_1.reload.task_completed).to eq(70)
          expect(task_1_2.reload.task_completed).to eq(90)
          expect(task_1_main.reload.task_completed).to eq(86)
        end
      end

      context 'there is level 3 task' do
        let!(:task_1_1_1) { create(:task, parent: task_1_1, budgeted_cost: 5) }
        let!(:task_1_1_2) { create(:task, parent: task_1_1, budgeted_cost: 10) }
        let!(:task_user4) { create(:task_user, user: user, task: task_1_1_1, task_role: TaskUser::TASK_ROLES[:task_owner]) }
        let!(:task_user5) { create(:task_user, user: user, task: task_1_1_2, task_role: TaskUser::TASK_ROLES[:task_owner]) }
        let!(:work_period_1_1_1) { create(:work_period, task: task_1_1_1, user: user, progress: 50) }
        let!(:work_period_1_1_2) { create(:work_period, task: task_1_1_2, user: user, progress: 50) }
        it 'should update the task completed correctly' do
          expect(task_1_main.reload.task_completed).to eq(79.5)
        end
      end
    end
  end

  describe '#log_hours_enabled' do
    let!(:task_1) { create(:task, budgeted_cost: 100) }
    let!(:task_1_1) { create(:task, budgeted_cost: 100, parent: task_1) }

    context 'user role' do
      context 'manager and higher level staffs' do
        before do
          user.update_attributes(role: 'manager')
        end

        it 'the log hours feature should be enabled' do
          expect(task_1_1.log_hours_enabled(user)).to eq true
        end
      end

      context 'normal staff' do
        before do
          user.update_attributes(role: 'staff')
        end

        it 'the log hours feature should be disabled' do
          expect(task_1.log_hours_enabled(user)).to eq false
        end
      end
    end

    context 'task role' do
      before do
        user.update_attributes(role: 'staff')
      end

      context 'user is in the task' do
        let!(:task_user) { create(:task_user, task: task_1_1, task_role: 'member', user: user) }

        it 'should enable log hour button' do
          expect(task_1_1.reload.log_hours_enabled(user)).to eq true
        end
      end

      context 'user is not in the task' do
        it 'should disable log hour button' do
          expect(task_1.log_hours_enabled(user)).to eq false
        end
      end
    end

    context 'task hierarchy' do
      let!(:task_1_children) { create(:task, parent: task_1, status: 'created') }
      let!(:task_2_children) { create(:task, parent: task_1, status: 'removed') }

      before do
        user.update_attributes(role: 'manager')
      end

      context 'task has children task' do
        it 'should disable log hours feature' do
          expect(task_1.reload.log_hours_enabled(user)).to eq false
        end
      end

      context 'task has removed children task' do
        before do
          task_1_children.update_attributes(status: 'removed')
        end
        it 'should enable log hours feature' do
          expect(task_1_1.reload.log_hours_enabled(user)).to eq true
        end
      end
    end
  end

  describe '#hours_logged_by_staff' do
    let(:report_params) do
     {
       created_after: 2.days.ago.to_s,
       created_before: 2.day.from_now.to_s,
       user_id: user.id
     }
    end

    let!(:work_period_1) { create(:work_period, task: task, user: user, date_worked: 1.day.ago, amount_hours: 2) }
    let!(:work_period_2) { create(:work_period, task: task, user: user, date_worked: 1.day.ago, amount_hours: 1) }

    it 'returns the sum of working hours' do
      expect(task.reload.hours_logged_by_staff(report_params)).to eq 3
    end
  end

  describe '#unique_title' do
    it 'returns the unique level of the task' do
      expect(task.unique_title).to eq "#{task.title} (Level #{task.depth + 1})"
    end
  end

  describe '.send_budget_reports' do
    before(:each) do
      allow(PayrollCalendar).to receive(:latest_fortnight) { 14.days.ago }
    end

    context 'daily' do
      it 'should not raise error' do
        expect { Task.send_budget_reports(:daily) }.not_to raise_error
      end
    end

    context 'weekly' do
      it 'should not raise error' do
        expect { Task.send_budget_reports(:weekly) }.not_to raise_error
      end
    end

    context 'fortnightly' do
      it 'should not raise error' do
        expect { Task.send_budget_reports(:fortnightly) }.not_to raise_error
      end
    end

    context 'invalid' do
      it 'should raise error' do
        expect { Task.send_budget_reports(:invalid) }.to raise_error(String)
      end
    end
  end

  describe '#actual_pay_rate' do
    let!(:pay_item) { create(:pay_item, task: task) }
    let!(:pay_item_category) { create(:pay_item_category, earnings_rate_id: pay_item.earnings_rate_id, pay_rate: 35) }

    context 'no xero account connected' do
      it 'should return the task pay rate' do
        expect(task.actual_pay_rate user).to eq pay_item_category.pay_rate
      end
    end

    context 'no pay template' do
      let(:xero_account) { create(:xero_account, user: user) }
      it 'should return the task pay rate' do
        expect(task.actual_pay_rate user).to eq pay_item_category.pay_rate
      end
    end

    context 'pay template not match' do
      let!(:xero_account) { create(:xero_account, user: user) }
      let(:pay_template) { create(:pay_template, xero_account: xero_account) }

      it 'should return pay rate of the task' do
        expect(task.actual_pay_rate user).to eq task.pay_item.pay_rate
      end
    end

    context 'pay item and pay template matched' do
      let!(:xero_account) { create(:xero_account, user: user) }
      let!(:pay_template) { create(:pay_template, xero_account: xero_account, earnings_rate_id: pay_item.earnings_rate_id, pay_rate: 40) }

      it 'should return pay template rate' do
        expect(task.actual_pay_rate user).to eq user.pay_templates.first.pay_rate
      end
    end
  end

  describe '#record_changed_attributes' do
    context 'when No ChangeRecord Present' do
      it 'should record the changed values of a task in ChangeRecord Model' do
        expect do
          task.update_attributes(title: 'TestTitle')
        end.to change { ChangeRecord.count }.by(2)
      end
    end
    context 'when ChangeRecord Exists' do
      before do
        task.update_attributes(title: 'TestTitle1')
      end

      it 'should record the new values of a task in ChangeRecord Model' do
        task.update_attributes(title: 'TestTitle2')
        expect(task.change_records.find_by(
                 name: 'title'
               ).try(:new_value)).to eq('TestTitle2')
      end
    end
  end

  describe '#ancestors_escalation_managers' do
    let!(:task_1) { create(:task, budgeted_cost: 100) }
    let!(:task_1_1) { create(:task, budgeted_cost: 100, parent: task_1) }

    let!(:task_user) { create(:task_user, :em, task: task_1, user: user) }

    it 'returns a list of unique escalation_managers of all the ancestors of a task' do
      expect(task_1_1.ancestors_escalation_managers.ids).to  match_array([user.id])
    end
  end

  describe '#create_summary' do

  end

  describe '#assign_new_parent' do
    let!(:old_parent) { create(:task, budgeted_cost: 1000) }
    let!(:new_parent) { create(:task, budgeted_cost: 100) }
    let!(:child_task) { create(:task, budgeted_cost: 900, parent: old_parent) }

    context 'when the child_task is valid' do
      context 'when the increase_new_parents_budget is false and child_task budget exceeds new_parent budget' do
        it 'should raise_error ' do
          expect(child_task.assign_new_parent(new_parent.id, true, false)).to eq([false, 'You must permit to increase parents budget as your budgets is out of parents budget'])
        end
      end

      context 'when the increase_new_parents_budget is true and child_task budget exceeds new_parent budget' do
        it 'should increase the new_parent budget ' do
          expect(child_task.assign_new_parent(new_parent.id, true, true)).to eq([true, nil])
        end
      end
    end

    context 'when the child_task is invalid' do
      before :each do
        child_task.budgeted_cost = -1
      end
      it 'should retuns false result and error ' do
        expect(child_task.assign_new_parent(new_parent.id, true, false)).to eq([false, child_task.errors.full_messages.join(', ')])
      end
    end
  end
end
