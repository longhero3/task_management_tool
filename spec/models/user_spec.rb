require 'rails_helper'

RSpec.describe User, type: :model do

  it 'should have many work periods' do
    expect(User.reflect_on_association(:work_periods).macro).to eq :has_many
  end

  it 'should have many task users' do
    expect(User.reflect_on_association(:task_users).macro).to eq :has_many
  end

  it 'should have many costs' do
    expect(User.reflect_on_association(:costs).macro).to eq :has_many
  end

  it 'should belong to wage' do
    expect(User.reflect_on_association(:wage).macro).to eq :belongs_to
  end

  describe "role?" do
    it "should return true if user role is greater than base role" do
      user = create(:user, role: "super_admin")
      expect(user.admin?).to eq(true)
    end

    it "should return true if user role is equal to base role" do
      user = create(:user, role: "super_admin")
      expect(user.super_admin?).to eq(true)
    end

    it "should return false if user role is less than base role" do
      user = create(:user, role: "admin")
      expect(user.super_admin?).to eq(false)
    end
  end

  describe "sending xero connect email to hr" do
    let(:staff) { create(:user, role: 'staff') }

    it "should send the email" do
      expect { staff.send_xero_connect_email }
            .to change { ActionMailer::Base.deliveries.count }.by(1)
    end
  end

  describe "get task" do
    let(:admin) { create(:user, role: 'admin') }
    let(:staff) { create(:user, role: 'staff') }
    let(:manager) { create(:user, role: 'manager') }
    let(:task) { create(:task) }

    context 'current user is admin' do
      context 'task is created by staff' do
        before do
          staff.task_users.create(task: task)
        end

        it 'should show the task created by the staff for admin' do
          expect(admin.get_tasks).to eq Task.all
        end
      end

      context 'task is created by admin' do
        before do
          admin.task_users.create(task: task)
        end

        it 'should show the task created by the admin for admin' do
          expect(admin.get_tasks).to eq Task.all
        end
      end
    end

    context 'current user is staff' do
      context 'task is created by staff' do
        before do
          staff.task_users.create(task: task)
        end

        it 'should show the task created by the staff for staff' do
          expect(staff.get_tasks).to eq staff.tasks
        end
      end

      context 'task is created by admin' do
        before do
          admin.task_users.create(task: task)
        end

        it 'should not show the task created by the admin for staff' do
          expect(staff.get_tasks).to eq []
        end
      end
    end

    context 'current user is manager' do
      context 'task is created by staff' do
        before do
          staff.task_users.create(task: task)
        end

        it 'should show the task created by the staff for manager' do
          expect(manager.get_tasks).to eq Task.all
        end
      end

      context 'task is created by manager' do
        before do
          manager.task_users.create(task: task)
        end

        it 'should show the task created by the manager for manager' do
          expect(manager.get_tasks).to eq Task.all
        end
      end
    end
  end
end
