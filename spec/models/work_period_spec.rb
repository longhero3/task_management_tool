require 'rails_helper'

RSpec.describe WorkPeriod, type: :model do
  let!(:task) { create(:task, pay_rate: 20) }
  let(:user) { create(:user) }

  it { should validate_presence_of(:user_id) }
  it { should validate_presence_of(:task_id) }

  describe '#can_log_hours?' do
    context 'with no children' do
      let(:work_period) do
        create(:work_period, user_id: user.id,
                             task_id: task.id)
      end
      it 'should return true' do
        expect(work_period.can_log_hours?).to eq(true)
      end
    end

    context 'with some children' do
      let!(:child_task) { create(:task, pay_rate: 1, parent_id: task.id) }
      let(:work_period) do
        build(:work_period, user_id: user.id,
                            task_id: task.id)
      end
      it 'should return false' do
        expect(work_period.can_log_hours?).to eq(false)
      end
    end
  end
end
