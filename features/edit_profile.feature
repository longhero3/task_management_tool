@javascript
Feature: User should be able to view and edit their profile

  Background:
    Given there is a registered user
    When the user signs in
    And the user clicks on edit profile button

  Scenario: User edit profile successfully
    Then the user should see their profile info
    When the user edit their profile
    Then the user profile should be updated

  Scenario: User edit profile unsuccessfully
    When the user edit their profile with invalid info
    Then the user should see the error message