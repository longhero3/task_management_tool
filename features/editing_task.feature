@javascript @wip
Feature: User should be able to edit tasks
  Background:
    Given there is a registered user
    And there is a number of tasks
    When the user signs in
    Then the user should see successful login message
    Then the user should see a list of tasks

  Scenario: User edit task
    When the user clicks on the first task
    Then the user should see more info of the task
    When the user edit the task info
    Then the task is updated accordingly
    When the user changes to another task
    Then the user should see the info of the another task
