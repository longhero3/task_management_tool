@javascript
Feature: User should be able to sort tasks by vital attributes
  Background:
    Given there is a registered user
    And there is a number of tasks
  Scenario: User sorting out the task
    When the user signs in
    Then the user should see successful login message
    Then the user should see a list of tasks
    And the user sort the task by deadline in the ascending order
    Then the user should see the task have earlier deadline coming first
    When the user changes to sort by descending order
    Then the user should see the task have earlier deadline coming last
