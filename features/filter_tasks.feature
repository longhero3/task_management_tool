@javascript
Feature: User should be able to filter tasks by levels
  Background:
    Given there is a registered user
    And there is a number of tasks
    When the user signs in
    Then the user should see successful login message
    Then the user should see a list of tasks

  Scenario: User filter first level tasks
    And the user filter the task by level 1
    Then the user should only see the level 1 tasks

  Scenario: Useer filter leaflet tasks
    And the user filter the task by level Leaflet
    Then the user should only see the level Leaflet tasks