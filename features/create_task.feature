@javascript
Feature: User should be able to create new task

  Background:
    Given there are a number of registered users
    And there is a registered user
    When the user signs in

  Scenario: Patient books an appointment
    When user click on add new task button
    And user fill in new task info
    Then user should see the newly created task