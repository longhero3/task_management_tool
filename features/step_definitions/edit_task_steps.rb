When(/^the user clicks on the first task$/) do
  expect(page).to have_css '.tasks-table'
  page.execute_script("$('.tasks-table tbody tr:first td:first a').click()")
  sleep 50
end

Then(/^the user should see more info of the task$/) do
  expect(page).to have_css '.task-view'
  expect(page).to have_content @task1.title
  expect(page).to have_content @task1.budgeted_cost
  expect(page).to have_content @task1.target_finish.strftime('%d/%m/%Y')
end

When(/^the user edit the task info$/) do
  page.execute_script("$('.task-card .ui.input:first').trigger('click')")
  page.execute_script("$('input:first').val('test_value')")
  find('.task-card>input').native.send_keys(:return)
end

Then(/^the task is updated accordingly$/) do
  expect(page).to have_content 'test_value'
end

When(/^the user changes to another task$/) do
  page.execute_script("$('.header:last').click()")
end

Then(/^the user should see the info of the another task$/) do
  expect(page).to have_content @task3.title
end