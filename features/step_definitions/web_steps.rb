def wait_until_element_visible(selector)
  page.has_css?(selector, :visible => true)
  sleep 0.2
end