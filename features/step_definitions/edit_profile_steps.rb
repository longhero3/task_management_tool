When(/^the user clicks on edit profile button$/) do
  click_on 'Edit profile'
end

Then(/^the user should see their profile info$/) do
  expect(find_field('user_first_name').value).to eq @user.first_name
  expect(find_field('user_last_name').value).to eq @user.last_name
  expect(find_field('user_email').value).to eq @user.email
  expect(page).to have_content @user.date_of_birth.strftime('%Y-%m-%d')
end

When(/^the user edit their profile$/) do
  fill_in 'user_first_name', with: 'Test First Name'
  click_on 'Update'
end

Then(/^the user profile should be updated$/) do
  expect(find_field('user_first_name').value).to eq 'Test First Name'
end

When(/^the user edit their profile with invalid info$/) do
  fill_in 'user_password', with: ''
  click_on 'Update'
end

Then(/^the user should see the error message$/) do
  expect(page).to have_content 'Current password can\'t be blank'
end