Then(/^the user filter the task by level (\d+)$/) do |level|
  page.execute_script("$('.selected-task-level').val(#{level})")
end

Then(/^the user should only see the level 1 tasks$/) do
  expect(page).to have_css '.order-dropdown'
  title1 = page.evaluate_script("$('.tasks-table tbody tr:first td:first').text()")
  expect(title1.include? @task1.title).to eq true
  title2 = page.evaluate_script("$('.tasks-table tbody tr:last td:first').text()")
  expect(title2.include? @task2.title).to eq true
end

Then(/^the user filter the task by level Leaflet$/) do
  page.execute_script("$('.selected-task-level').val('Leaflet')")
end

Then(/^the user should only see the level Leaflet tasks$/) do
  expect(page).to have_css '.order-dropdown'
  title1 = page.evaluate_script("$('.tasks-table tbody tr:first td:first').text()")
  expect(title1.include? @task1.title).to eq true
  title2 = page.evaluate_script("$('.tasks-table tbody tr:last td:first').text()")
  expect(title2.include? @task2.title).to eq true
end