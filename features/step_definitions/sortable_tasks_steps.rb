Given(/^there is a number of tasks$/) do
  @task1 = FactoryGirl.create(:task, target_finish: 6.days.from_now)
  @task2 = FactoryGirl.create(:task, target_finish: 7.days.from_now)
  @task3 = @task1.children.create(target_finish: 6.days.from_now)
  TaskUser.create(user: @user, task: @task1, isOwner: true)
  TaskUser.create(user: @user, task: @task2, isOwner: true)
  TaskUser.create(user: @user, task: @task3, isOwner: true)
end

When(/^the user goes to the task page$/) do
  visit "/tasks"
end

Then(/^the user should see a list of tasks$/) do
  expect(page).to have_css('.tasks-table')
end

When(/^the user sort the task by deadline in the ascending order$/) do
  page.execute_script("$('.stats-dropdown').click(); $('.stats-dropdown .item:first').click();")
end

Then(/^the user should see the task have earlier deadline coming first$/) do
  #make the cucumber wait a bit
  expect(page).to have_css '.stats-dropdown'
  title = page.evaluate_script("$('.tasks-table tbody tr:first td:first').text()")
  expect(title.include? @task1.title).to eq true
end

When (/^the user changes to sort by descending order$/) do
  page.execute_script("$('.order-dropdown').click(); $('.order-dropdown .item:last').click();")
end

Then (/^the user should see the task have earlier deadline coming last$/) do
  #make the cucumber wait a bit
  expect(page).to have_css '.order-dropdown'
  title = page.evaluate_script("$('.tasks-table tbody tr:last td:first').text()")
  expect(title.include? @task1.title).to eq true
end