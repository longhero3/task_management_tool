Then(/^the user should see successful login message$/) do
  expect(page).to have_content 'Signed in successfully.'
end

When(/^the user types incorrect username or password$/) do
  visit '/users/sign_in'
  fill_in 'user_email', with: ''
  fill_in 'user_password', with: ''
  click_on 'Log in'
end

Then(/^the user cannot go to the main page$/) do
  expect(current_path).not_to eq root_path
end