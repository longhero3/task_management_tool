When(/^user click on add new task button$/) do
  click_on 'Add'
end

When(/^user fill in new task info$/) do
  fill_in 'task_title', with: 'Test title'
  fill_in 'task_description', with: 'Test description'
  fill_in 'task_target_start', with: '12/02/2015'
  fill_in 'task_target_finish', with: '12/12/2015'
  fill_in 'task_budgeted_cost', with: 2000
  fill_in 'task_pay_rate', with: 100
  within '.task_users' do
    page.execute_script("$('.dropdown').click(); $('.item:first').click();")
  end
  click_on 'Submit'
end

Then(/^user should see the newly created task/) do
  expect(page).to have_content('Task was successfully created.')
end