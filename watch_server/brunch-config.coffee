exports.config =
  # See http://brunch.io/#documentation for docs.
  files:
    javascripts:
      joinTo: 'compiled_components.js'
      order:
        before: [
          '../app-src/react-jsx-components/base_components/editable_date_picker.js.jsx',
          '../app-src/react-jsx-components/base_components/editable_field.js.jsx',
          '../app-src/react-jsx-components/base_components/editable_search_dropdown.js.jsx',
          '../app-src/react-jsx-components/base_components/editable_text_area.js.jsx',
          '../app-src/react-jsx-components/home/task_tree_members.js.jsx',
          '../app-src/react-jsx-components/home/task_tree_item.js.jsx',
          '../app-src/react-jsx-components/home/log_hour_button.js.jsx',
          '../app-src/react-jsx-components/home/delete_task_button.js.jsx',
          '../app-src/react-jsx-components/home/parent_task_chooser.js.jsx',
          '../app-src/react-jsx-components/home/pay_item_chooser.js.jsx',
          '../app-src/react-jsx-components/home/task_card.js.jsx',
          '../app-src/react-jsx-components/home/task_tree_view.js.jsx',
          '../app-src/react-jsx-components/home/task_modal.js.jsx',
          '../app-src/react-jsx-components/home/tool_bar_view.js.jsx',
          '../app-src/react-jsx-components/home/task_row.js.jsx',
          '../app-src/react-jsx-components/home/vital_tasks.js.jsx',
          '../app-src/react-jsx-components/home/main_view.js.jsx',
          '../app-src/react-jsx-components/work_periods/work_period_row.js.jsx',
          '../app-src/react-jsx-components/work_periods/work_period_filter.js.jsx',
          '../app-src/react-jsx-components/work_periods/work_periods.js.jsx',
          '../app-src/react-jsx-components/work_periods/work_period_index.js.jsx',
          '../app-src/react-jsx-components/admin/work_periods/admin_work_period_row.js.jsx',
          '../app-src/react-jsx-components/admin/work_periods/admin_work_periods.js.jsx',
          '../app-src/react-jsx-components/admin/work_periods/admin_work_period_filter.js.jsx',
          '../app-src/react-jsx-components/admin/work_periods/admin_work_period_index.js.jsx',
          '../app-src/react-jsx-components/reports/report_extension_component.js.jsx',
          '../app-src/react-jsx-components/reports/report_filter.js.jsx',
          '../app-src/react-jsx-components/reports/staff_hours_report.js.jsx',
          '../app-src/react-jsx-components/reports/reports_main_view.js.jsx',
        ]
    stylesheets:
      joinTo: 'components.css'
    templates:
      joinTo: 'components.js'
  modules:
    wrapper: 'amd'
    definition: 'amd'

  paths:
    watched: ["../app-src/react-jsx-components"]

    public: "../app/assets/javascripts/react-components/"

  plugins:
    babel:
      presets: ['es2015','react']
      ignore: [
        /^(bower_components|vendor)/
        'app/legacyES5Code/**/*'
      ]
      pattern: /\.(es6|jsx)$/

  npm:
    enabled: false