desc "This task is called by the Heroku scheduler add-on"

namespace :scheduler do
  task :send_daily_budget_emails => :environment do
    Rails.logger.debug("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<")
    Rails.logger.debug("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<")
    puts "Sending daily budget reports"
    Rails.logger.debug("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<")
    Task.send_budget_reports :daily
  end

  task :send_daily_hours_emails => :environment do
    Rails.logger.debug("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<")
    Rails.logger.debug("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<")
    puts "Sending daily hours reports"
    Rails.logger.debug("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<")
    Task.send_staff_hours_reports :daily
  end

  task :send_fortnightly_budget_emails => :environment do
    pay_cycle_start = PayrollCalendar.latest_fortnight

    if (pay_cycle_start + 14.days).today? || (pay_cycle_start + 17.days).today?
      puts "Sending fortnightly budget reports"
      Task.send_budget_reports :fortnightly
    end
  end
end
