namespace :tmt do
  task :check_report_and_timesheet_match => :environment do
    latest_fortnight = PayrollCalendar.latest_fortnight

    xero_approach = User.xero_integrated.map do |user|
      from = Time.parse latest_fortnight.iso8601
      to = Time.parse (latest_fortnight.end_of_day + 13.days).iso8601
      user.work_periods.where(created_at: from .. to).sum(:amount_hours)
    end.sort

    from_time = PayrollCalendar.latest_fortnight
    to_time = from_time + 14.days

    hours_approach = User.xero_integrated.map do |user|
      user.work_periods.current_sprint_time_sheet_data(from_time).values.sum
    end

    report_approach = User.xero_integrated.map do |user|
      start_time = Time.parse from_time.iso8601
      end_time = Time.parse to_time.iso8601
      user.work_periods.where(created_at: start_time .. end_time).sum(:amount_hours)
    end.sort

    if xero_approach.sum == report_approach.sum && xero_approach.sum == hours_approach.sum
      p '3 systems are matched'
    else
      p '3 systems are not matched'
      p "xero = #{xero_approach.sum.to_s}"
      p "report = #{report_approach.sum.to_s}"
      p "hours= #{hours_approach.sum.to_s}"
    end
  end
end
