namespace :test do
  desc 'Clear memcache'
  task :warmup => :environment do
    require 'capybara/rails'
    Capybara.default_driver = :selenium
    Capybara.default_max_wait_time = 50 # wait for 50 seconds during warm up
    Capybara.register_driver :selenium do |app|
      Capybara::Selenium::Driver.new(app, :browser => :chrome)
    end

    include Capybara::DSL

    puts "Warm up assets compilation by requesting root_path"
    visit "/"
    page.has_content?('Sign In')
  end
end
