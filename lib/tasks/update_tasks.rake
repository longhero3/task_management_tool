namespace :update_tasks do
  task :task_progress => :environment do
    Task.leaves.all.each do |task|
      if task.work_periods.count > 0
        max_date = task.work_periods.maximum(:date_worked)
        total_progress = task.work_periods.find_by(date_worked: max_date).progress || 0
        task.update_columns(total_progress: total_progress)
      end
    end
  end

  task :task_budget => :environment do
    Task.all.each do |task|
      unless task.leaf?
        task.update_columns(budgeted_cost: task.leaves.sum(:budgeted_cost))
      end
    end
  end
end
