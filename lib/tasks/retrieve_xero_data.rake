require 'xero_client'
namespace :xero do
  task :retrieve_data => :environment do
    client = XeroClient.new
    client.sync_employees
    client.sync_pay_items
  end
end
