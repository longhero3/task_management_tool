class RequestProcessor

  attr_reader :method, :url, :params, :timestamp, :nonce

  def initialize method, url, params={}
    @method, @url, @params = method, url, params
    @timestamp = Time.now.to_i
    @nonce = [*('a'..'z'),*('0'..'9')].shuffle[0,8].join
    if @params['xml']
      temp = @params['xml']
      @params['xml'] = CGI.escape temp
    end
  end

  def processed_params
    params['oauth_consumer_key'] = ENV['XERO_OAUTH_CONSUMER_KEY']
    params['oauth_token'] = ENV['XERO_OAUTH_CONSUMER_KEY']
    params['oauth_signature_method'] = 'RSA-SHA1'
    params['oauth_timestamp'] = timestamp
    params['oauth_nonce'] = nonce
    params['oauth_version'] = '1.0'
    params.sort.to_h
  end

  def base_string
    param_string = processed_params.reduce('') { |result, (key, value)| result += "&#{key}=#{value}"}
    "#{method.upcase}&#{CGI.escape url}&#{CGI.escape param_string[1..-1]}"
  end

  def self.sign_key
    OpenSSL::PKey::RSA.new(File.read(ENV['XERO_KEY_PATH']))
  end

  def signature
    digest = OpenSSL::Digest::SHA1.new()
    encode = self.class.sign_key.sign(digest, base_string)
    CGI.escape(Base64.encode64( encode ).chomp.gsub( /\n/, '' ))
  end

  def full_url
    new_params = {}
    new_params['oauth_consumer_key'] = ENV['XERO_OAUTH_CONSUMER_KEY']
    new_params['oauth_token'] = ENV['XERO_OAUTH_CONSUMER_KEY']
    new_params['oauth_signature_method'] = 'RSA-SHA1'
    new_params['oauth_signature'] = signature
    new_params['oauth_timestamp'] = timestamp
    new_params['oauth_nonce'] = nonce
    new_params['oauth_version'] = '1.0'
    param_string = new_params.reduce('') { |result, (key, value)| result += "&#{key}=#{value}"}
    "#{url}?#{param_string[1..-1]}"
  end

  def header_params
    new_params = {}
    new_params['oauth_consumer_key'] = ENV['XERO_OAUTH_CONSUMER_KEY']
    new_params['oauth_token'] = ENV['XERO_OAUTH_CONSUMER_KEY']
    new_params['oauth_signature_method'] = 'RSA-SHA1'
    new_params['oauth_signature'] = signature
    new_params['oauth_timestamp'] = timestamp
    new_params['oauth_nonce'] = nonce
    new_params['oauth_version'] = '1.0'
    param_string = new_params.reduce('') { |result, (key, value)| result += ", #{key}=\"#{value}\""}

    header =
    {
      "Authorization" => "OAuth#{param_string[1..-1]}",
      'charset' => 'utf-8'
    }
    unless @method.eql? 'GET'
      header["Content-Type"] = "application/x-www-form-urlencoded"
    end

    header
  end
end