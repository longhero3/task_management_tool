require 'request_processor'
require 'signet/oauth_1/client'

class XeroClient
  def initialize
  end

  def sync_payroll_calendar
    processor = RequestProcessor.new('GET', 'https://api.xero.com/payroll.xro/1.0/PayrollCalendars')
    response = HTTParty.get('https://api.xero.com/payroll.xro/1.0/PayrollCalendars', headers: processor.header_params)
    success = response.parsed_response['Response']
    if success
      payroll_calendars = response.parsed_response['Response']['PayrollCalendars']['PayrollCalendar']
      PayrollCalendar.destroy_all
      payroll_calendars.each do |payroll_calendar|
        PayrollCalendar.create(payroll_calendar_id: payroll_calendar['PayrollCalendarID'], name: payroll_calendar['Name'],
                               calendar_type: payroll_calendar['CalendarType'], start_date: DateTime.parse(payroll_calendar['StartDate']).beginning_of_day,
                               payment_date: payroll_calendar['PaymentDate'])
      end
    end
  end

  def sync_pay_items
    Rails.logger.info "==========================================="
    Rails.logger.info "PayItemCategory created"
    Rails.logger.info "==========================================="

    processor = RequestProcessor.new('GET', 'https://api.xero.com/payroll.xro/1.0/PayItems')
    response = HTTParty.get('https://api.xero.com/payroll.xro/1.0/PayItems', headers: processor.header_params)
    success = response.parsed_response['Response']
    if success
      PayItemCategory.delete_all
      earning_rate_array = response.parsed_response['Response']['PayItems']['EarningsRates']['EarningsRate']
      earning_rate_array.each do |earning_rate|
        PayItemCategory.create(earnings_rate_id: earning_rate['EarningsRateID'], name: earning_rate['Name'], earnings_type: earning_rate['EarningsType'], pay_rate: earning_rate['RatePerUnit'].try(:to_d))
      end
    end
  end

  def sync_employees page=1
    if page == 1
      Employee.delete_all
    end
    processor = RequestProcessor.new('GET', 'https://api.xero.com/payroll.xro/1.0/Employees', { 'page' => page})
    response = HTTParty.get("https://api.xero.com/payroll.xro/1.0/Employees?page=#{page}", headers: processor.header_params)
    success = response.parsed_response['Response']['Employees']
    if success
      employee_array = response.parsed_response['Response']['Employees']['Employee']
      employee_array.each do |employee|
        Employee.create(employee_id: employee['EmployeeID'], first_name: employee['FirstName'], last_name: employee['LastName'], email: employee['Email'])
      end
      page += 1
      sync_employees page
    else
      return
    end
  end

  def send_timesheet current_user, xml_timesheet_data, payroll_calendar
    start_time = PayrollCalendar.latest_fortnight
    xero_timesheet = XeroTimesheet.find_by(user: current_user, payroll_calendar_id: payroll_calendar.payroll_calendar_id)
    xm = Builder::XmlMarkup.new
    xm.Timesheets {
      xm.Timesheet {
        if xero_timesheet
          xm.TimesheetID(xero_timesheet.timesheet_id)
        end
        xm.EmployeeID(current_user.try(:xero_account).try(:employee_id))
        xm.StartDate(start_time.strftime('%Y-%m-%d'))
        xm.EndDate((start_time + 13.days).strftime('%Y-%m-%d'))
        xm.Status('Draft')
        xm.TimesheetLines {
          xml_timesheet_data.each do |earnings_rate_id, hours_logged_arr|
            xm.TimesheetLine {
              xm.EarningsRateID(earnings_rate_id)
              xm.NumberOfUnits {
                hours_logged_arr.each do |hours_logged|
                  xm.NumberOfUnit(hours_logged)
                end
              }
            }
          end
        }
      }
    }
    converted_xml_string = xm.to_s[0 .. xm.to_s.length - 15]
    processor = RequestProcessor.new('POST', 'https://api.xero.com/payroll.xro/1.0/Timesheets', { 'xml' => converted_xml_string })
    response = HTTParty.post("https://api.xero.com/payroll.xro/1.0/Timesheets", headers: processor.header_params, body: "xml=#{converted_xml_string}")
    status = response.parsed_response['Response'].try(:[],'Status')
    if status == "OK"
      timesheet_string = response.parsed_response['Response']['Timesheets']['Timesheet']
      XeroTimesheet.find_or_create_by(user: current_user, timesheet_id: timesheet_string['TimesheetID'], payroll_calendar_id: payroll_calendar.payroll_calendar_id)
    end
  end

  def generate_timesheet_xml current_user, xml_timesheet_data
    start_time = PayrollCalendar.latest_fortnight
    xero_timesheet = XeroTimesheet.find_by(user: current_user, payroll_calendar_id: PayrollCalendar.current_fortnight_calendar.payroll_calendar_id)
    xm = Builder::XmlMarkup.new
    xm.Timesheets {
      xm.Timesheet {
        if xero_timesheet
          xm.TimesheetID(xero_timesheet.timesheet_id)
        end
        xm.EmployeeID(current_user.try(:xero_account).try(:employee_id))
        xm.StartDate(start_time.strftime('%Y-%m-%d'))
        xm.EndDate((start_time + 13.days).strftime('%Y-%m-%d'))
        xm.Status('Draft')
        xm.TimesheetLines {
          xml_timesheet_data.each do |earnings_rate_id, hours_logged_arr|
            xm.TimesheetLine {
              xm.EarningsRateID(earnings_rate_id)
              xm.NumberOfUnits {
                hours_logged_arr.each do |hours_logged|
                  xm.NumberOfUnit(hours_logged)
                end
              }
            }
          end
        }
      }
    }

    xm.to_s[0 .. xm.to_s.length - 15]
  end

  def send_timesheet_snapshot snapshot
    payroll_calendar = PayrollCalendar.current_fortnight_calendar
    processor = RequestProcessor.new('POST', 'https://api.xero.com/payroll.xro/1.0/Timesheets', { 'xml' => snapshot.content })
    response = HTTParty.post("https://api.xero.com/payroll.xro/1.0/Timesheets", headers: processor.header_params, body: "xml=#{snapshot.content}")
    status = response.parsed_response['Response'].try(:[],'Status')
    if status == "OK"
      timesheet_string = response.parsed_response['Response']['Timesheets']['Timesheet']
      XeroTimesheet.find_or_create_by(user: snapshot.user, timesheet_id: timesheet_string['TimesheetID'], payroll_calendar_id: payroll_calendar.payroll_calendar_id)
    end
  end

  def create_pay_run
    payroll_calendar = PayrollCalendar.current_fortnight_calendar
    xm = Builder::XmlMarkup.new
    xm.PayRuns {
      xm.PayRun {
        xm.PayrollCalendarID(payroll_calendar.payroll_calendar_id)
      }
    }

    xml_string = xm.to_s[0 .. xm.to_s.length - 15]
    processor = RequestProcessor.new('POST', 'https://api.xero.com/payroll.xro/1.0/PayRuns', { 'xml' => xml_string })
    response = HTTParty.post("https://api.xero.com/payroll.xro/1.0/PayRuns", headers: processor.header_params, body: "xml=#{xml_string}")
    response.parsed_response['Response'].try(:[],'Status')
  end

  def sync_xero_account_pay_template
    XeroAccount.all.each do |xero_account|
      processor = RequestProcessor.new('GET', "https://api.xero.com/payroll.xro/1.0/Employees/#{xero_account.employee_id}")
      response = HTTParty.get("https://api.xero.com/payroll.xro/1.0/Employees/#{xero_account.employee_id}", headers: processor.header_params)
      success = response.parsed_response['Response'].try(:[], 'Employees')
      if success
        pay_template_array = [response.parsed_response['Response']['Employees']['Employee']['PayTemplate']['EarningsLines'].try(:[], 'EarningsLine')].flatten
        unless pay_template_array[0].nil?
          xero_account.pay_templates.delete_all
          pay_template_array.each do |pay_array|
            xero_account.pay_templates.create(earnings_rate_id: pay_array['EarningsRateID'], pay_rate: pay_array.try(:[], 'RatePerUnit'))
          end
        end
      end
    end
  end
end
